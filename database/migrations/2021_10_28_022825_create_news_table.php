<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->text('title')->comment('Tiêu đề nội dung');
            $table->text('title_en')->nullable()->comment('Tiêu đề nội dung tiếng anh');
            $table->longText('content')->comment('Nội dung');
            $table->longText('content_en')->nullable()->comment('Nội dung tiếng anh');
            $table->tinyInteger('type')->nullable()->comment('Loại tin tức');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
