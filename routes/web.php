<?php

use App\Http\Livewire\Product\Index;
use App\Http\Livewire\Shop\Cart;
use App\Http\Livewire\Shop\Checkout;
use App\Http\Livewire\Shop\Index as ShopIndex;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/lang/{lang}','App\Http\Controllers\LangController@changeLang')->name('lang');
// Route::get('/admin/product', Index::class)->middleware('auth')->name('admin.product');
// Route::get('/cart', Cart::class)->name('shop.cart');
// Route::get('/checkout', Checkout::class)->name('shop.checkout');
Route::get('/tuyen-dung', 'App\Http\Controllers\RecruitmentController@index')->name('page.mb.recruitment.index');
Route::get('/tuyen-dung/{slug}', 'App\Http\Controllers\RecruitmentController@detail')->name('page.mb.recuiment.detail');
//tuyển dụng

//quan hệ đầu tư
Route::get('/quan-he-dau-tu/{slug?}', 'App\Http\Controllers\InvestorController@index')->name('page.investors.index');

//tin tức
Route::get('/tin-tuc', [App\Http\Controllers\News\NewsController::class, 'news'])->name('page.news.index');
Route::get('/tin-tuc/{slug}', [App\Http\Controllers\News\NewsController::class, 'details'])->name('page.news.details.index');
Route::get('/quan-ly-quy-dau-tu', 'App\Http\Controllers\FundsController@index')->name('page.fund.index');
Route::get('/quan-ly-quy/{slug}', 'App\Http\Controllers\FundsController@detail')->name('page.fund.detail');

//ủy thác đầu tư
Route::get('/uy-thac-dau-tu-to-chuc', 'App\Http\Controllers\TrustController@index')->name('page.trust.index');
Route::get('/uy-thac-dau-tu-ca-nhan', 'App\Http\Controllers\TrustController@personal')->name('page.trust.personal');

//về MBCapital
Route::get('/about', [App\Http\Controllers\About\AboutController::class, 'index'])->name('page.mb.about');
Route::get('/we-mb-staff-of', [App\Http\Controllers\About\AboutController::class, 'index'])->name('page.mb.about');
Route::get('/about-us-general-introduction', [App\Http\Controllers\About\AboutController::class, 'index'])->name('page.mb.about');

//Tìm kiếm
Route::get('/tim-kiem', [App\Http\Controllers\Search\SearchController::class, 'index'])->name('page.search.index');

//Liên hệ
Route::get('/lien-he', [App\Http\Controllers\Contact\ContactController::class, 'index'])->name('page.contact.index');

//FAQ
Route::get('/faq', [App\Http\Controllers\FAQ\FAQController::class, 'index'])->name('page.faq.index');

//Hướng dẫn sử dụng
Route::get('/huong-dan-su-dung', [App\Http\Controllers\Instruction\InstructionController::class, 'index'])->name('page.instruction.index');

//Kiến thức đầu tư


Route::get('/kien-thuc-dau-tu', [App\Http\Controllers\Investment\InvestmentController::class, 'index'])->name('page.investment.index');

//công cụ hỗ trợ
Route::get('/cong-cu-ho-tro', [App\Http\Controllers\ToolSupportController::class, 'index'])->name('page.tool.support.index');

//nhà đầu tư
Route::get('/nha-dau-tu-moi', [App\Http\Controllers\InvestorController::class, 'new'])->name('page.investors.new');
Route::get('/nha-dau-tu-chuyen-nghiep', [App\Http\Controllers\InvestorController::class, 'expert'])->name('page.investors.expert');
Route::get('/disclaimer', [App\Http\Controllers\DisclaimerController::class, 'index'])->name('page.disclaimer.index');
