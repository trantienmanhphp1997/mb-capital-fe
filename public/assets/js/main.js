/**
 * Template Name: BizLand - v3.6.0
 * Template URL: https://bootstrapmade.com/bizland-bootstrap-business-template/
 * Author: BootstrapMade.com
 * License: https://bootstrapmade.com/license/
 */
(function () {
    "use strict";

    /**
     * Easy selector helper function
     */
    const select = (el, all = false) => {
        el = el.trim()
        if (all) {
            return [...document.querySelectorAll(el)]
        } else {
            return document.querySelector(el)
        }
    }

    /**
     * Easy event listener function
     */
    const on = (type, el, listener, all = false) => {
        let selectEl = select(el, all)
        if (selectEl) {
            if (all) {
                selectEl.forEach(e => e.addEventListener(type, listener))
            } else {
                selectEl.addEventListener(type, listener)
            }
        }
    }

    /**
     * Easy on scroll event listener
     */
    const onscroll = (el, listener) => {
        el.addEventListener('scroll', listener)
    }

    /**
     * Navbar links active state on scroll
     */
    let navbarlinks = select('#navbar .scrollto', true)
    const navbarlinksActive = () => {
        let position = window.scrollY + 200
        navbarlinks.forEach(navbarlink => {
            if (!navbarlink.hash) return
            let section = select(navbarlink.hash)
            if (!section) return
            if (position >= section.offsetTop && position <= (section.offsetTop + section.offsetHeight)) {
                navbarlink.classList.add('active')
            } else {
                navbarlink.classList.remove('active')
            }
        })
    }
    window.addEventListener('load', navbarlinksActive)
    onscroll(document, navbarlinksActive)

    /**
     * Scrolls to an element with header offset
     */
    const scrollto = (el) => {
        let header = select('#header')
        let offset = header.offsetHeight

        if (!header.classList.contains('header-scrolled')) {
            offset -= 16
        }

        let elementPos = select(el).offsetTop
        window.scrollTo({
            top: elementPos - offset,
            behavior: 'smooth'
        })
    }

    /**
     * Header fixed top on scroll
     */
    let selectHeader = select('#header')
    if (selectHeader) {
        let headerOffset = selectHeader.offsetTop
        let nextElement = selectHeader.nextElementSibling
        const headerFixed = () => {
            if ((headerOffset - window.scrollY) <= 0) {
                selectHeader.classList.add('fixed-top')
                nextElement.classList.add('scrolled-offset')
            } else {
                selectHeader.classList.remove('fixed-top')
                nextElement.classList.remove('scrolled-offset')
            }
        }
        window.addEventListener('load', headerFixed)
        onscroll(document, headerFixed)
    }

    /**
     * Back to top button
     */
    let backtotop = select('.back-to-top')
    if (backtotop) {
        const toggleBacktotop = () => {
            if (window.scrollY > 100) {
                backtotop.classList.add('active')
            } else {
                backtotop.classList.remove('active')
            }
        }
        window.addEventListener('load', toggleBacktotop)
        onscroll(document, toggleBacktotop)
    }

    /**
     * Mobile nav toggle
     */
    on('click', '.mobile-nav-toggle', function (e) {
        select('#navbar').classList.toggle('navbar-mobile')
        this.classList.toggle('bi-list')
        this.classList.toggle('bi-x')
    })

    /**
     * Mobile nav dropdowns activate
     */
    on('click', '.navbar .dropdown > a', function (e) {
        if (select('#navbar').classList.contains('navbar-mobile')) {
            e.preventDefault()
            this.nextElementSibling.classList.toggle('dropdown-active')
        }
    }, true)

    /**
     * Scrool with ofset on links with a class name .scrollto
     */
    on('click', '.scrollto', function (e) {
        if (select(this.hash)) {
            e.preventDefault()

            let navbar = select('#navbar')
            if (navbar.classList.contains('navbar-mobile')) {
                navbar.classList.remove('navbar-mobile')
                let navbarToggle = select('.mobile-nav-toggle')
                navbarToggle.classList.toggle('bi-list')
                navbarToggle.classList.toggle('bi-x')
            }
            scrollto(this.hash)
        }
    }, true)

    /**
     * Scroll with ofset on page load with hash links in the url
     */
    window.addEventListener('load', () => {
        if (window.location.hash) {
            if (select(window.location.hash)) {
                scrollto(window.location.hash)
            }
        }
    });

    /**
     * Preloader
     */
    let preloader = select('#preloader');
    if (preloader) {
        window.addEventListener('load', () => {
            preloader.remove()
        });
    }

    /**
     * Initiate glightbox
     */
    const glightbox = GLightbox({
        selector: '.glightbox'
    });

    /**
     * Skills animation
     */
    let skilsContent = select('.skills-content');
    if (skilsContent) {
        new Waypoint({
            element: skilsContent,
            offset: '80%',
            handler: function (direction) {
                let progress = select('.progress .progress-bar', true);
                progress.forEach((el) => {
                    el.style.width = el.getAttribute('aria-valuenow') + '%'
                });
            }
        })
    }

    /**
     * Testimonials slider
     */
    new Swiper('.testimonials-slider', {
        speed: 600,
        loop: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false
        },
        slidesPerView: 'auto',
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        }
    });

    /**
     * Porfolio isotope and filter
     */
    window.addEventListener('load', () => {
        let portfolioContainer = select('.portfolio-container');
        if (portfolioContainer) {
            let portfolioIsotope = new Isotope(portfolioContainer, {
                itemSelector: '.portfolio-item'
            });

            let portfolioFilters = select('#portfolio-flters li', true);

            on('click', '#portfolio-flters li', function (e) {
                e.preventDefault();
                portfolioFilters.forEach(function (el) {
                    el.classList.remove('filter-active');
                });
                this.classList.add('filter-active');

                portfolioIsotope.arrange({
                    filter: this.getAttribute('data-filter')
                });
                portfolioIsotope.on('arrangeComplete', function () {
                    AOS.refresh()
                });
            }, true);
        }

    });

    /**
     * Initiate portfolio lightbox
     */
    const portfolioLightbox = GLightbox({
        selector: '.portfolio-lightbox'
    });

    /**
     * Portfolio details slider
     */
    new Swiper('.portfolio-details-slider', {
        speed: 400,
        loop: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        }
    });

    /**
     * Animation on scroll
     */
    window.addEventListener('load', () => {
        AOS.init({
            duration: 1000,
            easing: 'ease-in-out',
            once: true,
            mirror: false
        })
    });

    // refesh animation when change tabs
    let tabEl = document.querySelector('button[data-bs-toggle="pill"]')

    if (tabEl) {
        tabEl.addEventListener('hidden.bs.tab', function (event) {

            AOS.refreshHard();
        })
        tabEl.addEventListener('shown.bs.tab', function (event) {

            AOS.refreshHard();
        })
    }
})()

$('#myCarousel').carousel({
    interval: 6000,
})

function convertValue(params) {
    let convertValue = params;
    if (params.length <= 3) {
        if(locale == 'vi')
            convertValue += ' triệu'
        else
            convertValue += ' milion'
    }
    else {
        if(locale == 'vi')
            convertValue = (Math.round((parseFloat(params) / 100)) / 10) + ' tỷ';
        else
            convertValue = (Math.round((parseFloat(params) / 100)) / 10) + ' bilion';
    }
    return convertValue;
}

function rangeCommit (range, rangeV = '', content='', mark) {
    var setValue = () => {
        const
            newValue = Number((range.value - range.min) * 100 / (range.max - range.min)),
            newPosition = 10 - (newValue * 0.2);
        // let contents = range.value == 500 ? 'Xe ô tô' : content
        if (rangeV) {
            // console.log(navigator.language);
            rangeV.innerHTML = `<span class='text-center'>${content}<br/>${convertValue(range.value)}</span>`;
            rangeV.style.left = `calc(${newValue}% + (${newPosition}px))`;
        }
        else {
            document.getElementById("monney-r").innerHTML = convertValue(range.value).replace('.', ',');
        }

        range.style.background = 'linear-gradient(to right, #DDB996 0%, #DDB996 ' + newValue + '%, #fff ' + newValue + '%, white 100%)'
    };
    if (range) {
        document.addEventListener("DOMContentLoaded", setValue);
        range.addEventListener('input', setValue);
    }
}
// console.log(document.getElementsByTagName("html")[0].getAttribute("lang"));
rangeCommit(document.getElementById('range'));
rangeCommit(document.getElementById('mark15'),document.getElementById('range15'), (document.getElementsByTagName("html")[0].getAttribute("lang") == 'vi' ? 'Nghỉ hưu': 'Early Retirement'), 15000);
rangeCommit(document.getElementById('mark3'),document.getElementById('range3'), (document.getElementsByTagName("html")[0].getAttribute("lang") == 'vi' ? 'Cho con đi học': 'Oversea Education'), 3000);
rangeCommit(document.getElementById('mark2'),document.getElementById('range2'),(document.getElementsByTagName("html")[0].getAttribute("lang") == 'vi' ? 'Nhà': 'House'), 2000);
rangeCommit(document.getElementById('mark20'),document.getElementById('range20'));
rangeCommit(document.getElementById('mark05'),document.getElementById('range05'),(document.getElementsByTagName("html")[0].getAttribute("lang") == 'vi' ? 'Xe ô tô': 'Car'));

// công cụ hỗ trợ -> công cụ tính
document.addEventListener("DOMContentLoaded", function() {
    window.livewire.on('show-range-commit', function() {
        function convertValue(params) {
            let convertValue = params;
            if (params.length <= 3) {
                if(locale == 'vi')
                    convertValue += ' triệu'
                else
                    convertValue += ' milion'
            }
            else {
                if(locale == 'vi')
                    convertValue = (Math.round((parseFloat(params) / 100)) / 10) + ' tỷ';
                else
                    convertValue = (Math.round((parseFloat(params) / 100)) / 10) + ' bilion';
            }
            return convertValue;
        }

        function rangeCommit (range, rangeV = '', content='') {
            const
                newValue = Number((range.value - range.min) * 100 / (range.max - range.min)),
                newPosition = 10 - (newValue * 0.2);
            if (rangeV) {
                rangeV.innerHTML = `<span class='text-center'>${content}<br/>${convertValue(range.value)}</span>`;
                rangeV.style.left = `calc(${newValue}% + (${newPosition}px))`;
            }
        }
        
        rangeCommit(document.getElementById('range'));
        rangeCommit(document.getElementById('mark15'),document.getElementById('range15'), (document.getElementsByTagName("html")[0].getAttribute("lang") == 'vi' ? 'Nghỉ hưu': 'Retirement'), 15000);
        rangeCommit(document.getElementById('mark3'),document.getElementById('range3'), (document.getElementsByTagName("html")[0].getAttribute("lang") == 'vi' ? 'Cho con đi học': 'Children Education'), 4000);
        rangeCommit(document.getElementById('mark2'),document.getElementById('range2'),(document.getElementsByTagName("html")[0].getAttribute("lang") == 'vi' ? 'Nhà': 'House'), 2000);
        rangeCommit(document.getElementById('mark20'),document.getElementById('range20'));
        rangeCommit(document.getElementById('mark05'),document.getElementById('range05'),(document.getElementsByTagName("html")[0].getAttribute("lang") == 'vi' ? 'Xe ô tô': 'Car'));

        var money_input = document.getElementById("range");
        var money_output = document.getElementById("money-range");
        money_output.innerHTML = convertValue(money_input.value);
        money_input.oninput = function() {
            money_output.innerHTML = convertValue(this.value);
        }

        var profit_input = document.getElementById("range1");
        var profit_output = document.getElementById("profit-range");
        profit_output.innerHTML = profit_input.value + ' %';
        profit_input.oninput = function() {
            profit_output.innerHTML = formatNumber(this.value) + ' %';
        }

        var year_input = document.getElementById("range11");
        var year_output = document.getElementById("year-range");
        var label;
        if(locale == 'vi') label = ' năm';
        else label = ' years';
        year_output.innerHTML = year_input.value + label;
        year_input.oninput = function() {
            year_output.innerHTML = this.value + label;
        }
    });
});

$('document').ready(function () {
    $('.nav-link').on('click', function(){
        window.scrollTo(0, 0);
    });
})

$(document).ready(function () {
    $(".owl-carousel-home").owlCarousel({
        loop: true,
        // margin:10,
        autoplay: true,
        autoplayTimeout: 5000,
        navigation: true,
        dots:false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 1,
                nav: false
            },
            1024: {
                items: 3,
                nav: false,
                loop: true
            },
            1300: {
                items: 2,
                nav: false,
                loop: true
            },
            1366: {
                items: 3,
                nav: false,
                loop: true
            }
        }
    });
    $.extend($.fn, {
        fixTab: function(){
            var x = this,
                p = x.parent(),
                top = p.next().offset().top,
                fix;
            $("header").after(
                fix = $("<div>", { class:"bg-white banner-tab-fixed d-none" }).append(
                    $("<div>", { class:"container" })
                )
            );
            $(window).on("scroll", function(){
                var i = $(this).scrollTop() > top;
                localStorage.setItem("waitingLock", i);

                if(i !== localStorage.getItem("waitingLock")){
                    (i ? fix.find(".container") : p).append(x[i ? 'addClass' : 'removeClass']("m-0 styleBarScroll"));
                    fix[!i ? 'addClass' : 'removeClass']("d-none").css({
                        opacity: i * 1
                    });
                }
            });
            $(window).scroll();
            return x;
        }
    })

});
