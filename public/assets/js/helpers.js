var locale = $('html').attr('lang');
let DECIMAL_SEPARATOR;
let THOUSAND_SEPARATOR;

if(locale == 'en'){
    DECIMAL_SEPARATOR = '.';
    THOUSAND_SEPARATOR = ',';
}
else{
    DECIMAL_SEPARATOR = ',';
    THOUSAND_SEPARATOR = '.';
}

function formatInputNumber(input) {
    let maxValue = Number(input.getAttribute('max'));

    let value = input.value;
    let lastChar = value[value.length - 1];

    if (lastChar == DECIMAL_SEPARATOR) {
        let foundDecimal = value.indexOf(lastChar);
        input.value = (foundDecimal == (value.length -1)) ? value : value.substr(0, value.length - 1);
    } else if(/[^0-9]/.test(lastChar)) {
        input.value = value.substr(0, value.length - 1);
    } else {
       value = removeFormatNumber(value);
       if(maxValue && value > maxValue) value = maxValue;
       input.value = formatNumber(value);
    }
}

function formatNumber(value) {
    if (isNaN(value)) return '';
    let parts = value.toLocaleString('en-US').split('.');
    parts[0] = parts[0].replaceAll(',', THOUSAND_SEPARATOR);
    return parts.join(DECIMAL_SEPARATOR);
}

function removeFormatNumber(value) {
    value += '';
    return Number(value.replaceAll(THOUSAND_SEPARATOR, '').replaceAll(DECIMAL_SEPARATOR, '.'));
}

function FV(rate = 0, nper = 0, pmt = 0, pv = 0, type = 0) {
    // Validate parameters
    if (type != 0 && type != 1) {
        return false;
    }

    // Calculate
    if (rate != 0.0) {
        return -pv * Math.pow(1 + rate, nper) - pmt * (1 + rate * type) * (Math.pow(1 + rate, nper) - 1) / rate;
    } else {
        return -pv - pmt * nper;
    }
}

function PMT(rate = 0, nper = 0, fv = 0, pv = 0, type = 0) {
    return (-fv - pv * Math.pow(1 + rate, nper)) / (1 + rate * type) / ((Math.pow(1 + rate, nper) - 1) / rate);
}


function arrayChunk(arr, chunkLength = 3) {
    let result = [];
    let chunkCount = Math.ceil(arr.length / chunkLength);

    let j = 0;
    for(let i = 0; i < chunkCount; i++) {
        result.push(arr.slice(j, j + chunkLength));
        j += chunkLength;
    }

    return result;
}

function arrayChunk2(arr, chunkCount = 10, useEmptyChunk = false) {
    let result = [];
    let chunkLength = Math.ceil(arr.length / chunkCount);

    let j = 0;
    for(let i = 0; i < chunkCount; i++) {
        result.push(arr.slice(j, j + chunkLength));
        j += chunkLength;
    }
    if(!useEmptyChunk) {
        return result.filter(item => item.length > 0);
    }
    return result;
}

function arrayRestrict(arr, step) {
    if(step == 0) return [];
    let length = arr.length;
    let result = [];
    for(let i = 0; i < length; i += step) {
        let item = arr[i] || arr[length - 1];
        result.push(item);
    }

    return result;
}

function validateValue(item, max, min, iderr){
    var element = document.getElementById('estimate');
    var element1 = document.getElementById(iderr);
    if(item.value<min || item.value>max){
        item.style.color ='red';
        element.disabled = true;
        element1.style.display = "block";
    } else{
        item.style.color ='#ffff';
        element.disabled = false;
        element1.style.display = "none";
    }
}
