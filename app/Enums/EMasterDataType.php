<?php


namespace App\Enums;


class EMasterDataType {
    const BANNER = 1;
    const INVESTMENT_NEEDS = 2;
    const INVESTMENT_STEP = 3;
    const COMPANY_AFFILIATION = 4;
    const COMPANY_ADDRESS = 5;
    const TELEPHONE = 6;
    const FAX = 7;
    const CONTENT_TO_ADVISE = 14;
    const INVESTMENT_BASIC = 15;
    const INVESTMENT_TERM = 16;
    const INVESTMENT_FUND_BASIC = 17;
    const POPUP = 21;
    const RATE_LEVEL_RISK = 25;
}
