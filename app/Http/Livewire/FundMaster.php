<?php

namespace App\Http\Livewire;

use Livewire\Component;

class FundMaster extends Component
{
    public function render()
    {
        return view('livewire.fund-master');
    }
}
