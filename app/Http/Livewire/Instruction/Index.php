<?php

namespace App\Http\Livewire\Instruction;
use App\Models\Guideline;
use DB;
use App\Models\GuidelineDetail;
use App\Models\FundMaster;
use Illuminate\Support\Facades\Storage;

use Livewire\Component;

class Index extends Component
{
    public $fundId = 3;
    public $file;

    public function mount() {
        if (url()->previous() == url('quan-ly-quy/MBVF')) {
            $this->fundId = 2;
        }
        if (url()->previous() == url('quan-ly-quy/MBBOND')) {
            $this->fundId = 3;
        }
        if(isset($_GET['fund'])){
            $this->fundId = $_GET['fund'];
        }
    }

    public function render()
    {
        $faqData_app = $this->getData();
        $faqData_web = $this->getData(2);
        $this->file = FundMaster::query()->where('type',17)->where('fund_id', $this->fundId)->get()->first();
        return view('livewire.instruction.index', compact('faqData_app', 'faqData_web'));
    }

    public function getData($type = 1){
        $query = Guideline::query();
        if(!empty($this->fundId)){
            $query = $query->where('fund_id', $this->fundId);
        }
        return $query->where('type', $type)->with(['guidelineDetail' => function($q){
            $q->orderBy('order_number', 'ASC');
        }])->orderBy('created_at', 'desc')->get();
    }

}
