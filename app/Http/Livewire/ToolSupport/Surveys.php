<?php

namespace App\Http\Livewire\ToolSupport;

use App\Enums\EMasterDataType;
use App\Http\Livewire\Base\BaseLive;
use App\Models\MasterData;
use App\Models\Survey;
use App\Models\SurveyResponse;
use App\Models\SurveyResponseDetail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Pagination\Paginator;
use Livewire\Component;
use Browser;
use DB;

class Surveys extends BaseLive
{
    public $show_start = true;
    public $show_end = false;
    public $event = [];
    public $total = 0;
    public $maxPage = 1;
    public $invalidMessage = false;
    public $result_rate = '',$result_content='' ,$result_chart=[],$result_text=[], $rate_id;
    protected $listeners=[
        'loadingChart'
    ];

    public function mount()
    {
        $this->resetPage();
        $this->loadingChart();
    }
    public function loadingChart(){
        $cookie = request()->cookie('rate-level-risk');
        if($cookie){
            $master_data = MasterData::where('type',EMasterDataType::RATE_LEVEL_RISK)->where('order_number',$cookie)->first();
            if($master_data){
                $arr_content=json_decode($master_data->v_content, true);
                if($arr_content){
                    $this->result_rate= $master_data->v_value;
                    $this->result_content= $master_data->note;
                    $this->result_chart= $arr_content["survey_chart"]??[];
                    $this->rate_id=$master_data->order_number;
                    $this->result_text = $arr_content["survey_text"]??[];
                }
        }
        $this->show_end = true;
        $this->show_start = false;
        }
    }
    public function render()
    {
        $survey = Survey::paginate(1);
        $this->totalEvent();
        if ($this->maxPage < $survey->currentPage()) {
            $this->maxPage = $survey->currentPage();
        }
        $dataPie=collect();
        $arr_pie=[];

        $color=['#E5E5E5','#6285C1','#123673'];
        if($this->result_chart){
            foreach ($this->result_chart as $key => $item) {
                $arr_pie['value']=['value'=>$item['value'],'color'=>$color[$key]];
                $arr_pie['label']=$item['name'];
                $dataPie->push($arr_pie);
            }
            $this->dispatchBrowserEvent('update_scripts',['data' => $dataPie]);
        }
        return view('livewire.tool-support.surveys', ['surveys' => $survey,'dataPie'=>$dataPie]);
    }

    public function startSurvey()
    {
        $cookie = request()->cookie('rate-level-risk');
        if($cookie){
            $this->show_end = true;
            $this->show_start = false;
        }else{
            if ($this->show_start) {
                $this->show_start = false;
            } else {
                $this->show_start = true;
            }
        }

    }

    public function endSurvey()
    {
        $count = Survey::count();
        $sum_point = 0;
        $rate_risk = null;
        $this->invalidMessage = true;
        if ($count == $this->total) {
            $sum_point = $this->sumPoint();
            $master_data = MasterData::where('type',EMasterDataType::RATE_LEVEL_RISK)->where('number_value','>=',$sum_point)->orderBy('number_value', 'asc')->get()->first();
            if($master_data){
                $arr_content=json_decode($master_data->v_content, true);
                $this->result_rate= $master_data->v_value;
                $this->result_content= $master_data->note;
                $this->rate_id=$master_data->order_number;
                $rate_risk=$master_data->id;
                if($arr_content ){
                    $this->result_chart= $arr_content["survey_chart"]??[];
                    $this->result_text = $arr_content["survey_text"]??[];
                }
            }
            $device = '';
            if (Browser::isMobile()) {
                $device = 'mobile';
            } else if (Browser::isTablet()) {
                $device = 'tablet';
            } else if (Browser::isDesktop()) {
                $device = 'desktop';
            }
            $maxSaveTime = SurveyResponse::where('ip_address', \request()->ip())
                ->select('ip_address', DB::raw('MAX(save_time) as maxtime'))->groupBy('ip_address')->pluck('maxtime')->toArray();
            $maxtime = 1;
            if ($maxSaveTime) {
                $maxtime = $maxSaveTime[0] + 1;
            }
            $response = SurveyResponse::create([
                'ip_address' => \request()->ip(),
                'sum_point' => $sum_point,
                'rate_level_risk' => $rate_risk,
                'request_date' => Carbon::now(),
                'device' => $device,
                'browser' => Browser::browserName(),
                'save_time' => $maxtime,
            ]);
            foreach ($this->event as $question => $answers) {
                foreach ($answers as $answer_id => $point){
                    $response_detail = new SurveyResponseDetail();
                    $response_detail->survey_response_id =$response->id;
                    $response_detail->survey_id = $question;
                    $response_detail->survey_details_id = $answer_id;
                    $response_detail->save();
                }
            }
            $cookie = $this->getCookie();
            Log::info('Survey has cookie rate level risk : '.$cookie);
            $this->setCookie();
            $this->emit('loadProductConsulting');
            $this->show_end = true;
            $this->invalidMessage = false;
        }
    }

    public function resetSurvey()
    {
        $this->resetPage();
        $this->show_start = false;
        $this->show_end = false;
    }

    public function changeSingleSelect($question, $answer, $point)
    {
        if (count($this->event[$question]) > 1) {
            unset($this->event[$question]);
            $this->event[$question] = ["$answer" => $point];
        } else {
            foreach ($this->event[$question] as $key => $answer) {
                if ($answer === false) {
                    unset($this->event[$question][$key]);
                }
            }
        }
    }

    public function changeMultiSelect($question)
    {
        if (empty($this->event[$question])) {
            unset($this->event[$question]);
        } else {
            foreach ($this->event[$question] as $key => $answer) {
                if ($answer === false) {
                    unset($this->event[$question][$key]);
                }
            }
        }
    }

    public function totalEvent()
    {
        $totalEvent = 0;
        foreach ($this->event as $question) {
            if ($question) {
                $totalEvent++;
            }
        }
        $this->total = $totalEvent;
    }

    public function sumPoint()
    {
        $sumPoint= 0;
        foreach ($this->event as $key => $ques) {
            if ($ques) {
                foreach ($ques as $answer) {
                    $sumPoint += $answer;
                }
            }
        }
        return $sumPoint;
    }
    public function setCookie()
    {
        $minutes = 365 * 24 * 60;
        Log::info('Survey setCookie: ' . $this->rate_id);
        Cookie::queue(Cookie::make('rate-level-risk',$this->rate_id, $minutes, null, null, false, false));
    }
    public function getCookie()
    {
        return request()->cookie('rate-level-risk');
    }
}
