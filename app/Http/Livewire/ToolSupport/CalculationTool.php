<?php

namespace App\Http\Livewire\ToolSupport;

use App\Models\MasterData;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Livewire\Component;

class CalculationTool extends Component
{
    public $typeCalc = 1;

    public $fundData = [];
    public $labels;
    public $dataSets = [];
    public $timeSelect = '1Y';
    public $color = "rgba(20, 29, 210, 0.1)";
    public $background_colors;
    public $blueColor = "#141ED1";
    public $fillStyleColors;
    public $borderColors = ["#141ED1", "#BD9976"];
    public $amount = 1000000000;
    public $profit = 0.12;
    public $year = 1;
    protected $listeners = [
        'getChartMBVF',
        'getChartMBVFSIP',
        'getDataHighChart',
        'hideChart',
        'get-default-highchart' => 'checkCookie',
        'loadRenderCal' => 'render',
    ];

    public function mount()
    {
        $this->background_colors = [$this->color, "rgba(189, 153, 118, 0.1)"];
        $this->fillStyleColors = [$this->blueColor, "#BD9976"];
    }

    public function render()
    {
        return view('livewire.tool-support.calculation-tool');
    }

    public function filterData() {
        if ($this->typeCalc == 3) {
            $this->emit('show-range-commit');
        }
    }

    public function getLabel() {
        if (App::getLocale() == 'en') {
            return 'M';
        }
        else if (App::getLocale() == 'vi') {
            return 'T';
        }
    }

    public function getChartMBVF($amount, $profit, $year)
    {
        $this->amount = $amount;
        $this->profit = $profit;
        $this->year = $year;
        $this->reset('fundData', 'labels');
        $this->fundData[0] = $this->amount;
        $this->labels[0] = $this->getLabel().'0';
        if ($this->year % 2 == 0) {
            $index = $this->year / 2;

        } else {
            $index = ($this->year + 1) / 2;
        }
        $data = [];
        $data[0] = $this->amount;
        for ($i = 1; $i <= $this->year * 12; $i ++) {
            $data[$i] = $data[$i-1] * (1 + (pow((float) 1 + $this->profit, (float) 1/12) - 1));
        }
        $i = 1;
        $j = $index;
        while ($j <= $this->year * 12) {
            $this->fundData[$i] = $data[$i * $index];
            $this->labels[$i] =  $this->getLabel().$j;
            $i ++;
            $j += $index;
        }
        if ($j > $this->year * 12) {
            $j -= $index;
            if ($j != $this->year * 12) {
                $this->fundData[$i] = $data[$this->year * 12];
                $this->labels[$i] =  $this->getLabel().$this->year * 12;
            }
        }
        $this->dataSets = [];
        $dataSet = new \stdClass();
        $dataSet->backgroundColor = data_get($this->background_colors, 0, $this->color);;
        $dataSet->borderColor = data_get($this->borderColors, 0, $this->blueColor);
        $dataSet->fill = true;
        $dataSet->label = __('mbvf.calculation_tool.amount_expected_to_receive');
        $dataSet->data = $this->fundData;
        $dataSet->fillStyle = data_get($this->fillStyleColors, 0, $this->blueColor);
        $this->dataSets[] = $dataSet;
        $this->emit('showChartMBVF', json_encode($this->dataSets), $this->labels);
    }

    public function getChartMBVFSIP($amount, $profit, $year)
    {
        $this->amount = $amount;
        $this->profit = $profit;
        $this->year = $year;
        $this->reset('fundData', 'labels');
        $this->fundData[0] = 0;
        $this->labels[0] =  $this->getLabel().'0';
        $monthProfit = (1 + $this->profit) ** (1 / 12) - 1;
        if ($this->year % 2 == 0) {
            $index = $this->year / 2;

        } else {
            $index = ($this->year + 1) / 2;
        }
        $data = [];
        $data[0] = $this->amount;
        for ($i = 1; $i <= $this->year * 12; $i ++) {
            $data[$i] = $data[$i-1] * (1 + $monthProfit) + $this->amount;
        }
        $i = 1;
        $j = $index;
        while ($j <= $this->year * 12) {
            $this->fundData[$i] = $data[$i * $index] - $this->amount;
            $this->labels[$i] =  $this->getLabel().$j;
            $i ++;
            $j += $index;
        }
        if ($j > $this->year * 12) {
            $j -= $index;
            if ($j != $this->year * 12) {
                $this->fundData[$i] = $data[$this->year * 12] - $this->amount;
                $this->labels[$i] =  $this->getLabel().$this->year * 12;
            }
        }
        $this->dataSets = [];
        $dataSet = new \stdClass();
        $dataSet->backgroundColor = data_get($this->background_colors, 0, $this->color);
        $dataSet->borderColor = data_get($this->borderColors, 0, $this->blueColor);
        $dataSet->fill = true;
        $dataSet->label = __('mbvf.calculation_tool.amount_expected_to_receive');
        $dataSet->data = $this->fundData;
        $dataSet->fillStyle = data_get($this->fillStyleColors, 0, $this->blueColor);
        $this->dataSets[] = $dataSet;
        $this->emit('showChartMBVFSIP', json_encode($this->dataSets), $this->labels);
    }

    public function getDataHighChart($expectedProfit) {
        $this->profit = $expectedProfit;
        $config = MasterData::where('type',24)->where('order_number', '>=', $this->profit * 100)->get()->first();
        if($config) {
            $content = isset($config->v_content)?json_decode($config->v_content, true):'';
            $this->fundData = $content;
        }
        $this->setCookie();
        $this->emit('setHighChart', $this->fundData);
    }

    public function hideChart() {
        $this->reset([
            'fundData',
            'labels',
            'dataSets',
            'amount',
            'profit',
            'year',
        ]);
    }

    public function setCookie() {
        $minutes = 365 * 24 * 60;
        Cookie::queue(Cookie::make('result-calculation', $this->profit,$minutes, null, null, false, false));
    }
    public function checkCookie() {

        if (request()->cookie('result-calculation')) {
            $this->typeCalc = 3;
            $this->emit('show-range-commit');
            $this->profit = request()->cookie('result-calculation');
            $this->emit('set-default-highchart', $this->profit);
        }
    }
}
