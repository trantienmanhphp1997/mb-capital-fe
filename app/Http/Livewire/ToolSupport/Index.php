<?php

namespace App\Http\Livewire\ToolSupport;

use App\Enums\EMasterDataType;
use App\Models\MasterData;
use hisorange\BrowserDetect\Exceptions\Exception;
use Livewire\Component;

class Index extends Component
{
    protected $listeners =['loadIndex'=>'render'];
    public function render()
    {
        $dataPie = collect();
        $arr_pie = [];
        $cookie = request()->cookie('rate-level-risk');
        if ($cookie) {
            $rate_level = MasterData::where('type', EMasterDataType::RATE_LEVEL_RISK)->where('order_number', $cookie)->first();
            $color = ['#6285C1', '#123673'];
            if ($rate_level) {
                $arr_content = json_decode($rate_level->v_content, true);
                if ($arr_content) {
                    $content_chart = $arr_content['survey_chart'] ?? [];
                    if ($content_chart) {
                        foreach ($content_chart as $key => $item) {
                            $arr_pie['value'] = ['value' => $item['value'], 'color' => $color[$key - 1]];
                            $arr_pie['label'] = $item['name'];
                            $dataPie->push($arr_pie);
                        }
                        $arrayPie = json_decode($dataPie);
                        $this->dispatchBrowserEvent('update_scripts', ['data' => $arrayPie]);
                    }
                }
            }
        }
        $profit = request()->cookie('result-calculation');
            if ($profit) {
            $config = MasterData::where('type', 24)->where('order_number', '>=', $profit * 100)->get()->first();
            if ($config) {
                $content = isset($config->v_content) ? json_decode($config->v_content, true) : '';
                $this->emit('setHighChart', $content);
            }
        }
        return view('livewire.tool-support.index', compact('dataPie'));
    }
    public function calculationTool(){
        $this->emit('get-default-highchart');
    }
    public function loadProductCons(){
        $this->emit('loadProductConsulting');
        $this->emit('loadingChart2');
    }
    public function survey(){
        $this->emit('loadingChart');
    }
}
