<?php

namespace App\Http\Livewire\ToolSupport;

use App\Models\MasterData;
use Browser;
use Livewire\Component;

class ProductConsulting extends Component
{
    public $cookie;
    public $cookieCal;
    protected $listeners=[
        'loadProductConsulting'=>'render',
        'loadingChart2'
    ];
    public function render()
    {
        $this->cookie = request()->cookie('rate-level-risk');
        $this->cookieCal= request()->cookie('result-calculation');
//        dd($this->cookieCal);
        $queryDesktop =MasterData::query();
        if($this->cookie){
            $queryDesktop->where('v_content','LIKE',"%$this->cookie%");
        }
        $getDataDesktop = $queryDesktop->where('type',22)->first();
        $getDataMobile = MasterData::query()->where('type',23)->get()->toArray();
        $isDesktop = Browser::isDesktop();
        return view('livewire.tool-support.product-consulting',[
            'getDataDesktop' => $getDataDesktop,
            'getDataMobile' => $getDataMobile,
            'isDesktop' => $isDesktop
        ]);
    }
    public function loadingChart2(){
        if($this->cookieCal){
            $this->emit('get-default-highchart');
        }
    }
}
