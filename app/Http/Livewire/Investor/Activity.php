<?php

namespace App\Http\Livewire\Investor;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Fund;
use App\Models\FundNews;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class Activity extends BaseLive
{
    public $pagination = 10;
    public $fundid;
    public $year;
    public $searchActivity;

    protected $listeners = ['refresh'];

    public function mount($fundid, $year){
        $this->year = $year;
        $this->fundid = $fundid;
    }

    public function refresh($fundid, $year){
        $this->year = $year;
        $this->fundid = $fundid;
        $this->resetPage();
    }

    public function updatingSearchActivity()
    {
        $this->resetPage();
    }

    public function updatingFundid()
    {
        $this->resetPage();
    }

    public function updatingYear()
    {
        $this->resetPage();
    }

    public function render()
    {

        $fund = Fund::query()->with('childFund')->find($this->fundid);
        $fundIds = $fund->childFund->pluck('id')->toArray();
        array_push($fundIds, $fund->id);

        $query = FundNews::query();
        $query->where( DB::raw('YEAR(public_date)'), $this->year ?? date('Y'))
            ->whereIn('fund_id', $fundIds)
            ->where('type', 2);
        if($this->searchActivity){
            $query->where('title_vi','like','%'. $this->searchActivity .'%');
        }
        $reportActivity = $query->orderBy('public_date', 'desc')->paginate($this->pagination);
        $reportActivity->setPageName('pageActivity');
        return view('livewire.investor.activity', compact(
            'reportActivity',
        ));
    }
}
