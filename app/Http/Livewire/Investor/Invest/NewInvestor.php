<?php

namespace App\Http\Livewire\Investor\Invest;

use App\Enums\EMasterDataType;
use App\Models\MasterData;
use Livewire\Component;

class NewInvestor extends Component
{
    public function render()
    {
        $first_item = MasterData::Where('type', EMasterDataType::INVESTMENT_BASIC)->orderBy('order_number')->first();
        $second_item = MasterData::Where('type', EMasterDataType::INVESTMENT_FUND_BASIC)->orderBy('order_number')->first();
        $last_item = MasterData::Where('type', EMasterDataType::INVESTMENT_TERM)->orderBy('order_number')->first();
        return view('livewire.investor.invest.new-investor', compact('last_item', 'second_item', 'first_item'));
    }
}
