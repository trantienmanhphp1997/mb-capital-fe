<?php

namespace App\Http\Livewire\Investor\Invest;

use App\Enums\EMasterDataType;
use App\Models\MasterData;
use Livewire\Component;

class ProfessionalInvestor extends Component
{
    public function render()
    {
        $listInvestmentStep = MasterData::query()->where('type', EMasterDataType::INVESTMENT_STEP)->orderBy('order_number')->get();
        return view('livewire.investor.invest.professional-investor', [
            'listInvestmentStep' => $listInvestmentStep,
        ]);
    }
}
