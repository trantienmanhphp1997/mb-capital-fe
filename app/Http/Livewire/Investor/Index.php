<?php

namespace App\Http\Livewire\Investor;

use App\Models\Fund;
use Livewire\Component;
use Illuminate\Support\Facades\DB;
use App\Models\FundNews;
use Carbon\Carbon;
class Index extends Component
{
    public $fundid;
    public $year;
    public $tab;
    public function mount($slug)
    {
        $fund = Fund::where('slug', $slug)->first();
        if($fund && $slug){
            $this->fundid = $fund->id;
        }
        $yearHasValue = $this->getYearHasValue(); 
        // $this->year = end($yearHasValue);
    }

    public function filterData(){
        $this->emit('refresh', $this->fundid, $this->year);
    }

    public function render(){
        $funds = Fund::whereNull('parent_id')->where('shortname','!=','JAMBF')->orderBy('priority', 'asc')->get();
        if(!$this->fundid && $funds->count()>0){
            $this->fundid = $funds[0]->id;
        }
        $yearHasValue = $this->getYearHasValue(); 
        return view('livewire.investor.index', compact(
            'funds','yearHasValue'
        ));
    }
    public function getYearHasValue(){
        $curentYear = Carbon::now()->format('Y');
        $yearHasValue= FundNews::query()->groupBy( DB::raw('YEAR(public_date)'))
            ->select(DB::raw('YEAR(public_date) as value'))
            ->orderBy(DB::raw('YEAR(public_date)'),'desc')->pluck('value')->toArray();
        foreach($yearHasValue as $key => $value){
            if($value>$curentYear){
                unset($yearHasValue[$key]);
            }
        }
        return $yearHasValue;
    }
}
