<?php

namespace App\Http\Livewire\Home;
use Livewire\Component;
use App\Models\MasterData;
use App\Models\Fund;
use App\Models\Advise;
use App\Enums\EMasterDataType;
use Illuminate\Support\Facades\App;

class Index extends Component {
    public $ip_client;
    public $popup;
    protected $listeners=[
        'getModal',
    ];

    public function render()
    {
        $listBanner = MasterData::query()->where('type', EMasterDataType::BANNER)->orderBy('id')->get();
        $listInvestmentNeed =  MasterData::query()->where('type', EMasterDataType::INVESTMENT_NEEDS)->orderBy('id')->get();
        $listFund = Fund::orderBy('priority')->with('childFund')->whereNotNull('type')->get();
        $listInvestmentStep = MasterData::query()->where('type', EMasterDataType::INVESTMENT_STEP)->orderBy('order_number')->get();
        $this->popup = MasterData::where('type', EMasterDataType::POPUP)->where('status', 1)->first();
        return view('livewire.home.index', ['listBanner' => $listBanner, 'listInvestmentNeed' => $listInvestmentNeed, 'listFund' => $listFund, 'listInvestmentStep' => $listInvestmentStep, 'popup' => $this->popup]);
    }

    public function getModal(){
        if($this->popup){
            $this->emit('showModal');
        }
    }
}
