<?php

namespace App\Http\Livewire\Search;

use Livewire\Component;
use App\Models\Fund;
use App\Models\Guideline;

class Index extends Component
{
    public $searchFund;
    public $selectFund;

    public function render()
    {
        if($this->selectFund){
            $query = Fund::where('id',$this->selectFund);
        }
        else {
            $query = Fund::whereNotNull('type')->orderBy('priority');
            if($this->searchFund){
                $query->where('fullname', 'like', '%'.$this->searchFund.'%')
                ->orWhere('fullname2', 'like', '%'.$this->searchFund.'%')
                ->orWhere('shortname', 'like', '%'.$this->searchFund.'%')
                ->orWhere('shortname2', 'like', '%'.$this->searchFund.'%');
            }
        }
        $data = $query->get();
        $listFund = Fund::whereNotNull('type')->orderBy('priority')->pluck('fullname2','id');
        $listFundEn = Fund::whereNotNull('type')->orderBy('priority')->pluck('fullname2_en','id');

        $listFund->put( 'trust', __('common.header.investment_trust'));
        $listFundEn->put( 'trust', __('common.header.investment_trust'));
        $listFundIdHasGuideline = Guideline::query()->groupBy('fund_id')->pluck('fund_id')->toArray();

        return view('livewire.search.index', compact('data', 'listFund', 'listFundIdHasGuideline','listFundEn'));
    }
}
