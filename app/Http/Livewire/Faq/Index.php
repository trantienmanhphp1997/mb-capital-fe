<?php

namespace App\Http\Livewire\Faq;

use Livewire\Component;
use App\Models\Faq;
use App\Enums\EFaqType;
use App\Models\Fund;
use Illuminate\Support\Facades\App;

class Index extends Component
{
    public $searchTerm;
    public $filterFundId;
    public function render()
    {
        $query = Faq::query();
        if(!is_null($this->filterFundId)) {
            $query->where('fund_id',$this->filterFundId);
        } else {
            $query->where('type', EFaqType::FREQUENT);
        }
        if(!empty($this->searchTerm)){
            if(App::getLocale() == 'vi') {
                $query->where(function($q) {
                    $q->where('question','like', '%'.$this->searchTerm.'%')
                    ->orWhere('content','like', '%'.$this->searchTerm.'%');
                });
            } else {
                $query->where(function($q) {
                    $q->where('question_en','like', '%'.$this->searchTerm.'%')
                    ->orWhere('content_en','like', '%'.$this->searchTerm.'%');
                });
            }
            
        }
        $listFaq = $query->orderBy('id')->get();
        $listFund = Fund::query()->whereNull('parent_id')->orderBy('priority')->get();
        return view('livewire.faq.index', ['listFaq' => $listFaq, 'listFund' => $listFund]);
    }

    public function setFund($id = null) {
        $this->filterFundId = $id;
    }
}
