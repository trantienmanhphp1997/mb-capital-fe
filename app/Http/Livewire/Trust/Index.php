<?php

namespace App\Http\Livewire\Trust;

use Livewire\Component;
use DB;
use App\Models\MasterData;

class Index extends Component
{
    public function render(){
        $trust = MasterData::where('type',11)->where('order_number',1)->get()->first();
        $trustContact = MasterData::where('type',11)->where('order_number',2)->get()->first();
        return view('livewire.trust.index',compact('trust','trustContact'));
    }
}
