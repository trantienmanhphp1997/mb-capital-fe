<?php

namespace App\Http\Livewire\Trust;

use Livewire\Component;
use DB;
use App\Models\MasterData;

class Personal extends Component
{
    public function render(){
        $trust = MasterData::where('type',12)->first();
        $trustList = MasterData::where('type',13)->get();
        return view('livewire.trust.personal', compact('trust','trustList'));
    }
}
