<?php

namespace App\Http\Livewire\Trust;

use Livewire\Component;
use DB;
use App\Models\MasterData;
class Example extends Component
{
    public $checkConfig;
    public $numberValue;
    public $data;
    protected $listeners=['getDataHighChart'];
    public function mount(){
        $this->checkConfig = 2;
        $this->getData();
    }
    public function render(){
        $this->getData();
        $trustConfig = MasterData::where('type',8)->orderBy('order_number','asc')->get();
        $this->emit('setHighChart',$this->data);
        return view('livewire.trust.example',compact('trustConfig'));
    }
    public function getData(){
        $config = MasterData::where('type',8)->where('order_number',$this->checkConfig)->get()->first();
        if($config){
            $content = isset($config->v_content)?json_decode($config->v_content, true):'';
            $this->data = $content;
            $this->numberValue = $config->number_value??0;
        }
    }
    public function getDataHighChart(){
        $this->getData();
        $this->emit('setHighChart', $this->data);       
    }
}
