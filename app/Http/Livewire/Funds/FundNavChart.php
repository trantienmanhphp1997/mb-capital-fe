<?php

namespace App\Http\Livewire\Funds;

use App\Models\Fund;
use App\Models\File;
use App\Models\FundNav;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use DateTime;
use DatePeriod;
use DateInterval;

class FundNavChart extends Component
{

    public $fundIds = [];
    public $type = 0;
    public $file_fund_id;
    public $urlDieuLe;
    public $urlBaoCao;
    public $urlThongTin;
    public $fileNameDieuLe;
    public $fileNameBaoCao;
    public $fileNameThongTin;
    public $modelName = 'App\Models\FundMaster';
    public function mount(){
        $id = ( count($this->fundIds)> 1)?$this->file_fund_id:$this->fundIds[0];
        // get url file
        $data = DB::table('fund_master')->where('fund_id',$id)->where('type',14)->get()->first();
        if($data&&$data->img){
            $this->urlDieuLe = $data->img;
            $data = File::where('model_name',$this->modelName)->where('model_id',$data->id)->orderBy('id','desc')->first();
            $this->fileNameDieuLe = $data->file_name??'';
        }
        $data = DB::table('fund_master')->where('fund_id',$id)->where('type',15)->get()->first();
        if($data&&$data->img){
            $this->urlBaoCao = $data->img;
            $data = File::where('model_name',$this->modelName)->where('model_id',$data->id)->orderBy('id','desc')->first();
            $this->fileNameBaoCao = $data->file_name??'';
        }
        $data = DB::table('fund_master')->where('fund_id',$id)->where('type',16)->get()->first();
        if($data&&$data->img){
            $this->urlThongTin = $data->img;
            $data = File::where('model_name',$this->modelName)->where('model_id',$data->id)->orderBy('id','desc')->first();
            $this->fileNameThongTin = $data->file_name??'';
        }
    }
    public function render()
    {
        $startDate = Carbon::now()->subMonths(11)->startOf('month')->format('Y-m-d');

        $fundNavs = DB::table('fund_nav')
            ->selectRaw('max(amount) as total, month(trading_session_time) as the_month, fund_id, year(trading_session_time) as the_year, day(trading_session_time) as the_day')
            ->whereNull('deleted_at')
            ->whereIn('fund_id', $this->fundIds)
            ->where('trading_session_time', '>=',$startDate)
            ->groupByRaw('fund_id, the_month, the_year')
            ->orderBy('trading_session_time')
            ->get()
            ->groupBy('fund_id')
            ->mapWithKeys(function($nav, $index) {
                $fund = Fund::query()->find($index);
                return [
                    trim($fund->shortname ?? $index) => $nav->map(function($item) {
                        return [
                                'x' => $item->the_month."/".$item->the_year,
                                'y' => $item->total,
                                'date' => Carbon::create($item->the_year, $item->the_month, $item->the_day)->format('d/m/Y')
                        ];
                    })->keyBy('x')->values()
                ];
            });
        return view('livewire.funds.fund-nav-chart', [
            'fundNavs' => $fundNavs
        ]);
    }
}
