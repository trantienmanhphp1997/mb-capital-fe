<?php

namespace App\Http\Livewire\Funds;
use Livewire\Component;
use App\Models\MasterData;
use App\Models\Fund;
use App\Models\FundMaster;
use App\Models\FundNews;
use App\Models\ExpectedProfit;
use App\Models\FunExpectedProfit;
use App\Models\FunEmployeeXRF;
use App\Models\FundNav;
use App\Models\File;
use App\Models\Employee;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;
use App\Http\Livewire\Base\BaseLive;
use DB;
use Browser;

class Mbbond extends BaseLive {

    public $fund_id;
    public $chart_filter_select = 2;

    public $amount;
    public $fromDate;
    public $toDate;
    public $percent;
    public $month;
    public $action = false;
    public $locale;
    public $result = [];
    public $content;
    public $fullname;
    public $title;
    public $listeners = [
        'load-page' => 'render'
    ];

    public function mount() {
        $this->locale = App::getLocale();
        $this->result = [
            'profitMB' => 0,
            'valueMB' => 0,
            'value' => 0,
            'valuePromotion' => 0,
            'diffPrice' => 0,
            'day' => 0,
            'month' => 0,
        ];
    }

    public function render()
    {
        $expected_profit = $this->getProfitMbbond($this->fund_id,1);
        $fun_employee_xrf = Employee::where('fund_id',$this->fund_id)->orderBy('priority','asc')->get();
        foreach($fun_employee_xrf as $item) {
            if ($item->sex == 1) {
                $item->fullname = __('mbbond.mr') . $item->fullname;
            } else {
                $item->fullname = __('mbbond.mrs') . $item->fullname;
            }
        }

        foreach($expected_profit as $item) {
            $item->percent = sprintf('%0.2f', $item->percent);
            if(config('app.locale') == 'vi') {
                $item->percent = str_replace('.', ',', $item->percent);
            }
        }
        $currentFunds = Fund::where('id', $this->fund_id)->first();
        $this->public_date = ($this->locale == 'vi') ? $currentFunds->publid_date : $currentFunds->publid_date_en;

        $data = [
            'fun_master_type_1' => $this->getDataFundMaster(1, $this->fund_id),
            'fun_master_type_2' => $this->getDataFundMaster(2, $this->fund_id),
            'fun_master_type_3' => $this->getDataFundMaster(3, $this->fund_id),
            'fun_master_type_4' => $this->getDataFundMaster(4, $this->fund_id),
            'fun_master_type_5' => $this->getDataFundMaster(5, $this->fund_id),
            'fun_master_type_6' => $this->getDataFundMaster(6, $this->fund_id),
            'fun_master_type_7' => $this->getDataFundMaster(7, $this->fund_id),
            'fun_master_type_8' => $this->getDataFundMaster(8, $this->fund_id),
            'fun_master_type_18' => $this->getDataFundMasterFirst(18, $this->fund_id)
        ];
        $this->calculator();
        return view('livewire.funds.mbbond', [
            'expected_profit' => $expected_profit,
            'fun_employee_xrf' => $fun_employee_xrf,
            'currentFunds' => $currentFunds,
            'data' => $data,
            'locale' => $this->locale,
        ]);
    }

    public function getDataFundMaster($type, $fund_id) {
        return FundMaster::where('fund_id', $this->fund_id)->where('type', $type)->get();
    }

    public function getDataFundMasterFirst($type, $fund_id) {
        return FundMaster::where('fund_id', $this->fund_id)->where('type', $type)->first();
    }
    public function getProfitMbbond($fund_id, $type){
        $query = FunExpectedProfit::where('fund_id', $this->fund_id);
        if(!Browser::isDesktop()){
            $query->where('period', '<', 7);
        }
            // ->where('period','!=',0)
        return $query->where('type', $type)
            ->get();
    }
    public function getDataProfit($type, $period) {
        if ($period > 360) {
            $data = FunExpectedProfit::where('fund_id', $this->fund_id)
                ->where('period_time_min', '<=', $period)
                ->whereNull('period_time_max')
                ->where('type', $type)
                ->first();
        } else {
            $data = FunExpectedProfit::where('fund_id', $this->fund_id)
                ->where('period_time_min', '<=', $period)
                ->where('period_time_max', '>=', $period)
                ->where('type', $type)
                ->first();
        }


        return $data;
    }

    public function calculator($action = false) {
        $this->action = $action;
        if (!empty($this->amount) && !empty($this->fromDate) && !empty($this->toDate)) {
            if ($this->locale == 'vi') {
                $amount = !empty($this->amount) ? (int)str_replace('.', '', $this->amount) : 0;

            } else {
                $amount = !empty($this->amount) ? (int)str_replace(',', '', $this->amount) : 0;
            }

            $period = Carbon::parse($this->toDate)->diffInDays(Carbon::parse($this->fromDate));

            $query = FunExpectedProfit::query();
            $promotion = $this->getDataProfit(0, $period);

            $currentProfit = $this->getDataProfit(1, $period);
            $profitMB_res = 0;

            // Tính % lãi xuất cho lãi xuất của MBBond
            if (!empty($currentProfit)) {
                $percent = $currentProfit->percent;
                $profitNextMonth = FunExpectedProfit::where('fund_id', $this->fund_id)
                        ->where('period', ((int)$currentProfit->period) + 1)
                        ->where('type', 1)->first();
                // dd($profitNextMonth);
                if ($profitNextMonth !== null && ($period - $currentProfit->period_time_min > 0) ) {     // Nếu số ngày đã nhập lớn hơn mốc hiện tại
                    if (!empty($profitNextMonth)) {
                        $percentNextMonth = $profitNextMonth->percent;
                    } else {
                        $percentNextMonth = $percent;
                    }
                    // Tính phần trăm lãi xuất kỳ vọng mb bank
                    // (% hiện tại + (số tháng chênh lệch * số ngày chênh lệch)) / 100 = lãi xuất dự kiến nhận đc
                    $profitMB_res = $percent + (($percentNextMonth - $percent) / 30 * ($period - $currentProfit->period_time_min));
                    $profitMB = $profitMB_res / 100;
                } else { // Nếu số ngày nhỏ hơn mốc hiện tại
                    $profitMB_res = $percent;
                    $profitMB =  $profitMB_res / 100;
                }
            } else {
                $percent = 0;
            }

            $profitProNextMonth = $this->getDataProfit(0, $period + 1);
            $profitPromotion = $promotion->percent / 100;
            $valuePromotion = $amount * (1 + ($profitPromotion * $period / 365));
            $valueMB = $amount * (1 + ($profitMB * $period / 365));
            $diffPrice = $valueMB - $valuePromotion;

            if ($profitMB_res > 0) {
                $profitMB_res = substr($profitMB_res, 0, 4);
            } else {
                $profitMB_res = substr($profitMB_res, 0, 5);
            }

            $day = $period;
            $month = 0;
            if ($day >= 30) {
                $month = floor($day / 30);
                $day = $day % 30;
            }

            if ($action) {
                if ($this->locale == 'vi') {
                    $profitMB = str_replace('.', ',', $profitMB_res);
                    $this->result = [
                        'profitMB' => $profitMB,
                        'valueMB' => number_format($valueMB, 0, ',', '.'),
                        'value' => number_format($valueMB - $amount, 0, ',', '.'),
                        'valuePromotion' => $valuePromotion,
                        'diffPrice' => number_format($diffPrice, 0, ',', '.'),
                        'month' => $month,
                        'day' => $day,
                    ];
                } else {
                    $this->result = [
                        'profitMB' => $profitMB_res,
                        'valueMB' => number_format($valueMB, 0, '.', ','),
                        'value' => number_format($valueMB - $amount, 0, '.', ','),
                        'valuePromotion' => $valuePromotion,
                        'diffPrice' => number_format($diffPrice, 0, '.', ','),
                        'month' => $month,
                        'day' => $day,
                    ];
                }

            } else {
                $currentProfit = $this->getDataProfit(1, $period);
                $percent = $currentProfit->percent_view;
                if ($this->locale == 'vi') {
                    $this->percent = str_replace('.', ',', $percent) . ' %/'.__('mbbond.year');
                } else {
                    $this->percent = $percent . ' %/'.__('mbbond.year');
                }
            }
        }
    }

    public function setContentEmployee($item) {
        if ($this->locale == 'vi') {
            $this->content = $item['content'];
            $this->fullname = $item['fullname'];
            $this->title = $item['title'];
        } else {
            $this->content = $item['content_en'];
            $this->fullname = $item['fullname'];
            $this->title = $item['title_en'];
        }
    }

}
