<?php

namespace App\Http\Livewire\Funds;
use App\Models\FundNav;
use Livewire\Component;
use App\Models\MasterData;
use App\Models\Fund;
use App\Models\FundMaster;
use App\Models\FundNews;
use App\Models\ExpectedProfit;
use App\Models\FunEmployeeXRF;
use Illuminate\Support\Facades\App;

class Mbretire extends Component {

    public $fund_id;
    public $fund_tool;
    public $current_age = 30, $total_money,
    $retire_age = 62, $experience_year = 20;
    public $money=0 ,$moneyMonth=0;
    public $timeChart, $xScape=[];
    public $interest_view = 0;

    public function mount(){
        $this->total_money = numberFormat(4000000);
    }
    public function render()
    {
        $fund=[];
        $fundOther=[];
        $fundMasterReason=[];
        $fundMasterTool=[];
        $fundTime=[];
        $fundProcess=[];
        $yScape=[];

        $labels=[];
        if($this->fund_id){
            $fund=Fund::findOrFail($this->fund_id);
            $fundMasterReason=FundMaster::where('fund_id',$this->fund_id)->where('type',1)->get();
            $fundMasterTool=Fund::where('parent_id',$this->fund_id)->get();
            $fundOther=Fund::where('id','!=',$this->fund_id)->get();
            $fundTime = FundMaster::where('fund_id',$this->fund_id)->where('type',4)->first();
            $fundProcess = FundMaster::where('fund_id',$this->fund_id)->where('type',5)->first();
            $labels=Fund::where('parent_id',$this->fund_id)->pluck(App::getLocale() == 'en'?'fullname':'fullname_en')->toArray();
            $query5=FundNav::where('fund_id',5);
            $yScape[5]=$query5->pluck('amount')->toArray();
            $yScape[6]=FundNav::where('fund_id',6)->pluck('amount')->toArray();
            if($this->interest_view == 0){
                $this->interest_view =  count($fundMasterTool) > 0 ? (count($fundMasterTool) > 1 ? $fundMasterTool[1]-> interest_view : $fundMasterTool[0]-> interest_view) : 0;
                $this->fund_tool = count($fundMasterTool) > 0 ? (count($fundMasterTool) > 1 ? $fundMasterTool[1]-> interest : $fundMasterTool[0]-> interest) : 0;
            }
        }
        $healthChart = self::getCategoryChart(5);
        $prosperousChart = self::getCategoryChart(6);
        self::estimate();
        return view('livewire.funds.mbretire', compact('fund','fundMasterReason','fundMasterTool','fundOther','fundTime','fundProcess','yScape','labels', 'healthChart', 'prosperousChart'));
    }
    public function resetInput(){
        $this->current_age='';
        $this->total_money='';
        $this->retire_age='';
        $this->experience_year = '';
        $this->money=0;
        $this->moneyMonth=0;
    }
    public function estimate(){
        $total_money = str_replace('.', '', $this->total_money);
        $total_money = (int)str_replace(',', '', $total_money);
        if($this->fund_tool){
            if($total_money&&$this->current_age && $this->retire_age && $this->experience_year){
                $this->money = $total_money * (pow(( 1 + $this->fund_tool),(  $this->retire_age - $this->current_age ) * 12 ) - 1 )/$this->fund_tool;
                $this->moneyMonth = $this->money * $this->fund_tool/(1- 1/(pow(( 1 + $this->fund_tool),$this->experience_year * 12 )));
            }
        }else{
            $this->money=0;
            $this->moneyMonth=0;
        }
    }
    public function timeChart($value){
        if($value=='1w'){
            if(App::getLocale() == 'en'){
                $this->xScape=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
            }else{
                $this->xScape=['Thứ 2','Thứ 3','Thứ 4','Thứ 5','Thứ 6','Thứ 7','Chủ nhật'];
            }
        }

    }

    public function getCategoryChart($fund_id)
    {
        $data = Fund::findOrFail($fund_id);
        if($data && $data->allocate){
            $content = $data->allocate->content;
            $content = json_decode($content);
            $collect = collect();
            $array = [];
            foreach ($content as $key => $value){
                $array['label'] = $key;
                $array['value'] = $value;
                $collect->push($array);
            }
            return $collect;
        }else{
            return null;
        }


    }

    public function setInterest($interest_view, $interest)
    {
        $this->interest_view = $interest_view;
        $this->fund_tool = $interest;
    }
}
