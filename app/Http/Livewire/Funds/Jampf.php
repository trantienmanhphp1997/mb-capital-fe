<?php

namespace App\Http\Livewire\Funds;
use Livewire\Component;
use App\Models\MasterData;
use App\Models\Fund;
use App\Models\FundMaster;
use App\Models\FundNews;
use App\Models\ExpectedProfit;
use App\Models\FunEmployeeXRF;
use App\Models\File;
use Illuminate\Support\Facades\App;

class Jampf extends Component {

    public $fund_id;
    public function render()
    {
        $data = Fund::findOrFail($this->fund_id);
        return view('livewire.funds.jampf', ['data' => $data]);
    }
}
