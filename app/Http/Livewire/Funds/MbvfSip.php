<?php

namespace App\Http\Livewire\Funds;

use App\Models\Fund;
use Livewire\Component;

class MbvfSip extends Component
{
    public $fund_id = 0;

    public function render()
    {
        $fund = Fund::query()->findOrFail($this->fund_id);
        return view('livewire.funds.mbvf-sip', [
            'fund' => $fund
        ]);

    }
}
