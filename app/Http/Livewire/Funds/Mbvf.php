<?php

namespace App\Http\Livewire\Funds;
use Livewire\Component;
use App\Models\MasterData;
use App\Models\Fund;
use App\Models\FundMaster;
use App\Models\FundNav;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class Mbvf extends Component {

    public $fund_id;
    public $content;
    public $fullname;
    public $title;
    protected $listeners=[
        'getAllocateData',
    ];

    public $selectedFundDocumentation;

    public function render()
    {
        $data = Fund::findOrFail($this->fund_id);

        $fundMasterType5 = FundMaster::query()->where('fund_id', $this->fund_id)->where('type', 5)->get();
        $fundMasterType8 = FundMaster::query()->where('fund_id', $this->fund_id)->where('type', 8)->first();
        $fundMasterType10 = FundMaster::query()->where('fund_id', $this->fund_id)->where('type', 10)->first();
        $fundMasterType18 = FundMaster::where('fund_id', $this->fund_id)->where('type', 18)->first();
        if(is_null($this->selectedFundDocumentation)){
            $this->selectedFundDocumentation = $fundMasterType10->files[0] ?? null;
        }

        $lastNav = FundNav::query()->where('fund_id', $this->fund_id)->orderBy('created_at', 'desc')->first();

        return view('livewire.funds.mbvf', compact('data','fundMasterType8', 'fundMasterType5', 'fundMasterType10', 'lastNav', 'fundMasterType18'));
    }

    public function getAllocateData()
    {
        $data = Fund::findOrFail($this->fund_id)->allocate->content??'';
        $data = $data?json_decode($data):[];

        $collect = collect();
        $array = [];
        foreach ($data as $key => $value){
            // ở phần JS thêm r
            $array['label'] = $key;
            $array['value'] = $value;
            $collect->push($array);
        }
        $this->emit('showAllocateChart', $collect);
    }

    public function downloadFile($file){
        try {
            return Storage::download($file->url, $file->file_name);
        } catch(Exception $e) {
            dump($e->getMessage());
        }
    }


    public function setContentEmployee($item) {
        if (App::getLocale() == 'vi') {
            $this->content = $item['content'];
            $this->fullname = $item['fullname'];
            $this->title = $item['title'];
        } else {
            $this->content = $item['content_en'];
            $this->fullname = removeStringUtf8($item['fullname']);
            $this->title = $item['title_en'];
        }
    }
}
