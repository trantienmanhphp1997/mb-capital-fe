<?php

namespace App\Http\Livewire\About;

use Livewire\Component;
use DB; 
class Index extends Component
{
    public $activeTab;
    public function mount() {
        if(is_null($this->activeTab)) {
            $this->activeTab = 'about';
        }
    }
    public function render()
    {
        $data = DB::table('master_data')->where('type',19)->get()->first();
        return view('livewire.about.index',compact('data'));
    }

    public function setActiveTab($tab) {
        $this->activeTab = $tab;
    }
}
