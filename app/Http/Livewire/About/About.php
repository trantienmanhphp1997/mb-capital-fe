<?php

namespace App\Http\Livewire\About;

use Livewire\Component;
use DB;
use Illuminate\Support\Facades\Storage;
use App\Models\MasterData;
class About extends Component
{
    public function render()
    {
        $data = MasterData::where('type',19)->get()->first();
        $achievements = MasterData::where('type',18)->get();
        $milestones = MasterData::where('type',20)->orderBy('order_number', 'desc')->get();
        $arrayContent = json_decode($data->v_content);
        return view('livewire.about.about',compact('data', 'achievements', 'milestones', 'arrayContent'));
    }
    public function getFile(){
        $data = DB::table('master_data')->where('type',19)->get()->first();
        if($data&&$data->image){
            return Storage::download($data->image);
        }
    }
}
