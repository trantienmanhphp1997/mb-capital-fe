<?php

namespace App\Http\Livewire\About;

use Livewire\Component;

class Employee extends Component
{

    public $employee;

    public $activeTab;
    public function mount(){
        $this->activeTab = 1;
    }
    public function render()
    {
        $admins = \App\Models\Employee::whereNull('fund_id')
            ->where('type', 1)
            ->where(function($query){
                $query->whereNull('active');
                $query->orWhere('active', '!=', -1);
            })
            ->orderBy('priority', 'asc')->get();
        $controls = \App\Models\Employee::whereNull('fund_id')
            ->where('type', 2)
            ->where(function($query){
                $query->whereNull('active');
                $query->orWhere('active', '!=', -1);
            })
            ->orderBy('priority', 'asc')->get();
        $managers = \App\Models\Employee::whereNull('fund_id')
            ->where('type', 3)
            ->where(function($query){
                $query->whereNull('active');
                $query->orWhere('active', '!=', -1);
            })
            ->orderBy('priority', 'asc')->get();
        return view('livewire.about.employee', compact(
            'managers',
            'controls',
            'admins'
        ));
    }

    public function showInfo($id){
        $this->employee = \App\Models\Employee::findOrFail($id);
    }

    public function changeActiveTab($tab){
        $this->activeTab = $tab;
    }
}
