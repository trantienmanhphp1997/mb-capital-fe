<?php

namespace App\Http\Livewire\About;

use App\Http\Livewire\Base\BaseLive;
use App\Models\FundNews;
use Illuminate\Support\Facades\DB;

class Investor extends BaseLive
{
    public $pagination = 10;
    public $search;
    public $year;

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingYear()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = FundNews::query();
        $query->whereIn('type', [6])->where('fund_id',null)->where( DB::raw('YEAR(public_date)'), $this->year ?? date('Y'));
        if($this->search){
            $query->where('title_vi','like','%'. $this->search .'%');
        }
        $investors = $query->latest('public_date')->paginate($this->pagination);
        $investorRelations = DB::table('fund_news')->where('type',6)->groupBy(DB::raw('YEAR(public_date)'))->select(DB::raw('YEAR(public_date) as value'))->orderBy(DB::raw('YEAR(public_date)'),'desc')->get();
        return view('livewire.about.investor', compact(
            'investors','investorRelations'
        ));
    }
}
