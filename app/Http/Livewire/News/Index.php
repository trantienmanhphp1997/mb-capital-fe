<?php

namespace App\Http\Livewire\News;

use App\Models\Article;
use App\Models\News;
use Livewire\Component;

class Index extends Component
{
    public $pagination = 6;
    public $search = '';
    public $breakingNews;
    public function render()
    {
        $this->breakingNews = Article::where('category', 1)->first();
        if (!$this->breakingNews) {
            $this->breakingNews = Article::query()->orderBy('created_at', 'desc')->first();
        }
        $listNews = (isset($this->search))
            ? Article::where('name_vi', 'like', '%'. $this->search .'%')
                ->where('id', '!=', $this->breakingNews->id ?? 0)
                ->where('status', 1)
                ->where(function($query){
                    $query->whereNull('category');
                    $query->orWhere('category', '!=', -1);
                })
                ->where(function($query){
                    $query->where('date_submit', '<=' ,date('Y-m-d'));
                    $query->orWhereNull('date_submit');
                })
                ->latest()->get()
            : Article::where('id', '!=', $this->breakingNews->id ?? 0)
                ->where('status', 1)
                ->where(function($query){
                    $query->whereNull('category');
                    $query->orWhere('category', '!=', -1);
                })
                ->where(function($query){
                    $query->where('date_submit', '<=' ,date('Y-m-d'));
                    $query->orWhereNull('date_submit');
                })

                ->latest()->get();
        return view('livewire.news.index', compact('listNews'));
    }
}
