<?php

namespace App\Http\Livewire\News;

use App\Models\Article;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class Detail extends Component
{
    public $slug;
    public $news;
    public $moreNews;

    public function mount($slug)
    {
        $this->slug = $slug;
    }

    public function render()
    {
        $this->news = Article::where('slug_en', $this->slug)->orWhere('slug',$this->slug)->first();
        $this->moreNews = Article::where('id', '!=', $this->news->id ?? 0)->orderBy('created_at', 'desc')->take(4)->get();
        return view('livewire.news.detail', [
            'locale' => App::getLocale()
        ]);
    }
}
