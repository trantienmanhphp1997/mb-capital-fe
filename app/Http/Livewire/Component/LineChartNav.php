<?php

namespace App\Http\Livewire\Component;

use App\Models\Fund;
use App\Models\FundNav;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\Livewire;
use Log;

class LineChartNav extends Component
{
    protected $listeners = [
        'lineChartNavRequestupdateDataSets' => 'lineChartNavRequestupdateDataSets',
        'getChart',
    ];
    public $labels = [];
    protected $fundData = [];
    public $fundIds; // = [5,6];
    public $VNIndex = false;
    protected $dataSets = [];
    public $toDate;
    public $fromDate;
    public $timeSelect;
    public $background_colors = ["rgba(20, 29, 210, 0.1)", "rgba(189, 153, 118, 0.1)"];
    public $yellowColor = "#BD9976";
    public $blueColor = "#141ED1";
    public $fillStyleColors;
    public $borderColors;

    public function mount()
    {
        $this->fillStyleColors = [$this->yellowColor, $this->blueColor];
        $this->borderColors = [$this->yellowColor, $this->blueColor];
        $this->labels = Fund::whereIn('id', $this->fundIds)->pluck(App::getLocale() == 'en' ? 'fullname_en' : 'fullname', 'id')->toArray();
        if ($this->VNIndex) {
            $this->labels[0] = 'VNIndex';
        }

        //Mặc định sẽ lấy dữ liệu 1 tháng
        // $endTime = Carbon::now();
        // $startTime = $endTime->copy()->startOfYear();
        // if($this->fundIds[0] == 2){
        //     $startTime = Carbon::parse('1-2-2018');
        // }
        $this->timeSelect = 'all';
    }

    public function render()
    {

        $this->getChart();
        return view('livewire.component.line-chart-nav');
    }

    public function getChart()
    {
        if($this->dataSets == null) {
            $this->caculateDataSets(null, null);
        }
        $this->emit('showChart', json_encode($this->dataSets));
    }

    public function search()
    {
        $this->timeSelect = '';
        $startTime = Carbon::parse($this->fromDate);
        $endTime = Carbon::parse($this->toDate);
        $this->caculateDataSets($startTime, $endTime);
    }

    public function timeChart($timeRange)
    {
        $this->fromDate = null;
        $this->toDate = null;
        $this->timeSelect = $timeRange;
        $endTime = Carbon::now();
        switch ($this->timeSelect) {
            case "1w":
                $startTime = Carbon::now()->subDays(7);
                break;
            case "1m":
                $startTime = Carbon::now()->subDays(30);
                break;
            case "3m":
                $startTime = Carbon::now()->subDays(90);
                break;
            case "6m":
                $startTime = Carbon::now()->subDays(180);
                break;
            case "1Y":
                $startTime = Carbon::now()->subDays(365);
                break;
            case "YTD":
                $startTime = $endTime->copy()->startOfYear();
                break;
            case "all":
                $startTime = null;
                $endTime = null;
                break;
            default:
        }
        $this->caculateDataSets($startTime, $endTime);
    }

    private function caculateDataSets($startTime, $endTime)
    {
        if ($startTime == null) {
            $funds = Fund::query()->where('id', $this->fundIds)->get();
            foreach ($funds as $fund) {
                if ($fund->public_date){
                    $publicDate = Carbon::createFromFormat('d/m/Y', $fund->public_date);
                    if ($startTime == null || $startTime < $publicDate)
                        $startTime = $publicDate;
                }
            }
        }
        $query = FundNav::query()->where(function ($q) {
            $q->whereIn('fund_id', array_merge($this->fundIds, ($this->VNIndex ? [0, null] : [])));
            if ($this->VNIndex) {
                $q->orWhereRaw("type = 1");
            }
        });

        if (!empty($startTime)) {
            $query->whereDate('trading_session_time', '>=', $startTime->format('Y-m-d'));
        }
        if (!empty($endTime)) {
            $query->whereDate('trading_session_time', '<=', $endTime->format('Y-m-d'));
        }

        $this->fundData = $query->select('fund_id', 'amount', 'type', 'trading_session_time')
            ->orderBy('trading_session_time', 'asc')
            ->get()
            ->groupBy('trading_session_time');

        $this->fundData = $this->fundData
            // ->filter(fn ($item, $index) => $item->count() == count($this->labels))
            ->collapse()
            ->reduce(function($result, $item) {
                $itemData = new \stdClass();
                $itemData->x = date('Y-m-d', strtotime($item['trading_session_time']));
                $itemData->amount = $item['amount'];
                $itemData->change = 0;

                $key = $item['fund_id'] ?? 0;
                if (!empty($result)) {
                    $arr = $result[$key] ?? [];
                    $itemCount = count($arr);
                    if ($itemCount > 0) {
                        $firstItemData = $arr[0];
                        $itemData->change = $firstItemData->amount ?  round(($itemData->amount - $firstItemData->amount) * 100 / $firstItemData->amount, 2) : 0;
                    } else {
                        $itemData->change = 0;
                    }
                }

                $itemData->y = $itemData->change;

                $result[$key][] =  $itemData;
                return $result;
            });


        $this->dataSets = [];
        foreach ($this->fundIds as $key => $fundId) {
            $dataSet = new \stdClass();
            $dataSet->label = $this->labels[$fundId];
            $dataSet->backgroundColor = count($this->fundIds)>1 ? $this->background_colors[$key] : "rgba(20, 29, 210, 0.1)";
            $dataSet->borderColor = count($this->fundIds)>1 ? $this->borderColors[$key] : $this->blueColor;
            $dataSet->fill = true;
            $dataSet->data = isset($this->fundData[$fundId]) ? $this->fundData[$fundId] : null;
            $dataSet->fillStyle =  count($this->fundIds)>1 ? $this->fillStyleColors[$key] : $this->blueColor;
            $this->dataSets[] = $dataSet;
        }

        if($this->VNIndex && isset($this->fundData[0])) {
            $dataSet = new \stdClass();
            $dataSet->label = $this->labels[0];
            $dataSet->backgroundColor = 'rgba(189, 153, 118, 0.1)';
            $dataSet->borderColor = $this->yellowColor;
            $dataSet->fill = true;
            $dataSet->data = $this->fundData[0];
            $dataSet->fillStyle = $this->yellowColor;
            $dataSet->hidden = true;
            $this->dataSets[] = $dataSet;
        }

        $this->fromDate = $startTime ? $startTime->format('Y-m-d') : '';
        $this->toDate = $endTime ? $endTime->format('Y-m-d') : '';
    }

    // public function lineChartNavRequestupdateDataSets()
    // {
    //     //Mặc định sẽ lấy dữ liệu 1 tháng
    //     $endTime = Carbon::now();
    //     $startTime = Carbon::now()->subDays(30);
    //     $this->caculateDataSets($startTime, $endTime);
    // }
}
