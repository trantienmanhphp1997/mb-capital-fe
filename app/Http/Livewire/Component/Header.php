<?php

namespace App\Http\Livewire\Component;
use Livewire\Component;
use App\Models\Fund;

class Header extends Component {

    public $listFund;
    public $signUpLink;
    public function mount() {
        $this->listFund = Fund::query()->orderBy('priority')->get();
        $this->signUpLink = "https://online.mbcapital.com.vn/createaccount?language=".((config('app.locale')=='en')?"en":"vie");
    }

    public function render()
    {
        return view('livewire.component.header');
    }
}
