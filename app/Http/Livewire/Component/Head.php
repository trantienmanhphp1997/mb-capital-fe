<?php

namespace App\Http\Livewire\Component;

use Livewire\Component;
use App\Models\Menu;
use Route;

class Head extends Component
{
    public function render()
    {
        $name = Route::getCurrentRoute()->getName();
        $data = Menu::where('alias', $name)->first();
        return view('livewire.component.head',[
            'data' => $data,
        ]);
    }
}
