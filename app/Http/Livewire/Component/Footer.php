<?php

namespace App\Http\Livewire\Component;
use Livewire\Component;
use App\Models\MasterData;
use App\Enums\EMasterDataType;
use App\Models\Fund;


class Footer extends Component {

    public $listCompanyAffiliation;
    public $address;
    public $tel;
    public $fax;

    public function mount() {
        $this->listCompanyAffiliation = MasterData::query()->where('type', EMasterDataType::COMPANY_AFFILIATION)->orderBy('id')->get();
        $this->address = MasterData::query()->where('type', EMasterDataType::COMPANY_ADDRESS)->first();
        $this->tel = MasterData::query()->where('type', EMasterDataType::TELEPHONE)->first();
        $this->fax = MasterData::query()->where('type', EMasterDataType::FAX)->first();
    }

    public function render()
    {
        $listFund = Fund::query()->orderBy('priority')->get();
        return view('livewire.component.footer', ['listFund' => $listFund]);
    }
}