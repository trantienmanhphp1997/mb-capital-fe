<?php

namespace App\Http\Livewire\Component\Fund;
use Livewire\Component;
use App\Models\FundMaster;
use Illuminate\Support\Facades\App;
use App\Http\Livewire\Base\BaseLive;

class AccessLink extends BaseLive {

    public $type;
    public $fund_id;
    public function render() {
        $fundMaster = FundMaster::where('fund_id', $this->fund_id)
            ->where('type', $this->type)
            ->get();
        return view('livewire.component.fund.access-link', [
            'data' => $fundMaster
        ]);
    }

}
