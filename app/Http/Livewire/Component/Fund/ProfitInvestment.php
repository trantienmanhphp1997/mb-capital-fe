<?php

namespace App\Http\Livewire\Component\Fund;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Fund;
use App\Models\FundNav;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class ProfitInvestment extends BaseLive
{
    public $fund_id, $vnindex;
    public $nav_found;
    public $public_date;
    public $locale;

    public function mount($fund_id, $vnindex) {
        $this->locale = App::getLocale();
        $this->fund_id = $fund_id;
        $this->vnindex = $vnindex;
    }
    public function render() {
        $fund = Fund::where('id', $this->fund_id)->first();
        $this->public_date = ($this->locale == 'vi') ? $fund->public_date : $fund->public_date_en;
        $dataTable = [
            array(
                'name' => $fund->short_name,
                'data' => $this->getData($this->fund_id, false),
            ),
        ];
        if ($this->vnindex == true) {
            $dataTable[] = [
                'name' => 'VNINDEX',
                'data' => $this->getData(0, true),
            ];
        }
        return view('livewire.component.fund.profit-investment', [
            'dataTable' => $dataTable,
        ]);
    }
    public function getData($fund_id, $vnindex) {
        if (!$vnindex) {
            $type = 0;
            if ($fund_id == 2) {
                $this->nav_found = 9300; // mbvf do nav của mbvf không lấy từ ngày thành lập
            }else if ($fund_id == 3) {
                $this->nav_found = 11248; // mbbond do nav của mbbond k lấy từ ngày thành lập
            } else { //Trường hợp khác lấy từ dữ liệu NAV
                $fundNav = FundNav::where('fund_id', $fund_id)->where('type', $type)->orderBy('trading_session_time', 'asc')->first();
                $this->nav_found = data_get($fundNav, 'amount', 0);
            }

            $nav_now = FundNav::where('fund_id', $fund_id)->where('type', $type)->orderBy('trading_session_time', 'desc')->first();
            $nav_one_month = FundNav::where('fund_id', $fund_id)->where('type', $type)
                                    ->where('trading_session_time', '>=', now()->subDays(30)->format('Y-m-d'))
                                    ->orderBy('trading_session_time', 'asc')->first();
            $nav_three_month = FundNav::where('fund_id', $fund_id)->where('type', $type)
                                    ->where('trading_session_time', '>=', now()->subDays(90)->format('Y-m-d'))
                                    ->orderBy('trading_session_time', 'asc')->first();
            $nav_start_year = FundNav::where('fund_id', $fund_id)->where('type', $type)
                                    ->where('trading_session_time', '>=', Carbon::parse(now()->startOfYear()->format('Y-m-d')))
                                    ->orderBy('trading_session_time', 'asc')->first();
            $nav_twelve_month = FundNav::where('fund_id', $fund_id)->where('type', $type)
                                    ->where('trading_session_time', '>=', now()->subDays(365)->format('Y-m-d'))
                                    ->orderBy('trading_session_time', 'asc')->first();
        } else {
            $type = 1;
            $this->nav_found = 578.92; // vnindex
            $nav_now = FundNav::where('type', $type)->orderBy('trading_session_time', 'desc')->first();
            $nav_one_month = FundNav::where('type', $type)
                                    ->where('trading_session_time', '>=', now()->subDays(30)->format('Y-m-d'))
                                    ->orderBy('trading_session_time', 'asc')->first();
            $nav_three_month = FundNav::where('type', $type)
                                    ->where('trading_session_time', '>=', now()->subDays(90)->format('Y-m-d'))
                                    ->orderBy('trading_session_time', 'asc')->first();
            $nav_start_year = FundNav::where('type', $type)
                                    ->where('trading_session_time', '>=', Carbon::parse(now()->startOfYear()->format('Y-m-d')))
                                    ->orderBy('trading_session_time', 'asc')->first();
            $nav_twelve_month = FundNav::where('type', $type)
                                    ->where('trading_session_time', '>=', now()->subDays(365)->format('Y-m-d'))
                                    ->orderBy('trading_session_time', 'asc')->first();
        }
        // dd(round($nav_now->amount,2));
        return [
            'now' => isset($nav_now)?round($nav_now->amount,2):0,
            'one_month' => $nav_one_month?round($this->formCalculate(data_get($nav_now, 'amount', 0), data_get($nav_one_month, 'amount', 0)),2):'',
            'three_month' => round($this->formCalculate(data_get($nav_now, 'amount', 0), data_get($nav_three_month, 'amount', 0)),2),
            'start_year' => isset($nav_start_year->amount) ? round($this->formCalculate(data_get($nav_now, 'amount', 0), data_get($nav_start_year, 'amount', 0)),2) : null,
            'twelve_month' => round($this->formCalculate(data_get($nav_now, 'amount', 0), data_get($nav_twelve_month, 'amount', 0)),2),
            'found' => round($this->formCalculate(data_get($nav_now, 'amount', 0), $this->nav_found??0),2),
        ];
    }

    public function formCalculate($first, $second) {
        if ($first == 0 || $second == 0) return 0;
        return (($first/$second)-1) * 100;
    }
}
