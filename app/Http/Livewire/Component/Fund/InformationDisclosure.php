<?php

namespace App\Http\Livewire\Component\Fund;
use Livewire\Component;
use App\Models\Fund;
use App\Models\FundNews;
use App\Models\File;
use Illuminate\Support\Facades\App;
use App\Http\Livewire\Base\BaseLive;
use Carbon\Carbon;

class InformationDisclosure extends BaseLive {

    public $type;
    public $fund_id;
    public $show_filter = false;
    public $month;
    public $currentMonth;
    protected $listeners = [
        'changeMonthInfo' => 'setMonth',
    ];
    public function mount() {
        $this->currentMonth = Carbon::parse(Carbon::now())->format('m-Y');
    }
    public function render() {
        $fileList = FundNews::query();
        if ($this->show_filter) {
            if (empty($this->month)) {
                $fileList->where('created_at', '>=', Carbon::parse($this->month)->startOfMonth())
                ->where('created_at', '<=', Carbon::parse($this->month)->endOfMonth());
            }else {
                $fileList->where('created_at', '>=', $this->month->copy()->startOfMonth())
                ->where('created_at', '<=', $this->month->copy()->endOfMonth());
            }
        }
        $fileList = $fileList->whereIn('fund_id', $this->fund_id)
            ->where('type', $this->type)
            ->orderBy('public_date', 'desc')->paginate(10);
        return view('livewire.component.fund.information-disclosure', [
            'fileList' => $fileList
        ]);
    }
    public function setMonth($data) {
        $this->month = Carbon::parse(explode('-', $data)[1] . '-' . explode('-', $data)[0]);
    }

}
