<?php

namespace App\Http\Livewire\Component\Fund;
use Livewire\Component;
use App\Models\FundMaster;
use Illuminate\Support\Facades\App;
use App\Http\Livewire\Base\BaseLive;
use Browser;
class DistributionAgent extends BaseLive {

    public $type;
    public $fund_id;
    public function render() {
        $query = FundMaster::where('fund_id', $this->fund_id)
            ->where('type', $this->type);
        if(Browser::isDesktop()){
            $query->orderBy('order_number', "desc");
        }else{
            $query->orderBy('number_value', "ASC");
        }
        $fundMaster= $query->get();
        return view('livewire.component.fund.distribution-agent', [
            'data' => $fundMaster
        ]);
    }

}
