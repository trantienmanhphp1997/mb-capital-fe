<?php

namespace App\Http\Livewire\Component\Fund;
use Livewire\Component;
use App\Models\Fund;
use Illuminate\Support\Facades\App;
use App\Http\Livewire\Base\BaseLive;

class MaybeYouAreInterested extends BaseLive {

    public $fund_id;
    public $include;
    public $uy_thac = false;
    public $home_page = false;
    public function render() {
        $funds = Fund::query();
        if(!empty($this->include)){
            $funds->whereIn('id', $this->include);
        }
        if (!empty($this->fund_id)) {
           $funds->where('id', '!=', $this->fund_id);
        }
        $funds = $funds->orderBy('priority', 'asc')->get();
        return view('livewire.component.fund.maybe-you-are-interested', [
            'funds' => $funds
        ]);
    }

}
