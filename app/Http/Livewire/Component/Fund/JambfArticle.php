<?php

namespace App\Http\Livewire\Component\Fund;
use App\Models\FundNews;
use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;

class JambfArticle extends BaseLive
{
    public $fund_id;
    public $pagination = 3;
    public function render()
    {
        $data = $this->getDataJampf($this->fund_id);
        return view('livewire.component.fund.jambf-article', ['jambfs' => $data]);
    }

    public function getDataJampf($fund_id = null) {
        $data = [];
        if($fund_id){
            $data =FundNews::Where('fund_id',$fund_id)->paginate($this->pagination);
        }else{
            $data = FundNews::Where('type', '=', 5)->paginate($this->pagination);
        }
        return $data;
    }
}
