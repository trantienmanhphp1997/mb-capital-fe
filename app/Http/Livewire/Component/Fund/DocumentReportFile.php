<?php

namespace App\Http\Livewire\Component\Fund;
use Livewire\Component;
use App\Models\FundMaster;
use Illuminate\Support\Facades\App;
use App\Http\Livewire\Base\BaseLive;
use App\Models\File;
use App\Models\FundNews;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Exception;
class DocumentReportFile extends BaseLive {

    public $fund_id;
    public $month;
    public $file_type;
    public $fund_master_type;
    public $itemSelect;
    public $currentMonth;
    protected $listeners = [
        'changeMonthDocument' => 'setMonth',
    ];
    public function mount() {
        $file = FundNews::where('type', '7')->where('fund_id', $this->fund_id)->where('file_path', '!=', '')->orderBy('public_date', 'desc')->first();
        if ($file) {
            $this->currentMonth = reFormatDate($file->public_date, 'm-Y');
        } else {
            $this->currentMonth = Carbon::parse(Carbon::now())->format('m-Y');
        }
    }
    public function render() {
        if(empty($this->month)){
            if ($this->currentMonth) {
                $this->month = Carbon::parse(Carbon::create(substr($this->currentMonth,3,4), substr($this->currentMonth,0,2)))->format('Y-m');
            } else {
                $this->month = Carbon::parse(Carbon::now())->format('Y-m');
            }
        }

        $data = File::from('files')
            ->join('fund_master as fm', 'fm.id', 'model_id')
            ->where('model_name', FundMaster::class)
            ->where('fund_id', $this->fund_id)
            ->where('files.type', $this->file_type)
            ->where('fm.type', $this->fund_master_type)
            ->select('files.*');
        if ($this->fund_master_type == 11) {
            $data = $data
                ->orderBy('id', 'desc')
                ->first();
        } else {
            $data = $data->get();
        }
        return view('livewire.component.fund.document-report-file', [
            'data' => $data,
        ]);
    }
    public function setMonth($data) {
        $this->month = Carbon::parse(explode('-', $data)[1] . '-' . explode('-', $data)[0]);
    }

    public function downloadFile($file){
        try {
            return Storage::download($file->url, $file->file_name);
        } catch(Exception $e) {
            dump($e->getMessage());
        }
    }

    public function downloadFundDocumentation()
    {
        return $this->downloadFile(json_decode($this->itemSelect));
    }

    public function downloadInvestorReportFile() {
        $investor_report_file = FundNews::where('type', '7')->where('fund_id', $this->fund_id)
            ->where('public_date', '>=', Carbon::parse($this->month)->startOfMonth()->format('Y-m-d'))
            ->where('public_date', '<=', Carbon::parse($this->month)->endOfMonth()->format('Y-m-d'))
            ->orderBy('public_date', 'desc')->first();
        // try {
        if (isset($investor_report_file) && isset($investor_report_file->file_path)&&file_exists('./storage/'. $investor_report_file->file_path)){

$document_name= str_replace(array("/", "\\", ":", "*", "?", "«", "<", ">", "|"), "-", $investor_report_file->title_vi);


            return Storage::download($investor_report_file->file_path, $document_name);
        }
    }

}
