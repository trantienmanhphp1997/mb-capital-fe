<?php

namespace App\Http\Livewire\Component;
use Livewire\Component;
use App\Models\MasterData;
use App\Models\Advise;
use App\Enums\EMasterDataType;
use App\Models\Fund;
use Illuminate\Http\Request;


class AdviseRequest extends Component {

    public $nameCustomer;
    public $emailAndPhoneCustomer;
    public $contentToAdviseId;
    public $ip_client;

    public function render()
    {
        $listContentToAdvise = MasterData::query()
        ->where('type', EMasterDataType::CONTENT_TO_ADVISE)->orderBy('order_number')->get();
        return view('livewire.component.advise-request', ['listContentToAdvise' => $listContentToAdvise]);
    }

    public function resetFormAdviseRequest()
    {
        $this->reset(['nameCustomer', 'emailAndPhoneCustomer', 'contentToAdviseId']);
    }

    public function saveRequest(Request $request) {
        $this->ip_client = $request->ip();
        $this->validate([
            'nameCustomer' => 'required',
            'emailAndPhoneCustomer' => 'required',
            'contentToAdviseId' => 'required',
        ], [], [
            'nameCustomer' => __('home.advise.name'),
            'emailAndPhoneCustomer' => __('home.advise.email_or_phone'),
            'contentToAdviseId' => __('home.advise.content'),
        ]);
        $adviseContent = MasterData::query()
        ->where('type', EMasterDataType::CONTENT_TO_ADVISE)->where('id', $this->contentToAdviseId)->first();
        $request_advise = new Advise();
        $request_advise->name = $this->nameCustomer;
        $request_advise->email_phone = $this->emailAndPhoneCustomer;
        $request_advise->advise_content = $adviseContent->v_content ?? '';
        $request_advise->IP = $this->ip_client;
        $request_advise->save();
        $this->resetFormAdviseRequest();
    }
}