<?php

namespace App\Http\Livewire\Investment;
use App\Enums\EMasterDataType;
use Livewire\Component;
use App\Models\MasterData;

class Index extends Component
{
    public function render()
    {
        $first_item = MasterData::Where('type', EMasterDataType::INVESTMENT_FUND_BASIC)->orderBy('order_number')->first();
        $mid_item = MasterData::Where('type', EMasterDataType::INVESTMENT_BASIC)->orderBy('order_number')->first();
        $last_item = MasterData::Where('type', EMasterDataType::INVESTMENT_TERM)->orderBy('order_number')->first();
        return view('livewire.investment.index', compact('first_item', 'mid_item', 'last_item'));
    }
}
