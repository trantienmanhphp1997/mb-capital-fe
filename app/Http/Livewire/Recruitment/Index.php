<?php

namespace App\Http\Livewire\Recruitment;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Recruitment;
use App\Http\Livewire\Base\BaseLive;
use App\Models\MasterData;
use DB;
class Index extends BaseLive
{
    public $showId;
    public $content;
    public $title;
    public function render(){
        $envoriment = MasterData::where('type',10)->get()->first();
        $social = MasterData::where('type',9)->get()->first();
        return view('livewire.recruitment.index',['envoriment' => $envoriment, 'social'=>$social]);
    }
    public function show($id){
        $this->showId = $id;
        $recruitment = Recruitment::find($id);
        $this->content = $recruitment->conttent;
        $this->title = $recruitment->title;
    }
}
