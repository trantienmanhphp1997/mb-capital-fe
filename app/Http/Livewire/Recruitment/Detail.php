<?php

namespace App\Http\Livewire\Recruitment;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Recruitment;
use App\Http\Livewire\Base\BaseLive;
use App\Models\MasterData;
use DB;

class Detail extends BaseLive
{
    public $recruitment;
    public $slug_detail;
    public function render()
    {
        $this->recruitment = $this->getDetailData();
        return view('livewire.recruitment.detail');
    }

    public function getDetailData() 
    {
        return Recruitment::where('slug', $this->slug_detail)->first();
    }
}
