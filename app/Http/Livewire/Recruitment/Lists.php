<?php

namespace App\Http\Livewire\Recruitment;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Recruitment;
use App\Http\Livewire\Base\BaseLive;
use DB;
class Lists extends BaseLive
{
    public $showId;
    public $content;
    public $title;
    public function render(){
        $query = Recruitment::query();
        $data = $query->orderBy('recruitments.id')->paginate(10);
        return view('livewire.recruitment.lists',['data'=>$data]);
    }
    public function show($id){
        $this->showId = $id;
        $recruitment = Recruitment::find($id);
        $this->content = $recruitment->conttent;
        $this->title = $recruitment->title;
    }
}
