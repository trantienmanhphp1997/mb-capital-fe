<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Fund;

class FundsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detail($slug)
    {
        if (!empty(request()->get('page'))) {
            return redirect()->route('page.fund.detail', ['slug' => $slug]);
        }
        if (strpos($slug, 'mbbond') || strpos($slug, 'mb-bond')) {
            $slug = 'MBBOND';
        }
        $fund = Fund::where('slug', $slug)->first();
        if (!empty($fund)) {
            return view('pages.funds.detail', [
                'type' => $fund->type,
                'fund_id' => $fund->id,
                'parent_id' => $fund->parent_id,
                'fund_name' => $fund->fullname,
            ]);
        } else {
            return redirect()->route('home');
        }
    }

    public function index()
    {
        return view('pages.funds.index');
    }
}
