<?php

namespace App\Http\Controllers\About;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show info MBCapital.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $activeTab = $request->get('activeTab');
        return view('pages.about.index', ['activeTab' => $activeTab]);
    }
}
