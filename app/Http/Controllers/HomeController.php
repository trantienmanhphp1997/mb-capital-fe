<?php

namespace App\Http\Controllers;

use App\Models\LogAccess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Browser;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $value = $this->getCookie($request);
        Log::info('HomeController has cookie log_user: '.$value);
        if (!$value) {
            $this->setCookie($request);
        }
        return view('home', ['ip_client' => request()->ip()]);
    }
    public function setCookie()
    {
        $minutes = 365 * 24 * 60;
        $uid = Str::uuid();
        Log::info('HomeController setCookie: ' . $uid);
        Cookie::queue(Cookie::make('log_user', $uid, $minutes, null, null, false, false));
    }
    public function getCookie()
    {
        return request()->cookie('log_user');
    }
}
