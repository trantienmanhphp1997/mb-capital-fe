<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrustController extends Controller{
    public function index(){
        return view('pages.trust.index');
    }
    public function personal(){
        return view('pages.trust.personal');
    }
}
