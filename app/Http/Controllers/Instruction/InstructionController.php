<?php

namespace App\Http\Controllers\Instruction;

use App\Http\Controllers\Controller;
use App\Models\News;

class InstructionController extends Controller
{
    /**
     * Show the application faq.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pages.instruction.index');
    }
}
