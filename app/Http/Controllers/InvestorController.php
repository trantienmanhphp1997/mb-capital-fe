<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InvestorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($slug = null){
        return view('investor', ['slug' => $slug]);
    }

    public function new() {
        return view('pages.investor.new-investor');
    }

    public function expert() {
        return view('pages.investor.professional-investor');
    }
}
