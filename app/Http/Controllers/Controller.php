<?php

namespace App\Http\Controllers;

use App\Models\LogAccess;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Browser;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $value = request()->cookie('log_user');
        $log = new LogAccess();
        $log->ip_address = request()->ip();
        $log->url_previous = url()->previous();
        $log->url_current = url()->full();
        $log->user_agent = $_SERVER['HTTP_USER_AGENT'];
        $log->browser = Browser::browserName();
        $device = '';
        if (Browser::isMobile()) {
            $device = 'mobile';
        } else if (Browser::isTablet()) {
            $device = 'tablet';
        } else if (Browser::isDesktop()) {
            $device = 'desktop';
        }
        $log->device = $device;
        $log->cookies = $value ? 'log_user' : '';
        $log->save();
    }
}
