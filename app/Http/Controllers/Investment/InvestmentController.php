<?php

namespace App\Http\Controllers\Investment;

use App\Http\Controllers\Controller;

class InvestmentController extends Controller
{
    /**
     * Show the application faq.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pages.invest.index');
    }
}
