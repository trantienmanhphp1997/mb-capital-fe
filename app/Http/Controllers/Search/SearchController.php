<?php

namespace App\Http\Controllers\Search;

use App\Http\Controllers\Controller;
use App\Models\News;

class SearchController extends Controller
{
    /**
     * Show the application search.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pages.search.index');
    }
}
