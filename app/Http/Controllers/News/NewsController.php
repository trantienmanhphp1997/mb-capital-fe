<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Models\Article;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application news.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function news()
    {
        return view('pages.news.index');
    }

    /**
     * Show the detail news.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function details($slug)
    {
        $news = Article::where('slug_en', $slug)->orWhere('slug',$slug)->first();
        if($news){
            return view('pages.news.details', ['slug' => $slug]);
        }

        return redirect()->route('page.news.index');
    }
}
