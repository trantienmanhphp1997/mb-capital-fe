<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LangController extends Controller
{
    private $langActive = [
        'vi',
        'en',
    ];

    public function changeLang($lang)
    {
        if (in_array($lang, $this->langActive)) {
            session(['locale' => $lang]);
            return redirect()->back();
        }
    }

}
