<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class FunExpectedProfit extends Model {
    public $month_vi = "tháng";
    public $month_en = "months";
    protected $table = 'fun_expected_profit';
    public function getPercentViewAttribute()
    {
        return $this->attributes['percent_view'] ? $this->attributes['percent_view'] : $this->attributes['percent']  ;
    }
    public function getPeriodAttribute()
    {
        

        if (App::getLocale() == 'en') {
            return ($this->attributes['period_name_en'] ? $this->attributes['period_name_en'] : $this->attributes['period_name']);
        }
          return ($this->attributes['period_name'] ? $this->attributes['period_name'] : $this->attributes['period_name_en']);    }
}
