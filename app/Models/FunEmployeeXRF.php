<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class FunEmployeeXRF extends Model
{
    use HasFactory;
    protected $table = 'fun_employee_xrf';

    public function getRoleNameAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['role_name_en'] ?? $this->attributes['role_name']);
        }
        return ($this->attributes['role_name'] ?? $this->attributes['role_name_en']);
    }

    public function employee(){
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}
