<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;


class MasterData extends Model {
    protected $table = 'master_data';

    public function getVValueAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['v_value_en'] ?? $this->attributes['v_value']);
        }

        return ($this->attributes['v_value'] ?? $this->attributes['v_value_en']);
    }

    public function getVContentAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['v_content_en'] ?? $this->attributes['v_content']);
        }

        return ($this->attributes['v_content'] ?? $this->attributes['v_content_en']);
    }
    public function getNoteAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['note_en'] ?? $this->attributes['note']);
        }

        return ($this->attributes['note'] ?? $this->attributes['note_en']);
    }
    
}
