<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\App;

class Survey extends Model
{
    use HasFactory;
    protected $table='survey';
    public function detail()
    {
        return $this->hasMany(SurveyDetail::class, 'survey_id');
    }
    public function getValueAttribute($columnName)
    {
        if (App::getLocale() == 'en') {
            if (strlen($this->attributes[$columnName . '_en']) == 0) {
                return $this->attributes[$columnName];
            } else {
                return $this->attributes[$columnName . '_en'];
            }
        }
    }
    public function getNameAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['name_en'] ?? $this->attributes['name']);
        }

        return ($this->attributes['name'] ?? $this->attributes['name_en']);
    }
}
