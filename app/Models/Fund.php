<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Browser;
use App\Scopes\ActiveScope;

class Fund extends Model
{

    use SoftDeletes;

    protected $table = 'fund';
    protected $fillable = ['shortname', 'shortname_en', 'fullname', 'fullname_en', 'description', 'description_en', 'content', 'content_en', 'created_by', 'created_at', 'updated_at', 'type', 'current_nav', 'growth', 'deleted_at', 'slug', 'slug_en', 'fun_code', 'interest', 'parent_id', 'priority', 'shortname2', 'shortname2_en', 'fullname2', 'fullname2_en'];

    protected static function booted(){
        static::addGlobalScope(new ActiveScope);
    }
    public function getShotnameAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['shortname_en'] ?? $this->attributes['shortname']);
        }

        return ($this->attributes['shortname'] ?? $this->attributes['shortname_en']);
    }
    public function getShortname2Attribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['shortname2_en'] ?? $this->attributes['shortname2']);
        }

        return ($this->attributes['shortname2'] ?? $this->attributes['shortname2_en']);
    }

    public function getFullnameAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['fullname_en'] ?? $this->attributes['fullname']);
        }

        return ($this->attributes['fullname'] ?? $this->attributes['fullname_en']);
    }
    public function getFullname2Attribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['fullname2_en'] ?? $this->attributes['fullname2']);
        }

        return ($this->attributes['fullname2'] ?? $this->attributes['fullname2_en']);
    }

    public function getDescriptionAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['description_en'] ?? $this->attributes['description']);
        }

        return ($this->attributes['description'] ?? $this->attributes['description_en']);
    }

    public function reason()
    {
        return $this->hasMany(FundMaster::class, 'fund_id')->where('type', 1);
    }

    public function childFund()
    {
        return $this->hasMany(Fund::class, 'parent_id');
    }

    public function typeInvestment()
    {
        return $this->hasMany(FundMaster::class, 'fund_id')->where('type', 7);
    }

    public function priceService()
    {
        return $this->hasOne(FundMaster::class, 'fund_id')->where('type', 2);
    }

    public function transactionTime()
    {
        return $this->hasOne(FundMaster::class, 'fund_id')->where('type', 4);
    }

    public function profitInvestment()
    {
        return $this->hasOne(FundMaster::class, 'fund_id')->where('type', 3);
    }

    public function allocate()
    {
        return $this->hasOne(FundMaster::class, 'fund_id')->where('type', 8);
    }

    public function topStock()
    {
        return $this->hasOne(FundMaster::class, 'fund_id')->where('type', 9);
    }

    public function reportInvestor()
    {
        return $this->hasOne(FundMaster::class, 'fund_id')->where('type', 11);
    }

    public function reportNAV()
    {
        return $this->hasMany(FundNews::class, 'fund_id')->where('type', 1);
    }

    public function reportActivity()
    {
        return $this->hasMany(FundNews::class, 'fund_id')->where('type', 2);
    }

    public function investorCongress()
    {
        return $this->hasMany(FundNews::class, 'fund_id')->where('type', 3);
    }

    public function otherInfo()
    {
        return $this->hasMany(FundNews::class, 'fund_id')->where('type', 4);
    }

    public function serviceSupplier()
    {
        return $this->hasOne(FundMaster::class, 'fund_id')->where('type', 6);
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'fun_employee_xrf', 'fund_id', 'employee_id')->withPivot('role_name');
    }

    public function getShortnameAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['shortname_en'] ?? $this->attributes['shortname']);
        }

        return ($this->attributes['shortname'] ?? $this->attributes['shortname_en']);
    }

    public function getContentAttribute()
    {
        if(Browser::isDesktop())
        {

            if (App::getLocale() == 'en') {
                return ($this->attributes['content_en'] ?? $this->attributes['content']);
            }

            return ($this->attributes['content'] ?? $this->attributes['content_en']);
        }
        else {
            if (App::getLocale() == 'en') {
                return ($this->attributes['content_mobile_en'] ?? $this->attributes['content_mobile'] ?? $this->attributes['content_en' ?? $this->attributes['content']]);
            }

            return ($this->attributes['content_mobile'] ?? $this->attributes['content_mobile_en'] ?? $this->attributes['content'] ?? $this->attributes['content_en']);
        }
    }

    public function getNameRetireFund()
    {
        if(App::getLocale() == 'en')
        {
            return 'An Thinh Pensions' ?? $this->attributes['shortname'];
        }

        return ($this->attributes['shortname'] ?? 'An Thinh Pensions');
    }
}
