<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class SurveyDetail extends Model
{
    use HasFactory;
    protected $table = 'survey_detail';
    public function getNameAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['name_en'] ?? $this->attributes['name']);
        }

        return ($this->attributes['name'] ?? $this->attributes['name_en']);
    }
}
