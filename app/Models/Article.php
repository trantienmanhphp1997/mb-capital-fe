<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Article extends Model {
    use HasFactory;
    protected $table = 'article';

    public function getNameViAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['name_en'] ?? $this->attributes['name_vi']);
        }

        return ($this->attributes['name_vi'] ?? $this->attributes['name_en']);
    }

    public function getIntroViAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['intro_en'] ?? $this->attributes['intro_vi']);
        }

        return ($this->attributes['intro_vi'] ?? $this->attributes['intro_en']);
    }

    public function getMetaDesViAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['meta_des_en'] ?? $this->attributes['meta_des_vi']);
        }

        return ($this->attributes['meta_des_vi'] ?? $this->attributes['meta_des_en']);
    }

    public function getContentViAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['content_en'] ?? $this->attributes['content_vi']);
        }

        return ($this->attributes['content_vi'] ?? $this->attributes['content_en']);
    }

    public function getMetaTitleViAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['meta_title_en'] ?? $this->attributes['meta_title_vi']);
        }

        return ($this->attributes['meta_title_vi'] ?? $this->attributes['meta_title_en']);
    }

    public function getSlugAttribute()
    {
        if (App::getLocale() == 'en') {
            if(strlen($this->attributes['slug_en']) == 0) {
                return $this->attributes['slug'];
            }
            else {
                return $this->attributes['slug_en'];
            }
        }

        if(strlen($this->attributes['slug']) == 0) {
            return $this->attributes['slug_en'];
        }
        else {
            return $this->attributes['slug'];
        }
    }
}
