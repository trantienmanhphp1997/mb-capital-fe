<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model {
    use SoftDeletes;

    protected $table = 'faq';

    public function getQuestionAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['question_en'] ?? $this->attributes['question']);
        }

        return ($this->attributes['question'] ?? $this->attributes['question_en']);
    }

    public function getAnswerAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['answer_en'] ?? $this->attributes['answer']);
        }

        return ($this->attributes['answer'] ?? $this->attributes['answer_en']);
    }

    public function getContentAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['content_en'] ?? $this->attributes['content']);
        }

        return ($this->attributes['content'] ?? $this->attributes['content_en']);
    }
}