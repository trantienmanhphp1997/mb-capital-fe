<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SurveyResponse extends Model
{
    use HasFactory;
    protected $table='survey_response';
    protected $fillable = ['ip_address','sum_point','rate_level_risk','request_date','device','browser','save_time'];
}
