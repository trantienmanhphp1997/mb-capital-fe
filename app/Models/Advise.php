<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advise extends Model {
	use SoftDeletes;
    protected $table = 'advise_list';
}