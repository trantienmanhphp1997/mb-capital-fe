<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Menu extends Model
{
    use HasFactory;
    protected $table = "menu";
    public $autoincrement = true;
    protected $guarded=['*'];
    protected $fillable = [
        'name',
        'code',
        'permission_name',
        'alias',
        'note',
        'admin_id',
        'meta',
        'meta_en',
        'title',
        'title_en',
        'description',
        'description_en',
        'keywords',
        'keywords_en',
    ];

    public function getMetaAttribute() {
        if (App::getLocale() == 'en') {
            return ($this->attributes['meta_en'] ?? $this->attributes['meta']);
        }

        return ($this->attributes['meta'] ?? $this->attributes['meta_en']);
    }
    
    public function getTitleAttribute() {
        if (App::getLocale() == 'en') {
            return ($this->attributes['title_en'] ?? $this->attributes['title']);
        }

        return ($this->attributes['title'] ?? $this->attributes['title_en']);
    }

    public function getDescriptionAttribute() {
        if (App::getLocale() == 'en') {
            return ($this->attributes['description_en'] ?? $this->attributes['description']);
        }

        return ($this->attributes['description'] ?? $this->attributes['description_en']);
    }

    public function getKeywordsAttribute() {
        if (App::getLocale() == 'en') {
            return ($this->attributes['keywords_en'] ?? $this->attributes['keywords']);
        }

        return ($this->attributes['keywords'] ?? $this->attributes['keywords_en']);
    }
}
