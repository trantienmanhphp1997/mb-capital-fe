<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\SoftDeletes;


class Guideline extends Model {
    use SoftDeletes;
    protected $table = 'guideline';

    public function getNameAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['name_en'] ?? $this->attributes['name_vi']);
        }

        return ($this->attributes['name'] ?? $this->attributes['name_en']);
    }


    public function guidelineDetail() {
        return $this->hasMany('App\Models\GuidelineDetail');
    }
}
