<?php

namespace App\Models;

use Browser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\SoftDeletes;

class GuidelineDetail extends Model
{
    use SoftDeletes;
    protected $table = 'guideline_detail';

    public function getNameViAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['name_en'] ?? $this->attributes['name']);
        }

        return ($this->attributes['name'] ?? $this->attributes['name_en']);
    }

    public function getContentAttribute()
    {
        if(Browser::isDesktop()) {
            if (App::getLocale() == 'en') {
                return ($this->attributes['content_en'] ?? $this->attributes['content']);
            }

            return ($this->attributes['content'] ?? $this->attributes['content_en']);
        } else {
            if (App::getLocale() == 'en') {
                return ($this->attributes['content_mobile_en'] ?? $this->attributes['content_mobile'] ?? $this->attributes['content_en' ?? $this->attributes['content']]);
            }

            return ($this->attributes['content_mobile'] ?? $this->attributes['content_mobile_en'] ?? $this->attributes['content'] ?? $this->attributes['content_en']);
        }
    }

    public function guideline()
    {
        return $this->belongsTo('Guideline');
    }
}
