<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\App;
class Recruitment extends Model
{
    use HasFactory;
    protected $fillable = [
        'title', 'content','url','title_en', 'content_en', 'date_submit', 'file_path', 'position', 'job', 'skill', 'language', 'position_en', 'job_en', 'skill_en', 'language_en', 'slug', 'slug_en','meta_title', 'meta_title_en', 'meta_des', 'meta_des_en'
    ];

    public function getValueAttribute($columnName)
    {
        if (App::getLocale() == 'en') {
            if(strlen($this->attributes[$columnName.'_en']) == 0) {
                return $this->attributes[$columnName];
            }
            else {
                return $this->attributes[$columnName.'_en'];
            }
        }
        if(strlen($this->attributes[$columnName]) == 0) {
            return $this->attributes[$columnName.'_en'];
        }
        else {
            return $this->attributes[$columnName];
        }
    }
}
