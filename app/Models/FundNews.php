<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class FundNews extends Model {
    use SoftDeletes;
    protected $table = 'fund_news';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title_vi',
        'content_vi',
        'title_en',
        'content_en',
        'type',
        
    ];
    public function getContentViAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['content_en'] ?? $this->attributes['content_vi']);
        }

        return ($this->attributes['content_vi'] ?? $this->attributes['content_en']);
    }

    public function getTitleViAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['title_en'] ?? $this->attributes['title_vi']);
        }

        return ($this->attributes['title_vi'] ?? $this->attributes['title_en']);
    }
}
