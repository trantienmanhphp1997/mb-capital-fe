<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class File extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table='files';
    protected $fillable=['url','file_name','model_name','size_file','model_id','type', 'admin_id'];

    public function getNoteAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['note_en'] ?? $this->attributes['note']);
        }
        return ($this->attributes['note'] ?? $this->attributes['note_en']);
    }
}
