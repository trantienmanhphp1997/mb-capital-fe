<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\SoftDeletes;
class FundNav extends Model {
    use SoftDeletes;
    protected $table = 'fund_nav';
}