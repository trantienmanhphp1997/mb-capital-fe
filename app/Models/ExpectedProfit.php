<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class ExpectedProfit extends Model {
    public $month_vi = "tháng";
    public $month_en = "months";
    protected $table = 'expected_profit';

    //  public function getPeriodNameViAttribute()
    // {
    //     if (App::getLocale() == 'en') {
    //         return ($this->attributes['period_name_en'] ?? $this->attributes['period_name_vi']);
    //     }

    //     return ($this->attributes['period_name_vi'] ?? $this->attributes['period_name_en']);
    // }

    public function getPeriodAttribute()
    {
        
        if (App::getLocale() == 'en') {
            return ($this->attributes['period_name_en'] ?? $this->attributes['period_name']);
        }

        return ($this->attributes['period_name'] ?? $this->attributes['period_name_en']);
        // if (App::getLocale() == 'en') {

        //     if($this->attributes['period'] <= 9)
        //     {
        //         if($this->attributes['period'] == 1)
        //         {
        //             return '0'.$this->attributes['period'].' '.'month';
        //         }
        //         return '0'.$this->attributes['period'].' '.$this->month_en;
        //     }
        //     return ($this->attributes['period'].' '.$this->month_en);
        // }
        // else{
        //     if($this->attributes['period'] <= 9)
        //     {
        //         return '0'.$this->attributes['period'].' '.$this->month_vi;
        //     }
        //     return ($this->attributes['period'].' '.$this->month_vi);
        // }

        // return ($this->attributes['period'].' '.$this->month_en ?? $this->attributes['period'].' '.$this->month_vi);
    }
}
