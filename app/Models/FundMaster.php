<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Browser;

class FundMaster extends Model
{
    use SoftDeletes;
    protected $table = 'fund_master';
    protected $fillable = ['fund_id', 'title', 'title_en', 'content', 'content_en', 'type', 'img', 'url', 'deleted_at', 'created_at', 'updated_at', 'v_key', 'order_number', 'number_value', 'note', 'note_en', 'v_key_en'];

    public function getTitleAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['title_en'] ?? $this->attributes['title']);
        }

        return ($this->attributes['title'] ?? $this->attributes['title_en'] ?? '');
    }

    public function getContentAttribute()
    {
        if(Browser::isDesktop())
        {
            if (App::getLocale() == 'en') {
                return ($this->attributes['content_en'] ?? $this->attributes['content']);
            }

            return ($this->attributes['content'] ?? $this->attributes['content_en']);
        }
        else {
            if (App::getLocale() == 'en') {
                return ($this->attributes['content_mobile_en'] ?? $this->attributes['content_mobile'] ?? $this->attributes['content_en' ?? $this->attributes['content']]);
            }

            return ($this->attributes['content_mobile'] ?? $this->attributes['content_mobile_en'] ?? $this->attributes['content'] ?? $this->attributes['content_en']);
        }
    }

    public function files()
    {
        return $this->hasMany(File::class, 'model_id')->where('model_name', 'App\\Models\\FundMaster');
    }

    public function getNoteAttribute()
    {

        if(Browser::isDesktop())
        {
            if (App::getLocale() == 'en') {
                return ($this->attributes['note_en'] ?? $this->attributes['note']);
            }

            return ($this->attributes['note'] ?? $this->attributes['note_en']);
        }
        else {
            if (App::getLocale() == 'en') {
                return ($this->attributes['content_mobile_en'] ?? $this->attributes['content_mobile'] ?? $this->attributes['note_en' ?? $this->attributes['note']]);
            }

            return ($this->attributes['content_mobile'] ?? $this->attributes['content_mobile_en'] ?? $this->attributes['note'] ?? $this->attributes['note_en']);
        }
    }

    public function getVKeyAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['v_key_en'] ?? $this->attributes['v_key']);
        }

        return ($this->attributes['v_key'] ?? $this->attributes['v_key_en']);
    }
}
