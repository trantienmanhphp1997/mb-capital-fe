<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model {
    use SoftDeletes;
    protected $table = 'news';

    public function getContentAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['content_en'] ?? $this->attributes['content']);
        }

        return ($this->attributes['content'] ?? $this->attributes['content_en']);
    }

    public function getTitleAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['title_en'] ?? $this->attributes['title']);
        }

        return ($this->attributes['title'] ?? $this->attributes['title_en']);
    }

    public function getSlugAttribute()
    {
        if (App::getLocale() == 'en') {
            return ($this->attributes['slug_en'] ?? $this->attributes['slug']);
        }

        return ($this->attributes['slug'] ?? $this->attributes['slug_en']);
    }
}
