<div class="paginate text-center mb-5">
    @if ($paginator->hasPages())
    <nav class="mb-pagination" aria-label="Page navigation example">
        <ul class="pagination mb-0 justify-content-center d-flex">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
            <li class="page-item disabled" id="previous1" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <a class="btn-prev" aria-hidden="true">
                    <span aria-hidden="true"><i class="bi bi-chevron-left"></i></span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
            @else
            <li class="page-item" id="previous2">
                <a style="cursor: pointer" class="btn-prev active" wire:click="previousPage"
                    wire:loading.attr="disabled" rel="prev" aria-label="@lang('pagination.previous')"
                    href="javascript:void(0)">
                    <span aria-hidden="true"><i class="bi bi-chevron-left"></i></span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
            @endif
            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
            <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
            @foreach ($element as $page => $url)
            @if ($page == $paginator->currentPage())
            <li class="page-item active" wire:key="paginator-page-{{ $page }}" aria-current="page" id="page-{{ $page }}"><a
                    class="page-link">{{ $page }}</a></li>
            @else
            <li class="page-item" wire:key="paginator-page-{{ $page }}" id="page-{{ $page }}">
                <a class="page-link" wire:click="gotoPage({{ $page }})" href="javascript:void(0)">
                    {{ $page }}
                </a>
            </li>
            @endif
            @endforeach
            @endif
            @endforeach
            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
            <li class="page-item" id="next1">
                <a class="btn-next active" wire:click="nextPage" wire:loading.attr="disabled"
                    aria-label="@lang('pagination.next')" href="javascript:void(0)">
                    <span aria-hidden="true"><i class="bi bi-chevron-right"></i></span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
            @else
            <li class="page-item" aria-disabled="true" aria-label="@lang('pagination.next')" id="next2">
                <a class="btn-next">
                    <span aria-hidden="true"><i class="bi bi-chevron-right"></i></span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
            @endif
        </ul>
    </nav>
    @endif
</div>
