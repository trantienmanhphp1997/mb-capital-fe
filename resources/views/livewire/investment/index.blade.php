<div>
    <!-- =======  Banner ======= -->
    <section id="banner-investment" class="d-flex align-items-center bg-guideline">
        <div class="container" data-aos="zoom-out" data-aos-delay="100">
            <div class="row mb-md-5">
                <div class="col-md-12">
                    <h1 class="title-news  color-w fw-regular mb-md-5">{{__('investment.investment_knowledge')}}</h1>
                </div>
            </div>
        </div>
    </section>
    <main id="main">
        <div class="container page-mbbond">
            <div class="tab-bond">
                <ul class="nav nav-pills mb-3 d-flex" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active w-100 tab" id="pills-mid-tab" data-bs-toggle="pill" data-bs-target="#pills-mid"
                                type="button" role="tab" aria-controls="pills-mid" aria-selected="false">{{ data_get($mid_item, 'v_value', '') }}</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100 tab" id="pills-first-tab" data-bs-toggle="pill" data-bs-target="#pills-first"
                                type="button" role="tab" aria-controls="pills-first" aria-selected="true">{{ data_get($first_item, 'v_value', '') }}</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100 tab" id="pills-last-tab" data-bs-toggle="pill" data-bs-target="#pills-last"
                                type="button" role="tab" aria-controls="pills-last" aria-selected="false">{{ data_get($last_item, 'v_value', '') }}</button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade" id="pills-first" role="tabpanel" aria-labelledby="pills-first-tab">
                <div class="container">
                    {!! sanitizationText(data_get($first_item, 'note', '')) !!}
                </div>
            </div>
            <div class="tab-pane fade show active"  id="pills-mid" role="tabpanel" aria-labelledby="pills-mid-tab">
                <div class="container">
                    {!! sanitizationText(data_get($mid_item, 'note', '')) !!}
                </div>
            </div>
            <div class="tab-pane fade" id="pills-last" role="tabpanel" aria-labelledby="pills-last-tab">
                <div class="container">
                    {!! sanitizationText(data_get($last_item, 'note', '')) !!}
                </div>
            </div>
        </div>
        @livewire('component.advise-request', ['ip_client' => Request::ip()])
    </main>
</div>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $( ".tab" ).click(function() {
            window.scrollTo(0, 0);
            });
        });
    </script>
