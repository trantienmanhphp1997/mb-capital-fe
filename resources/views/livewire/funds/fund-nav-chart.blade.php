<div style="border-radius: 10px; background-color: rgba(255, 255, 255, 0.1); padding:20px; margin-bottom: 60px;">
    @if($fundNavs->count()==2)
        <div class="chart" style="position: relative; width: 90%; margin-left: auto;margin-right: auto;">
    @else
        <div class="chart" style="position: relative; width: 100%;">
    @endif
        <div id="fund-nav-chart-legend-container" class="d-flex justify-content-center"></div>
        <canvas id="fund-nav-chart"></canvas>
    </div>
    <hr>
    <div class="d-flex justify-content-between">
        <a href="{{!empty($urlDieuLe)?asset('storage/'.$urlDieuLe):'#'}}" class="text-white fw-regular" download="{{$fileNameDieuLe??''}}">{{__('fund.fund_nav_chart.fund_charter')}}<i class="bi bi-download ml-2"></i></a>
        <a href="{{!empty($urlBaoCao)?asset('storage/'.$urlBaoCao):'#'}}" class="text-white fw-regular" download="{{$fileNameBaoCao??''}}">{{__('fund.fund_nav_chart.prospectus')}}<i class="bi bi-download ml-2"></i></a>
        <a href="{{!empty($urlThongTin)?asset('storage/'.$urlThongTin):'#'}}" class="text-white fw-regular" download="{{$fileNameThongTin??''}}">{{__('fund.fund_nav_chart.fund_info')}}<i class="bi bi-download ml-2"></i></a>
    </div>
    <script type="text/javascript">
        window.addEventListener('load', function() {
            let fundNavs = JSON.parse(`{!! $fundNavs->toJson() !!}`);
            let datasets = [];

            for (const key in fundNavs) {
                datasets.push({
                    label: key,
                    data: fundNavs[key]
                });
            }
            if (datasets.length == 1) {
                datasets[0].label = '';
            }
            let ctx = document.getElementById('fund-nav-chart').getContext('2d');
            let colorList = ['#ffffff', '#4285f4', '#db4437', '#0f9d58', '#ffcd40'];
            const chart = new Chart(ctx, {
                type: 'line',
                data: {
                    datasets: datasets,
                },
                plugins: [HTMLLegendPlugin],
                options: {
                    pointHoverBackgroundColor: 'white',
                    pointBorderWidth: 1,
                    pointBorderColor: 'white',
                    borderWidth: 1.5,
                    borderColor: colorList,
                    pointRadius: 0,
                    pointHoverRadius: 5,
                    pointHitRadius: 50,
                    tension: 0.3,
                    scales: {
                        y: {
                            position: 'right', // `axis` is determined by the position as `'y'`
                            ticks: {
                                callback: function(value, index, values) {
                                    var locale = $('html').attr('lang');
                                    if(locale == 'en') return (value/1000 + 'k');
                                    return (value/1000 + 'k').replace('.',',');
                                },
                                color: 'white'
                            },
                        },
                        x: {
                            ticks: {
                                color: 'white'
                            },
                            grid: {
                                display: false
                            }
                        }
                    },
                    plugins: {
                        tooltip: {
                            callbacks: {
                                title: function(tooltipItems) {
                                    return tooltipItems[0].raw.date;
                                },
                                label: function(tooltipItem) {
                                    return tooltipItem.formattedValue + " {{__('fund.vnd')}}";
                                },
                            },
                            backgroundColor: '#DDB996',
                            displayColors: false
                        },
                        legend: {
                            display: false,
                        },
                        htmlLegend: {
                            containerID: 'fund-nav-chart-legend-container',
                            display: datasets.length > 1,
                            fontColor: 'white',
                            hiddenFontColor: 'gray'
                        }
                    }
                }
            });
        });
    </script>
</div>
