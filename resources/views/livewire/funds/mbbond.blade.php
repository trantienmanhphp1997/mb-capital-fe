<div>
  <section wire:ignore id="hero" class="d-flex align-items-center">
    <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100">
        <div class="row">
            <div class="col-lg-6 col-lg-6 pt-4">
                <h1>
                  {{ $currentFunds->shortname ?? '' }}
                  <span class="size23 px-sm-3">NAV {{ numberFormat($currentFunds->current_nav ?? 0) }}
                      <span class="size16 fw-normal">{{__('fund.vnd')}}</span>
                      @if(($currentFunds->growth ?? 0) > 0)
                        <i class="bi bi-arrow-up-short color-g"></i>
                      @else
                        <i class="bi bi-arrow-down-short color-g"></i>
                      @endif

                      <span class="size20 fw-normal">{{ ($currentFunds->growth ?? 0) }}%</span>
                  </span>
                </h1>
                <h2 class="fw-normal">{{__('mbbond.expected_profit_upto')}}<span class="ps-md-4 color-g size50">{{ $data['fun_master_type_18']->content ?? '0%' }}</span><span class="color-g size22 fw-normal">/{{__('mbbond.year')}}</span></span></h2>
                <p class="color-w size20">{{__('mbbond.invest_through')}}</p>
                @livewire('component.fund.access-link', ['type' => 12, 'fund_id' => $fund_id])
                <div class="d-flex">
                    <a href="{{route('page.instruction.index')}}" target="_blank" class="btn-watch-video color-w">
                        <i class="bi bi-play-circle"></i>
                        <span>{{__('mbbond.video_introduce_guide')}}</span>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-lg-6 pb-4">
                @livewire('funds.fund-nav-chart', ['fundIds' => [$fund_id]])
            </div>

        </div>
    </div>
</section>
<main id="main">
  <div wire:ignore class="container page-mbbond">
      <div class="tab-bond">
        <ul class="nav nav-pills mb-3 d-flex" id="pills-tab" role="tablist">
          <li class="nav-item" role="presentation">
            <button class="nav-link active w-100 tab" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home"
              type="button" role="tab" aria-controls="pills-home" aria-selected="true">{{__('fund.general_information')}}</button>
          </li>
          <li class="nav-item" role="presentation">
            <button class="nav-link w-100 tab" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile"
              type="button" role="tab" aria-controls="pills-profile" aria-selected="false" {{$currentFunds->enable_performance == 0 ? "disabled" : ""}}>{{__('fund.performance_results')}}</button>
          </li>
          <li class="nav-item" role="presentation">
            <button class="nav-link w-100 tab" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact"
              type="button" role="tab" aria-controls="pills-contact" aria-selected="false" {{$currentFunds->enable_info_disclosure == 0 ? "disabled" : ""}}>{{__('fund.information_disclosure')}}</button>
          </li>
        </ul>
      </div>
    </div>
  <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <div wire:ignore class="container hide_mobilde">
                @foreach($data['fun_master_type_3'] as $item)
                  <h3 class="title-bond ">{!!sanitizationText($item->title)!!}</h3>
                  <p class="size20 size18-md">*{!!sanitizationText($item->content)!!}</p>
                @endforeach
            </div>
             <!-- ======= skills ======= -->
             <section wire:ignore id="skills" class="skills mt-5">
              <div class="" data-aos="fade-up">
                <div class="paragraph item-three">

                  <section class="ps-timeline-sec alignment-bond">
                    <div class="container">
                      <div class="owl-carousel-line owl-carousel owl-theme">
                        @foreach($expected_profit as $item)
                            <div class="node-month">
                              <div class="node-content">
                                <p>{{$item->percent_view}}%</p>
                              </div>
                              <span class="ps-line-top"></span>
                              <div class="node-cr"></div>
                              <div class="line-hoz-div"></div>
                              <div class="month"><span>{{$item->period}}</span></div>
                            </div>
                         @endforeach
                    </div>
                  </section>
                </div>
              </div>
            </section>
            <div class="mb-profit text-center  hide-desktop  ">
              @foreach($data['fun_master_type_3'] as $item)
              <p class="size20 size18-md">*{!!sanitizationText($item->content)!!}</p>
              @endforeach
            </div>
             <!-- ======= Contact to  advise Section ======= -->
             <div wire:ignore.self class="paragraph item-three contact contact-bond" data-aos="fade-up">
              <div class="container">
                <h3 class="title-bond-tool">{{__('mbbond.calculation_tool.title')}}</h3>
                <div class="box-tool">
                  <div wire:ignore.self class="form-contact d-flex justify-content-center mt-5 " data-aos="fade-up" data-aos-delay="200">
                    <div class="mr-3 mb-3">
                      <label class="hide-desktop">{{__('mbbond.calculation_tool.amount')}}</label>
                      <input type="text" placeholder="{{__('mbbond.calculation_tool.amount')}}" oninput="formatInputNumber(this)" onkeypress="return isNumberKey(event)" class="form-select input-custom format_number" wire:model.defer="amount">
                    </div>
                    <div class="mr-3 mb-3">
                      <label class="hide-desktop">{{__('mbbond.calculation_tool.start_time')}}</label>
                      <input type="date" placeholder="{{__('mbbond.calculation_tool.start_time')}}" class="form-select input-custom" wire:model="fromDate">
                    </div>
                    <div class="mr-3 mb-3">
                      <label class="hide-desktop">{{__('mbbond.calculation_tool.end_time')}}</label>
                      <input type="date" placeholder="{{__('mbbond.calculation_tool.end_time')}}" class="form-select input-custom" wire:model="toDate">
                    </div>
                    <div class="mr-3 mb-3">
                     <label class="hide-desktop">%/{{__('mbbond.year')}}</label>
                      <input type="text" placeholder="%/{{__('mbbond.year')}}" readonly="" class="form-select input-custom" style="width: 155px" wire:model.defer="percent">
                    </div>
                    <button class="btn custom-btn-op1 custom-btn-op1-fullfill text-uppercase mb-3" wire:click="calculator(true)">
                        {{__('mbbond.calculation_tool.see_the_results')}}
                      </button>
                  </div>
                  <div class="text-center pt-30">
                    {{__('mbbond.calculation_tool.investment_time')}}:
                          <span class="px-1">
                            @if(!empty($result['month']))
                              {{ $result['month']}} {{__('mbbond.calculation_tool.month')}}@if($locale == 'en' && $result['month'] > 1)s @endif {{ $result['day']}}
                            @else
                              {{ $result['day'] }}
                            @endif
                          </span>{{__('mbbond.calculation_tool.day')}}@if($locale == 'en' && $result['day'] > 1)s @endif
                          {{-- Từ ngày<span class="px-1">3</span> tháng<span class="px-1">20</span>ngày --}}
                  </div>
                  <div wire:ignore.self class="d-flex pt-30 interest-rate">
                    <div class="mb-3">
                      <div class="text-center size18 h-50">{{__('mbbond.calculation_tool.expected_interest_rate_to_receive')}}</div>
                      <div class="totalmoney text-center">{{$result['profitMB']}} <span class="size17">%/{{__('mbbond.year')}}</span></div>
                    </div>
                    <div class="mb-3">
                      <div class="text-center size18 h-50">{{__('mbbond.calculation_tool.the_value_that_investors_get_back')}}</div>
                      <div class="totalmoney text-center">{{$result['valueMB']}} <span class=" size17">{{__('fund.vnd')}}</span></div>
                    </div>
                    <div class="mb-3">
                      <div class="text-center size18 h-50">{{__('mbbond.calculation_tool.interest_received_by_investors')}}</div>
                      <div class="totalmoney text-center">{{$result['value']}} <span class="size17">{{__('fund.vnd')}}</span></div>
                    </div>
                    <div class="mb-3">
                      <div class="text-center size18 h-50">{{__('mbbond.calculation_tool.difference_between_mbbond_and_savings')}}</div>
                      <div class="totalmoney text-center">{{$result['diffPrice']}} <span class="size17">{{__('fund.vnd')}}</span></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Contact Section -->
            <section wire:ignore id="utilities" class="utilities ">
                <div  class="container aos-init aos-animate " data-aos="fade-up">
                    <h3 class="title-bond pb-md-5">{{__('mbbond.why_should_you_invest_in_mbbond')}}</h3>
                    <div class="row" wire:ignore>
                        @foreach($data['fun_master_type_1'] as $item)
                            <div class="col-lg-4 col-md-6 prb-56-35" data-aos="zoom-in" data-aos-delay="100">
                                <div class="icon-box d-flex">
                                    <div class="icon">
                                        <img src="{{Storage::url($item->img)}}"  width="50" alt="1">
                                    </div>
                                    <div class="utilities-box justify">
                                        <h4>{{$item->title}}</h4>
                                        <p>{!!sanitizationText($item->content)!!}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
             <!-- ======= services ======= -->
            <section wire:ignore id="services" class="services">
                <div class="container aos-init aos-animate" data-aos="fade-up">
                    <h3 class="title-bond pb-md-5">{{__('mbbond.general_introduction_of_the_fund')}}</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive table-w600">
                              {!!sanitizationText($currentFunds->content)!!}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
              <!-- ======= services ======= -->
            <section wire:ignore id="services" class="services ">
                <div class="container aos-init aos-animate pb-md-5" data-aos="fade-up">
                    <h3 class="title-bond pb-md-5">{{__('mbbond.service_price_list')}}</h3>
                    <div class="table-responsive">
                        @foreach($data['fun_master_type_2'] as $item)
                            <td >{!!sanitizationText($item->content)!!}</td>
                        @endforeach
                    </div>
                    <h3 class="title-bond pb-md-5">{{__('mbbond.transaction_time')}}</h3>
                    @foreach($data['fun_master_type_4'] as $item)
                        {!!sanitizationText($item->content)!!}
                    @endforeach
                    <section class="skills participation-process mb-bond-process mt-5 timeline-mbbond">
                      <div class="" data-aos="fade-up">
                        <div class="paragraph container item-three">
                          <h3 class="title-bond">{{__('mbbond.transaction_process')}}</h3>
                          <section class="ps-timeline-sec">
                            <div class="container">
                            <div class="ps-timeline-div">
                              <ol class="ps-timeline">
                                @if(count($data['fun_master_type_5']) > 0)
                                  @foreach($data['fun_master_type_5'] as $index => $item)
                                      <li data-aos="zoom-in-up" data-aos-delay="200">
                                        <div class="img-handler-top">
                                          <div class="content-step d-flex">
                                            <div class="step-number">
                                              <p>{{$index + 1}}</p>
                                            </div>
                                            <div class="content-s">
                                              {!!sanitizationText($item->content)!!}
                                            </div>
                                          </div>
                                        </div>
                                        <div class="ps-bot">
                                        </div>
                                        <span class="ps-sp-top"></span>
                                      </li>
                                  @endforeach
                                @endif
                              </ol>
                            </div>
                          </div>
                          </section>
                        </div>
                      </div>
                    </section>
                    <h3 class="title-bond pb-md-5">{{__('mbbond.service_provider_organization')}}</h3>
                    <div class="table-responsive">
                        @foreach($data['fun_master_type_6'] as $index => $item)
                            {!!sanitizationText($item->content)!!}
                        @endforeach
                    </div>
                    <h3 class="title-bond pb-30">{{__('mbbond.board_of_representatives')}}</h3>
                    <div class="table-responsive hide_mobilde">
                        <table class="table table-fund ">
                            <tbody>
                                @foreach($fun_employee_xrf as $item)
                                <tr>
                                    <td class="td-odd"><a href="#" data-toggle="modal" data-target="#info" wire:click="setContentEmployee({{$item}})">{{$item->fullname}} </a></td>
                                    <td >{{$item->title}}</td>
                                  </tr>
                                @endforeach
                            </tbody>
                          </table>
                    </div>
                    <div class="table-mbbond-responsive">
                        @foreach($fun_employee_xrf as $item)
                          <a href="#" data-toggle="modal" data-target="#info" class="size14" wire:click="setContentEmployee({{$item}})">{{$item->fullname}}</a>
                          <p class="size14"><b>{{$item->title}}</b></p>
                          <hr>
                        @endforeach
                    </div>
                    <h3 class="title-bond pb-md-5 hide_mobilde">{{__('mbbond.fund_documents')}}</h3>
                    <div class="mbbond-documents hide_mobilde">
                      @livewire('component.fund.document-report-file', ['fund_id' => $fund_id, 'file_type' => 1, 'fund_master_type' => 10])
                      <hr style="height: 2px;">
                      <div class="d-flex">
                        <a href="{{route('page.instruction.index')}}"><i class="bi bi-box-arrow-up-right color-g mr-2"></i> {{__('mbbond.trading_instructions_via_the_app')}}</a>
                        <a href="{{route('page.instruction.index') . '#pills-web-tab' }}"><i class="bi bi-box-arrow-up-right color-g mr-2"></i> {{__('mbbond.trading_instructions_via_the_website')}}</a>
                      </div>
                    </div>
                </div>
            </section>
             <!-- ======= agent ======= -->

               @livewire('component.fund.distribution-agent', ['type' => 13, 'fund_id' => $fund_id])

        </div>

        <div wire:ignore class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        <section class="nav">
          <div class="container pb-md-5" data-aos="fade-up">
            @livewire('component.line-chart-nav', ['fundIds' => [$fund_id]])
            <h3 class="title-bond pb-md-4">{{__('mbbond.returns_on_investment')}}</h3>
            <span>{{__('mbbond.unit')}}: %</span>
            <div class=" mt-2">
              {{-- @foreach($data['fun_master_type_3'] as $index => $item)
                  {!!sanitizationText($item->content)!!}
              @endforeach --}}
              @livewire('component.fund.profit-investment', ['fund_id' => $fund_id, 'vnindex' => false])
            </div>
            <h3 class="title-bond pb-md-5">{{__('mbbond.investor_report')}}</h3>
            <div class="mbbond-documents">
              @livewire('component.fund.document-report-file', ['fund_id' => $fund_id, 'file_type' => 2, 'fund_master_type' => 11])
            </div>
            <h3 class="title-bond">{{$data['fun_master_type_8'][0]->v_key??"Danh mục hiện tại"}}</h3>
            <div class="chart-container-dou">
              <div class="d-flex mt-4 align-items-center">
                <div style="width:63%">
                  <canvas id="mbbond-pie"></canvas>
                </div>
                <div id="mbbond-pie-legend-container" class="ml-3 w-100"></div>
              </div>
            </div>
          </div>
        </section>
      </div>
        {{-- <div wire:ignore class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
          <section class="nav">
            <div class="container aos-init aos-animate pb-md-5" data-aos="fade-up">
              <h3 class="title-bond pb-md-5">{{__('mbbond.net_access_value_report')}}</h3>
              <div class="row">
                  <div class="col-md-3">
                    <div class="mbbond-documents">
                      <select class="form-select bg-yellow" aria-label="Default select example">
                        <option selected="">Số tiền</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                    </div>
                  </div>
                    @foreach($fund_news_type_1 as $item)
                        @if(!empty($item->files))
                            @foreach($item->files as $file)
                                <div class="col-md-3">
                                    <a href="{{ !empty($file->url) ? asset('storage/' . $files->url) : '#' }}"><i class="bi bi-box-arrow-down color-g"></i> Tải xuống</a>
                                </div>
                            @endforeach
                        @endif
                    @endforeach
              </div>
              <h3 class="title-bond pb-md-5">{{__('mbbond.activity_report')}}</h3>
              @foreach($fund_news_type_2 as $item)
                    @if(!empty($item->files))
                        @foreach($item->files as $file)
                            <div class="col-md-3">
                                <a href="{{ !empty($file->url) ? asset('storage/' . $files->url) : '#' }}"><i class="bi bi-box-arrow-down color-g"></i> Tải xuống</a>
                            </div>
                        @endforeach
                    @endif
                @endforeach
            </div>
          </section>
        </div> --}}
        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
        <div class="container pb-md-5 aos-init aos-animate" data-aos="fade-up">
          <h3 class="title-bond pb-md-3">{{__('mbbond.net_access_value_report')}}</h3>
          <div class="list-report pt-md-3 aos-init aos-animate" data-aos="fade-up">
            @livewire('component.fund.information-disclosure', ['type' => 1, 'fund_id' => array($fund_id), 'show_filter' => false])
          </div>
          <h3 class="title-bond pt-30">{{__('mbbond.activity_report')}}</h3>
          <div class="list-report pt-md-3 aos-init aos-animate" data-aos="fade-up">
            @livewire('component.fund.information-disclosure', ['type' => 2, 'fund_id' => array($fund_id)])
            <h3 class="title-bond pt-30">{{__('mbbond.publish_other_news')}}</h3>
          <div class="list-report pt-md-3 aos-init aos-animate" data-aos="fade-up">
            @livewire('component.fund.information-disclosure', ['type' => 3, 'fund_id' => array($fund_id)])
          </div>
        </div>
      </div>
      </div>
     <!-- ======= package ======= -->
     @livewire('component.fund.maybe-you-are-interested', ['include' => ['2', '4'], 'uy_thac' => true])
    <!-- ======= Contact to  advise Section ======= -->
    @livewire('component.advise-request', ['ip_client' => Request::ip()])
    <div wire:ignore.self class="modal fade in" id="info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{$fullname}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="card-body">
                          <p class="card-text">{{$title}}</p>
                          <p class="justify card-text">{{$content}}</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('fund.back')}}</button>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
@section('js')
    <script type="text/javascript">
        function isNumberKey(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;
            return true;
        }
        window.onload = function() {
            livewire.emit('load-page');
            let data = [];
            let widthScreen = $(window).width();
            data.push(<?php
                foreach ($data['fun_master_type_8'] as $item) {
                    echo $item . ',';
                }
            ?>);

            let label = [];
            let value = [];
            for (let i = 0; i < data.length ; i++) {
                label.push(data[i].title);
                value.push(data[i].number_value);
            }

            var mbbondPie = document.getElementById('mbbond-pie')
            var mbbondPieChart = mbbondPie ? new Chart(mbbondPie, {
                type: 'doughnut',
                data: {
                labels: label,
                datasets: [{
                    label: 'My First Dataset',
                    data: value,
                    backgroundColor: [
                    '#0A1E40',
                    '#254275',
                    '#B9CDE5',
                    '#DCE6F2',
                    ],
                    hoverOffset: 4
                }],

                },
                plugins: [HTMLLegendPlugin],
                options: {
                plugins: {
                    htmlLegend: {
                        containerID: 'mbbond-pie-legend-container'
                    },
                    legend: {
                      display: false,
                    }
                },
                }
            }) : null;
            $('.owl-carousel-line').owlCarousel(
            {
            loop: false,
            responsiveClass: true,
            nav: true,
            dots: false,
            navText:["<div class='nav-btn prev-slide'><i class='bi bi-chevron-left'></i></div>","<div class='nav-btn next-slide'><i class='bi bi-chevron-right'></i></div>"],
            startPosition: 1,
            responsive: {
                0: {
                items: 6,
                nav: true
                },
                600: {
                items: 6,
                nav: true
                },
                1200: {
                items: 6,
                nav: true,
                loop: false
                },
                1300: {
                items: 6,
                nav: true,
                loop: false
                },
                1366: {
                items: 6,
                nav: true,
                loop: false
                }
            }
            })
        }
      $(function(){
          $(".tab-bond").fixTab();
      });
</script>
<script type="text/javascript">
    $( ".tab" ).click(function() {
      window.scrollTo(0, 0);
    });
    $(".datepicker-info").datepicker({
        viewMode: "months",
        minViewMode: "months",
        language: 'vi'
    });

    $(".datepicker-document").datepicker({
        viewMode: "months",
        minViewMode: "months",
        language: 'vi'
    });

    $('.datepicker-info').datepicker().on('changeDate', function(e) {
        window.livewire.emit('changeMonthInfo', $('.datepicker-info').val());
    });
	 function isDateGreaterThanToday(b) {
    var dS = b.split("-");
    var d1 = new Date(dS[1], (+dS[0] - 1));
    var today = new Date();
    if (d1 > today) {
       $('.mbbond-documents').addClass("down_hide");
    } else {
       $('.mbbond-documents').removeClass("down_hide");
    }
}
	
    $('.datepicker-document').datepicker().on('changeDate', function(e) {
        window.livewire.emit('changeMonthDocument', $('.datepicker-document').val());
		isDateGreaterThanToday($('.datepicker-document').val());
    });
</script>
@endsection
