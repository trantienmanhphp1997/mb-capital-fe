<div>
    <section id="hero" class="d-flex align-items-center">
        <div class="container" data-aos="zoom-out" data-aos-delay="100">
            <div class="row">
                <div class="col-lg-6 pt-4">
                    <h1>{{__('mbvf-sip.periodic_investment')}}</h1>
                    <h2 class="mt-4">MBVF-SIP</h2>
                    <p class="color-w size20">{{__('mbvf-sip.invest_through')}}</p>
                        @livewire('component.fund.access-link', ['type' => 12, 'fund_id' => $fund->parent_id])
                    <div class="d-flex">
                        <a href="{{route('page.instruction.index')}}" class="glightbox btn-watch-video color-w">
                            <i class="bi bi-play-circle"></i>
                            <span>{{__('mbvf-sip.video_introduce_guide')}}</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    @livewire('funds.fund-nav-chart', ['fundIds' => [$fund->parent_id]])
                </div>
            </div>
        </div>
    </section>
    <main id="main">
        <div class="container page-mbbond">
            <div class="tab-bond">
                <ul class="nav nav-pills mb-3 d-flex" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active w-100" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">{{__('fund.general_information')}}</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="" type="button" role="tab" aria-controls="pills-profile" aria-selected="false" {{$fund->enable_performance == 0 ? "disabled" : ""}}>{{__('fund.performance_results')}}</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="" type="button" role="tab" aria-controls="pills-contact" aria-selected="false" {{$fund->enable_info_disclosure == 0 ? "disabled" : ""}}>{{__('fund.information_disclosure')}}</button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <!-- End Contact Section -->
                <section id="utilities" class="utilities pb-md-4">
                    <div class="container aos-init aos-animate " data-aos="fade-up">
                        <h3 class="title-bond pb-md-5">{{ $fund->reason->first()->v_key ?? '' }}</h3>
                        <div class="row">
                            @foreach ($fund->reason ?? [] as $reason)
                                {{-- @dump($reason) --}}
                                <div class="col-lg-4 col-md-6 prb-56-35" data-aos="zoom-in" data-aos-delay="100">
                                    <div class="icon-box d-flex">
                                        <div class="icon">
                                            <img src="{{Storage::url($reason->img)}}" width="50" alt="1">
                                        </div>
                                        <div class="utilities-box ">
                                            <h4>{{ $reason->title }}</h4>
                                            <p>{{ $reason->content }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </section>
                <!-- ======= Contact to  advise Section ======= -->
                <div class="contact contact-bond h-auto pb-md-4 pt-0 calculation-mbvfsip" data-aos="fade-up">
                    <div class="container">
                        <h3 class="title-bond color-w pt-30 mb-md-5">{{__('mbvf-sip.calculation_tool.title')}}</h3>
                        <h6 class="size22 mb-md-4">{{__('mbvf-sip.calculation_tool.determine_the_target')}}</h6>
                        <div class="box-tool mb-3">
                        <div class="d-flex mbvf-target">
                            <span>{{__('mbvf-sip.calculation_tool.if_you_invest')}}</span>
                            <div class="mbfvinput mbfvinput1">
                                <input type="text" class="form-control" oninput="formatInputNumber(this)" id="money-input-1" max="100000000000">
                                <span>{{__('fund.vnd')}}</span>
                            </div>
                            <span>{{__('mbvf-sip.calculation_tool.monthly_with_expected_profit')}}</span>
                            <div class="mbfvinput mbfvinput2">
                                <input type="text" class="form-control" id="profit-input-1" oninput="formatInputNumber(this)" max="20">
                                <span class="color-w">%/{{__('mbvf-sip.calculation_tool.year')}}</span>
                            </div>
                            <span>{{__('mbvf-sip.calculation_tool.with_in')}}</span>
                            <div class="mbfvinput mbfvinput2">
                                <input type="text" class="form-control mbfvinput2" id="year-input-1" oninput="formatInputNumber(this)" max="100">
                                <span class="color-w">{{__('mbvf-sip.calculation_tool.year')}}</span>
                            </div>
                            <span>{{__('mbvf-sip.calculation_tool.you_will_get')}}</span>
                            <button class="bg-gold btn-estimate" onclick="calculateMBVFSIP()">{{__('mbvf-sip.calculation_tool.see_the_results')}}</button>
                        </div>
                        <div class="mbfv-total">
                            <div class="row">
                                <div class="col-lg-3">
                                    <h6 class="text-center">{{__('mbvf-sip.calculation_tool.total_investment')}}</h6>
                                    <div class="totalmoney text-center"><span id="investment-output">0</span> <span class="size17">{{__('fund.vnd')}}</span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <h6 class="text-center">{{__('mbvf-sip.calculation_tool.expected_profit')}}</h6>
                                    <div class="totalmoney text-center"><span id="expected-profit-output-1">0</span> <span class="size17">%/{{__('mbvf-sip.calculation_tool.year')}}</span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <h6 class="text-center">{{__('mbvf-sip.calculation_tool.total_profit')}}</h6>
                                    <div class="totalmoney text-center"><span id="total-profit-output">0</span> <span class="size17">{{__('fund.vnd')}}</span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <h6 class="text-center">{{__('mbvf-sip.calculation_tool.amount_received')}}</h6>
                                    <div class="totalmoney text-center color-yellow pt-0"><span id="money-received-output">0</span> <span class="size17">{{__('fund.vnd')}}</span>
                                    </div>
                                </div>
                            </div>
                            <p class="text-center mb-0 pt-50">{{__('mbvf-sip.calculation_tool.the_results_are_for_reference_only_and_do_not_imply_future_profit_of_the_fund')}}</p>
                        </div>
                        </div>
                        <div class="mnfv-sip">
                            <h6 class="size22">{{__('mbvf-sip.calculation_tool.amount_to_invest_periodically_according_to_purpose')}}</h6>
                            <div class="d-flex mbvf-target py-4">
                                <span class="pl-0">{{__('mbvf-sip.calculation_tool.expected_profit')}}</span>
                                <div class="mbfvinput mbfvinput2">
                                    <input type="text" class="form-control" id="expected-profit-input-2" onchange="calculateReverseMBVFSIP()" oninput="formatInputNumber(this)" max="15" value="12">
                                    <span>%/{{__('mbvf-sip.calculation_tool.year')}}</span>
                                </div>
                                <span>{{__('mbvf-sip.calculation_tool.number_of_years_to_invest_in_sip')}}</span>
                                <div class="mbfvinput mbfvinput2">
                                    <input type="text" class="form-control mbfvinput2" id="year-input-2" onchange="calculateReverseMBVFSIP()" oninput="formatInputNumber(this)" oninput="formatInputNumber(this)" max="100">
                                    <span>{{__('mbvf-sip.calculation_tool.year')}}</span>
                                </div>
                            </div>
                            <div class="size18 pr-5 fw-bold" style="color:rgb(221, 185, 150);">
                                <p>{{__('mbvf-sip.calculation_tool.your_investment_goals')}}:</p>
                            </div>
                            <div class="mbfv-slide mt-5 pt-50">
                                <div class="range-wrap">
                                    <div class="range-value" id="range20"></div>
                                    <div class="range-value" id="range15"></div>
                                    <div class="range-value" id="range3"></div>
                                    <div class="range-value" id="range2"></div>
                                    <div class="range-value" id="range05"></div>
                                    <input disabled id="mark20" type="range" min="0" max="20000" value="20000" step="1" class="mark" style="position: absolute">
                                    <input disabled id="mark15" type="range" min="0" max="20000" value="15000" step="1" class="mark" style="position: absolute">
                                    <input disabled id="mark3" type="range" min="0" max="20000" value="4000" step="1" class="mark" style="position: absolute">
                                    <input disabled id="mark2" type="range" min="0" max="20000" value="2000" step="1" class="mark" style="position: absolute">
                                    <input disabled id="mark05" type="range" min="0" max="20000" value="500" step="1" class="mark" style="position: absolute">
                                    <input id="range" type="range" min="0" max="20000" value="500" step="1" class="money-input-2" onchange="calculateReverseMBVFSIP()" style="position: relative;" list="my-detents">
                                    <p class="size18 pr-5">{{__('mbvf-sip.calculation_tool.amount_you_need')}}: <span id="monney-r"></span></p>
                                </div>
                                <datalist id="my-detents">
                                    <option value="500">
                                    <option value="2000">
                                    <option value="4000">
                                    <option value="15000">
                                    <option value="20000">
                                </datalist>
                            </div>
                            <div class="mbfv-total">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="text-center">{{__('mbvf-sip.calculation_tool.amount_you_need_to_invest_monthly')}}</h6>
                                        <div class="totalmoney text-center color-yellow pt-0"><span id="money-output-2">0</span> <span class="size17">{{__('fund.vnd')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ======= services ======= -->
                <section id="services" class="services ">
                    <div class="container aos-init aos-animate pb-md-5" data-aos="fade-up">
                        <h3 class="title-bond pb-md-5">{{ $fund->priceService->title ?? '' }}</h3>
                        <div class="table-responsive">
                            {!! sanitizationText($fund->priceService->content ?? '') !!}
                        </div>
                        <h3 class="title-bond pb-md-5">{{ $fund->transactionTime->title ?? '' }}</h3>
                        {!! sanitizationText($fund->transactionTime->content ?? '') !!}
                    </div>
                </section>
            </div>
            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                <section class="nav">
                    <div class="container aos-init aos-animate pb-md-5" data-aos="fade-up">
                        <h3 class="title-bond pb-md-5">{{__('mbvf-sip.nav_growth')}}</h3>
                        <div>
                            <div class="filter-bar d-flex align-items-center">
                                <button type="button" class="btn time-filter mr-2">1 tuần</button>
                                <button type="button" class="btn time-filter active mr-2">1 tháng</button>
                                <button type="button" class="btn time-filter mr-2">3 tháng</button>
                                <button type="button" class="btn time-filter mr-2">6 tháng</button>
                                <button type="button" class="btn time-filter mr-2">1 năm</button>
                                <div class="range-time d-flex align-items-center mr-4">
                                    <div class="from mr-2">
                                        <label for="birthdaytime">Từ</label>
                                        <input type="datetime-local" id="from_date" name="from_date" class="custom-input-time ml-2">
                                    </div>
                                    <div class="to">
                                        <label for="birthdaytime">đến</label>
                                        <input type="datetime-local" id="to_date" name="to_date" class="custom-input-time ml-2">
                                    </div>
                                </div>

                                <button type="button" class="btn custom-btn-op1 custom-btn-op1-fullfill"> Tìm
                                    kiếm</button>
                            </div>
                            <div class="chart">
                                <canvas id="line1" width="450px" height="450px"></canvas>
                            </div>
                        </div>
                        <h3 class="title-bond pb-md-5">Lợi suất đầu tư</h3>
                        <div class="table-responsive">
                            <table class="table table-bond table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center">{{__('mbvf-sip.calculation_tool.type')}}</th>
                                        <th scope="col" class="text-center">{{__('mbvf-sip.calculation_tool.investment_property')}}</th>
                                        <th scope="col" class="text-center">{{__('mbvf-sip.calculation_tool.safety_level')}}</th>
                                        <th scope="col" class="text-center">{{__('mbvf-sip.calculation_tool.fund_size')}}</th>
                                        <th scope="col" class="text-center">{{__('mbvf-sip.calculation_tool.investment_objective')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Quỹ mở trái phiếu</td>
                                        <td>Quỹ mở trái phiếu</td>
                                        <td>Quỹ mở trái phiếu</td>
                                        <td>@Quỹ mở trái phiếu</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <h3 class="title-bond pb-md-5">Phân bổ nghành</h3>
                        <div class="chart-container" style="position: relative; width: 650px; height: 400px; bottom: 5%;">
                            <canvas id="mbbond-pie"></canvas>
                        </div>
                        <h3 class="title-bond pb-md-5">Báo cáo nhà đầu tư</h3>
                        <div class="mbbond-documents">
                            <div class="d-flex">
                                <a href=""><i class="bi bi-box-arrow-up-right color-g"></i> Biểu mẫu báo cáo.PDF</a>
                                <a href=""><i class="bi bi-box-arrow-up-right color-g"></i> Biểu mẫu báo cáo2.PDF</a>
                            </div>
                        </div>
                        <h3 class="title-bond pb-md-5">Top 5 cổ phiếu có tỷ trọng cao nhất</h3>
                        <div class="table-responsive">
                            <table class="table table-bond table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center">Loại cổ phiếu</th>
                                        <th scope="col" class="text-center">Sàn giao dịch</th>
                                        <th scope="col" class="text-center">Tên doanh nghiệp</th>
                                        <th scope="col" class="text-center">Ngành</th>
                                        <th scope="col" class="text-center">Tỷ trọng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">VNM</td>
                                        <td class="text-center">VNM</td>
                                        <td class="text-center">VNM</td>
                                        <td class="text-center">VNM</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                <section class="nav pt-md-4">
                    <div class="container py-md-5" data-aos="fade-up">
                        <h3 class="fw-regular pb-md-3 color-second size26">Báo cáo NAV</h3>
                        <div class="row mbbond-documents">
                            <div class="col-lg-3">
                                <select class="form-select bg-yellow" aria-label="Default select example">
                                    <option selected="">Chọn ngày báo cáo</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                            <div class="col-lg-3 text-center">
                                <a href=""><i class="bi bi-box-arrow-in-down color-g"></i> Báo cáo NAV</a>
                            </div>
                            <div class="col-lg-3">
                                <a href=""><i class="bi bi-box-arrow-in-down color-g"></i> Báo cáo NAV</a>
                            </div>
                            <div class="col-lg-3">
                                <a href=""><i class="bi bi-box-arrow-in-down color-g"></i> Báo cáo NAV</a>
                            </div>
                        </div>
                        <h3 class="title-bond pb-md-3">Báo cáo NAV</h3>
                        <div class="list-report pt-md-3" data-aos="fade-up">
                            <div class="row" data-aos="fade-up" data-aos-delay="200">
                                <div class="col-lg-5">
                                    <div class="icon-box d-flex">
                                        <div class="icon">
                                            <img src="assets/img/excel.png" alt="1">
                                        </div>
                                        <div class="investment-box ">
                                            <h4>Báo cáo giá trị tài sản ròng ngày 25/10/2021 của quỹ MBVF</h4>
                                            <p>21/10/2021</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-lg-5">
                                    <div class="icon-box d-flex">
                                        <div class="icon">
                                            <img src="assets/img/excel.png" alt="1">
                                        </div>
                                        <div class="investment-box ">
                                            <h4>Báo cáo giá trị tài sản ròng ngày 25/10/2021 của quỹ MBVF</h4>
                                            <p>21/10/2021</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row " data-aos="fade-up" data-aos-delay="200">
                                <div class="col-lg-5">
                                    <div class="icon-box d-flex">
                                        <div class="icon">
                                            <img src="assets/img/excel.png" alt="1">
                                        </div>
                                        <div class="investment-box ">
                                            <h4>Báo cáo giá trị tài sản ròng ngày 25/10/2021 của quỹ MBVF</h4>
                                            <p>21/10/2021</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-lg-5">
                                    <div class="icon-box d-flex">
                                        <div class="icon">
                                            <img src="assets/img/excel.png" alt="1">
                                        </div>
                                        <div class="investment-box ">
                                            <h4>Báo cáo giá trị tài sản ròng ngày 25/10/2021 của quỹ MBVF</h4>
                                            <p>21/10/2021</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- ======= package ======= -->
            @livewire('component.fund.maybe-you-are-interested', ['include' => ['3', '2', '4']])
        <!-- ======= Contact to  advise Section ======= -->
            @livewire('component.advise-request', ['ip_client' => Request::ip()])
        <!-- End Counts Section -->
    </main><!-- End #main -->

    <script>
        function calculateMBVFSIP() {
            let amount = removeFormatNumber(document.getElementById('money-input-1').value);
            let profit = removeFormatNumber(document.getElementById('profit-input-1').value) / 100;
            let year = removeFormatNumber(document.getElementById('year-input-1').value);

            if(!amount || !profit || !year) return;

            let totalInvestment = amount * year * 12;
            let monthProfit = (1 + profit) ** (1 / 12) - 1;
            let receivedMoney = FV(monthProfit, year * 12, -amount, 0, 1);
            let totalProfit = receivedMoney - totalInvestment;

            document.getElementById('investment-output').innerHTML = formatNumber(Math.round(totalInvestment));
            document.getElementById('expected-profit-output-1').innerHTML = Math.round(profit * 100);
            document.getElementById('total-profit-output').innerHTML = formatNumber(Math.round(totalProfit));
            document.getElementById('money-received-output').innerHTML = formatNumber(Math.round(receivedMoney));
        }

        function calculateReverseMBVFSIP() {
            let money = removeFormatNumber(document.querySelector('.money-input-2').value) * 1000000;
            let expectedProfit = removeFormatNumber(document.getElementById('expected-profit-input-2').value) / 100;
            let year = removeFormatNumber(document.getElementById('year-input-2').value);

            if(!money || !expectedProfit || !year) return;

            let pmt = Math.round(PMT(
                (1 + expectedProfit) ** (1/12) - 1,
                year * 12,
                -money,
                0,
                1
            ));
            document.getElementById('money-output-2').innerHTML = formatNumber(pmt);
        }
        window.addEventListener('load', function() {
            $(".tab-bond").fixTab();
        });
    </script>
</div>
