<div>
  <section id="hero" class="d-flex jampf">
    <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100">
        <div class="row">
            <div class="col-md-6 col-lg-6 pt-4">
                <h1> {{ $data->shortname ?? '' }}</h1>
                <h2 class="mt-3">{{$data->fullname ?? ''}}</h2>
            </div>
        </div>
    </div>
</section>
<main id="main">
    <div class="container  page-mbbond">
        <div class="tab-bond">
          <ul class="nav nav-pills mb-3 d-flex" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active w-100 tab" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">{{__('fund.general_information')}}</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link w-100 tab" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false" {{$data->enable_performance == 0 ? "disabled" : ""}}>{{__('fund.performance_results')}}</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link w-100 tab" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false" {{$data->enable_info_disclosure == 0 ? "disabled" : ""}}>{{__('fund.information_disclosure')}}</button>
            </li>
        </ul>
        </div>
    </div>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <div class="container py-5">
              {!! sanitizationText($data->content ?? '') !!}
            </div>
        </div>
        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
            <section id="utilities" class="utilities pb-xxl-4 paragraph item-three">
                <div class="container" data-aos="fade-up">
                    <div class="jamp_new">
                        @livewire('component.fund.jambf-article', ['fund_id' =>$fund_id])
                    </div>
                </div>
            </section>
        </div>
        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
            <!--  -->
            <div class="container pb-md-5 aos-init aos-animate" data-aos="fade-up">
              <h3 class="title-bond pt-30"></h3>
              <div class="list-report pt-md-3 aos-init aos-animate" data-aos="fade-up">
                @livewire('component.fund.information-disclosure', ['type' => 3, 'fund_id' => array($fund_id)])
              </div>
            </div>
        </div>
    </div>

    <div wire:ignore>
      @livewire('component.fund.maybe-you-are-interested', ['include' => ['3', '2', '4']])
    </div>

    <!-- ======= Contact to  advise Section ======= -->
    @livewire('component.advise-request', ['ip_client' => Request::ip()])
    <!-- End Counts Section -->
    </main>
</div>
@section('js')
{{--    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.5.1/dist/chart.min.js"></script>--}}
    <script src="{{ asset('assets/js/chart.min.js') }}"></script>
    <script type="text/javascript">
        $( ".tab" ).click(function() {
          window.scrollTo(0, 0);
        });

        let data = [];
        let label = [];
        let value = [];
        for (let i = 0; i < data.length ; i++) {
            label.push(data[i].title);
            value.push(data[i].number_value);
        }

        window.onload = function () {
          var mbbondPie = document.getElementById('mbbond-pie')
          var mbbondPieChart = mbbondPie ? new Chart(mbbondPie, {
            type: 'doughnut',
            data: {
              labels: label,
              datasets: [{
                label: 'My First Dataset',
                data: value,
                backgroundColor: [
                  '#0A1E40',
                  '#254275',
                  '#B9CDE5',
                  '#DCE6F2',
                ],
                hoverOffset: 4
              }],

            },
            options: {
              plugins: {
                legend: {
                  position: 'right'
                }
              }
            }
          }) : null;

          var line1 = document.getElementById('line1');
          var pie1 = document.getElementById('pie-1');
          var pie2 = document.getElementById('pie-2');
          var myChart1 = pie1 ? new Chart(pie1, {
            type: 'doughnut',
            data: {
              labels: [
                'Quỹ đầu tư cổ phiếu',
                'Tài sản thu nhập cố định',
              ],
              datasets: [{
                label: 'My First Dataset',
                data: [70, 30],
                backgroundColor: [
                  '#0A1E40',
                  '#B9CDE5',
                ],
                hoverOffset: 4
              }],

            },
            options: {
              plugins: {
                // title: {
                //   display: true,
                //   text: 'Chart Title',
                // },
                legend: {
                  position: 'right'
                }
              }
            }
          }) : null;

          var myChart2 = pie2 ? new Chart(pie2, {
            type: 'doughnut',
            data: {
              labels: [
                'Quỹ đầu tư cổ phiếu',
                'Tài sản thu nhập cố định',
              ],
              datasets: [{
                label: 'My First Dataset',
                data: [300, 300],
                backgroundColor: [
                  '#0A1E40',
                  '#B9CDE5',
                ],
                hoverOffset: 4
              }]
            },
            options: {
              plugins: {
                // title: {
                //   display: true,
                //   text: 'Chart Title',
                // },
                legend: {
                  position: 'right'
                }
              }
            }
          }) : null;

          var myChart3 = line1 ? new Chart(line1, {
            type: 'line',
            data: {
              labels: ["11/10/2021", "11/10/2021", "11/10/2021", "11/10/2021", "11/10/2021", "11/10/2021", "11/10/2021"],
              datasets: [{
                label: "My First dataset",
                data: [1, 1.5, 2, 2.5, 1, 4, 2],
              }]
            },
            options: {
              scales: {
                xAxes: [{
                  type: 'time',
                  time: {
                    displayFormats: {
                      'millisecond': 'MMM DD',
                      'second': 'MMM DD',
                      'minute': 'MMM DD',
                      'hour': 'MMM DD',
                      'day': 'MMM DD',
                      'week': 'MMM DD',
                      'month': 'MMM DD',
                      'quarter': 'MMM DD',
                      'year': 'MMM DD',
                    }
                  }
                }],
              },
            }
          }) : null;
        };
        $(function(){
            $(".tab-bond").fixTab();
        });
    </script>
@endsection
