<div wire:ignore.self>
  <section id="hero" class="d-flex align-items-center bn-mbretire">
    <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100">
        <div class="row">
            <div class="col-lg-6 col-lg-6 pt-4">
                <h1>{{$fund->getNameRetireFund()}}
                </h1>
                <br>
                {{-- <h2 class="mt-4">{{__('mbretire.increase_prosperity_give_peace_of_mind')}}</h2> --}}
                @livewire('component.fund.access-link', ['type' => 12, 'fund_id' => $fund_id])
            </div>
            <div class="col-lg-6 pb-4">
                @livewire('funds.fund-nav-chart', ['fundIds' => [5,6], 'file_fund_id' => $fund_id])
            </div>
        </div>
    </div>
</section>
<main id="main">
    <div class="container  page-mbbond">
      <div class="tab-bond">
        <ul class="nav nav-pills mb-3 d-flex" id="pills-tab" role="tablist">
          <li class="nav-item" role="presentation">
            <button class="nav-link active w-100 tab" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home"
              type="button" role="tab" aria-controls="pills-home" aria-selected="true">{{__('fund.general_information')}}</button>
          </li>
          <li class="nav-item" role="presentation">
            <button class="nav-link w-100 tab" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile"
              type="button" role="tab" aria-controls="pills-profile" aria-selected="false" {{$fund->enable_performance == 0 ? "disabled" : ""}}>{{__('fund.performance_results')}}</button>
          </li>
          <li class="nav-item" role="presentation">
            <button class="nav-link w-100 tab" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact"
              type="button" role="tab" aria-controls="pills-contact" aria-selected="false" {{$fund->enable_info_disclosure == 0 ? "disabled" : ""}}>{{__('fund.information_disclosure')}}</button>
          </li>
        </ul>
      </div>
    </div>
    <div class="tab-content" id="pills-tabContent">
      <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        <!-- End Contact Section -->
        <section id="utilities" class="utilities pb-xxl-4">
          <div class="container aos-init aos-animate " data-aos="fade-up">
            <h3 class="title-bond pb-xxl-5">{{$fundMasterReason->last()->v_key}}</h3>
            <div class="row">
                @foreach($fundMasterReason as $item)
              <div class="col-lg-4 col-md-6 pe-xxl-5 " data-aos="zoom-in" data-aos-delay="100">
                <div class="icon-box d-flex">
                  <div class="icon">
                    <img src="{{asset($item->img)}}" width="50" alt="1">
                  </div>
                  <div class="utilities-box ">
                    <h4>{{$item->getTitleAttribute()}}</h4>
                    <p>{{$item->getContentAttribute()}}</p>
                  </div>
                </div>
              </div>
                @endforeach
            </div>
          </div>
        </section>
        <!-- ======= services ======= -->
        <section id="services" class="services mb-5">
          <div class="container">
            {!! sanitizationText($fund->content) !!}
            <div class="charts d-flex flex-wrap" wire:ignore>
              <div class="col-md-6">
              <div class="chart-item">
                <h4 class="title-bond">{{__('mbretire.AK_fund')}}</h4>
                <h6 class="title-bond">{{__('mbretire.calculation_tool.unit')}}: %</h6>
                <div class="chart-container-dou3">
                  <div class="d-flex mt-4 align-items-center flex-wrap">
                    <div class="mb-3">
                      <canvas id="pie-1"></canvas>
                    </div>
                    <div id="pie-1-legend-container" class="ml-4"></div>
                  </div>
                </div>
              </div>
              </div>
              <div class="col-md-6">
              <div class="chart-item">
                <h4 class="title-bond">{{__('mbretire.TV_fund')}}</h4>
                <h6 class="title-bond">{{__('mbretire.calculation_tool.unit')}}: %</h6>
                <div class="chart-container-dou3">
                  <div class="d-flex mt-4 align-items-center flex-wrap">
                    <div class="mb-3">
                      <canvas id="pie-2"></canvas>
                    </div>
                    <div id="pie-2-legend-container" class="ml-4"></div>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </section>
        <div class="paragraph item-three contact contact-bond">
          <div class="container">
            <h3 class="title-bond-tool">{{__('mbretire.calculation_tool.title')}}</h3>
            <div class="fund-options mt-3" wire:ignore>
                @foreach($fundMasterTool as $key =>$item)
                  <div class="radio-item mr-5">
                    <input type="radio" id="ritema_{{$item->id}}" {{$loop->index == 1 ? 'checked' : ''}} name="ritem" wire:click="setInterest({{$item->interest_view}}, {{$item->interest}})" {{--wire:model="fund_tool"--}} value="{{$item->interest}}">
                    <label for="ritema_{{$item->id}}">{{$item->getFullnameAttribute()}}</label>
                  </div>
                @endforeach
            </div>
            <div class="calculation-frame mt-5">
              <div class="row form-calculator">
                <div class="col-md-6 mt-4">
                  <div class="row">
                    <div class="col-md-3 label">
                      <span>{{__('mbretire.calculation_tool.current_age')}}</span>
                    </div>
                    <div class="col-md-8 custom-input-gr">
                        <input type="text" max="61" min="20" tabindex="1" onChange="validateValue(event.target, 61, 20, 'errage')" wire:model.defer="current_age" oninput="formatInput(this)"/>
                        <span>{{__('mbretire.calculation_tool.age')}}</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 mt-4">
                  <div class="row">
                      <div class="col-md-4 label">
                          <span>{{__('mbretire.calculation_tool.total_monthly_payment')}}</span>
                      </div>
                      <div class="col-md-8 custom-input-gr">
                          <input type="text" tabindex="3" class="form-control" wire:model.defer="total_money" oninput="formatInputNumber(this)"/>
                          <span>{{__('fund.vnd')}}</span>
                      </div>
                  </div>
                </div>
              </div>
              <div class="row form-calculator mt-3">
                <div class="col-md-6 mt-4">
                  <div class="row">
                      <div class="col-md-3 label">
                          <span>{{__('mbretire.calculation_tool.retired_age')}}</span>
                      </div>
                      <div class="col-md-8 custom-input-gr">
                          <input type="text" max="65" min="50" tabindex="2" onChange="validateValue(event.target, 65, 50, 'errretire')" wire:model.defer="retire_age" oninput="formatInput(this)"/>
                          <span>{{__('mbretire.calculation_tool.age')}}</span>
                      </div>
                  </div>
                </div>
                <div class="col-md-6 mt-4">
                  <div class="row">
                    <div class="col-md-4 label">
                      <span>{{__('mbretire.calculation_tool.number_of_years_to_receive_payment_upon_retirement')}}</span>
                    </div>
                    <div class="col-md-8 custom-input-gr">
                      <input list="dataYear" type="text" max="30" min="10" tabindex="4" onChange="validateValue(event.target, 30, 10, 'erryear')" class="form-control" wire:model.defer="experience_year" oninput="formatInput(this)"/>
                      <span>{{__('mbretire.calculation_tool.year')}}</span>
                      <datalist id="dataYear">
                          <option value="10">
                          <option value="15">
                          <option value="20">
                      </datalist>
                    </div>
                  </div>
                </div>
              </div>
              <div class="btn-action mt-4 d-flex justify-content-center">
                <button type="button" class="btn custom-btn-op1 custom-btn-op1-outline text-uppercase mr-3" wire:click="resetInput">{{__('mbretire.calculation_tool.reset')}}</button>
                <button type="button" id="estimate" class="btn custom-btn-op1 custom-btn-op1-fullfill text-uppercase" wire:click="estimate">{{__('mbretire.calculation_tool.see_the_results')}}</button>
              </div>
              <div class="result d-flex justify-content-center mt-5">
                <div class="r-item">
                    <div class="label">
                        <p>{{__('mbretire.calculation_tool.amount_received_monthly_in')}} {{$experience_year}} {{__('mbretire.calculation_tool.year')}}</p>
                    </div>
                    <div class="value">
                        <span>{{$moneyMonth?numberFormat(round($moneyMonth)):0}} <span class="unit-price">{{__('fund.vnd')}}</span></span>
                    </div>
                  </div>
                <div class="r-item ml-3 mr-3">
                    <div class="label">
                         <i style="font-size: 20px;">{{__('mbretire.calculation_tool.or')}}</i>
                    </div>
                    <div class="value">
                    </div>
                </div>
                <div class="r-item">
                    <div class="label">
                          <p>{{__('mbretire.calculation_tool.amount_received_once')}}</p>
                    </div>
                    <div class="value">
                        <span>{{$money?numberFormat(round($money)):0}} <span class="unit-price">{{__('fund.vnd')}}</span></span>
                    </div>
                </div>
              </div>
              <div class="note text-center mt-5">
                <p>(*) {{__('mbretire.calculation_tool.based_on_expected_long_term_profit')}} {{$interest_view}}%/{{__('mbretire.calculation_tool.year')}}</p>
              </div>
            </div>
            <div class="fw-bold text-danger text-center">
                <p id="errage" style="display: none">{{__('mbretire.calculation_tool.validate_current_age')}}</p>
                <p id="errretire" style="display: none">{{__('mbretire.calculation_tool.validate_retirement_age')}}</p>
                <p id="erryear" style="display: none">{{__('mbretire.calculation_tool.validate_year_receive')}}</p>
            </div>

          </div>
        </div>
        {!! sanitizationText($fundTime->getNoteAttribute()) !!}
        {!! sanitizationText($fundProcess->getNoteAttribute()) !!}
      </div>
      <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        @livewire('component.line-chart-nav', ['fundIds' => [5,6]])
      </div>
      <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
      <div class="container py-md-5" data-aos="fade-up">
          <div class="container pb-md-5 aos-init aos-animate" data-aos="fade-up">
            <h3 class="title-bond pb-md-3">@lang('mbretire.net_access_value_report')</h3>
            <div class="list-report pt-md-3 aos-init aos-animate" data-aos="fade-up">
              @livewire('component.fund.information-disclosure', ['type' => 1, 'fund_id' => array(4,5,6), 'show_filter' => false])
            </div>
            <h3 class="title-bond pt-30">@lang('mbretire.activity_report')</h3>
            <div class="list-report pt-md-3 aos-init aos-animate" data-aos="fade-up">
              @livewire('component.fund.information-disclosure', ['type' => 2, 'fund_id' => array(4,5,6)])
              <h3 class="title-bond pt-30">@lang('mbretire.publish_other_news')</h3>
            <div class="list-report pt-md-3 aos-init aos-animate" data-aos="fade-up">
              @livewire('component.fund.information-disclosure', ['type' => 3, 'fund_id' => array(4,5,6)])
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- ======= package ======= -->
        @livewire('component.fund.maybe-you-are-interested', ['include' => ['3', '2'], 'uy_thac' => true])
    <!-- ======= Contact to  advise Section ======= -->
        @livewire('component.advise-request', ['ip_client' => Request::ip()])
    <!-- End Counts Section -->
  </main>
</div>
@section('js')
    <script type="text/javascript">
        $( ".tab" ).click(function() {
          window.scrollTo(0, 0);
        });
        var pie1 = document.getElementById('pie-1');
        var pie2 = document.getElementById('pie-2');

        var dataHealth= @js($healthChart);
        var dataProsperous = @js($prosperousChart);
        var labelname = 'My First Dataset';
        var type = 'doughnut';

        var labels = dataHealth.map($item => $item.label ? ($item.label + ': ' + $item.value.value + '%') : ($item.value.value + '%'));
        var datas = dataHealth.map($item => $item.value.value);
        var colors = dataHealth.map($item => $item.value.color);

        var label1s = dataProsperous.map($item => $item.label ? ($item.label + ': ' + $item.value.value + '%') : ($item.value.value + '%'));
        var data1s = dataProsperous.map($item => $item.value.value);
        var color1s = dataProsperous.map($item => $item.value.color);

        let widthScreen = $(window).width();
        let position = widthScreen >= 768 ? 'right' : 'bottom'
        var myChart1 = initChart(pie1, type, labels, labelname, datas, colors, 4, position, 'pie-1');
        var myChart2 = initChart(pie2, type, label1s, labelname, data1s, color1s, 4, position, 'pie-2');

        $(function(){
            $(".tab-bond").fixTab();
        });
        var $window = $(window);
        $window.resize(function resize() {
          if ($window.width() < 514) {
                  return $(".timeline-mbbond").addClass("mb-bond-process");
                }
            $("timeline-mbbond").removeClass("mb-bond-process");
        }).trigger('resize');
    </script>
@endsection
