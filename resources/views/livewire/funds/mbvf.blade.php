<div>
    <section id="hero" class="d-flex align-items-center">
        <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100">
            <div class="row">
                <div class="col-lg-6 pt-4">
                    <h1>
                        {{ $data->shortname ?? '' }}
                        <span class="size23 px-sm-3">NAV {{ numberFormat($lastNav->amount ?? 0) }}
                            <span class="size16 fw-normal">{{__('fund.vnd')}}</span>
                            @if($data->growth > 0)
                                <i class="bi bi-arrow-up-short color-g"></i>
                            @else
                                <i class="bi bi-arrow-down-short color-g"></i>
                            @endif
                            <span class="size20 fw-normal">{{ numberFormat($data->growth ?? 0) }}%</span>
                        </span>
                    </h1>

                    <h2>{{__('mbvf.expected_profit_upto')}}<span class="ps-md-4 color-g size50">{{ $fundMasterType18->content ?? '0%' }}</span><span class="color-g size22 fw-normal">/{{__('mbvf.year')}}</span></h2>
                    <p class="color-w size20">{{__('mbvf.invest_through')}}</p>
                    @livewire('component.fund.access-link', ['type' => 12, 'fund_id' => $fund_id])
                    <div class="d-flex">
                        <a href="{{ route('page.instruction.index') }}" target="_blank" class="btn-watch-video color-w">
                            <i class="bi bi-play-circle"></i>
                            <span>{{__('mbvf.video_introduce_guide')}}</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 pb-4">
                    @livewire('funds.fund-nav-chart', ['fundIds' => [$fund_id]])
                </div>
            </div>
        </div>
    </section>
    <main id="main">
        <div class="container page-mbbond">
            <div class="tab-bond">
                <ul class="nav nav-pills mb-3 d-flex" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active w-100 tab" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">{{__('fund.general_information')}}</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100 tab" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false" {{$data->enable_performance == 0 ? "disabled" : ""}}>{{__('fund.performance_results')}}</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100 tab" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false" {{$data->enable_info_disclosure == 0 ? "disabled" : ""}}>{{__('fund.information_disclosure')}}</button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <!-- End Contact Section -->
                <section id="utilities" class="utilities pb-md-4">
                    <div class="container aos-init aos-animate " data-aos="fade-up">

                        <h3 class="title-bond pb-md-5">{{ $data->reason->first()->v_key ?? '' }}</h3>
                        <div class="row">
                            @foreach ($data->reason ?? [] as $reason)
                                <div class="col-lg-4 col-md-6 prb-56-35" data-aos="zoom-in" data-aos-delay="100">
                                    <div class="icon-box d-flex">
                                        <div class="icon">
                                            <img src="{{ Storage::url($reason->img) }}" width="50" alt="1">
                                        </div>
                                        <div class="utilities-box ">
                                            <h4>{{ $reason->title }}</h4>
                                            <p>{{ $reason->content }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <h3 class="title-bond pb-md-5">{{ $data->typeInvestment->first()->v_key ?? '' }}</h3>
                        <div class="row steps-investment" data-aos="fade-up">
                            @foreach ($data->typeInvestment ?? [] as $key => $item)
                                <div class="col-lg-6 pl-5" data-aos="zoom-in-up" data-aos-delay="100">
                                    <div class="d-flex">
                                        <div class="step01">{{ Str::padLeft($key + 1, 2, '0') }}</div>
                                        <div class="investment-list">
                                            <div class="title-steps">{{ $item->title }}</div>
                                            <p>{{ $item->content }}</p>

                                            @if($item->url)
                                                <a href="{{$item->url}}" class=" read-more color-g size18"><i class="bi bi-arrow-right"></i>
                                                    {{__('fund.view_more')}}</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
                <!-- ======= Contact to  advise Section ======= -->

                <div class="mbfv-calculate contact contact-bond h-auto   " data-aos="fade-up">
                    <div class="container">
                        <div class="row pd-tool">
                            <h3 class="title-bond color-w pt-0 mb-md-5 row hide-desktop">{{__('mbvf.calculation_tool.title')}}</h3>
                            <div class="col-md-12 box-tool">
                                <div class="row">
                                <div class="col-md-6 br-mbvf">
                                <h3 class="title-bond color-w pt-0 mb-md-5 hide_mobilde">{{__('mbvf.calculation_tool.title')}}</h3>
                                <div class="pl-5">
                                    <div class="d-flex mbvf-target pb-30">
                                        <span class="label-calculate">{{__('mbvf.calculation_tool.amount_to_buy_cccq')}}</span>
                                        <div class="mbfvinput mbfvinput1 w-50">
                                            <input type="text" id="amount-input" class="form-control" oninput="formatInputNumber(this)" max="100000000000" />
                                            <span>{{__('fund.vnd')}}</span>
                                        </div>
                                    </div>
                                    <div class="d-flex mbvf-target pb-30">
                                        <span class="label-calculate">{{__('mbvf.calculation_tool.holding_time')}}</span>
                                        <div class="mbfvinput mbfvinput2">
                                            <input type="text" id="hold-time-input" class="form-control" oninput="formatInputNumber(this)" max="100" />
                                            <span class="color-w">{{__('mbvf.year')}}</span>
                                        </div>
                                    </div>
                                    <div class="d-flex mbvf-target pb-30">
                                        <span class="label-calculate">{{__('mbvf.calculation_tool.average_interest_rate')}}</span>
                                        <div class="mbfvinput mbfvinput2">
                                            <input type="text" id="profit-input" class="form-control mbfvinput2" oninput="formatInputNumber(this)" max="20" value="15" />
                                            <span class="color-w">%/{{__('mbvf.year')}}</span>
                                        </div>
                                    </div>
                                    <div class="d-flex mbvf-target pb-30">
                                        <span class="label-calculate">{{__('mbvf.calculation_tool.you_will_receive')}}</span>
                                        <button class="bg-gold btn-estimate" onclick="calculateMBVF()">{{__('mbvf.calculation_tool.see_the_results')}}</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 d-table">
                                <div class="d-table-cell align-middle">
                                    <div class="d-flex mbvf-target">
                                        <span class="label-calculate text-right">{{__('mbvf.calculation_tool.amount_expected_to_receive')}}</span>
                                        <div class="totalmoney text-center pt-0 ml-4" id="final-nav-result">
                                            0 <span class="size17 fw-normal">{{__('fund.vnd')}}</span>
                                        </div>
                                    </div>
                                    <div class="pb-30 ml-3" style="font-style:italic;font-size:13px">
                                      * {{__('mbvf.calculation_tool.taxs_and_fees_are_not_included_when_selling_fund_certificate')}}
                                    </div>
                                    <div class="d-flex mbvf-target pb-30 ">
                                        <span class="label-calculate text-right">{{__('mbvf.calculation_tool.interest')}}</span>
                                        <div class="totalmoney text-center pt-0 ml-4" id="interest-result">
                                            0 <span class="size17 fw-normal">{{__('fund.vnd')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <section id="services" class="services">
                    <div class="container aos-init  pb-md-2 pt-md-5" data-aos="fade-up">
                        <h3 class="title-bond pb-md-5">{{__('mbvf.general_introduction_of_the_fund')}}</h3>
                        <div class="table-responsie">
                            {!! sanitizationText($data->content) !!}
                        </div>
                    </div>
                </section>
                <section id="services" class="services ">
                    <div class="container aos-init aos-animate pb-md-5" data-aos="fade-up">

                        <h3 class="title-bond pb-md-5">{{ $data->priceService->title ?? '' }} </h3>
                        <span>*{{__('mbvf.fees_charged_on_transaction_value')}}</span>
                        <div class="table-responsive mt-2">
                            {!! sanitizationText($data->priceService->content ?? '') !!}
                        </div>

                        <h3 class="title-bond pb-md-5">{{ $data->transactionTime->title ?? '' }}</h3>
                        {!! sanitizationText($data->transactionTime->content?? '') !!}
                        <h3 class="title-bond pb-md-5">{{ $fundMasterType5->first()->title ?? '' }}</h3>
                        <section id="skills" class="skills participation-process mb-bond-process mt-5 timeline-mbbond">
                            <div class="paragraph item-three">
                                <section class="ps-timeline-sec">
                                    <div class="container">
                                        <div class="ps-timeline-div">
                                            <ol class="ps-timeline">
                                                @foreach ($fundMasterType5 ?? [] as $key => $item)
                                                    <li data-aos="zoom-in-up" data-aos-delay="{{ ($key + 1) * 200 }}">
                                                        <div class="img-handler-top">
                                                            <div class="content-step d-flex">
                                                                <div class="step-number">
                                                                    <p>{{ $key + 1 }}</p>
                                                                </div>
                                                                <div class="content-s">
                                                                    {!! sanitizationText($item->content ?? '') !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="ps-bot">
                                                        </div>
                                                        <span class="ps-sp-top"></span>
                                                    </li>
                                                @endforeach
                                            </ol>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </section>
                        <h3 class="title-bond pb-md-5">{{ data_get($data, 'serviceSupplier.title', '') }}</h3>
                        <div class="table-responsie">
                            {!! sanitizationText($data->serviceSupplier->content ?? '') !!}
                        </div>

                        <h3 class="title-bond pb-30">{{__('mbvf.board_of_representatives')}}</h3>
                        <div class="table-responsie hide_mobilde">
                            <table class="table table-fund">
                                @foreach ($data->employees ?? [] as $employee)
                                    <tr>
                                        <td class="td-odd"><a href="#" data-toggle="modal" data-target="#info" wire:click="setContentEmployee({{ $employee }})">{{$employee->sex == 1 ? __('about.mr') : __('about.mrs') }} {{ $employee->fullname }} </a></td>
                                        <td>{{ $employee->title ?? '' }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="table-mbbond-responsive hide-desktop">
                          @foreach ($data->employees ?? [] as $employee)
                            <p class="size14"><b><a href="#" data-toggle="modal" data-target="#info" wire:click="setContentEmployee({{ $employee }})">{{$employee->sex == 1 ? __('about.mr') : __('about.mrs') }} {{ $employee->fullname }} </a></b></p>
                            <p class="size14">{{ $employee->title ?? '' }}	</p>
                            <hr>
                            @endforeach
                        </div>
                        <h3 class="title-bond pb-md-5 hide_mobilde">{{__('mbvf.investment_reports')}}</h3>
                        <div class="mbbond-documents hide_mobilde">
                      @livewire('component.fund.document-report-file', ['fund_id' => $fund_id, 'file_type' => 1, 'fund_master_type' => 10])
                      <hr style="height: 2px;">
                      <div class="d-flex">
                        <a href="{{route('page.instruction.index')}}"><i class="bi bi-box-arrow-up-right color-g mr-2"></i> {{__('mbbond.trading_instructions_via_the_app')}}</a>
                        <a href="{{route('page.instruction.index') . '#pills-web-tab' }}"><i class="bi bi-box-arrow-up-right color-g mr-2"></i> {{__('mbbond.trading_instructions_via_the_website')}}</a>
                      </div>
                    </div>
                    </div>
                    @livewire('component.fund.distribution-agent', ['fund_id' => $data->id, 'type' => 13])
                </section>
                <!-- ======= agent ======= -->
            </div>
            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                <section class="nav">
                    <div class="container aos-init aos-animate pb-md-5" data-aos="fade-up">

                        @livewire('component.line-chart-nav', ['fundIds' => [$fund_id], 'VNIndex' => true])

                        <h3 class="title-bond pb-md-4">{{ $data->profitInvestment->title ?? '' }} </h3>
                        <span>{{__('mbvf.unit')}}: %</span>
                        <div class="table-responsive mt-2">
                            {{-- {!! $data->profitInvestment->content ?? '' !!} --}}
                            @livewire('component.fund.profit-investment', ['fund_id' => $fund_id, 'vnindex' => true])
                        </div>

                        <h3 class="title-bond pb-md-5">{{ $data->reportInvestor->title ?? '' }}</h3>
                        <div class="mbbond-documents">
                            @livewire('component.fund.document-report-file', ['fund_id' => $fund_id, 'file_type' => 2, 'fund_master_type' => 11])
                        </div>

                        <h3 class="title-bond pb-md-5">{{__('mbvf.industry_allocation')}}</h3>

                        <div class="row">
                            <div class="col-lg-10">
                                *{{__('mbvf.data_updated_on')}} {{$fundMasterType8->v_key ?? ''}}
                                <canvas id="allocate"></canvas>
                            </div>
                        </div>
                        <br><br>
                        <h3 class="title-bond pb-md-5">{{ $data->topStock->title ?? '' }}</h3>
                        <div class="table-responsive"  wire:ignore>
                            {!! sanitizationText($data->topStock->content ?? '') !!}
                        </div>
                    </div>
                </section>
            </div>
            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                <section class="nav">
                    <div class="container pb-md-5 aos-init aos-animate" data-aos="fade-up">
                        <h3 class="title-bond pb-md-3">{{__('mbvf.net_access_value_report')}}</h3>
                        <div class="list-report pt-md-3 aos-init aos-animate" data-aos="fade-up">
                            @livewire('component.fund.information-disclosure', ['type' => 1, 'fund_id' => array($fund_id), 'show_filter' => false])
                        </div>
                        <h3 class="title-bond pt-30">{{__('mbvf.activity_report')}}</h3>
                        <div class="list-report pt-md-3 aos-init aos-animate" data-aos="fade-up">
                            @livewire('component.fund.information-disclosure', ['type' => 2, 'fund_id' => array($fund_id)])
                            <h3 class="title-bond pt-30">{{__('mbvf.publish_other_news')}}</h3>
                            <div class="list-report pt-md-3 aos-init aos-animate" data-aos="fade-up">
                                @livewire('component.fund.information-disclosure', ['type' => 3, 'fund_id' => array($fund_id)])
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- ======= package ======= -->
        @livewire('component.fund.maybe-you-are-interested', ['include' => ['31', '3'], 'uy_thac' => true])
        <!-- ======= Contact to  advise Section ======= -->
        @livewire('component.advise-request', ['ip_client' => Request::ip()])
        <!-- End Counts Section -->
        <div wire:ignore.self class="modal fade in" id="info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{$fullname}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="card-body">
                                <p class="card-text">{{$title}}</p>
                                <p class="justify card-text">{{$content}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('fund.back')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@section('js')
    <script type="text/javascript">
        function calculateMBVF() {
            let amount = removeFormatNumber(document.getElementById('amount-input').value);
            let holdTime = removeFormatNumber(document.getElementById('hold-time-input').value);
            let profit = removeFormatNumber(document.getElementById('profit-input').value) / 100;

            if (!amount || !holdTime || !profit) return {};

            let finalNAV = Math.round(amount * Math.pow(1 + profit, holdTime));
            let interest = finalNAV - amount;
            document.getElementById('final-nav-result').innerHTML = formatNumber(finalNAV) + ` <span class="size17 fw-normal">{{__('fund.vnd')}}</span>`;
            document.getElementById('interest-result').innerHTML = formatNumber(interest) + ` <span class="size17 fw-normal">{{__('fund.vnd')}}</span>`;
        }

        window.addEventListener('load', function() {
            $('.owl-carousel').owlCarousel({
                loop: false,
                responsiveClass: true,
                nav: true,
                responsive: {
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:5
                    }
                }
            })
        });

        window.addEventListener('load', function() {
            Livewire.emit('getAllocateData');
            Livewire.on('showAllocateChart', function(data) {
                let maxValue = Math.max(...data.map(item => Number(item.value)));
                console.log(maxValue);
                var allocate = document.getElementById('allocate')
                var allocateChart = allocate ? new Chart(allocate, {
                    type: 'bar',
                    data: {
                        labels: data.map(item => item.label),
                        datasets: [{
                            data: data.map(item => item.value),
                            backgroundColor: [
                                '#254275',
                                '#254275',
                                '#254275',
                                '#254275',
                            ],
                            hoverOffset: 4,
                        }],
                    },
                    plugins: [ChartDataLabels],
                    options: {
                        elements: {
                            rectangle: {
                                borderWidth: 2,
                                borderColor: 'rgb(0, 255, 0)',
                                borderSkipped: 'bottom'
                            }
                        },
                        maintainAspectRatio: true,
                        aspectRatio: 2,
                        indexAxis: 'y',
                        scales: {
                            x: {
                                beginAtZero: true,
                                max: Math.ceil(maxValue + 5),
                                grid: {
                                    display: false,
                                }
                            },
                            y: {
                                grid: {
                                    display: false,
                                }
                            }
                        },
                        plugins: {
                            legend: {
                                display: false
                            },
                            datalabels: {
                                anchor: 'end',
                                align: 'right',
                                offset: 10,
                                formatter: function(value, context) {
                                    return formatNumber(Number(value)) + '%';
                                }
                            }
                        }
                    }
                }) : null;
            });
        });

        $(function() {
            $(".tab-bond").fixTab();
        });
    </script>
    <script type="text/javascript">
        $( ".tab" ).click(function() {
          window.scrollTo(0, 0);
        });

        $(".datepicker-info").datepicker({
            viewMode: "months",
            minViewMode: "months",
            language: 'vi'
        });

        $(".datepicker-document").datepicker({
            viewMode: "months",
            minViewMode: "months",
            language: 'vi'
        });

        $('.datepicker-info').datepicker().on('changeDate', function(e) {
            window.livewire.emit('changeMonthInfo', $('.datepicker-info').val());
        });
		 function isDateGreaterThanToday(b) {
    var dS = b.split("-");
    var d1 = new Date(dS[1], (+dS[0] - 1));
    var today = new Date();
    if (d1 > today) {
       $('.mbbond-documents').addClass("down_hide");
    } else {
       $('.mbbond-documents').removeClass("down_hide");
    }
}
        $('.datepicker-document').datepicker().on('changeDate', function(e) {
            window.livewire.emit('changeMonthDocument', $('.datepicker-document').val());
			isDateGreaterThanToday($('.datepicker-document').val());
        });

    </script>
@endsection
