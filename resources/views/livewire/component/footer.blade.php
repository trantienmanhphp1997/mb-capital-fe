<!-- ======= Footer ======= -->
 <footer id="footer">
  <div class="container mt-4">
    <div class="logo">
      <img src="{{ asset('assets/img/logo.png') }}" alt="">
    </div>
    <div class="infor mt-4">
      <div class="col-item">
        <div class="head-col">
          <h6>{{__('common.topbar.about_MB')}}</h6>
        </div>
        <div class="horizontal"></div>
        <div class="body-col">
            <div class="line-item">
              <a href="{{route('page.mb.about')}}">{{__('common.footer.introduction')}}</a>
            </div>
            <div class="line-item">
              <a href="{{route('page.mb.about', ['activeTab' => 'employee'])}}">{{__('common.footer.hr_team')}}</a>
            </div>
            <div class="line-item">
              <a href="{{route('page.mb.about', ['activeTab' => 'investor'])}}">{{__('common.footer.investor_relation')}}</a>
            </div>
            {{-- <div class="line-item">
              <a href="#">Khen thưởng</a>
            </div> --}}
            <div class="line-item">
              <a href="{{route('page.mb.recruitment.index')}}">{{__('common.footer.environments_work')}}</a>
            </div>
            <div class="line-item">
              <a href="{{route('page.news.index')}}">{{__('common.topbar.news')}}</a>
            </div>
        </div>
      </div>
      <div class="col-item">
        <div class="head-col">
          <h6>{{__('common.header.manager_fund')}}</h6>
        </div>
        <div class="horizontal"></div>
        <div class="body-col">
            @foreach($listFund as $fund)
            @if(!$fund->parent_id)
              <div class="line-item">
                <a href="{{route('page.fund.detail',['slug'=>$fund->slug ?? 'default'])}}">
                  {{$fund->shortname2}}</a>
              </div>
            @endif
            @endforeach
        </div>
      </div>
      <div class="col-item">
        <div class="head-col" style="width: 280px">
          <h6>{{__('common.header.investment_trust')}}</h6>
        </div>
        <div class="horizontal"></div>
        <div class="body-col">
          <div class="line-item">
            <a href="{{route('page.trust.personal')}}">{{__('common.header.individual_customer')}}</a>
          </div>
          <div class="line-item">
            <a href="{{route('page.trust.index')}}">{{__('common.header.institutional_customer')}}</a>
          </div>
        </div>
      </div>
      <div class="col-item">
        <div class="head-col">
          <h6>{{__('common.header.investor_relations')}}</h6>
        </div>
        <div class="horizontal"></div>
        <div class="body-col">
          @foreach($listFund as $fund)
            @if(!$fund->parent_id && $fund->id!=1)
              <div class="line-item">
                <a href="{{route('page.investors.index',['slug'=>$fund->slug ?? 'default'])}}">
                  {{$fund->shortname2}}</a>
              </div>
            @endif
          @endforeach
        </div>
      </div>
      <div class="col-item">
        <div class="head-col">
          <h6>{{__('common.header.customer_support')}}</h6>
        </div>
        <div class="horizontal"></div>
        <div class="body-col">
          <div class="line-item">
            <a href="{{route('page.investment.index')}}">{{__('common.header.investment_knowledge')}}</a>
          </div>
          <div class="line-item">
            <a href="{{route('page.tool.support.index')}}">{{__('common.footer.tool_supports')}}</a>
          </div>
          <div class="line-item">
            <a href="{{route('page.instruction.index')}}">{{__('common.header.investment_guide')}}</a>
          </div>
          <div class="line-item">
            <a href="{{route('page.faq.index')}}">FAQ</a>
          </div>
        </div>
      </div>
      <div class="col-item">
        <div class="head-col">
          <h6>{{__('common.footer.alignment_of_companies')}}</h6>
        </div>
        <div class="horizontal"></div>
        <div class="body-col mt-3">
          <div class="dropdown my-2">
            <div class="w-100"
            style="border: 1px solid gray"
            id="dropdownMenuButton"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false">
              <div class="d-flex justify-content-between dropdown-selected">
                <div>
                  MB BANK
                </div>
                <div>
                  <i class="bi bi-caret-down-fill"></i>
                </div>
              </div>
            </div>
            <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
              @foreach($listCompanyAffiliation as $companyAffiliation)
              <a class="dropdown-item text-dark" href="{{$companyAffiliation->url}}" target="blank">
                  {{ $companyAffiliation->v_value?? '' }}</a>
              @endforeach
            </div>
        </div>
          <div class="social-netwok d-flex">
            <a href="https://www.facebook.com/MBcapital.vn/" class="mr-3"><img src="{{ asset('assets/icon/facebook.png') }}" alt="facebook"></a>
            <a href="https://www.youtube.com/channel/UCx0wKi7FwqKUjrzZLuPk9hw" class="mr-3"><img src="{{ asset('assets/icon/youtube.png') }}" alt="youtube"></a>
            <a href="https://vn.linkedin.com/company/mbcapitalvn" class="mr-3"><img src="{{ asset('assets/icon/insta.png') }}" alt="insta"></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container address-contact d-flex justify-content-center mt-5">
    <div class="address mr-4">
      <p>{{$address->v_value ??''}}</p>
    </div>
    <div class="tel d-flex flex-wrap mr-4">
      <span class="mr-2">Tel: </span>
      <span class="text-white" style="color: #ffffff !important">{{$tel->v_value ?? ''}}</span>
    </div>
        <div class="tel d-flex flex-wrap mr-4">
      <span class="mr-2">Fax: </span>
      <span class="text-white" style="color: #ffffff !important">{{$fax->v_value ?? ''}}</span>
    </div>
    <div class="w-100 social-mobile">
        <div class="social-netwok d-flex my-3">
          <a href="https://www.facebook.com/MBcapital.vn/" class="mr-3"><img src="{{ asset('assets/icon/facebook.png') }}" alt="facebook"></a>
          <a href="https://www.youtube.com/channel/UCx0wKi7FwqKUjrzZLuPk9hw" class="mr-3"><img src="{{ asset('assets/icon/youtube.png') }}" alt="youtube"></a>
          <a href="https://vn.linkedin.com/company/mbcapitalvn" class="mr-3"><img src="{{ asset('assets/icon/insta.png') }}" alt="insta"></a>
        </div>
        <div class="head-col">
          <h6>{{__('common.footer.alignment_of_companies')}}</h6>
        </div>
        <div class="body-col mt-3">
          <div class="dropdown my-2">
            <div class="w-100"
            style="border: 1px solid gray"
            id="dropdownMenuButton"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false">
              <div class="d-flex justify-content-between dropdown-selected">
                <div class="mblinks">
                  MB GROUP
                </div>
                <div>
                  <i class="bi bi-caret-down-fill"></i>
                </div>
              </div>
            </div>
            <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
              @foreach($listCompanyAffiliation as $companyAffiliation)
              <a class="dropdown-item text-dark" href="{{$companyAffiliation->url}}" target="blank">
                  {{ $companyAffiliation->v_value?? '' }}</a>
              @endforeach
            </div>
        </div>

        </div>
      </div>
  </div>
  <hr />
  <div class="copyright text-center" style="color: #DEE4FF!important">
    <span>Copyright 2021 MBCapital. All rights reserved</span>
  </div>
</footer><!-- End Footer -->
