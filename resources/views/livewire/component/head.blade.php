<title>@yield('title', $data ? ($data->getTitleAttribute() ?? 'MBCapital') : 'MBCapital')</title>
<meta property="og:site_name" content="MBCapital">
<meta property="og:rich_attachment" content="true">
<meta property="og:type" content="website">
<meta property="og:url" itemprop="url" content="{{Request::url()}}">
<meta property="og:image" itemprop="thumbnailUrl" content="{{ asset('assets/icon/favicon.ico') }}">
<meta property="og:image:width" content="800">
<meta property="og:image:height" content="354">
<meta content="@yield('meta_title', $data ? ($data->getMetaAttribute() ?? '') : 'meta_title')" property="og:title" itemprop="headline">
<meta content="@yield('meta_description', $data ? ($data->getDescriptionAttribute() ?? '') : 'meta_description')" property="og:description" itemprop="headline">
<meta content="@yield('keywords', $data ? ($data->getKeywordsAttribute() ?? '') : 'keywords')" name="keywords">
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-HK35GSGK5F"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-HK35GSGK5F');
</script>
