<header id="header">
    <div class="container d-flex align-items-center justify-content-between" style="height:100%">
      <div class="logo">
        <a href="{{ route('home') }}">
          <img src="../assets/img/logo.png" alt="">
        </a>
      </div>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt=""></a>-->
      <nav class="btn-login-xs">
        <ul  class="d-flex">
          <li>
            <a class="color-g nav-link " href="https://online.mbcapital.com.vn/sso/login">
              <span class="color-g p-2">{{__('common.header.login')}}</span>
            </a>
          </li>
          <li>
            <a class="color-w nav-link" href= {{$signUpLink}}>
              <span class="text-white p-2">{{__('common.header.open_account')}}</span>
            </a>
          </li>
        </ul>
      </nav>
      <div class="menu-mbbond">
        <nav id="navbar" class="navbar">
          <ul>
            <!-- <li><a class="nav-link scrollto active" href="#hero">Quản lý quỹ</a></li> -->
            <li class="dropdown"><a href="{{route('page.fund.index')}}"><span>{{__('common.header.manager_fund')}}</span> <i class="bi bi-chevron-down"></i></a>
              <ul style="background-color: #0a1e40;">
              @foreach($listFund as $fund)

              @if(!$fund->parent_id)
              <li><a style="justify-content: start;" href="{{route('page.fund.detail',['slug'=>$fund->slug ?? 'default'])}}">
                      {{$fund->fullname2}}</a></li>
              @endif
              @endforeach
              </ul>
            </li>
            <li class="dropdown"><a href="#"><span>{{__('common.header.investment_trust')}}</span> <i class="bi bi-chevron-down"></i></a>
              <ul style="background-color: #0a1e40;">
                <li><a style="justify-content: start;" href="{{route('page.trust.personal')}}">{{__('common.header.individual_customer')}}</a></li>
                <li><a style="justify-content: start;" href="{{route('page.trust.index')}}">{{__('common.header.institutional_customer')}}</a></li>
              </ul>
            </li>
            <li><a class="nav-link scrollto" href="{{route('page.investors.index')}}">{{__('common.header.investor_relations')}}</a></li>
            <li class="dropdown"><a href="#"><span>{{__('common.header.customer_support')}}</span> <i class="bi bi-chevron-down"></i></a>
              <ul style="background-color: #0a1e40;">
                <li><a style="justify-content: start;" href="{{route('page.investment.index')}}">{{__('common.header.investment_knowledge')}}</a></li>
                <!-- <li><a style="justify-content: start;" href="#">Công cụ hỗ trợ</a></li> -->
                <li><a style="justify-content: start;" href="{{route('page.tool.support.index')}}">{{__('common.header.support_tool')}}</a></li>
                <li><a style="justify-content: start;" href="{{route('page.instruction.index')}}">{{__('common.header.investment_guide')}}</a></li>
                <li><a style="justify-content: start;" href="{{route('page.faq.index')}}">FAQ</a></li>
                <li class="menu-support"><a style="justify-content: start;" href="{{route('page.tool.support.index')}}">{{__('common.header.support_tool')}}</a></li>
              </ul>
            </li>
            <li>
              <a class="nav-link" href="https://online.mbcapital.com.vn/sso/login">
                <span class="btn custom-btn-op1 custom-btn-op1-outline p-2">{{__('common.header.login')}}</span>
              </a>
            </li>
            <li>
              <a class="nav-link" href= {{$signUpLink}}>
                <span class="btn custom-btn-op1 custom-btn-op1-fullfill text-white p-2">{{__('common.header.open_account')}}</span>
              </a>
            </li>
            <!-- menu mobile -->
            <hr class="hide-desktop">
              <li class="hide-desktop"> <a href="{{route('page.mb.about')}}" class="text-capitalize">{{__('common.topbar.about_MB')}}</a></li>
              <li class="hide-desktop"> <a href="{{route('page.news.index')}}" class="text-capitalize" >{{__('common.topbar.news')}}</a></li>
              <li class="hide-desktop">  <a href="{{ route('page.contact.index') }}" class="text-capitalize">{{__('common.topbar.contact')}}</a></li>
              <li class="hide-desktop"> <a href="{{route('page.mb.recruitment.index')}}" class="text-capitalize">{{__('common.footer.environments_work')}}</a></li>
              <li class="hide-desktop"> <a href="{{route('page.search.index')}}" class="text-capitalize"><img src="{{asset('assets/icon/search-white.svg')}}" alt="">{{__('search.search')}}</a></li>
              <hr class="hide-desktop">
              <div class="transt-EN">
                <p class="color-lg mb-0 size14">Lựa chọn ngôn ngữ</p>
                @if (\Illuminate\Support\Facades\App::isLocale('en'))
                  <a class="" href="{{ route('lang', ['lang' => 'vi']) }}">
                  <img src="{{ asset('assets/icon/VN.png') }}" alt="">
                  </a>
                @else

                  <a class="" href="{{ route('lang', ['lang' => 'en']) }}">
                    <img style="width: 28px;height: 20px" src="{{ asset('assets/icon/EN.png') }}" alt="">
                  </a>
                @endif

                </div>
                {{-- <div class="transt-EN">
                <a  class="" href="{{ route('lang', ['lang' => 'en']) }}" ><img src="{{ asset('assets/icon/au.png') }}" alt=""></a>
                </div> --}}
                {{-- <div style="border: 1px solid #E5E5E5; height: 15px;"></div>
                --}}
          </ul>
          <i class="bi bi-list mobile-nav-toggle"></i>
        </nav>
      </div>
    </div>
  </header><!-- End Header -->
