<div class="row">
    @if($fund_master_type == 10)
        <div class="col-md-3 select">
          <select class="form-select bg-yellow" aria-label="Default select example" wire:model="itemSelect">
            <option selected="">{{__('fund.document_report_file.select_document')}}</option>
            @foreach($data as $item)
                <option value="{{$item}}">{{$item->note}}</option>
            @endforeach
          </select>
          <span class="icon pr-3 pt-1"><img src="/assets/icon/arrow-down.svg" alt=""></span>
        </div>
        <div class="col-md-4">
          <a href="javascript:void(0)" wire:click="downloadFundDocumentation"><img class="mr-2" src="/assets/img/download.png">{{__('fund.download')}}</a>
        </div>
    @elseif($fund_master_type == 11)
        <div class="col-md-12 mb-3">
            <div class="row">
                <div class="col-md-2">
                    <input class="datepicker-document form-control mb-2" type="text" readonly value="{{$currentMonth}}" style="height: 50px; font-size: 18px;">
                </div>
                <div class="col-md-3">
                    <a href="javascript:void(0)" wire:click="downloadInvestorReportFile"><img class="mr-2" src="/assets/img/download.png">{{__('fund.download')}}</a>
                </div>
            </div>
        </div>
    @else
        <div class="col-md-12 mb-3">
            <div class="row">
                <div class="col-md-2">
                    <input class="datepicker-document form-control" type="text" readonly value="{{$currentMonth}}" style="height: 50px; font-size: 18px;">
                </div>
                <div class="col-md-3">
                    <a href="{{ !empty($data->url) ? asset('storage/' . $data->url) : 'javascript:void(0)' }}" download=""><img class="mr-2" src="/assets/img/download.png">{{__('fund.download')}}</a>
                </div>
            </div>
        </div>
    @endif 
</div>
