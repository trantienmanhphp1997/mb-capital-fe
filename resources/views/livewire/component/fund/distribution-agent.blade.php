<section wire:ignore id="agent" class="agent pb-18 text-center ">
  <div class="container  aos-animate" data-aos="fade-up">
    <h3 class="title pb-md-5 text-left">@lang('mbbond.distribution_agent')</h3>
    <div class="hide_mobilde d-flex list-agent-top">
      @foreach($data as $index => $item)
          <div class="list-img"  style="width: {{$item->content == 'mbs' ? '320px' : ($item->content == 'rongviet' ? '200px' : 'auto')}};  margin: auto"  data-aos="zoom-in" data-aos-delay="100">
              <a><img src="{{Storage::url($item->img)}}" width="100%" alt="1"></a>
          </div>
      @endforeach
        <div  class="list-img" style="width: auto;  margin: auto;"   data-aos="zoom-in" data-aos-delay="100">
            <a href="https://www.mbbank.com.vn/maps/"><b style="font-size: 25px">{{__('mbbond.find_out_more_information_about_the_fund_at_the_branch')}}</b>&nbsp;<img src="{{ asset('assets/img/MBlogo.png') }}" width="100px" alt="1" style="margin-bottom:10px">&nbsp;<b style="font-size: 25px">{{__('mbbond.nearest')}}</b></a>
        </div>
    </div>
    <div class="row agent-mobile">
       <ul>
      @foreach($data as $index => $item)
          <li  style="width: {{$item->content == 'mbs' ? '320px' : ($item->content == 'rongviet' ? '200px' : 'auto')}}; margin: auto;padding: 0px 20px;">
            <a ><img src="{{Storage::url($item->img)}}" width="100%" alt="1"></a>
          </li>
      @endforeach
      </ul>
    </div>
    <div class="row agent-mobile agent-mobile2">
       <ul>
      @foreach($data as $index => $item)
          <li  style="width: {{$item->content == 'mbs' ? '320px' : ($item->content == 'rongviet' ? '200px' : 'auto')}}; margin: auto;padding: 0px 20px;">
            {{-- <a href="{{ $item->url??'#' }}"><img src="{{Storage::url($item->img)}}" width="100%" alt="1"></a> --}}
              <a><img src="{{Storage::url($item->img)}}" width="100%" alt="1"></a>
          </li>
      @endforeach
          <li class="mt-5"  style="width: {{isset($item)?($item->content == 'mbs' ? '320px' : ($item->content == 'rongviet' ? '200px' : 'auto')):'auto'}}; margin: auto;padding: 0px 20px;">
              <a href="https://www.mbbank.com.vn/maps/"><b style="font-size: 16px">{{__('mbbond.find_out_more_information_about_the_fund_at_the_branch')}}</b>&nbsp;<img src="{{ asset('assets/img/MBlogo.png') }}" width="100px" alt="1" style="margin-bottom:10px">&nbsp;<b style="font-size: 16px">{{__('mbbond.nearest')}}</b></a>
          </li>
      </ul>
    </div>
  </div>
</section>
