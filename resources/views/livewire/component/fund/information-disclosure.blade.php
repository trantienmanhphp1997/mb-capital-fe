<section>
              <div wire:loading class="loader"></div>

    <div class="row aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
        @if($show_filter)
            <div class="row ">
                <div class="col-md-2 mb-4">
                  <input class="datepicker-info form-control" type="text" readonly value="{{$currentMonth}}" style="height: 50px; font-size: 18px">
                </div>
            </div>
        @endif
         @foreach($fileList as $index => $item)
            @if($index % 2 == 0)
                <div class="col-lg-5">
                    <div class="icon-box d-flex">
                      <div class="icon">
                        <!-- <img src="{{$item->img}}" alt="1"> -->
                        <img src="/assets/img/Vector.png" alt="1">
                      </div>
                      <div class="investment-box ">
                        <h4><a href="{{ !empty($item->file_path) ? asset('storage/' . $item->file_path) : 'javascript:void(0)' }}">{{$item->title_vi}}{{--<i class="bi bi-box-arrow-down color-g"></i>--}}</a></h4>
                        <p>{{reFormatDate($item->public_date,'d/m/Y')}}</p>
                      </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            @else
                <div class="col-lg-5">
                    <div class="icon-box d-flex">
                      <div class="icon">
                        <!-- <img src="{{$item->img}}" alt="1"> -->
                        <img src="/assets/img/Vector.png" alt="1">
                      </div>
                      <div class="investment-box ">
                        <h4><a href="{{ !empty($item->file_path) ? asset('storage/' . $item->file_path) : 'javascript:void(0)' }}">{{$item->title_vi}}{{--<i class="bi bi-box-arrow-down color-g"></i>--}}</a></h4>
                        <p>{{reFormatDate($item->public_date,'d/m/Y')}}</p>
                      </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
    @if(count($fileList) > 0)
        {{$fileList->links()}}
    @endif
</section>
