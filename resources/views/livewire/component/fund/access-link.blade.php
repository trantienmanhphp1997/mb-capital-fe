<div class="d-flex pt-md-2">
    @foreach($data as $index => $item)
        @if($index == 0)
            <a href="{{$item->url}}" class="btn-get-started scrollto bg-gold">{{$item->title}}</a>
        @else
            <a href="{{$item->url}}" class="btn-get-started scrollto color-g border-gold">{{$item->title}}</a>
        @endif
    @endforeach           
</div>