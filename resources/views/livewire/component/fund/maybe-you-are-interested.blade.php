<section wire:ignore id="package" class="package pb-18">
    @if(!$home_page)
          <h3 class="title pb-md-5 op8">{{__('fund.maybe_you_are_interested.title')}}</h3>
    @endif
  <div class="container  aos-animate" data-aos="fade-up">
     <div class="row">
        @foreach($funds as $item)
            <div class="col-lg-4 col-md-6 mb-4 text-center" data-aos="zoom-in" data-aos-delay="100">
                <div class="icon-box ">
                  <h4 class="color-g fw-bolder">{{$item->shortname}}</h4>
                  <h5 class="color-dark fw-bolder">{{$item->fullname}}</h5>
                  <p class="size16 color-dark">{!! sanitizationText($item->description)!!}</p>
                  <a href="{{route('page.fund.detail', ['slug' => $item->slug ?? 'default'])}}" class="color-g size18"><i class="bi bi-arrow-right"></i> {{__('fund.view_more')}}</a>
                </div>
              </div>
        @endforeach
        @if($uy_thac)
            <div class="col-lg-4 col-md-6 mb-4 text-center" data-aos="zoom-in" data-aos-delay="100">
                <div class="icon-box ">
                    <h4 class="color-g fw-bolder">{{__('fund.maybe_you_are_interested.investment_trust')}}</h4>
                    <h5 class="color-dark fw-bolder">MB Capital Private</h5>
                    <p class="size16 color-dark">{{__('fund.maybe_you_are_interested.content')}}</p>
                    <a href="{{route('page.trust.personal')}}" class="color-g size18"><i class="bi bi-arrow-right"></i> {{__('fund.view_more')}}</a>
                </div>
            </div>
        @endif
    </div>
  </div>
</section>
