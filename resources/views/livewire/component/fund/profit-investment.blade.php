<section wire:ignore >
    <table class="table table-bond table-bordered hide_mobilde">
        <thead>
            <tr>
                <th scope="col" class="text-center"></th>
                <th scope="col" class="text-center">{{ __('fund.profit_investment.name', ['name' => ($fund_id == 2) ? '& Index' : '']) }}</th>
                <th scope="col" class="text-center">{{ __('fund.profit_investment.one_month') }}</th>
                <th scope="col" class="text-center">{{ __('fund.profit_investment.three_month') }}</th>
                <th scope="col" class="text-center">{{ __('fund.profit_investment.start_year') }}</th>
                <th scope="col" class="text-center">{{ __('fund.profit_investment.twelve_month') }}</th>
                <th scope="col" class="text-center">{{ __('fund.profit_investment.found', ['date' => ($public_date != '' ? $public_date : (($fund_id == 2) ? '25/04/2014' : '04/08/2020'))]) }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dataTable as $row)
            <tr>
                <td class="text-center">{{ $row['name'] }}</td>
                <td class="text-center">{{ numberFormat($row['data']['now']) }}</td>
                <td class="text-center">{{ $row['data']['one_month']?numberFormat($row['data']['one_month']): __('fund.have_no_data') }}</td>
                <td class="text-center">{{ $row['data']['three_month'] ? numberFormat($row['data']['three_month']) :  __('fund.have_no_data') }}</td>
                <td class="text-center">{{ $row['data']['start_year'] ? numberFormat($row['data']['start_year']) :  __('fund.have_no_data') }}</td>
                <td class="text-center">{{ $row['data']['twelve_month'] ? numberFormat($row['data']['twelve_month']) :  __('fund.have_no_data')}}</td>
                <td class="text-center">{{ $row['data']['found'] ? numberFormat($row['data']['found']) : __('fund.have_no_data') }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
                @foreach ($dataTable as $row)
                    <div class="table-mbbond-responsive">
                        <p class="size14">{{ $row['name'] }}</p>
                        <hr>
                    </div>
                    <div class="table-mbbond-responsive bieuphi">
                        <p class="size14"><span></span> {{ __('fund.profit_investment.name', ['name' => ($fund_id == 2) ? '& Index' : ''])}}</p>
                        <p class="size14"><b>{{ numberFormat($row['data']['now']) }}</b></p>
                        <p class="size14"> <span></span>{{ __('fund.profit_investment.one_month') }}		</p>
                        <p class="size14"><b>{{ $row['data']['one_month']?numberFormat($row['data']['one_month']):'' }}</b></p>
                        <p class="size14"> <span></span>{{ __('fund.profit_investment.three_month') }}</p>
                         <p class="size14"><b>{{ numberFormat($row['data']['three_month']) }}</b></p>
                        <p class="size14"> <span></span>{{ __('fund.profit_investment.start_year') }}</p>
                        <p class="size14"> <b>{{ numberFormat($row['data']['start_year']) }}</b></p>
                        <p class="size14"><span></span>{{ __('fund.profit_investment.twelve_month') }}</p>
                        <p class="size14"> <b>{{ numberFormat($row['data']['twelve_month']) }}</b></p>
                        <p class="size14"><span></span>{{ __('fund.profit_investment.found', ['date' => ($public_date != '' ? $public_date : (($fund_id == 2) ? '25/04/2014' : '04/08/2020'))]) }}</p>
                        <p class="size14"> <b>{{ numberFormat($row['data']['found']) }}</b></p>
                    </div>
                 @endforeach
</section>
