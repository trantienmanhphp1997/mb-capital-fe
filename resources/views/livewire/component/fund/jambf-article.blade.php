<div class="pt-5">
    @foreach($jambfs as $i => $item)
        <div data-aos="zoom-in" data-aos-delay="100" class="{{ 'py-3 ' . ($i == count($jambfs) - 1 ? '' : 'border-bottom' ) }}">
            <div class="row" href="#">
                <div class="col-md-3 col-xs-12">
                    <div class="item-image" style="
                        background-image:url({{ $item->file_path ? asset('storage/'.$item->file_path) : '/assets/img/jambf.png'}});
                        background-position: center;
                        background-repeat: no-repeat;
                        background-size: cover;
                        padding-bottom: 60%;"
                    >
                    </div>
                </div>
                <div class="col-md-9 col-xs-12 pt-3 pt-md-0">
                    <a href="{{$item->url ?? '#'}}" target="_blank" class="title-post fw-regular jambf-title"><h3 class="h4">{{$item->title_vi}}</h3></a>
                    <div class="pb-3">{{ ReFormatDate($item->release_date,'d-m-Y') }}</div>
                    <?php
                        $content = preg_replace('/<(?:[^\>]+)>/', ' ', ($item->content_vi ?: ''));
                        $content = preg_replace('/\s+/', ' ', $content);
                    ?>
                    <div class="color-dark jambf-content">{{ strlen($content) > 250 ? substr($content, 0, 250) . '...' : $content }}</div>

                    <div class="mt-3 d-flex align-items-center justify-content-between">
                        <a class="read-more color-g" target="_blank" href="{{$item->url ?? '#'}}">
                            <i class="bi bi-arrow-right"></i> {{__('fund.view_more')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    @if(count($jambfs) > 0)
        {{$jambfs->links()}}
    @endif
</div>
