<section id="utilities" class="utilities pb-xxl-4">
    <div class="container">
        <h3 class="title-bond pb-xxl-5">{{__('fund.line_chart_nav.nav_growth')}}</h3>
        <div class="filter-bar d-flex">
            <div id="buttonDIV" wire:ignore>
                <button type="button" class="btn time-filter mr-2 mt-2" wire:click="timeChart('1m')">1{{__('fund.line_chart_nav.month')}}</button>
                <button type="button" class="btn time-filter mr-2 mt-2" wire:click="timeChart('3m')">3{{__('fund.line_chart_nav.month')}}</button>
                <button type="button" class="btn time-filter mr-2 mt-2" wire:click="timeChart('6m')">6{{__('fund.line_chart_nav.month')}}</button>
                <button type="button" class="btn time-filter mr-2 mt-2" wire:click="timeChart('YTD')">YTD</button>
                <button type="button" class="btn time-filter mr-2 mt-2" wire:click="timeChart('1Y')">1{{__('fund.line_chart_nav.year')}}</button>
                <button type="button" class="btn time-filter active mr-2 mt-2" wire:click="timeChart('all')">{{__('fund.line_chart_nav.all')}}</button>
            </div>
            <div wire:loading class="loader"></div>
            <div class="range-time d-flex flex-wrap align-items-center mr-4 ">
                <div class="from mr-2 d-flex flex-wrap align-items-center mt-2">
                    <label for="birthdaytime" class="w-20">{{__('fund.line_chart_nav.from')}}</label>
                    <input type="date" id="from_date" name="from_date" wire:model.defer="fromDate" class="custom-input-time  w-80">                    
                </div>
                <div class="to d-flex flex-wrap align-items-center mt-2 mr-3">
                    <label for="birthdaytime" class="w-20">{{__('fund.line_chart_nav.to')}}</label>
                    <input type="date" id="to_date" name="to_date" wire:model.defer="toDate" class="custom-input-time w-80">
                </div>
                <button type="button" class="btn custom-btn-op1 custom-btn-op1-fullfill mt-2" wire:click="search()"> {{__('fund.line_chart_nav.search')}}</button>
            </div>			
        </div>
        <div class="chart" style="position: relative; width: 100%; height: 400px; margin: 3% 0 15% 0; ">
            <canvas id="line1"></canvas>
            <div class="mt-3 d-flex justify-content-center" id="line-chart-nav-legend-container"></div>
        </div>
    </div>
</section>


<script>

    document.addEventListener('DOMContentLoaded', function() {
        $('.btn').on('click', function(){
            $('.btn').removeClass('active');
            $(this).addClass('active');
        });
        window.addEventListener('load', drawChart);

        function drawChart() {
            var myChart3;
            Livewire.emit('getChart');
            Livewire.on('showChart', function(json) {
                const datasets = JSON.parse(json);
                let otherDatasets = [];
                if(datasets.length > 1) {
                    otherDatasets = JSON.parse(json);

                    let smallestDate = new Date();
                    if(otherDatasets[0].data) {
                        smallestDate = new Date(otherDatasets[0].data[otherDatasets[0].data.length - 1].x);
                    }


                    // tìm ngày cuối cùng bé nhất trong dataset

                    for(let i = 0; i < otherDatasets.length; i++) {
                        if(otherDatasets[i].data) {
                            let date = new Date(otherDatasets[i].data[otherDatasets[i].data.length - 1].x);
                            if(smallestDate > date) smallestDate = date;
                        }
                    }

                    otherDatasets.forEach(set => {
                        if(set.data) {
                            let lastIndex = set.data.findIndex(item => new Date(item.x) >= smallestDate);
                            set.data = set.data.slice(0, lastIndex + 1);
                        }
                    });
                }

                var line1 = document.getElementById('line1').getContext("2d");
                if (myChart3 != undefined) myChart3.destroy();
                myChart3 = new Chart(line1, {
                    plugins: [HTMLLegendPlugin],
                    type: 'line',
                    data: {
                        datasets: datasets
                    },
                    options: {
                        pointRadius: 0,
                        pointHitRadius: 10,
                        pointHoverRadius: 7,
                        plugins: {
                            htmlLegend: {
                                containerID: 'line-chart-nav-legend-container',
                                onclick: function (item, items, chart) {
                                    console.log(datasets);
                                    console.log(otherDatasets);
                                    if (datasets.length > 1) {
                                        let displayAll = items.every(el => !el.hidden);
                                        chart.data.datasets = displayAll ? otherDatasets : datasets;
                                        chart.data.datasets[item.datasetIndex].hidden = item.hidden;
                                    }
                                }
                            },
                            legend: {
                                display: false
                            },
                            tooltip: {
                                callbacks: {
                                    label: function(context) {
                                        let raw = context.raw;
                                        let label = formatNumber(raw.amount) + "{{__('fund.vnd')}}, "
                                        if (raw.y > 0) {
                                            label += "{{__('fund.line_chart_nav.growth')}} " + formatNumber(raw.y) + '%';
                                        } else if (raw.y == 0) {
                                            label += "{{__('fund.line_chart_nav.stable')}} " + formatNumber(raw.y) + '%';
                                        } else {
                                            label += "{{__('fund.line_chart_nav.reduce')}} " + formatNumber(raw.y) + '%';
                                        }
                                        return label;
                                    }
                                }
                            }
                        },
                        scales: {
                            // xAxes: [{
                            //     categoryPercentage: 1.0,
                            //     barPercentage: 1.0
                            // }],
                            y: {
                                ticks: {
                                    callback: function(value) {
                                        var locale = $('html').attr('lang');
                                        if(locale == 'en') return (Math.round(value * 100) / 100 + '%');
                                        return (Math.round(value * 100) / 100 + '%').replace('.',',');
                                    }
                                }
                            },
                            x: {
                                type: 'time',
                                time: {
                                    unit: 'day',
                                    displayFormats: {
                                        day: 'DD/MM/YYYY'
                                    },
                                },
                                grid: {
                                    display: false,
                                }
                            }
                        }
                    }
                });

            })
        }
    })
</script>
