<div class="paragraph item-three contact advice" wire:ignore id="contact-advise">
    <div class="box">
        <div class="container">
            <h3 class="title">{{__('home.investment_advice_title')}}</h3>
            <span class="sub-title">{{__('home.investment_advice_subtitle')}}</span>
            <div class="form-contact d-flex justify-content-center mt-3" data-aos="fade-up" data-aos-delay="500">
            <div class="mr-3 mt-3">
                <input type="text" wire:model.defer="nameCustomer" id="nameCustomer" onchange="myFunction()" class="form-control input-custom-ct" placeholder="{{__('home.advise.name')}}">
                <!-- @error('nameCustomer')
                    <div class="text-danger mt-1">{{$message}}</div>
                @enderror -->
            </div>
            <div class="mr-3 mt-3">
                <input type="text" wire:model.defer="emailAndPhoneCustomer" id="phoneCustomer" class="form-control input-custom-ct" placeholder="{{__('home.advise.email_or_phone')}}">
                <!-- @error('emailAndPhoneCustomer')
                    <div class="text-danger mt-1">{{$message}}</div>
                @enderror -->
            </div>
            <div class="mr-3 mt-3 select position-relative">
                <select  wire:model.defer="contentToAdviseId" id="optionCustomer" class="form-control input-custom-ct">
                    <option style="background-color: white !important;" value={{null}}>{{__('home.advise.content')}}</option>
                    @foreach($listContentToAdvise as $item)
                        <option style="background-color: white !important;" value="{{$item->id}}">{{$item->v_content}}</option>
                    @endforeach
                </select>
                <!-- <input type="text" wire:model.defer="" class="form-control input-custom" placeholder="{{__('home.advise.content')}}"> -->
                <span class="icon"><img src="{{asset('assets/icon/arrow-down.svg')}}" alt=""></span>
                <!-- @error('contentToAdvise')
                    <div class="text-danger mt-1">{{$message}}</div>
                @enderror -->
            </div>
            <button wire:click="saveRequest" id="submit" disabled  class="btn custom-btn-op1 custom-btn-op1-fullfill text-uppercase mt-3">
                {{__('home.advise.send_request')}}
            </button>
            </div>
        </div>
    </div>
</div>
<script>
document.getElementById("nameCustomer").onchange = function() {myFunction()};
document.getElementById("phoneCustomer").onchange = function() {myFunction()};
document.getElementById("optionCustomer").onchange = function() {myFunction()};
function myFunction() {
if(document.getElementById("nameCustomer").value=="" && document.getElementById("phoneCustomer").value=="")
{
 document.getElementById("submit").disabled = "disabled";
 } else
 {
 document.getElementById("submit").disabled = false;
 }
}
</script>