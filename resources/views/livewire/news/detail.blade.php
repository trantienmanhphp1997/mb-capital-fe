@section('title', $news->name_vi ?? 'MBCapital')
@section('meta_title', $news->meta_title_vi ?? '')
@section('meta_description', $news->meta_des_vi ?? '')
<!-- ======= Hero section ======= -->
<section id="news" class="d-flex align-items-center">
    <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100">
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">{{__('news.home_page')}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{__('news.news')}}</li>
                </ol>
            </nav>
            <h1 class="title-detail fw-bolder color-w">“{{$news->name_vi}}”</h1>
            <?php
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $timeEng = ['Sun','Mon','Tue','Wed', 'Thu', 'Fri', 'Sat'];
                $timeVie = ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'];
            ?>
            @if ($locale == 'vi')
                <div class="detail-meta">{{str_replace($timeEng, $timeVie, date('D, d/m/Y', strtotime($news->date_submit?$news->date_submit:$news->created_at)))}}</div>
            @else
                <div class="detail-meta">{{date('D, d/m/Y', strtotime($news->date_submit?$news->date_submit:$news->created_at))}}</div>
            @endif
        </div>
    </div>
</section>
<main id="main">
    <div class="news-list">
        <div class="container">
            <div class="box-detail pt-50">
                {!! sanitizationText($news->content_vi) !!}
            </div>
            @if($news->author)
            <div class="text-right pr-5 pt-3 color-second size18">
                {{__('news.author')}}: <span>{{$news->author}}</span>
            </div>
            @endif
            <h2 class="title-news fw-bolder color-second my-md-5">{{__('news.related_news')}}</h2>
            <div class="related-news">
                <div class="row">
                    @foreach ( $moreNews as $key => $news)
                    <div class="col-lg-5 mb-5">
                        <h4 class="title-post fw-regular">
                            <a href="{{ route('page.news.details.index', ['slug' => $news->slug])}}"
                                class="">{{$news->name_vi}}</a>
                        </h4>
                        <div class="post-date">
                            {{date('d/m/Y', strtotime($news->date_submit?$news->date_submit:$news->created_at))}}
                        </div>
                        <a href="{{ route('page.news.details.index', ['slug' => $news->slug])}}"
                            class="read-more color-g size18"><i class="bi bi-arrow-right"></i> {{__('news.hot_news.see_more')}}</a>
                    </div>
                    <div class="col-lg-1">
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</main><!-- End #main -->
