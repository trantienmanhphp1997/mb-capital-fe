<!-- ======= Hero section ======= -->
<section id="news" class="d-flex align-items-center">
    <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100">
        <div class="row">
            <div class="col-md-12">
                <h1 class="title-news fw-regular color-w">{{__('news.news')}}</h1>
            </div>    
        </div>
    </div>
</section>
<main id="main">
    <div class="news-list">
        <div class="container">
            <h2 class="title-news fw-bolder color-second my-md-5">{{__('news.hot_news.title')}}</h2>
            <div class="col-md-12">
                <div class="row">
                    @if($breakingNews->image)
                        <div class="col-lg-12 col-xl-6  pr-0 post-image">
                            <img src="{{$breakingNews->image}}" style="height: 100%;">
                        </div>
                    @endif
                    <div class="{{$breakingNews->image ? 'col-lg-12 col-xl-6 ' : 'col-lg-12'}} post-text">
                        @if($breakingNews)
                            <h3 class="title-post fw-bolder">
                                <a href="{{ route('page.news.details.index', ['slug' => $breakingNews->slug])}}" class="color-w">“{{$breakingNews->name_vi}}”</a>
                            </h3>
                            <div class="post-date">
                                {{($breakingNews->date_submit) ? date('d/m/Y', strtotime($breakingNews->date_submit)) : date('d/m/Y', strtotime($breakingNews->created_at))}}
                            </div>
                            <div class="post-excerpt">
                                {!!sanitizationText($breakingNews->intro_vi)!!}
                             </div>
                            <a href="{{ route('page.news.details.index', ['slug' => $breakingNews->slug])}}" class="read-more color-g size18"><i class="bi bi-arrow-right"></i> {{__('news.hot_news.see_more')}}</a>
                        @elseif(!$breakingNews)
                            <p>{{__('news.hot_news.no_data')}}</p>
                        @endif
                    </div>
                </div>
            </div>
            <h2 class="title-news fw-bolder color-second my-md-5">{{__('news.related_news')}}</h2>
            <div class="related-news">
                @foreach ( $listNews as $key => $news)
                    @if( $loop->index % 2 == 0)
                        <div class="row">
                    @endif
                    <div class="col-lg-5">
                        <h4 class="title-post fw-regular">
                            <a href="{{ route('page.news.details.index', ['slug' => $news->slug])}}" class="">{{$news->name_vi}}</a>
                        </h4>
                        <div class="post-date">
                            {{($news->date_submit)?date('d/m/Y', strtotime($news->date_submit)):date('d/m/Y', strtotime($news->created_at))}}
                        </div>
                        <a href="{{ route('page.news.details.index', ['slug' => $news->slug])}}" class="read-more color-g size18"><i class="bi bi-arrow-right"></i> {{__('news.hot_news.see_more')}}</a>
                    </div>
                    @if( $loop->index % 2 == 0)
                        <div class="col-lg-1"></div>
                    @endif
                    @if( $loop->index % 2 != 0)
                        <hr class="col-lg-10 my-md-4">
                    @endif
                    @if($loop->index % 2 != 0)
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</main><!-- End #main -->
