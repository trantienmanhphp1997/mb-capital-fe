<div>
    <section id="banner-investment" class="d-flex align-items-center">
        <div class="container" data-aos="zoom-out" data-aos-delay="100" wire:ignore>
            <div class="row mb-md-5">
                <h1 class="titele color-w fw-regular mb-md-5">{{__('investment.professional_investor.title')}}</h1>
            </div>
        </div>
    </section>
    <main id="main">
        <div class="container">
            <div class="tab-bond">
                <ul class="nav nav-pills mb-3 d-flex" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation" wire:ignore>
                        <button class="nav-link active  w-100" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">{{__('tool-support.calculation_tool')}}</button>
                    </li>
                    <li class="nav-item" role="presentation" wire:ignore>
                        <button class="nav-link w-100" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">{{__('investment.professional_investor.product_portfolio')}}</button>
                    </li>
                    <li class="nav-item" role="presentation" wire:ignore>
                        <button class="nav-link w-100" id="pills-other-tab" data-bs-toggle="pill" data-bs-target="#pills-other" type="button" role="tab" aria-controls="pills-other" aria-selected="false">{{__('common.header.investment_guide')}}</button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" wire:ignore.self>
                @livewire('tool-support.calculation-tool')
            </div>
            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" wire:ignore.self>
                <div class="fund-list py-3">
                    <div class="container">
                        <h3 class="title-bond pb-md-5">{{__('investment.professional_investor.open_fund')}}</h3>
                        @livewire('component.fund.maybe-you-are-interested', ['include' => ['3', '2', '31'], 'home_page' => true])
                    </div>
                    <div class="container">
                        <h3 class="title-bond pb-md-5">{{__('common.header.investment_trust')}}</h3>
                        <section wire:ignore id="package" class="package pb-18">
                            <div class="container aos-animate" data-aos="fade-up">
                                <div class="row">
                                    <div class="col-lg-4 col-md-6 mb-4 text-center" data-aos="zoom-in" data-aos-delay="100">
                                        <div class="icon-box ">
                                            <h4 class="color-g fw-bolder">{{__('investment.investment_trust')}}</h4>
                                            <h5 class="color-dark fw-bolder">{{__('common.header.individual_customer')}}</h5>
                                            <p class="size16 color-dark">{{__('investment.content_investment_trust')}}</p>
                                            <a href="{{route('page.trust.personal')}}" class="color-g size18"><i class="bi bi-arrow-right"></i> {{__('fund.view_more')}}</a>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 mb-4 text-center" data-aos="zoom-in" data-aos-delay="100">
                                        <div class="icon-box ">
                                            <h4 class="color-g fw-bolder">{{__('investment.investment_trust')}}</h4>
                                            <h5 class="color-dark fw-bolder">{{__('common.header.institutional_customer')}}</h5>
                                            <p class="size16 color-dark">{{__('investment.content_investment_trust_2')}}</p>
                                            <a href="{{route('page.trust.index')}}" class="color-g size18"><i class="bi bi-arrow-right"></i> {{__('fund.view_more')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    
                    <div class="container">
                        <h3 class="title-bond pb-md-5">{{__('investment.professional_investor.retirement_programme')}}</h3>
                        @livewire('component.fund.maybe-you-are-interested', ['include' => ['4'], 'home_page' => true])
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-other" role="tabpanel" aria-labelledby="pills-other-tab" wire:ignore.self>       
                <div class="fund-list py-3">
                    <div class="container">
                        <section id="skills" class="skills mt-5 investment-steps">
                            <div class="" data-aos="fade-up">
                              <div class="paragraph item-three home-item-three">
                                <h3 class="title">{{__('home.investment_step')}}</h3>
                                <section class="ps-timeline-sec" data-aos-delay="100">
                                  <div class="container">
                                    <div class="ps-timeline-div">
                                    <ol class="ps-timeline">
                                        @foreach($listInvestmentStep as $investmentStep)
                                        <li data-aos="zoom-in-up" data-aos-delay="{{$investmentStep->order_number*200}}">
                                            <div class="@if($loop->odd) img-handler-top @else img-handler-bot @endif">
                                                <div class="content-step d-flex align-items-center">
                                                    <div class="step-number">
                                                        @if($investmentStep->order_number == 1)
                                                            <p  onclick="$('#nameCustomer' ).focus()" style="cursor:pointer"><a style="color: black;" id='step1'>{{$investmentStep->order_number}} </a></p>
                                                        @else
                                                            <p>
                                                            <a href="{{route('page.instruction.index')}}" style="cursor:pointer">
                                                                {{$investmentStep->order_number}}
                                                            </a>
                                                            </p>
                                                        @endif
                                                    </div>
                                                    <div class="content-s">
                                                        <h4>{{$investmentStep->v_value}}</h4>
                                                        <p>{!!sanitizationText($investmentStep->note)!!}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="@if($loop->odd) ps-bot @else ps-top @endif">
                                            </div>
                                            <span class="@if($loop->odd) ps-sp-top @else ps-sp-bot @endif"></span>
                                        </li>
                                    @endforeach
                                    </ol>
                                  </div>
                                  </div>
                                </section>
                              </div>
                      
                            </div>
                        </section>
                        <h5>
                            <a style="cursor: pointer;" href="{{route('page.instruction.index')}}"  class=""><i class="bi bi-arrow-right"></i> {{__('investment.instruction')}}</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- ======= Contact to  advise Section ======= -->
        @livewire('component.advise-request', ['ip_client' => Request::ip()])
        <!-- End Counts Section -->
    </main><!-- End #main -->
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>
</div>

@section('js')
    <script>
        $(function(){
            $(".tab-bond").fixTab();
            $("#pick-tab-2").click(function(){
                $("#pills-profile-tab").click();
                window.scrollTo(0,0);
            })
        });
    </script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
@endsection
