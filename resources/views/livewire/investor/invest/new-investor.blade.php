<div>
    <section id="banner-investment" class="d-flex align-items-center">
        <div class="container" data-aos="zoom-out" data-aos-delay="100" wire:ignore>
            <div class="row mb-md-5">
                <h1 class="titele color-w fw-regular mb-md-5">{{__('investment.new_investor.title')}}</h1>
            </div>
        </div>
    </section>
    <main id="main">
        <div class="container">
            <div class="tab-bond">
                <ul class="nav nav-pills mb-3 d-flex" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation" wire:ignore>
                        <button class="nav-link active  w-100" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">{{__('common.header.investment_knowledge')}}</button>
                    </li>
                    <li class="nav-item" role="presentation" wire:ignore>
                        <button class="nav-link w-100" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">{{__('investment.new_investor.determine_risk_appetite')}}</button>
                    </li>
                    <li class="nav-item" role="presentation" wire:ignore>
                        <button class="nav-link w-100" id="pills-other-tab" data-bs-toggle="pill" data-bs-target="#pills-other" type="button" role="tab" aria-controls="pills-other" aria-selected="false">{{__('tool-support.product_consulting')}}</button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" wire:ignore.self>
                <div class="container">
                    <h3 style="margin-top: 33px">{{ $first_item->v_value }}</h3>
                    {!! sanitizationText(data_get($first_item, 'note', '')) !!}
                </div>
                <div class="container">
                    <h3>{{ $second_item->v_value }}</h3>
                    {!! sanitizationText(data_get($second_item, 'note', '')) !!}
                </div>
                <div class="container">
                    <h3>{{ $last_item->v_value }}</h3>
                    {!! sanitizationText(data_get($last_item, 'note', '')) !!}
                </div>
            </div>
            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" wire:ignore.self>
                @livewire('tool-support.surveys')
            </div>
            <div class="tab-pane fade" id="pills-other" role="tabpanel" aria-labelledby="pills-other-tab" wire:ignore.self>
                @livewire('tool-support.product-consulting')
            </div>
        </div>
        <!-- ======= Contact to  advise Section ======= -->
        @livewire('component.advise-request', ['ip_client' => Request::ip()])
        <!-- End Counts Section -->
    </main><!-- End #main -->
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>
</div>

<script>
    document.getElementById('pick-tab-3').onclick = function(){
        document.getElementById('pills-other-tab').click();
        window.scrollTo(0,0);
    };
</script>

@section('js')
    <script>
        $(function(){
            $(".tab-bond").fixTab();
        });
    </script>
@endsection
