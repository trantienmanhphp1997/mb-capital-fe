<div>
    <div class="container">
        <div class="search-investment">
            <input wire:model.debounce.1000ms="searchNAV" name="searchNAV" type="text" class="form-control" placeholder="{{__('investor.search')}}">
            <span><img src="{{asset('./assets/icon/search.svg')}}" alt="search icon"></span>
        </div>
        <div class="list-report" {{--data-aos="fade-up"--}}>
            @foreach ( $reportNAV as $key => $item)
                @if( $loop->index % 2 == 0)
                    <div class="row" {{--data-aos="fade-up" data-aos-delay="200"--}}>
                @endif
                <div class="col-lg-5">
                    <div class="icon-box d-flex" >
                        <div class="icon">
                            <img src="{{asset('assets/img/excel.png')}}" alt="1">
                        </div>
                        <div class="investment-box ">
                            <h4><a {{$item->file_path ? 'target="_blank"' : '' }} href="{{$item->file_path ? asset('storage/'.$item->file_path) : '#'}}">{{$item->title_vi}}</a></h4>
                            <p>{{date('d/m/Y', strtotime($item->public_date ?? now()))}}</p>
                        </div>
                    </div>
                </div>
                @if( $loop->index % 2 == 0)
                    <div class="col-lg-2"></div>
                @endif
                @if($loop->index % 2 != 0 || $reportNAV->count() == $loop->index + 1)
                    </div>
                @endif
            @endforeach
            @if(count($reportNAV) > 0)
                {{$reportNAV->links()}}
            @endif
        </div>

    </div>
</div>
