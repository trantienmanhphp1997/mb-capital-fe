<div>
    <section id="banner-investment" class="d-flex align-items-center">
        <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100" wire:ignore>
            <div class="row mb-md-5">
                <div class="col-md-12">
                    <h1 class="titele color-w fw-regular mb-md-5">{{__('investor.investor_relations')}}</h1>
                </div>    
                <div class="col-sm-12 col-md-3">
                    <select wire:model.defer="year" name="year" class="form-select input-custom" >
                        @foreach ($yearHasValue as $key => $item)
                            <option value="{{$item}}">{{$item}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-12 col-md-3">
                    <select wire:model.defer="fundid" name="fundid" class="form-select input-custom" >
                        @foreach ( $funds as $key => $item)
                            <option value="{{$item->id}}">{{$item->shortname}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-12 col-md-2 pb-5">
                    <button wire:click="filterData()" class="btn custom-btn-op1 custom-btn-op1-fullfill text-uppercase">
                        {{__('investor.select')}}
                    </button>
                </div>
            </div>
        </div>
    </section>
    <main id="main">
        <div class="container">
            <div class="tab-bond">
                <ul class="nav nav-pills mb-3 d-flex" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation" wire:ignore>
                        <button class="nav-link active w-100" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">{{__('investor.net_access_value_report')}}</button>
                    </li>
                    <li class="nav-item" role="presentation" wire:ignore>
                        <button class="nav-link w-100" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">{{__('investor.activity_report')}}</button>
                    </li>
                    <li class="nav-item" role="presentation" wire:ignore>
                        <button class="nav-link w-100" id="pills-other-tab" data-bs-toggle="pill" data-bs-target="#pills-other" type="button" role="tab" aria-controls="pills-other" aria-selected="false">{{__('investor.publish_other_news')}}</button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" wire:ignore.self>
                @livewire('investor.nav',['fundid' => $fundid, 'year' => $year])
            </div>
            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" wire:ignore.self>
                @livewire('investor.activity',['fundid' => $fundid, 'year' => $year])
            </div>
            <div class="tab-pane fade" id="pills-other" role="tabpanel" aria-labelledby="pills-other-tab" wire:ignore.self>
                @livewire('investor.other',['fundid' => $fundid, 'year' => $year])
            </div>
        </div>
        <!-- ======= Contact to  advise Section ======= -->
        @livewire('component.advise-request', ['ip_client' => Request::ip()])
        <!-- End Counts Section -->
    </main><!-- End #main -->
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>
</div>

@section('js')
    <script>
        $(function(){
            $(".tab-bond").fixTab();
        });
    </script>
@endsection
