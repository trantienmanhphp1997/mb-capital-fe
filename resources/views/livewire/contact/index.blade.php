<div>
    <!-- ======= Hero section ======= -->
    <section id="news" class="d-flex align-items-center">
        <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <h1 class="title-news fw-regular color-w">{{__('contact.contact')}}</h1>
                </div>
            </div>
        </div>
    </section>
    <main id="main">
        <div class="news-list pb-xxl-5">
            <div class="container">
                <h2 class="title-news fw-bolder color-second my-md-5">{{__('contact.contact_information.title')}}</h2>
                <div class="contac-info d-flex justify-content-between">
                    <div class="text-info-contact">
                        <div class="company-info mb-4">
                            <h4 class="fw-bolder color-second text-uppercase">{{__('contact.contact_information.company_name')}}</h4>
                            <h5 class="fw-bolder color-second">{{__('contact.contact_information.head_office')}}</h5>
                            <p class="color-second">{{__('contact.contact_information.address')}}</p>
                            <p class="color-second">Tel: (84) 24 3726 2808</p>
                            <p class="color-second">Fax: (84) 24 3726 2810</p>
                            <p class="color-second">Email: ir@mbcapital.com.vn</p>
                        </div>
                        <hr>
                        <div class="office-info mt-4">
                            <div class="social-netwok d-flex">
                                <a href="https://www.facebook.com/MBcapital.vn/" class="mr-3"><img src="assets/icon/fb-black.png" alt="facebook"></a>
                                <a href="https://www.youtube.com/channel/UCx0wKi7FwqKUjrzZLuPk9hw" class="mr-3"><img src="assets/icon/youtube-black.png" alt="youtube"></a>
                                <a href="https://vn.linkedin.com/company/mbcapitalvn" class="mr-3"><img src="assets/icon/insta-black.png" alt="insta"></a>
                            </div>
                        </div>
                    </div>
                    <div class="map pl-4">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1862.0511407268282!2d105.83006980059027!3d21.02859308739916!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab9e45c99319%3A0x7487108933092db0!2zVG_DoCBuaMOgIE1CQmFuayBDw6F0IExpbmg!5e0!3m2!1svi!2s!4v1636946465110!5m2!1svi!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- ======= Contact to  advise Section ======= -->
        @livewire('component.advise-request', ['ip_client' => Request::ip()])
    </main><!-- End #main -->
</div>
