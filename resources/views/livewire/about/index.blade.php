<div>
    <!-- ======= Hero section ======= -->
    <section id="hero" class="d-flex align-items-center" style="background: url(assets/img/investment.png) top left; background-size: cover; height: 350px;">
        <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <h1>{{ __('about.about_MB') }}</h1>
                </div>
            </div>
        </div>
    </section>
    <main id="main">
        <div class="container">
            <div class="tab-bond">
                <ul class="nav nav-pills mb-3 d-flex" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link @if ($activeTab == 'about') active @endif w-100" onclick="setTopHome()" id="pills-home-tab" data-bs-toggle="pill" type="button" wire:click="setActiveTab('about')" role="tab" aria-controls="pills-home" aria-selected="true">{{ __('about.mb_overview') }}</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link @if ($activeTab == 'employee') active @endif w-100" id="pills-profile-tab" data-bs-toggle="pill" type="button" wire:click="setActiveTab('employee')" role="tab" aria-controls="pills-profile" aria-selected="false">{{ __('about.staff') }}</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link @if ($activeTab == 'investor') active @endif w-100" id="pills-contact-tab" wire:click="setActiveTab('investor')" data-bs-toggle="pill" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">{{ __('about.invenstor_relations') }}</button>
                    </li>
                </ul>
            </div>
        </div>
        <div></div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade @if ($activeTab == 'about') show active @endif" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                @livewire('about.about')
            </div>
            <div class="tab-pane fade @if ($activeTab == 'employee') show active @endif" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                @livewire('about.employee')
            </div>
            <div class="tab-pane fade @if ($activeTab == 'investor') show active @endif" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                @livewire('about.investor')
            </div>
        </div>

        <!-- ======= package ======= -->
        @livewire('component.fund.maybe-you-are-interested', ['include' => ['3', '2', '4']])
        <!-- ======= end package ======= -->
        <!-- ======= Contact to  advise Section ======= -->
        @livewire('component.advise-request', ['ip_client' => Request::ip()])
        <!-- End Counts Section -->
    </main><!-- End #main -->
</div>
@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
          $(document).on("click", ".nav-link", function(){
            $("html").scrollTop(0)
          });

            $('.owl-carousel-year').owlCarousel({
                loop: false,
                responsiveClass: true,
                nav: true,
                dots: false,
                navText: ["<div class='nav-btn prev-slide'><i class='bi bi-chevron-left'></i></div>", "<div class='nav-btn next-slide'><i class='bi bi-chevron-right'></i></div>"],
                // startPosition: 2,
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 1,
                        nav: true
                    },
                    1200: {
                        items: 2,
                        nav: true,
                        loop: false
                    },
                    1300: {
                        items: 3,
                        nav: true,
                        loop: false
                    },
                    1366: {
                        items: 3,
                        nav: true,
                        loop: false
                    }
                }
            })
        });
        $('.owl-carousel-bonus').owlCarousel({
            loop: true,
            margin: 30,
            responsiveClass: true,
            autoplay: true,
            autoplayTimeout: 5000,
            nav: false,
            dots: false,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 1,
                    nav: false
                },
                1024: {
                    items: 3,
                    nav: false,
                    loop: true
                },
                1366: {
                    items: 3,
                    nav: false,
                    loop: true
                }
            }
        })
        $(function() {
            $(".tab-bond").fixTab();
        });
    </script>
@endsection
