<div wire:ignore>
    <!-- End Contact Section -->
    <section id="utilities" class="utilities pb-xxl-4">
        <div class="container" data-aos="fade-up">
            <h3 class="title-bond">{{$data->v_value??""}}</h3>
            <div class="mt-4">{!! sanitizationText($data->note??"") !!}</div>
            <div wire:click='getFile' class=" read-more color-g size18 mb-2" style="width: fit-content;cursor: pointer;">
                <i class="bi bi-arrow-right"></i> 
                {{__('about.more')}}
            </div>
            <!-- <a class="mt-4 italic" style='cursor: context-menu;'>Để tìm hiểu thông tin chi tiết về MB Capital vui lòng ấn <u wire:click='getFile' style='cursor: pointer;'>vào đây.</u></a> -->
<!--             <div class="mt-4 pb-5r">
                <a href="#" class="text-black" style="font-size: 15px;"><img src="assets/icon/pdf.png" alt="pdf" height="17rem" class="mr-2">Profile Công ty.PDF</a>
            </div> -->
        </div>
        <div class="package">
            <div class="container">
                <div class="introduce-detail">
                    <div class="intro-item">
                        <div class="title-intro">
                            <h6><img src="./assets/icon/mb-start.png" class="mr-2" alt="mb">{{__('about.visibility')}}</h6>
                        </div>
                        <div class="description-intro">
                            <p>{{__('about.visibility_content')}}</p>
                        </div>
                    </div>
                    <div class="intro-item">
                        <div class="title-intro">
                            <h6><img src="./assets/icon/mb-start.png" class="mr-2" alt="mb">{{__('about.mission')}}</h6>
                        </div>
                        <div class="description-intro">
                            <p>{{__('about.mission_content')}}</p>
                        </div>
                    </div>
                    <div class="intro-item">
                        <div class="title-intro">
                            <h6><img src="./assets/icon/mb-start.png" class="mr-2" alt="mb">{{__('about.core_values')}}
                            </h6>
                        </div>
                        <div class="description-intro">
                            <p>{{__('about.core_values_content_1')}}<br />{{__('about.core_values_content_2')}}<b>{{__('about.core_values_content_3')}}</b>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--<div class="container mt-5">
            <div class="sub-title-small">
                <h6>Giấy phép thành lập</h6>
                <p class="mt-4">Giấy phép thành lập số 07/UBCK-GP do Ủy ban Chứng khoán Nhà nước (UBCKNN)
                    cấp ngày 29/09/2006;</p>
                <p>Giấy phép điều chỉnh số 21/UBCK-GP do UBCKNN cấp ngày 16/11/2007</p>
            </div>
        </div>--}}
    </section>

    <section class="utilities pb-5r">
        <div class="container aos-init aos-animate " data-aos="fade-up">
            <h3 class="title-bond pb-xxl-4">{{__('about.field_of_activity')}}</h3>
            <div class="field-activity">
                <div class="f-ac-item">
                    <div class="number-order">
                        <span>01</span>
                    </div>
                    <div class="content-filed">
                        <p>{{__('about.securities_investment_fund_management')}}</p>
                    </div>
                </div>
                <div class="f-ac-item">
                    <div class="number-order">
                        <span>02</span>
                    </div>
                    <div class="content-filed">
                        <p>{{__('about.stock_portfolio_management')}}</p>
                    </div>
                </div>
                <div class="f-ac-item">
                    <div class="number-order">
                        <span>03</span>
                    </div>
                    <div class="content-filed">
                        <p>{{__('about.securities_investment_consulting')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="data-section">
        <div class="container" style="height: 100%;">
            <div class="statistical">
                <div class="statis-item">
                    <div class="label-statis">
                        <p>{{__('about.authorized_capital')}}</p>
                    </div>
                    <div class="number-statis">
                        <p>{{$arrayContent->Authorized_capital??"323"}}</p>
                    </div>
                    <div class="unit-statis">
                        <span>{{__('about.billion')}}</span>
                    </div>
                </div>
                <div class="statis-item">
                    <div class="label-statis">
                        <p>{{__('about.owned_by')}} <span>MBBank</span></p>
                    </div>
                    <div class="number-statis">
                        <p><span style="font-family: 'Avert';">~</span>{{$arrayContent->Owned_by_MBBank??'91'}} <span>%</span></p>
                    </div>
                    <div class="unit-statis">
                        <span>{{__('about.share')}}</span>
                    </div>
                </div>
                <div class="statis-item">
                    <div class="label-statis">
                        <p>{{__('about.total_assets_under_management')}}</p>
                    </div>
                    <div class="number-statis">
                        <p><span style="font-family: 'Avert';">~</span>{{$arrayContent->Total_AUM??'6.355'}}</p>
                    </div>
                    <div class="unit-statis">
                        <span>{{__('about.billion')}} ({{__('about.at')}} {{$arrayContent->date??'31/10/2021'}})</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ======= services ======= -->
    <section id="services" class="services">
        <div class="container" data-aos="fade-up">
            <h3 class="title-bond pb-xxl-5">{{__('about.important_milestones')}}</h3>
            <div class="container">
                <div class="owl-carousel-year owl-carousel owl-theme">  
                    @foreach($milestones as $index => $row)
                        <div class="node-year">
                            @if ($index % 2 == 0)
                                <div class="nodey-content top-ct" @if(strlen($row->note) > 255) style="width:530px" @endif>
                                    {{-- <p class="size22 fw-bolder">Tháng 8</p> --}}
                                    <p class="size20 text-justify">{!! sanitizationText($row->note) !!}</p>
                                </div>
                                <span class="ps-line top"></span>
                                <div class="node-cy">{{ $row->order_number }}</div>
                                <div class="line-hoz-div"></div>
                            @else
                                <div class="nodey-content bot-ct" @if(strlen($row->note) > 255) style="width:530px" @endif>
                                    {{-- <p class="size22 fw-bolder">Tháng 8</p> --}}
                                    <p class="size20 text-justify">{!! sanitizationText($row->note) !!}</p>
                                </div>
                                <span class="ps-line bot"></span>
                                <div class="node-cy">{{ $row->order_number }}</div>
                                <div class="line-hoz-div"></div>
                            @endif
                        </div>
                    @endforeach
                    @if (Browser::isDesktop())
                    <div class="node-year">
                        <div class="nodey-content top-ct" style="width:600px">
                        </div>
                        <div class="line-hoz-div"></div>
                    </div>
                    @endif
                </div>
            </div>
    </section>

    <section class="utilities mb-200">
        <div class="container" data-aos="fade-up">
            <h3 class="title-bond pb-5r pt-0">{{__('about.remunerative')}}</h3>
            <div class="owl-carousel-bonus owl-carousel owl-theme">
                @foreach ( $achievements as $key => $item)
                    <div class="bonus-item">
                        <div class="img-bonus">
                            <img src="{{asset('storage/' . $item->image)}}" alt="">
                        </div>
                        <div class="bonus-text">
                            <p>{{$item->v_content}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</div>
