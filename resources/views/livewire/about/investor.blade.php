<div>
    <section id="utilities" class="utilities pb-xxl-4">
        <div class="container">
            <div class="search-infomation d-flex justify-content-center mt-5">
                <div class="search-box">
                    <div class="icon">
                        <img src="./assets/icon/search.svg" alt="search-icon">
                    </div>
                    <input wire:model="search" name="search" type="text" class="form-control custom-input-search"
                           placeholder="{{__('about.search')}}">
                </div>
                <div class="select-year ml-3">  
                   <select wire:model="year" name="year" id="year" class="form-control custom-select">
                        @foreach ($investorRelations as $item)
                            <option value="{{$item->value}}">{{$item->value}}</option>
                        @endforeach
                    </select>
                    <span class="icon"><img src="./assets/icon/arrow-down-black.png" alt=""></span>
                </div>
            </div>

            @foreach ( $investors as $key => $investor)
                @if( $loop->index % 2 == 0)
                    <div class="row" {{--data-aos="fade-up" data-aos-delay="200"--}}>
                @endif
                <div class="col-lg-5">
                    <div class="d-flex" >
                        <div class="title-info">
                            <h4 class="mb-4 size18 fw-bolder">{{$investor->title_vi}}</h4>
                            <span class="mr-5">{{date('d/m/Y', strtotime($investor->public_date ?? now()))}}</span>
                            <a {{$investor->file_path ? 'target="_blank"' : '' }} download href="{{$investor->file_path ? asset('storage/'.$investor->file_path) : '#'}}"><img src="./assets/icon/download.svg" alt="" class="mb-2 mr-2">{{__('about.download')}}</a>
                        </div>
                    </div>
                </div>
                @if( $loop->index % 2 == 0)
                    <div class="col-lg-2"></div>
                @endif
                @if($loop->index % 2 != 0 || $investors->count() == $loop->index + 1)
                    </div>
                @endif
            @endforeach
        </div>
        {{$investors->links()}}
    </section>
</div>