<div>
    <section id="utilities" class="utilities pb-xxl-4">
        <div class="container">
          
            <div class="proifle-tabs">
                <ul class="nav nav-pills mb-3 d-flex" id="pills-tab-2" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100 {{$activeTab==1?'active':''}}" id="pills-administrative-tab"
                                data-bs-toggle="pill" data-bs-target="#pills-administrative" type="button"
                                role="tab" aria-controls="pills-administrative"  wire:click='changeActiveTab(1)'>{{__('about.administrative_council_and_board_of_supervisors')}}</button>
                    </li>

                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100 {{$activeTab==2?'active':''}}" id="pills-control-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-control" type="button" role="tab"
                                aria-controls="pills-control"  wire:click='changeActiveTab(2)'>{{__('about.management_staff')}}</button>
                    </li>
                   
                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100 {{$activeTab==3?'active':''}}" id="pills-manager-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-manager" type="button" role="tab"
                                aria-controls="pills-manager"  wire:click='changeActiveTab(3)'>{{__('about.investor_staff')}}</button>
                    </li>
                </ul>
            </div>
            <div class="tab-content mt-3" id="myTabContent">
                <div class="tab-pane fade {{$activeTab==1?'show active':''}}" id="pills-administrative" role="tabpanel"
                     aria-labelledby="pills-administrative-tab">
                    <!-- <div class="list-magager-card mt-5">
                        @foreach ( $admins as $key => $item)
                            @if( ($loop->index + 1) % 3 == 0 && $loop->index != 0)
                                <div class="row mt-5">
                            @elseif($loop->index == 0)
                                <div class="row justify-content-center">
                            @endif
                            <div class="col-md-4">
                                <div class="card text-center">
                                    <img class="card-img-top" src="{{asset($item->img)}}"
                                         alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$item->sex == 1 ? "(Ông)" : "(Bà)" }} {{$item->fullname}}</h5>
                                        <p class="card-text">{{$item->title}}</p>
                                    </div>
                                </div>
                            </div>
                            @if((($loop->index - 1) % 3 == 0 && $loop->index != 1) || ($admins->count() == $loop->index + 1))
                                </div>
                            @elseif($loop->index == 1)
                                </div>
                            @endif
                        @endforeach
                        {{--                                    <div class="row justify-content-center">--}}
                        {{--                                    </div>--}}
                    </div> -->
                    <div class="list-magager-card mt-5">
                        @foreach ( $admins as $key => $item)
                            @if( ($loop->index) % 3 == 0)
                                <div class="row mt-5">
                            @endif
                            <div class="col-md-4">
                                <div class="card text-center">
                                    <img class="card-img-top" src="{{asset($item->img)}}" style="width:323px;height:421px"
                                         alt="Card image cap" data-toggle="modal" data-target="#infoModal" wire:click='showInfo({{$item->id}})'>
                                    <div class="card-body">
                                        <h5 class="card-title">{{$item->sex == 1 ? __('about.mr') : __('about.mrs') }} {{$item->fullname}}</h5>
                                        <p class="card-text">{{$item->title}}</p>
                                        <p class="card-text">{{$item->title_2}}</p>
                                    </div>
                                </div>
                            </div>
                            @if((($loop->index + 1) % 3 == 0))
                                </div>
                            @endif
                        @endforeach
                        @if($admins->count() % 3 != 0)
                        </div>
                        @endif
                        {{--                                    <div class="row justify-content-center">--}}
                        {{--                                    </div>--}}
                    </div>
                </div>

                <div class="tab-pane fade {{$activeTab==2?'show active':''}}" id="pills-control" role="tabpanel"
                     aria-labelledby="pills-control-tab">
                    <!-- <div class="list-magager-card mt-5">
                        @foreach ( $controls as $key => $item)
                            @if( ($loop->index + 1) % 3 == 0 && $loop->index != 0)
                                <div class="row mt-5">
                            @elseif($loop->index == 0)
                                <div class="row justify-content-center">
                            @endif
                            <div class="col-md-4">
                                <div class="card text-center">
                                    <img class="card-img-top" src="{{asset($item->img)}}"
                                         alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$item->sex == 1 ? "(Ông)" : "(Bà)" }} {{$item->fullname}}</h5>
                                        <p class="card-text">{{$item->title}}</p>
                                    </div>
                                </div>
                            </div>
                            @if((($loop->index - 1) % 3 == 0 && $loop->index != 1) || ($controls->count() == $loop->index + 1))
                                </div>
                            @elseif($loop->index == 1)
                                </div>
                            @endif
                        @endforeach
                        {{--                                    <div class="row justify-content-center">--}}
                        {{--                                    </div>--}}
                    </div> -->
                    <div class="list-magager-card mt-5">
                        @foreach ( $controls as $key => $item)
                            @if( ($loop->index) % 3 == 0)
                                <div class="row mt-5">
                            @endif
                                <div class="col-md-4">
                                    <div class="card text-center">
                                        <img class="card-img-top" src="{{asset($item->img)}}"  style="width:323px;height:421px"
                                            alt="Card image cap" data-toggle="modal" data-target="#infoModal" wire:click='showInfo({{$item->id}})'>
                                        <div class="card-body">
                                            <h5 class="card-title">{{$item->sex == 1 ? __('about.mr') : __('about.mrs') }} {{$item->fullname}}</h5>
                                            <p class="card-text">{{$item->title}}</p>
                                            <p class="card-text">{{$item->title_2}}</p>
                                        </div>
                                    </div>
                                </div>
                            @if((($loop->index + 1) % 3 == 0))
                                </div>
                            @endif
                        @endforeach
                        @if($controls->count() % 3 != 0)
                        </div>
                        @endif
                        {{--                                    <div class="row justify-content-center">--}}
                        {{--                                    </div>--}}
                    </div>
                </div>
                <div class="tab-pane fade {{$activeTab==3?'show active':''}}" id="pills-manager" role="tabpanel"
                     aria-labelledby="pills-manager-tab">
                    <!-- <div class="list-magager-card mt-5">
                        @foreach ( $managers as $key => $item)
                            @if( ($loop->index + 1) % 3 == 0 && $loop->index != 0)
                                <div class="row mt-5">
                            @elseif($loop->index == 0)
                                <div class="row justify-content-center">
                            @endif
                            <div class="col-md-4">
                                <div class="card text-center">
                                    <img class="card-img-top" src="{{asset($item->img)}}"
                                         alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$item->sex == 1 ? "(Ông)" : "(Bà)" }} {{$item->fullname}}</h5>
                                        <p class="card-text">{{$item->title}}</p>
                                    </div>
                                </div>
                            </div>
                            @if((($loop->index - 1) % 3 == 0 && $loop->index != 1) || ($managers->count() == $loop->index + 1))
                                </div>
                            @elseif($loop->index == 1)
                                </div>
                            @endif
                        @endforeach
                        {{--                                    <div class="row justify-content-center">--}}
                        {{--                                    </div>--}}
                    </div> -->
                    <div class="list-magager-card mt-5">
                        @foreach ( $managers as $key => $item)
                            @if( ($loop->index) % 3 == 0)
                                <div class="row mt-5">
                            @endif
                            <div class="col-md-4">
                                <div class="card text-center">
                                    <img class="card-img-top" src="{{asset($item->img)}}"  style="width:323px;height:421px" 
                                         alt="Card image cap" data-toggle="modal" data-target="#infoModal" wire:click='showInfo({{$item->id}})'>
                                    <div class="card-body">
                                        <h5 class="card-title">{{$item->sex == 1 ? __('about.mr') :  __('about.mrs') }} {{$item->fullname}}</h5>
                                        <p class="card-text">{{$item->title}}</p>
                                        {{-- <p class="card-text"></p> --}}
                                    </div>
                                </div>
                            </div>
                            @if((($loop->index + 1) % 3 == 0))
                                </div>
                            @endif
                        @endforeach
                        @if($managers->count() % 3 != 0)
                            </div>
                        @endif
                        {{--                                    <div class="row justify-content-center">--}}
                        {{--                                    </div>--}}
                    </div>
                </div>
            </div>
            <div wire:ignore.self class="modal fade" tabindex="-1" id="infoModal" role="dialog">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-body">
                        <div class="col-md-12">
                            <div class="text-center">
                                <div class="row">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-6">
                                        <img class="card-img-top" src="{{asset($employee->img??'')}}"
                                            alt="Card image cap">
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title">{{$employee?$employee->sex == 1 ? __('about.mr') : __('about.mrs') : '' }} {{$employee->fullname??''}}</h5>
                                    <p class="card-text">{{$employee->title??''}}</p>
                                    <p class="card-text">{{$employee->title_2??''}}</p>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="modal-footer ">
                        <div class="col text-center">
                            <p class="card-text justify">{{$employee->content??''}}</p><br>
                            <button  type="button" class="btn btn-secondary" data-dismiss="modal">{{__('about.close')}}</button>
                        </div>

                      </div>
                    </div>
                  </div>
            </div>

        </div>
    </section>
</div>
