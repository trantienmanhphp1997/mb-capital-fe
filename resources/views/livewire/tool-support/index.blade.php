<div wire:ignore.self>
    <section id="banner-investment" class="d-flex align-items-center">
        <div class="container" data-aos="zoom-out" data-aos-delay="100" wire:ignore>
            <div class="row mb-md-5">
                <h1 class="titele color-w fw-regular mb-md-5">{{__('tool-support.title')}}</h1>
            </div>
        </div>
    </section>
    <main id="main">
        <div class="container page-mbbond">
            <div class="tab-bond" >
                <ul class="nav nav-pills mb-3 d-flex" id="pills-tab" role="tablist" >
                    <li class="nav-item" role="presentation" wire:ignore>
                        <button wire:click="survey" class="nav-link active  w-100" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home">{{__('tool-support.survey')}}</button>
                    </li>
                    <li class="nav-item" role="presentation" wire:ignore>
                        <button wire:click="calculationTool" class="nav-link w-100" id="pills-calculator-tab" data-bs-toggle="pill" data-bs-target="#pills-calculator" type="button" role="tab" aria-controls="pills-calculator" >{{__('tool-support.calculation_tool')}}</button>
                    </li>
                    <li class="nav-item" role="presentation" wire:ignore>
                        <button wire:click="loadProductCons" class="nav-link w-100" id="pills-other-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" >{{__('tool-support.product_consulting')}}</button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" wire:ignore.self>
                @livewire('tool-support.surveys')
            </div>
            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" wire:ignore.self>
                @livewire('tool-support.product-consulting')
            </div>
            <div class="tab-pane fade" id="pills-calculator" role="tabpanel" aria-labelledby="pills-calculator-tab" wire:ignore.self>
                @livewire('tool-support.calculation-tool')
            </div>
        </div>
        <!-- ======= Contact to  advise Section ======= -->
        @livewire('component.advise-request', ['ip_client' => Request::ip()])
        <!-- End Counts Section -->
    </main><!-- End #main -->
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
@section('js')
    <script>

        $('.nav-link').on('click', function(){
            $('.nav-link').removeClass('active');
            $(this).addClass('active');
        });
        $( ".tab" ).click(function() {
            window.scrollTo(0, 0);
        });
        $(function(){
            $(".tab-bond").fixTab();
        });
        $('#pick-tab-3').click(function(){
            document.getElementById("pills-other-tab").click();
            window.scrollTo(0,0);
        });

        $('#pick-tab-2').click(function(){
            document.getElementById("pills-other-tab").click();
            window.scrollTo(0,0);
        });
        var pie1 = document.getElementById('pie-1');
        var pie2 = document.getElementById('pie-2');
        var labelname = 'My First Dataset';
        var type = 'doughnut';
        var dataHealth= <?php echo json_encode($dataPie); ?>;
        let widthScreen = $(window).width();

        var datas = dataHealth.map($item => $item.value.value);
        var colors = dataHealth.map($item => $item.value.color);
        let position = widthScreen >= 768 ? 'right' : 'bottom'
        var labels = dataHealth.map($item => $item.label ? ($item.label + ': ' + $item.value.value + '%') : ($item.value.value + '%'));
        var myChart1 = initChart(pie1, type, labels, labelname, datas, colors, 4, position, 'pie-1');
        var myChart2 = initChart(pie2, type, labels, labelname, datas, colors, 4, position, 'pie-2');

        myChart2.resize(260, 260);

        function onlyOne(checkbox) {

            var checkboxes = document.getElementsByName('check')
            checkboxes.forEach((item) => {
                if (item !== checkbox) item.checked = false
            })
        }
        document.addEventListener('livewire:load', function () {
            $(function () {
                function onlyOne(checkbox) {
                    var checkboxes = document.getElementsByName('check')
                    checkboxes.forEach((item) => {
                        if (item !== checkbox) item.checked = false
                    })

                }
            });
        });

        window.addEventListener('update_scripts', event=> {
            dataHealth= event.detail.data;
            datas = dataHealth.map($item => $item.value.value);
            colors = dataHealth.map($item => $item.value.color);
            labels = dataHealth.map($item => $item.label ? ($item.label + ': ' + $item.value.value + '%') : ($item.value.value + '%'));
            if(myChart1!= undefined) myChart1.destroy();
            myChart1 = initChart(pie1, type, labels, labelname, datas, colors, 4, position, 'pie-1');
            if(myChart2!= undefined) myChart2.destroy();
            myChart2 = initChart(pie2, type, labels, labelname, datas, colors, 4, position, 'pie-2');

            myChart2.resize(260, 260);

        })
        document.addEventListener("DOMContentLoaded", function() {

            window.livewire.on('setHighChart', function (data) {
                colorList = [];
                if (data) {
                    for (var i in data) {
                        colorList.push(data[i]['color']);
                    }
                }
                Highcharts.setOptions({
                    colors: colorList,
                    chart: {
                        style: {
                            fontFamily: 'Avert',
                        }
                    },
                    legend: {
                        symbolRadius: 0,
                        align: widthScreen > 414 ? 'center' : 'bottom',
                        verticalAlign: widthScreen > 414 ? 'bottom' : 'bottom',
                        layout: 'vertical',
                        itemMarginBottom: widthScreen > 414 ? 15 : 0,
                        itemStyle: {
                            color: '#FFF',
                            fontSize: '18px',
                            fontWeight:100,

                        },
                        labelFormatter: function () {
                            return (
                                `<div class="legend-item">
                                <div class="mr-5">
                                    <span style=" color:'${this.color}'"></span><span>${this.name}</span>
                                </div>
                                <div>
                                    <span>${this.percentage}%</span>
                                </div>
                            </div>`
                            );
                        },
                    },
                });
                Highcharts.chart('containerChartPie', {
                    exporting: false,
                    credits: {
                        enabled: false
                    },
                    chart: {
                        backgroundColor: null,
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie',
                        width: widthScreen > 414 ? 500 : 300,
                        events: {
                            load: function () {
                                if (data) {
                                    for (var i in data) {
                                        this.series[0].addPoint([i, parseInt(data[i]['value'])]);
                                    }
                                }
                            }
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:f}%</b>',
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    title: {
                        text: '',
                        style: {
                            display: 'none'
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false,
                            },
                            showInLegend: true,
                            point: {
                                events: {
                                    legendItemClick: function (e) {
                                        e.preventDefault();
                                    }
                                },
                            }
                        }
                    },
                    series: [{
                        name: 'Tỷ lệ',
                        innerSize: '50%',
                        colorByPoint: true,
                    }]
                });
                AOS.init({
                    disable: function () {
                        let maxWidth = 800;
                        return window.innerWidth < maxWidth;
                    }
                });
            });
        });

// $('.chart-container-dou3').click(function(){
//     $('#pie-2-legend-container p').css('font-size', '18px');
// });

// $('#pie-2-legend-container p').css('font-size', '18px');
    </script>

@endsection
