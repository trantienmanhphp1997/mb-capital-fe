<div wire:ignore.self>
    <div class="search-investment" id="type-calculation-tool">
        <div class="row text-center">
            <div class="col-sm-12 col-md-9 select ">
                <select wire:model.defer="typeCalc" class="form-select form-control input-custom-ct">
                    <option style="background-color: white !important;" value="1">{{__('tool-support.calculator.determine_value_invest')}}</option>
                    <option style="background-color: white !important;" value="2">{{__('tool-support.calculator.estimated_return')}}</option>
                    <option style="background-color: white !important;" value="3">{{__('tool-support.calculator.determine_amount_to_invest')}}</option>
                </select>
            </div>
            <div class="col-sm-12 col-md-3">
                <button wire:click="filterData" class="btn custom-btn-op1 custom-btn-op1-fullfill text-uppercase">
                    {{__('investor.select')}}
                </button>
            </div>
        </div>
    </div>
    @if ($typeCalc == 1)
    <div class="mbfv-calculate contact contact-bond h-auto">
        <div class="container">
            <h5 class="title-bond color-w pt-0 mb-4">{{__('tool-support.calculator.determine_value_invest')}}</h5>
               <div class="box-tool">
                    <div class="row">
                    <div class="col-md-6 br-mbvf">
                        <div class="pl-5">
                            <div class="d-flex mbvf-target pb-30">
                                <span class="label-calculate">{{__('tool-support.calculator.investment_amount')}}</span>
                                <div class="mbfvinput mbfvinput1 width-50">
                                    <input type="text" id="amount-input" class="form-control" oninput="formatInputNumber(this)"/>
                                    <span>{{__('fund.vnd')}}</span>
                                </div>
                            </div>
                            <div class="d-flex mbvf-target pb-30">
                                <span class="label-calculate">{{__('tool-support.calculator.holding_period')}}</span>
                                <div class="mbfvinput mbfvinput2">
                                    <input type="text" id="hold-time-input" class="form-control" oninput="formatInputNumber(this)" max="100"/>
                                    <span class="color-w">{{__('mbvf.year')}}</span>
                                </div>
                            </div>
                            <div class="d-flex mbvf-target pb-30">
                                <span class="label-calculate">{{__('tool-support.calculator.average_return')}}</span>
                                <div class="mbfvinput mbfvinput2">
                                    <input type="text" id="profit-input" class="form-control mbfvinput2" oninput="formatInputNumber(this)" max="25" value="12" />
                                    <span class="color-w">%/{{__('mbvf.year')}}</span>
                                </div>
                            </div>
                            <div class="d-flex mbvf-target pb-30">
                                <span class="label-calculate">{{__('tool-support.calculator.ending_nav')}}</span>
                                <button id="calculate-mbvf-button" class="bg-gold btn-estimate" onclick="calculateMBVF()"
                                    >{{__('tool-support.calculator.calculate')}}</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 d-table">
                        <div class="d-table-cell align-middle">
                            <div class="d-flex mbvf-target">
                                <span class="label-calculate text-right">{{__('tool-support.calculator.estimated_nav')}}</span>
                                <div class="totalmoney text-center pt-0 ml-4" id="final-nav-result" wire:ignore>
                                    0 <span class="size17 fw-normal">{{__('fund.vnd')}}</span>
                                </div>
                            </div>
                            <div class="pb-30 ml-3" style="font-style:italic;font-size:13px">
                                * {{__('mbvf.calculation_tool.taxs_and_fees_are_not_included_when_selling_fund_certificate')}}
                            </div>
                            <div class="d-flex mbvf-target pb-30 ">
                                <span class="label-calculate text-right">{{__('tool-support.calculator.profit')}}</span>
                                <div class="totalmoney text-center pt-0 ml-4" id="interest-result" wire:ignore>
                                    0 <span class="size17 fw-normal">{{__('fund.vnd')}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
               </div>
        </div>
    </div>
    <div class="container">
        <section id="utilities" class="utilities pb-xxl-4" style="display: none; ">
            <div class="container">
                <div id="div-chart" class="chart" style="width: 100%; height: 400px; margin-bottom: 18%">
                    <canvas id="line1"></canvas>
                    <div class="mt-3 d-flex justify-content-center" id="line-chart-nav-legend-container"></div>
                </div>
            </div>
        </section>
    </div>
    @elseif ($typeCalc == 2)
    <div class="mbfv-calculate contact contact-bond h-auto">
        <div class="container">
            <h5 class="title-bond color-w pt-30 mb-5">{{__('tool-support.calculator.estimated_return')}}</h5>
            <div class="box-tool">
                <div class="d-flex mbvf-target">
                    <span>{{__('tool-support.calculator.if_you_invest')}}</span>
                    <div class="mbfvinput mbfvinput1">
                        <input type="text" class="form-control" oninput="formatInputNumber(this)" id="money-input-1" max="1000000000">
                        <span>{{__('fund.vnd')}}</span>
                    </div>
                    <span>{{__('tool-support.calculator.monthly_with_return')}}</span>
                    <div class="mbfvinput mbfvinput2">
                        <input type="text" class="form-control" id="profit-input-1" oninput="formatInputNumber(this)" max="20">
                        <span class="color-w">%/{{__('mbvf-sip.calculation_tool.year')}}</span>
                    </div>
                    <span>{{__('tool-support.calculator.in')}}</span>
                    <div class="mbfvinput mbfvinput2">
                        <input type="text" class="form-control mbfvinput2" id="year-input-1" oninput="formatInputNumber(this)" max="100">
                        <span class="color-w">{{__('mbvf-sip.calculation_tool.year')}}</span>
                    </div>
                    <span>{{__('tool-support.calculator.you_will_receive')}}</span>
                    <button class="bg-gold btn-estimate" id="calculate-mbvf-sip-button" onclick="calculateMBVFSIP()"
                        >{{__('tool-support.calculator.calculate')}}</button>
                </div>
                <div class="mbfv-total">
                    <div class="row">
                        <div class="col-lg-3" wire:ignore>
                            <h6 class="text-center">{{__('tool-support.calculator.total_invest')}}</h6>
                            <div class="totalmoney"><span id="investment-output">0</span> <span class="size17">{{__('fund.vnd')}}</span>
                            </div>
                        </div>
                        <div class="col-lg-3" wire:ignore>
                            <h6 class="text-center">{{__('tool-support.calculator.expected_return')}}</h6>
                            <div class="totalmoney"><span id="expected-profit-output-1">0</span> <span class="size17">%/{{__('mbvf-sip.calculation_tool.year')}}</span>
                            </div>
                        </div>
                        <div class="col-lg-3" wire:ignore>
                            <h6 class="text-center">{{__('tool-support.calculator.total_profit')}}</h6>
                            <div class="totalmoney"><span id="total-profit-output">0</span> <span class="size17">{{__('fund.vnd')}}</span>
                            </div>
                        </div>
                        <div class="col-lg-3" wire:ignore>
                            <h6 class="text-center">{{__('tool-support.calculator.total_nav')}}</h6>
                            <div class="totalmoney color-yellow pt-0"><span id="money-received-output">0</span> <span class="size17">{{__('fund.vnd')}}</span>
                            </div>
                        </div>
                    </div>
                    <p class="text-center mb-0 pt-50">{{__('tool-support.calculator.result_for_reference')}}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <section id="utilities" class="utilities pb-xxl-4" style="display: none;">
            <div class="container">
                <div id="div-chart-2" class="chart" style="position: relative; width: 100%; height: 400px; margin-bottom: 18%; ">
                    <canvas id="line2"></canvas>
                    <div class="mt-3 d-flex justify-content-center" id="line-chart-nav-legend-container"></div>
                </div>
            </div>
        </section>
    </div>
    @elseif ($typeCalc == 3)
    <div class="mbfv-calculate contact contact-bond h-auto">
        <div class="container">
            <div class="mnfv">
                <h5 class="size22">{{__('tool-support.calculator.determine_amount_to_invest')}}</h5>
                <div class="mbfv-slide mbfv-slide1 mt-5 pt-50">
                    <div class="range-wrap">
                        <input disabled id="mark125" type="range" min="0" max="25" value="25" step="1" class="mark" hidden style="position: absolute">
                        <input disabled id="mark120" type="range" min="0" max="25" value="20" step="1" class="mark" hidden style="position: absolute">
                        <input disabled id="mark115" type="range" min="0" max="25" value="15" step="1" class="mark" hidden style="position: absolute">
                        <input disabled id="mark112" type="range" min="0" max="25" value="12" step="1" class="mark" hidden style="position: absolute">
                        <input disabled id="mark110" type="range" min="0" max="25" value="10" step="1" class="mark" hidden style="position: absolute">
                        <input disabled id="mark105" type="range" min="0" max="25" value="5" step="1" class="mark" hidden style="position: absolute">
                        <input id="range1" type="range" min="0" max="25" value="12" step="1" class="expected-profit-input-2" onchange="calculateReverseMBVFSIP()" style="position: relative;" list="list-profits">
                        <p class="size18 pr-5">{{__('tool-support.calculator.expected_return')}}: <span wire:ignore id="profit-range"></span></p>
                    </div>
                    <datalist id="list-profits">
                        <option value="5">
                        <option value="10">
                        <option value="12">
                        <option value="15">
                        <option value="20">
                        <option value="25">
                    </datalist>
                </div>

                <div class="mbfv-slide mbfv-slide2 mt-5">
                    <div class="range-wrap">
                        <input disabled id="mark200" type="range" min="0" max="100" value="100" step="1" class="mark" hidden style="position: absolute">
                        <input disabled id="mark280" type="range" min="0" max="100" value="80" step="1" class="mark" hidden style="position: absolute">
                        <input disabled id="mark260" type="range" min="0" max="100" value="60" step="1" class="mark" hidden style="position: absolute">
                        <input disabled id="mark240" type="range" min="0" max="100" value="40" step="1" class="mark" hidden style="position: absolute">
                        <input disabled id="mark220" type="range" min="0" max="100" value="20" step="1" class="mark" hidden style="position: absolute">
                        <input id="range11" type="range" min="0" max="100" value="40" step="1" class="year-input-2" onchange="calculateReverseMBVFSIP()" style="position: relative;" list="list-profits">
                        <p class="size18 pr-5">{{__('tool-support.calculator.number_of_years')}}: <span wire:ignore id="year-range"></span></p>
                    </div>
                    <datalist id="list-profits">
                        <option value="5">
                        <option value="10">
                        <option value="12">
                        <option value="15">
                        <option value="20">
                        <option value="25">
                    </datalist>
                </div>

                <div class="mbfv-slide  mbfv-slide3 mt-5 pt-50">
                    <div class="range-wrap">
                        <div wire:ignore class="range-value" id="range20"></div>
                        <div wire:ignore class="range-value" id="range15"></div>
                        <div wire:ignore class="range-value" id="range3"></div>
                        <div wire:ignore class="range-value" id="range2"></div>
                        <div wire:ignore class="range-value" id="range05"></div>
                        <input disabled id="mark20" type="range" min="0" max="20000" value="20000" step="1" class="mark" style="position: absolute">
                        <input disabled id="mark15" type="range" min="0" max="20000" value="15000" step="1" class="mark" style="position: absolute">
                        <input disabled id="mark3" type="range" min="0" max="20000" value="4000" step="1" class="mark" style="position: absolute">
                        <input disabled id="mark2" type="range" min="0" max="20000" value="2000" step="1" class="mark" style="position: absolute">
                        <input disabled id="mark05" type="range" min="0" max="20000" value="500" step="1" class="mark" style="position: absolute">
                        <input id="range" type="range" min="0" max="20000" value="500" step="1" class="money-input-2" onchange="calculateReverseMBVFSIP()" style="position: relative;" list="my-detents">
                        <p class="size18 pr-5">{{__('tool-support.calculator.financial_goals')}}: <span wire:ignore id="money-range"></span></p>
                    </div>    
                    <datalist id="my-detents">
                        <option value="500">
                        <option value="2000">
                        <option value="3000">
                        <option value="15000">
                        <option value="20000">
                    </datalist>
                </div>
                <div class="mbfv-total">
                    <div class="row">
                        <div class="col-lg-12">
                            <h6 class="text-center">{{__('tool-support.calculator.amount_need')}}</h6>
                            <div class="totalmoney text-center color-yellow pt-0" wire:ignore><span id="money-output-2">0</span> <span class="size17">{{__('fund.vnd')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="div-chart-3" style="display: none;">
                    <h3>{{__('tool-support.calculator.proposed_investment_plan')}}:</h3>
                    <div class="row text-center">
                        <figure class="highcharts-figure">
                            <div id="container" class="chart-tool"></div>
                        </figure>
                     </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="container">
        <h5 style="margin: 33px 0px;">
            <a href="#" id="pick-tab-2" class=""><i class="bi bi-arrow-right"></i> {{__('investment.learn_more')}}</a>
        </h5>
    </div>
</div>

<script type="text/javascript">
    function calculateMBVF() {
        let amount = removeFormatNumber(document.getElementById('amount-input').value);
        let holdTime = removeFormatNumber(document.getElementById('hold-time-input').value);
        let profit = removeFormatNumber(document.getElementById('profit-input').value) / 100;

        if (!amount || !holdTime || !profit) {
            document.getElementById('final-nav-result').innerHTML = 0 + ` <span class="size17 fw-normal">{{__('fund.vnd')}}</span>`;
            document.getElementById('interest-result').innerHTML = 0 + ` <span class="size17 fw-normal">{{__('fund.vnd')}}</span>`;
            Livewire.emit('hideChart');
            return;
        }

        let finalNAV = Math.round(amount * Math.pow(1 + profit, holdTime));
        let interest = finalNAV - amount;
        document.getElementById('final-nav-result').innerHTML = formatNumber(finalNAV) + ` <span class="size17 fw-normal">{{__('fund.vnd')}}</span>`;
        document.getElementById('interest-result').innerHTML = formatNumber(interest) + ` <span class="size17 fw-normal">{{__('fund.vnd')}}</span>`;
        drawChartMBVF(amount, profit, holdTime);
    }
    function calculateMBVFSIP() {
        let amount = removeFormatNumber(document.getElementById('money-input-1').value);
        let profit = removeFormatNumber(document.getElementById('profit-input-1').value) / 100;
        let year = removeFormatNumber(document.getElementById('year-input-1').value);

        if(!amount || !profit || !year) {
            document.getElementById('investment-output').innerHTML = 0;
            document.getElementById('expected-profit-output-1').innerHTML = 0;
            document.getElementById('total-profit-output').innerHTML = 0;
            document.getElementById('money-received-output').innerHTML = 0;
            Livewire.emit('hideChart');
            return;
        }

        let totalInvestment = amount * year * 12;
        let monthProfit = (1 + profit) ** (1 / 12) - 1;
        let receivedMoney = FV(monthProfit, year * 12, -amount, 0, 1);
        let totalProfit = receivedMoney - totalInvestment;

        document.getElementById('investment-output').innerHTML = formatNumber(Math.round(totalInvestment));
        document.getElementById('expected-profit-output-1').innerHTML = Math.round(profit * 100);
        document.getElementById('total-profit-output').innerHTML = formatNumber(Math.round(totalProfit));
        document.getElementById('money-received-output').innerHTML = formatNumber(Math.round(receivedMoney));
        drawChartMBVFSIP(amount, profit, year);
    }
    function calculateReverseMBVFSIP() {
        let money = removeFormatNumber(document.querySelector('.money-input-2').value) * 1000000;
        let expectedProfit = removeFormatNumber(document.querySelector('.expected-profit-input-2').value) / 100;
        let year = removeFormatNumber(document.querySelector('.year-input-2').value);
        if(!money || !expectedProfit || !year) {
            document.getElementById('money-output-2').innerHTML = 0;
            Livewire.emit('hideChart');
            return;
        }

        let pmt = Math.round(PMT(
            (1 + expectedProfit) ** (1/12) - 1,
            year * 12,
            -money,
            0,
            1
        ));
        document.getElementById('money-output-2').innerHTML = formatNumber(pmt);
        drawChartMBVFSIPReverse(expectedProfit);
    }
</script>
<script>
    function drawChartMBVF(amount, profit, year) {
        var myChart1;
        Livewire.emit('getChartMBVF', amount, profit, year);
        Livewire.on('showChartMBVF', function(json, labels) {
            document.getElementById('utilities').style.display = 'block';
            const datasets = JSON.parse(json);
            var line1 = document.getElementById('line1').getContext("2d");
            if (myChart1 != undefined) myChart1.destroy();
            myChart1 = new Chart(line1, {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: datasets,
                },
                options: {
                    plugins: {
                        title: {
                            text: '',
                            style: {
                                display: 'none'
                            }
                        },
                    }
                }

            });

        })
    }

    function drawChartMBVFSIP(amount, profit, year) {
        var myChart1;
        Livewire.emit('getChartMBVFSIP', amount, profit, year);
        Livewire.on('showChartMBVFSIP', function(json, labels) {
            document.getElementById('utilities').style.display = 'block';
            const datasets = JSON.parse(json);
            var line1 = document.getElementById('line2').getContext("2d");
            if (myChart1 != undefined) myChart1.destroy();
            myChart1 = new Chart(line1, {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: datasets
                },
            });

        })
    }

    function drawChartMBVFSIPReverse(profit) {
        var widthScreen = $(window).width();
        livewire.emit('getDataHighChart', profit);
        livewire.on('setHighChart', function(data){
            document.getElementById('div-chart-3').style.display = 'block';
            colorList = [];
            if(data){
        		for(var i in data) {
					colorList.push(data[i]['color']);
        		}
        	}
            Highcharts.setOptions({
                colors: colorList,
                chart: {
                    style: {
                        fontFamily: 'Avert',
                    }
                },
                legend: {
                    symbolRadius: 0,
                    align: widthScreen > 414 ? 'right' : 'bottom',
                    verticalAlign: widthScreen > 414 ? 'middle' : 'bottom',
                    layout: 'vertical',
                    itemMarginBottom: widthScreen > 414 ? 15 : 0,
                    itemStyle: {
                        color: '#FFF',
                        fontSize: '18px'
                    },
                    labelFormatter: function() {
                        return (
                            `<div class="legend-item">
                                <div class="mr-5">
                                    <span style=" color:'${this.color}'"></span><span>${this.name}</span>
                                </div>
                                <div>
                                    <span>${this.percentage}%</span>
                                </div>
                            </div>`
                        );
                    },
                },
            });
            Highcharts.chart('container', {
                exporting:false,
                chart: {
                    backgroundColor: null,
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    width: widthScreen > 414 ? 500 : 300,
                    events: {
                        load: function() {
                            if(data){
                                for(var i in data) {
                                    this.series[0].addPoint([i, parseInt(data[i]['value'])]);
                                }
                            }
                        }
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:f}%</b>',
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
			    },
                plotOptions: {
                    pie: {
                        allowPointSelect: false,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false,
                        },
                        showInLegend:true,
                        point: {
                            events: {
                                legendItemClick: function(e){
                                    e.preventDefault();
                                }
                            },
                        }
                    }
                },
                series: [{
                    name: 'Tỷ lệ',
                    innerSize: '50%',
                    colorByPoint: true,
                }]
            });
        });
        AOS.init({
            disable: function() {
                    let maxWidth = 800;
                    return window.innerWidth < maxWidth;
            }
        });
    }
</script>

<script>
    window.onload = function() {
        livewire.on('set-default-highchart', function(data) {
            document.querySelector('.expected-profit-input-2').value = data * 100;
            document.getElementById("profit-range").innerHTML = formatNumber(data * 100) + ' %';
            calculateReverseMBVFSIP(data);
        })
    };
</script>
