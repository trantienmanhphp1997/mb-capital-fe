<div class="survey aos-init aos-animate" wire:ignore.self>
    <div class="container survey">
        <div class="survey-item text-center" @if(!$show_start) style="display: none" @endif
             style="margin-top: 90px;margin-bottom: 264px">
            <div>
                <img src="assets/icon/Group 1137.png"></div>
            <div class="">
                <p>{{__('tool-support.survey_start')}}</p>
            </div>
            <div>
                <button class="btn custom-btn-op1 custom-btn-op1-fullfill text-uppercase" wire:click="startSurvey()">
                    {{__('tool-support.survey_start_btn')}}
                </button>
            </div>
        </div>

        <div @if($show_start || $show_end) style="display: none" @endif>
                <div class="row survey-item-next" style="margin-top: 94px;">
                    <div class="col-md-10">
                    @if($invalidMessage)
                        @if($total!=$surveys->total())
                        <div class="alert alert-danger w-100 sur-answer" role="alert">
                            {{__('tool-support.message')}}
                        </div>@endif
                    @endif
                    </div>
                        <div class="float-right col-md-2">
                            <div class="row">
                                <p class="color-dark fw-bolder mt-3 pl-0">{{__('tool-support.answered')}}: {{$total}} /{{$surveys->total()}}</p>
                            </div>
                        </div>
                </div>
            @foreach($surveys as $key => $survey)
                <div class="sur-item mt-3" >
                    <div class="sur-question">
                        <p>{{$survey->order_number}}. {{$survey->name}}</p>
                    </div>
                    @foreach($survey->detail as $answer)
                        <div class="form-check sur-answer">
                            <input class="form-check-input" @if($survey->multi_select==0) onclick="onlyOne(this)"
                                   wire:change="changeSingleSelect({{$survey->id}},{{$answer->id}},{{$answer->point}})"
                                   @else
                                   wire:change="changeMultiSelect({{$survey->id}})"
                                   @endif
                            id="id_answer_{{$answer->id}}"  name="check" type="checkbox"
                                   wire:model="event.{{$survey->id}}.{{$answer->id}}" value="{{$answer->point}}">
                            <label class="form-check-label" for="id_answer_{{$answer->id}}">
                                {{$answer->name }}
                            </label>

                        </div>
                    @endforeach
                </div>
            @endforeach

            @if(count($surveys))
                    <div class="paginate text-center mb-4 mt-5">
                        @if ($surveys->hasPages())
                            <nav class="mb-pagination" aria-label="Page navigation example">
                                <ul class="pagination mb-0 justify-content-center d-flex">
                                    {{-- Previous Page Link --}}
                                    @if ($surveys->onFirstPage())
                                        <li class="page-item disabled" id="previous1" aria-disabled="true" aria-label="@lang('pagination.previous')">
                                            <a class="btn-prev" aria-hidden="true">
                                                <span aria-hidden="true"><i class="bi bi-chevron-left"></i></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                    @else
                                        <li class="page-item" id="previous2">
                                            <a style="cursor: pointer" class="btn-prev active" wire:click="previousPage"
                                               wire:loading.attr="disabled" rel="prev" aria-label="@lang('pagination.previous')"
                                               href="javascript:void(0)">
                                                <span aria-hidden="true"><i class="bi bi-chevron-left"></i></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                    @endif
                                    @for($i=1 ;$i<= $surveys->total() ; $i++)
                                        @if ($i == $surveys->currentPage())
                                            <li class="page-item active" wire:key="paginator-page-{{ $i }}" aria-current="page" id="page-{{ $i }}"><a
                                                    class="page-link" >{{ $i }}</a></li>
                                        @else
                                            <li class="page-item" wire:key="paginator-page-{{ $i }}" id="page-{{ $i }}">
                                                <a class="page-link @if((isset($event[$i])&& empty($event[$i]))|| (!isset($event[$i])&&$i <=$maxPage))bg-danger text-white @endif"
                                                   wire:click="gotoPage({{ $i }})" href="javascript:void(0)">
                                                    {{ $i }}
                                                </a>
                                            </li>
                                        @endif
                                    @endfor
                                    {{-- Next Page Link --}}
                                    @if ($surveys->hasMorePages())
                                        <li class="page-item" id="next1">
                                            <a class="btn-next active" wire:click="nextPage" wire:loading.attr="disabled"
                                               aria-label="@lang('pagination.next')" href="javascript:void(0)">
                                                <span aria-hidden="true"><i class="bi bi-chevron-right"></i></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    @else
                                        <li class="page-item" aria-disabled="true" aria-label="@lang('pagination.next')" id="next2">
                                            <a class="btn-next">
                                                <span aria-hidden="true"><i class="bi bi-chevron-right"></i></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </nav>
                        @endif
                    </div>
            @endif
                <hr>
            <div class="justify-content-center text-center bnt-result" >
                <button class="btn custom-btn-op1 custom-btn-op1-outline text-uppercase"
                         wire:click="previousPage()" @if($surveys->onFirstPage()) style="display: none" @endif
                >
                    {{__('tool-support.survey_back')}}
                </button>
                @if ($surveys->hasMorePages())
                    <button class="btn custom-btn-op1 custom-btn-op1-fullfill text-uppercase" wire:click="nextPage()">
                        {{__('tool-support.survey_next')}}
                    </button>
                @else
                    <button class="btn custom-btn-op1 custom-btn-op1-fullfill text-uppercase" wire:click="endSurvey">
                        {{__('tool-support.survey_result')}}
                    </button>
                @endif
            </div>
        </div>
        <div class="survey-end" @if(!$show_end) style="display: none" @endif>
            <h3>{{__('tool-support.result_survey')}}</h3>
            <div class="mt-3">
                <p>{{__('tool-support.rate_level_risk')}}: <span class="fw-bolder">{{$result_rate}}</span></p>
                <p>{!! sanitizationText($result_content) !!}</p>
            </div>
            <div class="mt-2">
                <p class="fw-bolder">{{__('tool-support.asset_categoties')}}:</p>
                <div class="chart-container-dou3">
                  <div class="row text-center cate-asset" >
                      @if($result_text)
                        @foreach($result_text as $item)
                            <div class="col-md-6" style="padding:0 20px">
                                <p>{{$item['name']}}</p>
                                <h2>{{$item['value']}}</h2>
                            </div>
                        @endforeach
                      @endif
                  </div>
                </div>
            </div>
            <div class="mt-2">
                <p class="fw-bolder">{{__('tool-support.chart_asset')}}:</p>
                <div class="chart-container-dou3">
                  <div class="d-flex mt-4 align-items-center flex-wrap">
                    <div class="mb-3">
                      <canvas id="pie-1"></canvas>
                    </div>
                    <div id="pie-1-legend-container" class="ml-4"></div>
                  </div>
                </div>
            </div>
            <hr>
            <div class="text-center">
                <button class="btn custom-btn-op1 custom-btn-op1-outline text-uppercase" wire:click="resetSurvey()">
                    {{__('tool-support.survey_reset')}}
                </button>
            </div>
        </div>
    </div>
    <div class="container">
        <h5 style="margin-bottom: 33px;">
            <a style="cursor: pointer;" id="pick-tab-3" class=""><i class="bi bi-arrow-right"></i> {{__('investment.learn_more')}}</a>
        </h5>
    </div>
</div>
