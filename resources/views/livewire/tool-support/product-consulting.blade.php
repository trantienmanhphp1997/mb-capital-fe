<div >
    <div class="container">
        @if($isDesktop == 'true')
            @if (isset($getDataDesktop)) {!! sanitizationText($getDataDesktop->note) !!} @endif
        @else
            @if (isset($getDataMobile))
                {{-- <ul class="nav nav-tabs nav-justified mt-2" role="tablist">
                    <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#mbvf"><p style="font-size: 12px">MBVF</p></a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#mbbond"><p style="font-size: 12px">MBBOND</p></a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#uythac"><p style="font-size: 12px">{{ __('tool-support.trust') }}</p></a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="mbvf" class="container tab-pane active"><br>
                        {!! sanitizationText($getDataMobile[0]['note']) !!}
                    </div>
                    <div id="mbbond" class="container tab-pane fade"><br>
                        {!! sanitizationText($getDataMobile[1]['note'] )!!}
                    </div>
                    <div id="uythac" class="container tab-pane fade"><br>
                        {!! sanitizationText($getDataMobile[2]['note']) !!}
                    </div>
                </div> --}}



                <div class="proifle-tabs">
                    <ul class="nav nav-pills mb-3 d-flex" id="pills-tab-2" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link w-100" id="pills-mbbond-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-mbbond" type="button" role="tab"
                                    aria-controls="pills-mbbond"><p style="font-size: 12px">MBBOND</p></button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link w-100 active" id="pills-mbvf-tab"
                                    data-bs-toggle="pill" data-bs-target="#pills-mbvf" type="button"
                                    role="tab" aria-controls="pills-mbvf"><p style="font-size: 12px">MBVF</p></button>
                        </li>

                        <li class="nav-item" role="presentation">
                            <button class="nav-link w-100" id="pills-trust-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-trust" type="button" role="tab"
                                    aria-controls="pills-trust"><p style="font-size: 12px">{{ __('tool-support.trust') }}</p></button>
                        </li>
                    </ul>
                </div>

                <div class="tab-content mt-3" id="myTabContent">
                    <div class="tab-pane fade show active" id="pills-mbvf" role="tabpanel"
                        aria-labelledby="pills-mbvf-tab">
                        {!! sanitizationText($getDataMobile[0]['note']) !!}
                    </div>
                    <div class="tab-pane fade" id="pills-mbbond" role="tabpanel"
                        aria-labelledby="pills-mbbond-tab">
                        {!! sanitizationText($getDataMobile[1]['note']) !!}
                    </div>
                    <div class="tab-pane fade" id="pills-trust" role="tabpanel"
                        aria-labelledby="pills-trust-tab">
                        {!! sanitizationText($getDataMobile[2]['note']) !!}
                    </div>
                </div>
            @endif
        @endif
    </div>
    <div class="contact contact-bond" @if(!$cookie && !$cookieCal) style="display: none" @endif>
        <div class="container">
            <div class="col-md-12 row mt-2">
                <div class=" col-md-6" @if(!$cookie) style="display: none" @endif>
                    <p class="fw-bolder">{{__('tool-support.chart_asset')}}:</p>
                    <div class="chart-container-dou3 chart-product-consulting">
                        <div class="d-flex mt-4 align-items-center flex-wrap">
                            <div class="mb-3">
                                <canvas id="pie-2"></canvas>
                            </div>
                            <div id="pie-2-legend-container" style="margin-left: -15px !important;"></div>
                        </div>
                    </div>
                </div>
                <div class=" col-md-6" @if(!$cookieCal) style="display: none" @endif>
                    <p class="fw-bolder">{{__('tool-support.calculator.proposed_investment_plan')}}:</p>
                    <div class="chart-container-dou3" style="margin-top: -12px">
                        <div class="d-flex align-items-center flex-wrap">
                            <figure class="highcharts-figure">
                                <div id="containerChartPie"></div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
