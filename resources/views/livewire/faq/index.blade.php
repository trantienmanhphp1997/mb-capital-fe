<div>
    <!-- =======  Banner ======= -->
    <section id="banner-investment" class="d-flex align-items-center">
        <div class="container aos-init aos-animate" data-aos="zoom-out">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <h1 class="titele color-w fw-regula">FAQ</h1>
                </div>    
            </div>
        </div>
    </section>
    <main id="main">
        <!-- =======  FAQ-LIST ======= -->
        <div class="mb-faq mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <ul class="menu-faq">
                            <li >
                                <div style="cursor:pointer" wire:click="setFund()" class="@if(is_null($filterFundId)) active @endif">{{ __('faq.common_question') }}</div>
                            </li>
                            @foreach($listFund as $fund)
                                @if($fund->shortname != 'JAMBF')
                                    <li>
                                        <div wire:click="setFund({{$fund->id}})" class="@if($filterFundId == $fund->id) active @endif"
                                            style="cursor:pointer">{{$fund->shortname2}}
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-lg-10">
                        <div class="row border-l">
                            <div class="search-investment">
                                <input type="text" wire:model.debounce.1000ms="searchTerm" class="form-control" placeholder="{{__('faq.search_placeholder')}}">
                                <span><i class="bi bi-search"></i></span>
                            </div>
                            <div class="list-faq px-0">
                                <div class="accordion" id="accordionExample">
                                    @foreach($listFaq as $faq)
                                    <div class="accordion-item">
                                        <h3 class="accordion-header" id="headingOne">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{$loop->index}}" aria-expanded="@if($loop->first) true @endif" aria-controls="collapseOne">
                                                {{$loop->index + 1}}. {{$faq->question}}
                                            </button>
                                        </h3>
                                        <div id="collapse{{$loop->index}}" class="accordion-collapse collapse @if($loop->first) show @endif" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                                {!!$faq->content!!}
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="space1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ======= Contact to  advise Section ======= -->
        @livewire('component.advise-request', ['ip_client' => Request::ip()])
        <!-- End Counts Section -->
    </main><!-- End #main -->
</div>