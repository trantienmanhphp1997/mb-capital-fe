<div>

  <!-- ======= banner  ======= -->
  <section id="news" class="investment-trust d-flex align-items-center ">
    <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <h1 class="title-news fw-regular color-w pb-md-2">{{__('trust.investment_trusts_individual_customers')}}</h1>
        </div>
        <div class="d-flex pt-md-2" >
            <a href="#contact-advise" class="btn-get-started scrollto bg-gold">{{__('trust.contact_consultant')}}</a>
            <a href="https://mbprivate.mbbank.com.vn/bespoke" class="btn-get-started scrollto color-g border-gold">{{__('trust.MBPrivate_link')}}</a>
          </div>
      </div>
    </div>
  </section>
  <main id="main">
    <!-- ======= mbvf-sip description======= -->
    <section class="mbvfsip-description">
        <div class="container">
            <h2 class="title-news fw-bolder color-second my-md-5">{{$trust->v_value??""}}</h2>
              <p style="color: #4C4C4E; font-family: Avert;" >
                {!! sanitizationText($trust->note??"") !!}
              </p>
        </div>
    </section>
	<!-- ======= utilities======= -->
    <section id="utilities" class="utilities pb-md-4">
        <div class="container " >
          <h3 class="title-bond pb-md-5">{{$trust->v_content??""}}</h3>
          <div class="row">
            @foreach($trustList as $key => $value)
            <div class="col-lg-4 col-md-6 prb-56-35 " >
              <div class="icon-box d-flex">
                <div class="icon">
                  <img src="{{'/storage/'.$value->image}}" width="50" alt="1">
                </div>
                <div class="utilities-box ">
                  <h4>{{$value->v_value}}</h4>
                  <p class="justify">{{$value->v_content}}</p>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </section>
      <!-- ======= cơ cấu danh mục tham khảo ======= -->
      <section class="cate-personal nbfv-calculate contact contact-bond  h-auto py-md-5" >
        @livewire('trust.example')
      </section>
       <!-- ======= timeline ======= -->

       <section class="skills ps-timeline-sec process process-participation pt-5 timeline-mbbond mb-bond-process hide_mobilde">
        <div class="container position-relative  ">
        <h3 class="title-bond pb-30">{{__('trust.participation_process.title')}}</h3>
        <div class="ps-timeline-div">
          <ol class="ps-timeline">
            <li data-aos="zoom-in-up"  >
              <div class="img-handler-top">
                <div class="content-step d-flex align-items-center">
                  <div class="number">1</div>
                  <div class="content-s">{{__('trust.participation_process.identify_customer_needs')}}
                  </div>
                </div>
              </div>
              <span class="ps-sp-top"></span>
            </li>
            <li data-aos="zoom-in-up"  >
              <div class="img-handler-top">
                <div class="content-step d-flex align-items-center">
                  <div class="number">2</div>
                  <div class="content-s">{{__('trust.participation_process.develop_an_investment_strategy')}}
                  </div>
                </div>
              </div>
              <span class="ps-sp-top"></span>
            </li>
            <li data-aos="zoom-in-up"  >
              <div class="img-handler-top">
                <div class="content-step d-flex align-items-center">
                  <div class="number">3</div>
                  <div class="content-s">{{__('trust.participation_process.implementing_investment_strategies')}}
                  </div>
                </div>
              </div>
              <span class="ps-sp-top"></span>
            </li>
            <li data-aos="zoom-in-up"  >
              <div class="img-handler-top">
                <div class="content-step d-flex align-items-center">
                  <div class="number">4</div>
                  <div class="content-s">{{__('trust.participation_process.track_and_manage_categories')}}
                  </div>
                </div>
              </div>
              <span class="ps-sp-top"></span>
            </li>
            <li data-aos="zoom-in-up"  >
                <div class="img-handler-top">
                  <div class="content-step d-flex align-items-center">
                    <div class="number">5</div>
                    <div class="content-s">{{__('trust.participation_process.periodic_reports')}}
                    </div>
                  </div>
                </div>
                <span class="ps-sp-top"></span>
              </li>
          </ol>
        </div>
        </div>
      </section>
      <section class="skills participation-process mb-bond-process mt-5 timeline-mbbond hide-desktop">
        <div class="container position-relative  ">
        <h3 class="title-bond pb-30">{{__('trust.participation_process.title')}}</h3>
        <div class="ps-timeline-div">
          <ol class="ps-timeline">
            <li data-aos="zoom-in-up"  >
              <div class="img-handler-top">
                <div class="content-step d-flex align-items-center">
                  <div class="number">1</div>
                  <div class="content-s">{{__('trust.participation_process.identify_customer_needs')}}
                  </div>
                </div>
              </div>
              <span class="ps-sp-top"></span>
            </li>
            <li data-aos="zoom-in-up"  >
              <div class="img-handler-top">
                <div class="content-step d-flex align-items-center">
                  <div class="number">2</div>
                  <div class="content-s">{{__('trust.participation_process.develop_an_investment_strategy')}}
                  </div>
                </div>
              </div>
              <span class="ps-sp-top"></span>
            </li>
            <li data-aos="zoom-in-up"  >
              <div class="img-handler-top">
                <div class="content-step d-flex align-items-center">
                  <div class="number">3</div>
                  <div class="content-s">{{__('trust.participation_process.implementing_investment_strategies')}}
                  </div>
                </div>
              </div>
              <span class="ps-sp-top"></span>
            </li>
            <li data-aos="zoom-in-up"  >
              <div class="img-handler-top">
                <div class="content-step d-flex align-items-center">
                  <div class="number">4</div>
                  <div class="content-s">{{__('trust.participation_process.track_and_manage_categories')}}
                  </div>
                </div>
              </div>
              <span class="ps-sp-top"></span>
            </li>
            <li data-aos="zoom-in-up"  >
                <div class="img-handler-top">
                  <div class="content-step d-flex align-items-center">
                    <div class="number">5</div>
                    <div class="content-s">{{__('trust.participation_process.periodic_reports')}}
                    </div>
                  </div>
                </div>
                <span class="ps-sp-top"></span>
              </li>
          </ol>
        </div>
        </div>
      </section>
      <!-- ======= package ======= -->
        @livewire('component.fund.maybe-you-are-interested', ['include' => ['31', '3', '2']])
      <!-- ======= end package ======= -->
      <!-- ======= Contact to  advise Section ======= -->
        @livewire('component.advise-request', ['ip_client' => Request::ip()])
    <!-- End Counts Section -->
  </main><!-- End #main -->
</div>
