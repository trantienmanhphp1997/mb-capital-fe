<div>
    <section id="news" class="investment-trust d-flex align-items-center ">
        <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-news fw-regular color-w pb-md-2">{{__('trust.investment_trust_institutional_customers')}}</h1>
                    </div>
                <div class="d-flex pt-md-2">
                    <a href="#contact-advise" class="btn-get-started scrollto bg-gold">{{__('trust.contact_consultant')}}</a>
                </div>
            </div>
        </div>
    </section>
    <main id="main">
        <!-- ======= mbvf-sip description======= -->
        <section class="mbvfsip-description mb-5">
            <div class="container">
                <div class="contents mb-5">
                    <!-- <h2 class="title-news fw-bolder color-second my-md-5">{{$trust->v_value??""}}</h2> -->
                    <h2 class="title-news fw-bolder color-second my-md-5"></h2>
                    {!! sanitizationText($trust->note??"") !!}
                </div>
                <hr style="background: #E5E5E5"/>
            </div>
        </section>

        <!-- ======= utilities======= -->
        <section id="utilities" class="utilities pb-md-5">
            <div class="container" data-aos="fade-up">
                {!!sanitizationText($trustContact->note??"")!!}
            </div>
        </section>

        <!-- ======= package ======= -->
            @livewire('component.fund.maybe-you-are-interested', ['include' => ['3', '2', '4']])
        <!-- ======= Contact to  advise Section ======= -->
            @livewire('component.advise-request', ['ip_client' => Request::ip()])
        <!-- End Counts Section -->
    </main><!-- End #main -->
</div>
