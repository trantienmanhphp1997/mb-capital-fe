<div>
	<style type="text/css">
		.highcharts-credits {
			display: none !important;
		}
		.newSize {
			font-size: 22px !important;
		}
	</style>
	<div class="container">
	    <div class="row">
	        <div class="col-md-6 structure border-right">
	            <div class="row">
	                <h3 class="title-bond color-w mt-0 mb-md-5">{{__('trust.reference_portfolio_structure.title')}}</h3>
	                <div class="col-lg-5 col-xl-6">
	                    <p class="size22 text-right">{{__('trust.reference_portfolio_structure.level_of_risk-taking')}}</p>
	                </div>
	                <div class="col-lg-7 col-xl-6 pl-5 ">
	                    @foreach($trustConfig as $key => $value)
	                    <div class="radio-item mr-5">
	                        <input type="radio" id='{{$key}}' name="ritem" value="{{$value->order_number}}" wire:model='checkConfig'>
	                        <label for='{{$key}}'class="fw-normal">{{$value->v_value}}</label>
	                    </div>
	                    @endforeach
	          {{--           <button class="bg-gold btn-estimate text-uppercase my-5">ước tính</button> --}}
	                </div>
	            </div>
	        </div>
            <div class="col-md-6 my-auto">
               <div class="row justify-content-center">
                    <div class="col-md-12">
			                <figure class="highcharts-figure">
							    <div id="container"></div>
							</figure>
							<div class="result-item d-flex justify-content-center w-50 ml-4">
	                            <p class="size16 mr-3 newSize">{{__('trust.reference_portfolio_structure.with_expected_returns')}}</p>
	                            <p class="newSize">{{$numberValue}}<span class="size16 color-w"> %</span></p>
	                        </div>
                    </div>
                    {{-- <div class="col-md-6" style="padding-top: 90px;">
                        <div class="resultsa">
	                      
                        	@if($data)
                        	@foreach($data as $key => $value)
		                        <div class="result-item d-flex justify-content-between">
		                            <p class="size16">{{$key}}</p>
		                            <p>{{$value['value']}}<span class="size16 color-gray"> %</span></p>
		                        </div>
		                    @endforeach
	                        @endif
                      	</div>
                    </div> --}}
                </div>
            </div>
	    </div>
	</div>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function(){
  window.onload=function(){
	var widthScreen = $(window).width();
  	livewire.emit('getDataHighChart');
		livewire.on('setHighChart', function(data){
			// Build the chart
			colorList = [];
        	if(data){
        		for(var i in data) {
					colorList.push(data[i]['color']);
        		}
        	}
			Highcharts.setOptions({
			    colors: colorList,
			    chart: {
			        style: {
			            fontFamily: 'Avert',
			        }
			    },
			  	legend: {
			  		symbolRadius: 0,
	  		        align: widthScreen > 414 ? 'right' : 'bottom',
			        verticalAlign: widthScreen > 414 ? 'middle' : 'bottom',
			        layout: 'vertical',
                    itemMarginBottom: widthScreen > 414 ? 15 : 0,
				    itemStyle: {
				        color: '#FFF',
				        fontSize: '18px'
				    },
					labelFormatter: function() {
						return (
							`<div class="legend-item">
								<div class="mr-5">
									<span style=" color:'${this.color}'"></span><span>${this.name}</span>
								</div>
								<div>
									<span>${this.percentage}%</span>
								</div>
							</div>`
						);
					},
				},
			});
			Highcharts.chart('container', {
			  exporting:false,
			  chart: {
			    backgroundColor: null,
			    plotBackgroundColor: null,
			    plotBorderWidth: null,
			    plotShadow: false,
			    type: 'pie',
				width: widthScreen > 414 ? 600 : 300,
	        	events: {
			        load: function() {
			        	if(data){
			        		for(var i in data) {
   							 	this.series[0].addPoint([i, parseInt(data[i]['value'])]);
			        		}
			        	}
			        }
			    }
			  },
			  title: {
			    text: '',
			    style: {
			        display: 'none'
			    }
			  },
			  tooltip: {
			    pointFormat: '{series.name}: <b>{point.percentage:f}%</b>',
			  },
			  accessibility: {
			    point: {
			      valueSuffix: '%'
			    }
			  },
			  plotOptions: {
			    pie: {
			      allowPointSelect: false,
			      cursor: 'pointer',
			      dataLabels: {
			        enabled: false,
			      },
			      showInLegend:true,
		          point: {
				    events: {
					    legendItemClick: function(e){
			            	e.preventDefault();
					    }
				    },
				  }
			    }
			  },
			  series: [{
			    name: 'Tỷ lệ',
			    colorByPoint: true,
			  }]
			});
		});
	}
    AOS.init({
        disable: function() {
                let maxWidth = 800;
                return window.innerWidth < maxWidth;
        }
    });
    
});
</script>
