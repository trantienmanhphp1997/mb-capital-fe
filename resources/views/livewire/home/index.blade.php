<div>
  <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner">
        @foreach($listBanner as $banner)
        <div class="carousel-item @if($loop->index == 0) active @endif">
          <img src="{{$banner->image ? 'storage/'.$banner->image : '/assets/img/banner.png' }}" class="mx-auto" alt="slide" style="height: 100%;">
          <div class="mask flex-center">
            <div class="container">
              <div class="row align-items-center">
                <div class="col-md-7 col-12 order-md-1 order-2 title-banner">
                  <div class="line1" style="font-size:{{$banner->number_value ?? 30}}px">{{$banner->v_value}}</div>
                  <div class="line2" style="font-size:{{$banner->number_value ?? 30}}px">{{$banner->v_content}}</div>
                </div>
                <div class="col-md-5 col-12 order-md-2 order-1"></div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span
          class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a
        class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span
          class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
  </div>
  <main id="main">
    <!-- ======= investment needs ======= -->
    <div class="container paragraph item-one mt-4 pb-60" class="carousel slide carousel-fade" data-ride="carousel">
      <h3 class="title mb-3"> {{__('home.investment_needs_title')}}</h3>
      <div class="investment-needs mt-5">
        @foreach($listInvestmentNeed as $investmentNeed)
        <div class="exigency-card">
          <a @if ($investmentNeed->url) href="{{$investmentNeed->url}}" @endif>
            <img src="{{$investmentNeed->image}}" alt="check-icon">
            <div class="title-card">
              <h6>{{$investmentNeed->v_value}}</h6>
            </div>
            <div class="description">
              {!!sanitizationText($investmentNeed->v_content)!!}
            </div>
          </a>
        </div>
        @endforeach
      </div>
    </div>
    <!-- ======= About Section ======= -->
    <div id="about" wire:ignore class="about section-bg">
      <div class="" data-aos="fade-up">
        <div class="paragraph item-two investment-choice mt-1">
          <h3 class="title">{{__('home.investment_options_title')}}</h3>
          <div class="container investment-options-section">
            <div class="owl-carousel-home owl-carousel owl-theme mt-2">
              @foreach($listFund as $fund)
              @if($loop->index == $loop->count - 1)
                <div class="option-card">
                  <div class="option-name">
                    <h6 class="short-name text-uppercase">{{__('home.list_fund.investment_trust')}}</h6>
                    <h6 class="full-name">MB Capital Private</h6>
                  </div>
                  <div class="description">
                    <span>{{__('home.list_fund.investment_trust_content')}}</span>
                  </div>
                  <div class="price d-flex justify-content-center text-center mt-2">
                    <div class="value">
                      <div class="form-inline">
                        <div class="text-nav">&nbsp</div>
                        <div class="vnd">&nbsp</div>
                      </div>
                    </div>
                  </div>
                  <div class="readmore text-center mt-3">
                    <a href="{{route('page.trust.personal')}}"><img src="assets/icon/left-arrow.svg" alt="" class="mr-2"> {{__('home.more')}}</a>
                  </div>
                </div>
              @endif
              @if($fund->id != 1)
              <div class="option-card">
                <div class="option-name">
                  <h6 class="short-name">{{ $fund->shortname }}</h6>
                  <h6 class="full-name">{{$fund->fullname}}</h6>
                </div>
                <div class="description">
                  <span>{!! sanitizationText($fund->description)!!}</span>
                </div>

                @if(count($fund->childFund->filter(function($value, $key) {
                  return $value->type == null;
                })) == 0)
                <div class="price d-flex justify-content-center text-center mt-2">
                  <div class="value">
                    <div class="form-inline">
                      @if($fund->shortname == 'MBVF-SIP')
                      <div class="text-nav">&nbsp</div>
                      <div class="vnd">&nbsp</div>
                      @else
                      <div class="text-nav">{{numberFormat($fund->current_nav)}}</div>
                      <div class="vnd">{{__('fund.vnd')}}</div>
                      @endif
                    </div>
                  </div>
                  {{-- <div class="change-icon mt-1 mr-2">
                    @if($fund->growth >= 0)
                    <i class="bi bi-arrow-up color-g"></i>
                    @else
                    <i class="bi bi-arrow-down color-g"></i>
                    @endif
                  </div>
                  <div class="percent">
                    <p><span class="fw-bolder">{{$fund->growth}}</span>%</p>
                  </div> --}}
                </div>
                @else
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                    @foreach($fund->childFund as $child)
                    <div class="carousel-item @if($loop->index == 0) active @endif">
                      <div class="price d-flex justify-content-center align-items-center text-center mt-2">
                        <div class="value">
                          <div class="form-inline">
                            <div class="mr-2 text-nav">{{$child->fullname}}</div>
                            <div class="text-nav">{{numberFormat($child->current_nav)}}</div>
                            <div class="vnd">{{__('fund.vnd')}}</div>
                          </div>
                        </div>
                        {{-- <div class="change-icon mt-1 mr-2">
                          @if($child->growth >= 0)
                          <i class="bi bi-arrow-up color-g"></i>
                          @else
                          <i class="bi bi-arrow-down color-g"></i>
                          @endif
                        </div>
                        <div class="percent">
                          <span><span class="fw-bolder">{{$child->growth}}</span>%</span>
                        </div> --}}
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
                @endif
                <div class="readmore text-center mt-3">
                  <a href="{{route('page.fund.detail',['slug'=>$fund->slug ?? 'default'])}}"><img src="assets/icon/left-arrow.svg" alt="" class="mr-2"> {{__('home.more')}}</a>
                </div>
              </div>
              @endif
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div><!-- End About Section -->

    <!-- ======= Skills Section ======= -->
    <section id="skills" class="skills mt-5 investment-steps">
      <div class="" data-aos="fade-up">
        <div class="paragraph item-three home-item-three">
          <h3 class="title">{{__('home.investment_step')}}</h3>
          <section class="ps-timeline-sec" data-aos-delay="100">
            <div class="container">
              <div class="ps-timeline-div">
              <ol class="ps-timeline">
                @foreach($listInvestmentStep as $investmentStep)
                <li data-aos="zoom-in-up" data-aos-delay="{{$investmentStep->order_number*200}}">
                  <div class="@if($loop->odd) img-handler-top @else img-handler-bot @endif">
                    <div class="content-step d-flex align-items-center">
                      <div class="step-number">
                        @if($investmentStep->order_number == 1)
                          <p  onclick="$('#nameCustomer' ).focus()" style="cursor:pointer"><a style="color: black;" id='step1'>{{$investmentStep->order_number}} </a></p>
                        @else
                          <p>
                            <a href="{{route('page.instruction.index')}}" style="cursor:pointer">
                              {{$investmentStep->order_number}}
                            </a>
                          </p>
                        @endif
                      </div>
                      <div class="content-s">
                        <h4>{{$investmentStep->v_value}}</h4>
                        <p>{!!sanitizationText($investmentStep->note)!!}</p>
                      </div>
                    </div>
                  </div>
                  <div class="@if($loop->odd) ps-bot @else ps-top @endif">
                  </div>
                  <span class="@if($loop->odd) ps-sp-top @else ps-sp-bot @endif"></span>
                </li>
                @endforeach
              </ol>
            </div>
            </div>
          </section>
        </div>

      </div>
    </section>

    <!-- ======= Contact to  advise Section ======= -->
    @livewire('component.advise-request')
  </main>
  <div wire:ignore.self class="modal fade" tabindex="-1" id="homeModal" role="dialog">
    <div class="modal-dialog modal-lg " role="document">
      <div class="modal-content" style="background-image: url({{'storage/'.($popup->image??'')}});">
        <div class="modal-body">
          <div class="text-right">
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="bi bi-x-lg"></i></button>
            </div>
            <div class="text-center">
                <img  src="/assets/img/logo_modal_new.svg" alt="">
                <h2>{{$popup->v_content??''}}</h2>
            </div>
            <div class="detail-modal">
              {!!sanitizationText($popup->note??'')!!}
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  window.onload = function() {
    $('#step1').hover(function()
    {
        $(this).css('color', '#bd9976');
    }, function()
    {
        $(this).css('color', 'black');
    });
    Livewire.emit('getModal');
    Livewire.on('showModal', function(data) {
      $("#homeModal").modal('show');
    });
  };
</script>
