<div>
  <!-- ======= Hero section ======= -->
  <section id="news" class="d-flex align-items-center">
    <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-6 col-lg-6">
          <h1 class="title-news fw-regular color-w">{{__('recruitment.working_environment')}}</h1>
        </div>
      </div>
    </div>
  </section>
  <main id="main">
   <div class="recruitment" data-aos="fade-up">
      <div class="container">
        <h2 class="title-news fw-bolder color-second mt-md-5">{{__('recruitment.working_environment')}}</h2>
        <p class="cl4ce">{!! sanitizationText($envoriment->note) !!}</p>
      </div>
    </div>
    <div class="recruitment mt-5" data-aos="fade-up">
      <div class="container">
        <h2 class="title-news fw-bolder color-second mt-md-5">{{__('recruitment.social_responsibility')}}</h2>
        <p class="cl4ce">
          {!! sanitizationText($social->note) !!}
        </p>
      </div>
    </div>
    <div class="recruitment" data-aos="fade-up">
      <div class="container">
        <h2 class="title-news fw-bolder color-second mt-md-5" data-aos="fade-up" data-aos-delay="200">
          {{__('recruitment.recruitment')}}</h2>
          @livewire('recruitment.lists')
      </div>
    </div>
      <!-- ======= Contact to  advise Section ======= -->
    @livewire('component.advise-request', ['ip_client' => Request::ip()])
    <!-- End Counts Section -->
  </main><!-- End #main -->
</div>
