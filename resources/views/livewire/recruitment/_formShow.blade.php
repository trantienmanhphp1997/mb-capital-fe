<div>
    <div wire:ignore.self class="modal fade in" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('recruitment.recruitment_details')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">{{__('recruitment.title')}}</label>
                        <textarea name="name" class="form-control" placeholder="{{__('recruitment.title')}}" wire:model.defer='title'> </textarea>
                        @error('title')
                            @include('layouts.partials.text._error')
                        @enderror
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">{{__('recruitment.content')}}</label>
                        <textarea name="name" class="form-control" placeholder="{{__('recruitment.content')}}" wire:model.defer='content' rows='10'> </textarea>
                        @error('content')
                            @include('layouts.partials.text._error')
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('recruitment.back')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>