@section('title', $recruitment->getValueAttribute('title') ?? 'MBCapital')
{{-- @section('meta_title', $recruitment->getValueAttribute('meta_title') ?? '') --}}
{{-- @section('meta_description', recruitment->getValueAttribute('meta_des') ?? '') --}}
<!-- ======= Hero section ======= -->
<section id="news" class="d-flex align-items-center">
    <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100">
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">{{__('recruitment.home_page')}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{__('recruitment.recruitment')}}</li>
                </ol>
            </nav>
            <h1 class="title-detail fw-bolder color-w">{{$recruitment->getValueAttribute('title')}}</h1>
            <a href="{{Storage::url('public/'.$recruitment->file_path)}}" download = 'cv_template.xls'>
                <span class="btn custom-btn-op1 custom-btn-op1-fullfill text-white p-2 d-inline-block">
                    <i class="fa fa-download"></i> {{__('recruitment.downloadTemp')}}
                </span>
            </a>
        </div>
    </div>
</section>
<main id="main">
    <div class="news-list" data-aos="fade-up" data-aos-delay="200"> 
        <div class="container">
            <div class="mt-5">
                <h2 class="title-news fw-bolder color-second mt-md-5 aos-init aos-animate">{{__('recruitment.recruitment_info')}}</h2>
            </div>
            <div class="row mb-5">
                <div class="col-lg-8 col-md-12">
                    <div class="box-detail pt-3">
                        {!! sanitizationText($recruitment->getValueAttribute('content')) !!}
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="pt-3">
                        <div class="recruitment-detail px-3">
                            <div class="detail-item py-3 d-flex">
                                <div>
                                    <img class="mt-1" src="{{asset('assets/icon/recruitment-icon-1.png')}}" alt="recruitment-icon" />
                                </div>
                                <div class="ml-2">
                                    <div>{{__('recruitment.recruitment_date')}}: </div>
                                    <div class="mt-1">{{($recruitment->date_submit) ? date('d/m/Y', strtotime($recruitment->date_submit)) : ''}}</div>
                                </div>
                            </div>

                            <div class="detail-item py-3 d-flex">
                                <div>
                                    <img class="mt-1" src="{{asset('assets/icon/recruitment-icon-2.svg')}}" alt="recruitment-icon" />
                                </div>
                                <div class="ml-2">
                                    <div>{{__('recruitment.rank')}}: </div>
                                    <div class="mt-1">{{$recruitment->getValueAttribute('position')}}</div>
                                </div>
                            </div>

                            <div class="detail-item py-3 d-flex">
                                <div>
                                    <img class="mt-1" src="{{asset('assets/icon/recruitment-icon-3.png')}}" alt="recruitment-icon" />
                                </div>
                                <div class="ml-2">
                                    <div>{{__('recruitment.career')}}:</div>
                                    <div class="mt-1">{{$recruitment->getValueAttribute('job')}}</div>
                                </div>
                            </div>

                            <div class="detail-item py-3 d-flex">
                                <div>
                                    <img class="mt-1" src="{{asset('assets/icon/recruitment-icon-4.png')}}" alt="recruitment-icon" />
                                </div>
                                <div class="ml-2">
                                    <div>{{__('recruitment.skills')}}: </div>
                                    <div class="mt-1">{{$recruitment->getValueAttribute('skill')}}</div>
                                </div>
                            </div>

                            <div class=" py-3 d-flex">
                                <div>
                                    <img class="mt-1" src="{{asset('assets/icon/recruitment-icon-5.png')}}" alt="recruitment-icon" />
                                </div>
                                <div class="ml-2">
                                    <div>{{__('recruitment.language_of_profile_presentation')}}: </div>
                                    <div class="mt-1">{{$recruitment->getValueAttribute('language')}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @livewire('component.advise-request', ['ip_client' => Request::ip()])
</main><!-- End #main -->
