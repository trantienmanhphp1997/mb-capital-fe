<div>
    <div class="recruitment-list">
      @foreach($data as $key => $value)
      <div class="item-recruitment" data-aos="fade-up" data-aos-delay="300">
        <h3 class="title-recruitment fw-regular">
          <a href="" class="">{{$value->getValueAttribute('title')}}</a>
        </h3>
        <div class="recruitment-excerpt">
          {!!sanitizationText($value->getValueAttribute('short_content'))!!}
        </div>
        <div class="text-left">
          <a href='{{route("page.mb.recuiment.detail", ["slug" => $value->slug])}}' class="read-more color-g size18 tag_a_show_detail"><i class="bi bi-arrow-right"></i> {{__('recruitment.see_more')}}</a>
        </div>
      </div>
      @endforeach
    </div>
    @if(count($data))
      {{$data->links()}}
    @endif
</div>