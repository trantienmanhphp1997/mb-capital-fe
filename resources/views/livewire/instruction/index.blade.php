<div>
    <!-- =======  Banner ======= -->
    <section id="banner-investment" class="d-flex align-items-center bg-guideline">
        <div class="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay="100">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-news  color-w fw-regular mb-md-5">{{__('instruction.instruct')}}</h1>
                </div>
                <div class="col-md-3 ">
                    <div class="select     position-relative">
                        <select class="form-select input-custom" wire:model.lazy="fundId">
                            <option value="3">{{__('instruction.mbbond_fund')}}</option>
                            <option value="2">{{__('instruction.mbvf_fund')}}</option>
                        </select>
                        <span class="icon"><img src="{{asset('assets/icon/arrow-down.svg')}}" alt=""></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <button class="btn custom-btn-op1 custom-btn-op1-fullfill text-white p-2">
                        {{__('instruction.choose')}}
                    </button>
                </div>
            </div>
        </div>
    </section>
    <main id="main">
    <div  class="container page-mbbond">
            <div class="tab-bond">
                <ul class="nav nav-pills mb-3 d-flex" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active w-100 tab" id="pills-app-tab" data-bs-toggle="pill" data-bs-target="#pills-app"
                                type="button" role="tab" aria-controls="pills-home" aria-selected="true">{{__('instruction.instructions_via_app')}}</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100 tab" id="pills-web-tab" data-bs-toggle="pill" data-bs-target="#pills-web"
                                type="button" role="tab" aria-controls="pills-profile" aria-selected="false">{{__('instruction.instructions_via_web')}}</button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-app" role="tabpanel" aria-labelledby="pills-app-tab">
                <div class="container">
                    <div class="mb-faq mb-guideline mb-5">
                        <div class="container" data-aos="fade-up" >
                            <div class="row">
                                <div class="col-lg-3" data-aos="fade-up" data-aos-delay="200">
                                    <ul class="menu-faq nav nav-pills">
                                        @foreach($faqData_app as $key => $value)
                                            <li class="w-100 text-left btn">
                                                <div id="{{'pills-tab' . $value->id}}" data-bs-toggle="pill" data-bs-target="{{'#pills-' . $value->id}}" class="{{!$key ? 'active' : ''}}">{{$value->name}}</div>
                                            </li>
                                        @endforeach
                                        <li>
                                            <div class="dashed-border"></div>
                                        </li>
                                        <li class="text-center">
                                            <p class="" style="color: #BDBDBD;">{{__('instruction.document')}}</p>
                                            @if(!empty($file) && !empty($file->img))
                                            <a href="{{asset('storage/' . $file->img)}}" download=""><img src="./assets/icon/pdf.png" alt="pdf-icon"> {{ $file->title  }}</a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-9 tab-content" id="pills-tabContent">
                                    @foreach($faqData_app as $key => $value)
                                        <div class="{{'row border-l mt-5 pl-3 tab-pane fade ' . (!$key ? 'show active' : '')}}" id="{{'pills-' . $value->id }}" role="tabpanel" aria-labelledby="{{'pills-tab' . $value->id}}">
                                            <div class="video-intro">
                                                <h2 class="fw-bolder color-second mb-3">{{__('instruction.video_tutorial')}}</h2>
                                                <div class="video-contrl text-center" style="background: #111">
                                                    <video class="w-25" controls id="video-app">
                                                        <source src="{{ $value->url_video ? $value->url_video : './storage/'. $value->video }}" type="video/mp4">
                                                    </video>
                                                </div>
                                                <!-- <img src="assets/img/video-example.png" alt="ig"> -->
                                            </div>
                                            <div class="procedure mt-5">
                                                <h2 class="fw-bolder color-second">{{__('instruction.implementation_process')}}</h2>
                                                <h6 class="color-second mb-4">{{$value->note}}</h6>
                                            </div>
                                            <div class="list-faq px-0" data-aos="fade-up">
                                                <div class="accordion" id="accordionExample">
                                                    @if(count($value->guidelineDetail) > 0)
                                                        @foreach($value->guidelineDetail as $k => $row)
                                                            <div class="accordion-item">
                                                                <h3 class="accordion-header" id="{{'heading'. $k}}">
                                                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="{{'#collapse'. $k }}" aria-expanded="true" aria-controls="{{'collapse'. $k}}">
                                                                        <span class="number-order-accord mr-3">{{$row->order_number}}</span> {{ $row->name }}
                                                                    </button>
                                                                </h3>
                                                                <div id="{{'collapse'. $k}}" class="{{'accordion-collapse collapse'. (!$k ? 'show' : '')}}" aria-labelledby="{{'heading'. $k}}" data-bs-parent="#accordionExample">
                                                                    <div class="accordion-body">
                                                                        {!! sanitizationText($row->content) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="space1">
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-web" role="tabpanel" aria-labelledby="pills-web-tab">
            <div class="container">
                    <div class="mb-faq mb-guideline mb-5">
                        <div class="container" data-aos="fade-up" >
                            <div class="row">
                                <div class="col-lg-3" data-aos="fade-up" data-aos-delay="200">
                                    <ul class="menu-faq nav nav-pills">
                                        @foreach($faqData_web as $key => $value)
                                            <li class="w-100 text-left btn">
                                                <div id="{{'pills-tab' . $value->id}}" data-bs-toggle="pill" data-bs-target="{{'#pills-' . $value->id}}" class="{{!$key ? 'active' : ''}}">{{$value->name}}</div>
                                            </li>
                                        @endforeach
                                        <li>
                                            <div class="dashed-border"></div>
                                        </li>
                                        <li class="text-center">
                                            <p class="" style="color: #BDBDBD;">{{__('instruction.document')}}</p>
                                            @if(!empty($file) && !empty($file->img))
                                            <a href="{{asset('storage/' . $file->img)}}" download=""><img src="./assets/icon/pdf.png" alt="pdf-icon"> {{ " " . $file->title  }}</a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-9 tab-content" id="pills-tabContent">
                                    @foreach($faqData_web as $key => $value)
                                        <div class="{{'row border-l mt-5 pl-3 tab-pane fade ' . (!$key ? 'show active' : '')}}" id="{{'pills-' . $value->id }}" role="tabpanel" aria-labelledby="{{'pills-tab' . $value->id}}">
                                            <div class="video-intro">
                                                <h2 class="fw-bolder color-second mb-3">{{__('instruction.video_tutorial')}}</h2>
                                                <div class="video-contrl text-center" style="background: #111">
                                                    <video class="w-100" controls id="video-web">
                                                        <source src="{{ $value->url_video ? $value->url_video : './storage/'. $value->video }}" type="video/mp4">
                                                    </video>
                                                </div>
                                                <!-- <img src="assets/img/video-example.png" alt="ig"> -->
                                            </div>
                                            <div class="procedure mt-5">
                                                <h2 class="fw-bolder color-second">{{__('instruction.implementation_process')}}</h2>
                                            </div>
                                            <div class="list-faq px-0" data-aos="fade-up">
                                                <div class="accordion" id="accordionExample">
                                                    @if(count($value->guidelineDetail) > 0)
                                                        @foreach($value->guidelineDetail as $k => $row)
                                                            <div class="accordion-item">
                                                                <h3 class="accordion-header" id="{{'heading'. $k}}">
                                                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="{{'#collapse'. $k }}" aria-expanded="true" aria-controls="{{'collapse'. $k}}">
                                                                        <span class="number-order-accord mr-3">{{$row->order_number}}</span> {{ $row->name }}
                                                                    </button>
                                                                </h3>
                                                                <div id="{{'collapse'. $k}}" class="{{'accordion-collapse collapse'. (!$k ? 'show' : '')}}" aria-labelledby="{{'heading'. $k}}" data-bs-parent="#accordionExample">
                                                                    <div class="accordion-body">
                                                                        {!! sanitizationText($row->content) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="space1">
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ======= Contact to  advise Section ======= -->
        @livewire('component.advise-request')
        <!-- End Counts Section -->

        <script>
            window.addEventListener('load', function() {
                let hash = window.location.hash.replace('#', '');
                if(hash == 'pills-web-tab') {
                    document.getElementById('pills-web-tab').click();
                }
            });
        </script>

    </main>
</div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $( ".tab" ).click(function() {
            window.scrollTo(0, 0);
            });
        });
    </script>
