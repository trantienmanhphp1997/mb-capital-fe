<div>
    <!-- ======= Hero section ======= -->
    <section id="news" class="d-flex align-items-center">
        <div class="container" data-aos="zoom-out" data-aos-delay="100">
            <div class="row">
                <h1 class="title-news fw-regular color-w">{{__('search.search')}}</h1>
            </div>
        </div>
    </section>
    <main id="main">
        <section id="utilities" class="utilities pb-xxl-4">
            <div class="container">
                <h1 class="title-news fw-bolder color-second my-md-5">{{__('search.search')}}</h1>
                <div class="search-infomation d-flex justify-content-center mt-5 mb-5">
                    <div class="search-box">
                        <div class="icon">
                            <img src="./assets/icon/search.svg" alt="search-icon">
                        </div>
                        <input wire:model.lazy="searchFund" type="text" class="form-control custom-input-search"
                            placeholder="{{__('search.search_placeholder')}}">
                    </div>
                    <div class="select-year ml-3">
                        <select class="form-control custom-select" wire:model="selectFund">
                            <option value="">{{__('search.select_fund')}}</option>
                            @if(App::getLocale() == 'vi')
                                @foreach($listFund as $key => $value)
                                    <option value="{{$key}}">{{ $value }}</option>
                                @endforeach
                            @else
                                @foreach($listFundEn as $key => $value)
                                    <option value="{{$key}}">{{ $value }}</option>
                                @endforeach
                            @endif
                        </select>
                        <span class="icon"><img src="./assets/icon/arrow-down-black.png" alt=""></span>
                    </div>
                    <button type="button" class="btn custom-btn-op1 custom-btn-op1-fullfill ml-3">{{__('search.search')}}</button>
                </div>
                <div wire:loading class="loader"></div>
                <hr/>
                <div class="result-search">
                    <!-- <h2 class="title-news fw-bolder color-second my-md-5">Có <span style="color: #DDB996;">03</span> kết quả tìm kiếm</h2> -->
                    <div class="list-result">
                        @if($selectFund == 'trust')
                            <div class="result-item">
                                <a><b>{{__('common.header.investment_trust')}}</b></a>
                                <a href="#" class="color-g size18"></a>
                                <div class="mbbond-documents">
                                  <div>
                                        <a href="{{route('page.trust.personal')}}"><i class="bi bi-box-arrow-up-right color-g mr-2"></i>{{__('common.header.individual_customer')}}</a>
                                        <br>
                                        <a href="{{route('page.trust.index')}}"><i class="bi bi-box-arrow-up-right color-g mr-2"></i>{{__('common.header.institutional_customer')}}</a>
                                  </div>
                            </div>
                        @else
                        @foreach($data as $fund)
                        <div class="result-item">
                            <a href="{{route('page.fund.detail',['slug'=>$fund->slug ?? 'default'])}}"><b>{{ $fund->fullname2 }}</b></a>
                            <a href="#" class="color-g size18"></a>
                            @if($data->count()==1)
                            <div class="mbbond-documents">
                              <div>
                                @if($fund->parent_id == null)
                                    <a href="{{route('page.investors.index',['slug'=>$fund->slug ?? 'default'])}}">
                                        <i class="bi bi-box-arrow-up-right color-g mr-2"></i>{{__('common.header.investor_relations')}}
                                    </a>
                                    <br>
                                @endif
                                @if(in_array($fund->id, $listFundIdHasGuideline))
                                    <a href="{{route('page.instruction.index', ['fund'=>$fund->id])}}"><i class="bi bi-box-arrow-up-right color-g mr-2"></i> {{__('mbbond.trading_instructions_via_the_app')}}</a>
                                    <br>
                                    <a href="{{route('page.instruction.index', ['fund'=>$fund->id]) . '#pills-web-tab' }}"><i class="bi bi-box-arrow-up-right color-g mr-2"></i> {{__('mbbond.trading_instructions_via_the_website')}}</a>
                                @endif
                              </div>
                            </div>
                            @endif
                        </div>
                        <hr>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <!-- ======= package ======= -->
        @livewire('component.fund.maybe-you-are-interested', ['include' => [ '2', '4'], 'uy_thac' => true])
        <!-- ======= end package ======= -->
    </main><!-- End #main -->
</div>
