@extends('layouts.master')
@section('content')
    <section id="news" class="d-flex align-items-center">
        <div class="container" data-aos="zoom-out" data-aos-delay="100">
            <div class="row">
                <h1 class="title-news fw-regular color-w">Khuyến cáo</h1>
            </div>
        </div>
    </section>
    <main id="main">
        <div class="fund-list py-3">
            <p class="container">- Nhà đầu tư cần đọc kỹ Bản cáo bạch trước khi mua chứng chỉ quỹ và nên chú ý tới các khoản giá dịch vụ khi giao dịch chứng chỉ quỹ;
            </p>
            <p class="container">
				- Giá giao dịch chứng chỉ quỹ có thể thay đổi phụ thuộc vào tình hình thị trường và nhà đầu tư có thể chịu thiệt hại về số vốn đầu tư vào quỹ;
            </p>
            <p class="container">
				- Các thông tin về kết quả hoạt động của quỹ trước đây chỉ mang tính tham khảo và không có nghĩa là việc đầu tư sẽ sinh lời cho nhà đầu tư.
            </p>
        </div>
        <!-- ======= Contact to  advise Section ======= -->
        <!-- End Counts Section -->
    </main><!-- End #main -->
@endsection
