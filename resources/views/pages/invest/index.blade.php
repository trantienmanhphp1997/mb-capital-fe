@extends('layouts.master')

@section('content')
    @livewire('investment.index')
<!-- End #main -->
@endsection

@section('js')
    <script>
        $(function(){
            $(".tab-bond").fixTab();
        });
    </script>
@endsection
