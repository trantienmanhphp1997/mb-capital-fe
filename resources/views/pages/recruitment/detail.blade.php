@extends('layouts.master')

@section('css')
	<link href="{{ asset('assets/css/mbbond.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('assets/css/step-line.css') }}">
@endsection
@section('content')
    @livewire('recruitment.detail', ['slug_detail' => $slug])
@endsection