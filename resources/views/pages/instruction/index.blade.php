@extends('layouts.master')

@section('content')
    @livewire('instruction.index')
<!-- End #main -->
@endsection

@section('js')
    <script>
        $(function(){
            $(".tab-bond").fixTab();
            $('video[id="video-app"]').bind('loadedmetadata', function(){
                var rawWidth = $(this).prop('videoWidth'); 
                var rawHeight = $(this).prop('videoHeight');
                if (rawHeight > rawWidth) {
                    $(this).addClass("w-25");
                } else {
                    $(this).addClass("w-100");
                }
            });
            $('video[id="video-web"]').bind('loadedmetadata', function(){
                var rawWidth = $(this).prop('videoWidth'); 
                var rawHeight = $(this).prop('videoHeight');
                if (rawHeight > rawWidth) {
                    $(this).addClass("w-25");
                } else {
                    $(this).addClass("w-100");
                }
            });
        });
    </script>
@endsection
