@extends('layouts.master')

@section('title')
	{{$fund_name}}
@endsection

@section('css')
	@if($type == 1)
		<link href="{{ asset('assets/css/mbhuutri.css') }}" rel="stylesheet">
	@elseif($type == 2)
		<link href="{{ asset('assets/css/mbcapital.css') }}" rel="stylesheet">
	@elseif($type == 3)
		<link href="{{ asset('assets/css/mbbond.css') }}" rel="stylesheet">
	@endif
  	<link rel="stylesheet" href="{{ asset('assets/css/step-line.css') }}">
@endsection
@section('content')

@if($type == 1)
	@livewire('funds.mbbond', ['fund_id' => $fund_id])
@elseif($type == 2)
	@livewire('funds.jampf', ['fund_id' => $fund_id])
@elseif($type == 3)
	@livewire('funds.mbretire', ['fund_id' => $fund_id])
@elseif($type == 4)
    @if (empty($parent_id))
	    @livewire('funds.mbvf', ['fund_id' => $fund_id])
    @else
        @livewire('funds.mbvf-sip', ['fund_id' => $fund_id])
    @endif
@endif

<!-- End #main -->
@endsection
