@extends('layouts.master')

@section('content')
    <!-- ======= Hero section ======= -->
    <section id="news" class="d-flex align-items-center">
        <div class="container" data-aos="zoom-out" data-aos-delay="100">
            <div class="row">
                <h1 class="title-news fw-regular color-w">{{__('fund.investment_fund_management')}}</h1>
            </div>
        </div>
    </section>
    <main id="main">
        <div class="fund-list py-3">
            <div class="container">
                @livewire('component.fund.maybe-you-are-interested', ['include' => ['4','3', '2', '1'], 'home_page' => true])
            </div>
        </div>
        <!-- ======= Contact to  advise Section ======= -->
        @livewire('component.advise-request', ['ip_client' => Request::ip()])
        <!-- End Counts Section -->
    </main><!-- End #main -->
@endsection
