@extends('layouts.master')
@section('content')
    @livewire('news.detail', ['slug' => $slug])
    <!-- End #main -->
@endsection

