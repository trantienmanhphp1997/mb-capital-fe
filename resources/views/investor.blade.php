@extends('layouts.master')
@section('content')
    @livewire('investor.index', ['slug' => $slug])
@endsection
