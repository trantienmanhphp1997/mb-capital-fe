@if(checkRole($model."-create"))
<a class="btn btn-success" onclick="getCreateURL()">Thêm mới</a>
@endif
<script>
    function getCreateURL() {
    	var url = window.location.href;
    	url = url.split('?')[0];
		window.location.href = url + "/create";
    }
</script>
