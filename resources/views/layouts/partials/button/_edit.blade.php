
@if(checkButtonCanView("edit"))
      <button class= "btn par6" title="{{__('common.button.edit')}}" onclick="getEditURL({{ $id }})" class="btn-edit-film btn-info" >
         <img src="/images/pent2.svg" alt="pent">
      </button>
@endif

<script>
   function getEditURL(id) {
    var url = window.location.href;
    url = url.split('?')[0];
    
    window.location.href = url +  "/edit"  + "/" + id ;
   }
</script>


