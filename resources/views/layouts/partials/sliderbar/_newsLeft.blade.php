<div class="col-md-2 col-xl-3 sidebar-left">
    <ul class="nav flex-column memu-main-left">
        <li class="nav-item">
            <a class="nav-link" href="#">
                    <span><img src="/images/news-list-icon.png" alt="news-list-icon"
                               height="24"></span>{{__('news/newsManager.menu_name.news')}}
            </a>
            <span class="angle-right">
              <img src="/images/angle-right.svg" alt="angle-right">
            </span>
            <ul class="dropdown-menu-left @if(strpos(Route::currentRouteName(),'admin.news') !== false) active @endif ">
                @if(checkPermission('admin.news.posts.index'))
                    <li>
                        <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.news.posts.index'])) active @endif"
                           href="{{route('admin.news.posts.index')}}">
                            <span class="circle"></span>{{__('news/newsManager.menu_name.news_child.post')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.news.posts.index'))
                    <li>
                        <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.news.posts.draft'])) active @endif"
                           href="{{route('admin.news.posts.draft')}}">
                            <span class="circle"></span>{{__('news/newsManager.menu_name.news_child.draft')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.news.posts.index'))
                    <li>
                        <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.news.posts.need_approval'])) active @endif"
                           href="{{route('admin.news.posts.need_approval')}}">
                            <span class="circle"></span>{{__('news/newsManager.menu_name.news_child.need_approval')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.news.posts.index'))
                    <li>
                        <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.news.posts.approved'])) active @endif"
                           href="{{route('admin.news.posts.approved')}}">
                            <span class="circle"></span>{{__('news/newsManager.menu_name.news_child.approve')}}
                        </a>
                    </li>
                @endif
            </ul>
        </li>
        @if(checkPermission('admin.news.category.index'))
            <li class="nav-item">
                <a class="nav-link" href="{{route('admin.news.category.index')}}">
                    <span><img src="/images/category-list-icon.png" alt="category-list-icon"
                               height="24"></span>{{__('news/newsManager.menu_name.category_page')}}
                </a>
            </li>
        @endif
    </ul>
</div>
