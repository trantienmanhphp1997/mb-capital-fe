<div class="col-md-2 col-xl-3 sidebar-left">
	<ul class="nav flex-column memu-main-left">
        @if(checkPermission('admin.executive.dashboard.index'))
            <li class="nav-item dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.executive.dashboard.index'])) active @endif" style="border:0;display:block;padding:0">
                <a class="nav-link @if(in_array(Route::currentRouteName(), [ 'admin.executive.dashboard.index'])) active @endif" href="{{route('admin.executive.dashboard.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.dashboard_executive')}}">
                    <span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span> {{__('menu_management.menu_name.dashboard_executive')}}
                </a>
            </li>
        @endif
		<li class="nav-item parent-menu @if(in_array(Route::currentRouteName(), ['admin.user.index','admin.department.index'])) active @endif">
			<a class="nav-link" href="#" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.user_info_list')}}">
				<span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span> {{__('menu_management.menu_name.user_info_list')}}
			</a>
			<span class="angle-right">
				<img src="/images/angle-right.svg" alt="angle-right">
			</span>
			<ul class="nav-item dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.user.index','admin.department.index'])) active @endif">
				@if(checkPermission('admin.department.index'))
				<li class="child-menu">
					<a class="nav-link @if(Route::currentRouteName() == 'admin.department.index') active @endif" href="{{route('admin.department.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.departments')}}">
						<span class="circle2"></span> {{__('menu_management.menu_name.departments')}}
					</a>
				</li>
				@endif
				@if(checkPermission('admin.user.index'))
				<li class="child-menu">
					<a class="nav-link @if(Route::currentRouteName() == 'admin.user.index') active @endif" href="{{route('admin.user.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.personnel')}}">
						<span class="circle2"></span> {{__('menu_management.menu_name.personnel')}}
					</a>
				</li>
				@endif
			</ul>
		</li>
		<!-- TỔng vụ -->
		<li class="nav-item parent-menu @if(in_array(Route::currentRouteName(), ['admin.general.work-plan.index','admin.general.businessfee.index','admin.general.form.index'])) active @endif">
			<a class="nav-link" href="#" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.general')}}">
				<span><img src="/images/Clipboard-list.png"  alt="clipboard-list" height="24"></span> {{__('menu_management.menu_name.general')}}
			</a>
			<span class="angle-right">
				<img src="/images/angle-right.svg" alt="angle-right">
			</span>
            <ul class="nav-item dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.general.work-plan.index','admin.general.businessfee.index'])) active @endif">
                @if(checkPermission('admin.general.work-plan.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.general.work-plan.index') active @endif"
                           href="{{route('admin.general.work-plan.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.business_plan')}}">
                            <span class="circle2"></span> {{__('menu_management.menu_name.business_plan')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.general.businessfee.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.general.businessfee.index') active @endif"
                           href="{{route('admin.general.businessfee.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.business_fee')}}">
                            <span class="circle2"></span> {{__('menu_management.menu_name.business_fee')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.general.form.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.general.form.index') active @endif"
                           href="{{route('admin.general.form.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('data_field_name.template_form.management')}}">
                            <span class="circle2"></span>
                            {{__('data_field_name.template_form.management')}}
                        </a>
                    </li>
                @endif
            </ul>
        </li>
        <!-- HĐLĐ -->
        <li class="nav-item parent-menu @if(in_array(Route::currentRouteName(), ['admin.executive.time-sheets.index', 'admin.executive.user-info-leave.index', 'admin.executive.contract-and-salary.contract.index', 'admin.executive.contract-and-salary.my-contract.index'])) active @endif">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.contract_and_salary')}}">
                <span><img src="/images/clipboard-list.svg" alt="clipboard-list"
                           height="24"></span> {{__('menu_management.menu_name.contract_and_salary')}}
            </a>
            <span class="angle-right">
				<img src="/images/angle-right.svg" alt="angle-right">
			</span>
            <ul class="dropdown-menu-left @if(in_array(Route::currentRouteName(), [ 'admin.executive.time-sheets.index', 'admin.executive.user-info-leave.index', 'admin.executive.contract-and-salary.contract.index', 'admin.executive.contract-and-salary.my-contract.index', 'admin.executive.my-time-sheets.index',
			'admin.executive.contract-and-salary.my-salary.index',
			'admin.executive.my-day-off.index',
			'admin.executive.salary.index'])) active @endif">
                <li class="parent-menu">
                    <a class="nav-link @if(in_array(Route::currentRouteName(), [ 'admin.executive.contract-and-salary.my-contract.index',
					'admin.executive.my-time-sheets.index',
					'admin.executive.my-day-off.index',
                    'admin.executive.contract-and-salary.my-salary.index'])) active @endif" href="#" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.infomation-management')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.infomation-management')}}
                    </a>
                    <ul class="dropdown-menu-left @if(in_array(Route::currentRouteName(), [ 'admin.executive.contract-and-salary.my-contract.index', 'admin.executive.my-time-sheets.index',
					'admin.executive.my-day-off.index',
                    'admin.executive.contract-and-salary.my-salary.index'])) active @endif">
                        @if(checkPermission('admin.executive.my-day-off.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(Route::currentRouteName() == 'admin.executive.my-day-off.index') active @endif"
                                   href="{{route('admin.executive.my-day-off.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.user-info-leave')}}">
                                    <span class="circle2"></span> {{__('menu_management.menu_name.user-info-leave')}}
                                </a>
                            </li>
                        @endif
                        @if(checkPermission('admin.executive.my-time-sheets.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(Route::currentRouteName() == 'admin.executive.my-time-sheets.index') active @endif"
                                   href="{{route('admin.executive.my-time-sheets.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.timekeeping')}}">
                                    <span class="circle2"></span> {{__('menu_management.menu_name.timekeeping')}}
                                </a>
                            </li>
                        @endif
                        @if(checkPermission('admin.executive.contract-and-salary.my-contract.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(Route::currentRouteName() == 'admin.executive.contract-and-salary.my-contract.index') active @endif"
                                   href="{{route('admin.executive.contract-and-salary.my-contract.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.my-contract-info')}}">
                                    <span class="circle2"></span> {{__('menu_management.menu_name.my-contract-info')}}
                                </a>
                            </li>
                        @endif
                        @if(checkPermission('admin.executive.contract-and-salary.my-salary.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(Route::currentRouteName() == 'admin.executive.contract-and-salary.my-salary.index') active @endif"
                                   href="{{route('admin.executive.contract-and-salary.my-salary.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.my_salary_info')}}">
                                    <span class="circle2"></span> {{__('menu_management.menu_name.my_salary_info')}}
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
                <li class="parent-menu">
                    <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.executive.time-sheets.index', 'admin.executive.user-info-leave.index', 'admin.executive.contract-and-salary.contract.index'])) active @endif"
                       href="#" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.data-management')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.data-management')}}
                    </a>
                    <ul class="dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.executive.time-sheets.index', 'admin.executive.user-info-leave.index', 'admin.executive.contract-and-salary.contract.index',
					'admin.executive.salary.index'])) active @endif">
                        @if(checkPermission('admin.executive.time-sheets.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.executive.time-sheets.index'])) active @endif"
                                   href="{{route('admin.executive.time-sheets.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.timekeeping-data')}}">
                                    <span class="circle2"></span>{{__('menu_management.menu_name.timekeeping-data')}}
                                </a>
                            </li>
                        @endif
                        @if(checkPermission('admin.executive.user-info-leave.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.executive.user-info-leave.index'])) active @endif"
                                   href="{{route('admin.executive.user-info-leave.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.resign')}}">
                                    <span class="circle2"></span>{{__('menu_management.menu_name.resign')}}
                                </a>
                            </li>
                        @endif
                        @if(checkPermission('admin.executive.contract-and-salary.contract.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.executive.contract-and-salary.contract.index'])) active @endif"
                                   href="{{route('admin.executive.contract-and-salary.contract.index')}}"data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.contract_info')}}">
                                    <span class="circle2"></span>{{__('menu_management.menu_name.contract_info')}}
                                </a>
                            </li>
                        @endif
                        @if(checkPermission('admin.executive.salary.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.executive.salary.index'])) active @endif"
                                   href="{{route('admin.executive.salary.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.salary_info')}}">
                                    <span class="circle2"></span>{{__('menu_management.menu_name.salary_info')}}
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            </ul>
        </li>
        <!-- Khen thưởng & kỷ luật -->
        @if(checkPermission('admin.decision-reward.index'))
            <li class="nav-item dropdown-menu-left" style="border:0;display:block;padding:0">
                <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.decision-reward.index'])) active @endif"
                   href="{{route('admin.decision-reward.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.reward_and_discipline')}}">
                    <span><img src="/images/clipboard-list.svg" alt="clipboard-list"
                               height="24"></span> {{__('menu_management.menu_name.reward_and_discipline')}}
                </a>
            </li>
        @endif
    <!-- Quản lý ngân sách -->
        <li class="nav-item parent-menu @if(in_array(Route::currentRouteName(), ['admin.budget.index','admin.budget.report.index','admin.budget.list-budget-approved.index','admin.budget.history-budget.index','admin.budget.budget-history-change'])) active @endif">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.budget-management')}}">
                <span><img src="/images/left-menu-money.png" alt="left-menu-money"
                           height="24"></span> {{__('menu_management.menu_name.budget-management')}}
            </a>
            <span class="angle-right">
				<img src="/images/angle-right.svg" alt="angle-right">
			</span>
            <ul class="nav-item dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.budget.index','admin.budget.report.index','admin.budget.list-budget-approved.index','admin.budget.history-budget.index','admin.budget.budget-history-change', 'admin.template.form.index'])) active @endif">
                @if(checkPermission('admin.budget.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.budget.index') active @endif"
                           href="{{route('admin.budget.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.budget_list')}}">
                            <span class="circle2"></span> {{__('menu_management.menu_name.budget_list')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.budget.report.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.budget.report.index') active @endif"
                           href="{{route('admin.budget.report.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.report_status_budget')}}">
                            <span class="circle2"></span> {{__('menu_management.menu_name.report_status_budget')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.budget.list-budget-approved.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.budget.list-budget-approved.index') active @endif"
                           href="{{route('admin.budget.list-budget-approved.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.monitor_budget')}}">
                            <span class="circle2"></span>  {{__('menu_management.menu_name.monitor_budget')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.budget.history-budget.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.budget.history-budget.index') active @endif"
                           href="{{route('admin.budget.history-budget.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('data_field_name.budget.history_change')}}">
                            <span class="circle2"></span>
                            {{__('data_field_name.budget.history_change')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.template.form.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.template.form.index') active @endif"
                           href="{{route('admin.template.form.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('data_field_name.template_form.management')}}">
                            <span class="circle2"></span>
                            {{__('data_field_name.template_form.management')}}
                        </a>
                    </li>
                @endif
            </ul>
        </li>
        <!-- Quản lý tài sản -->
        <li class="nav-item parent-menu @if(in_array(Route::currentRouteName(),['admin.asset.dashboard.index','admin.asset.category.index','admin.asset.index','admin.asset.allocated-revoke.index','admin.asset.transfer.index','admin.asset.lost-cancel-liquidation.index','admin.asset.maintenance.index','admin.asset.inventory.index','admin.asset.report.index','admin.asset.provider.index'])) active @endif">
            <a class="nav-link "
               href="#" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.asset-management')}}">
                <span><img src="/images/Commode.svg" alt="commode"
                           height="24"></span> {{__('menu_management.menu_name.asset-management')}}
            </a>
            <span class="angle-right">
				<img src="/images/angle-right.svg" alt="angle-right">
			</span>
            <ul id="drop_menu"
                class="dropdown-menu-left  @if(in_array(Route::currentRouteName(),['admin.asset.dashboard.index','admin.asset.category.index','admin.asset.index','admin.asset.allocated-revoke.index','admin.asset.transfer.index','admin.asset.lost-cancel-liquidation.index','admin.asset.maintenance.index','admin.asset.inventory.index','admin.asset.inventory_period.index','admin.asset.report.index','admin.asset.provider.index'])) active @endif">
                <li class="parent-menu @if(!checkPermission('admin.asset.dashboard.index') && !checkPermission('admin.asset.category.index') && !checkPermission('admin.asset.index')) d-none  @endif">
                    <a class="nav-link @if(in_array(Route::currentRouteName(),['admin.asset.dashboard.index','admin.asset.category.index','admin.asset.index'])) active @endif"
                       href="#" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.asset')}}">
                        <span class="circle"></span>{{__('menu_management.menu_name.asset')}}
                    </a>
                    <ul id="drop_menu"
                        class="dropdown-menu-left  @if(in_array(Route::currentRouteName(),['admin.asset.dashboard.index','admin.asset.category.index','admin.asset.index'])) active @endif">
                        @if(checkPermission('admin.asset.dashboard.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(Route::currentRouteName() == 'admin.asset.dashboard.index') active @endif"
                                   href="{{route('admin.asset.dashboard.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.dashboard_asset')}}">
                                    <span class="circle"></span> {{__('menu_management.menu_name.dashboard_asset')}}
                                </a>
                            </li>
                        @endif
                        @if(checkPermission('admin.asset.category.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(Route::currentRouteName() == 'admin.asset.category.index') active @endif"
                                   href="{{route('admin.asset.category.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('data_field_name.asset_category.asset_category_list')}}">
                                    <span
                                        class="circle"></span> {{__('data_field_name.asset_category.asset_category_list')}}
                                </a>
                            </li>
                        @endif

                        @if(checkPermission('admin.asset.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(Route::currentRouteName() == 'admin.asset.index') active @endif"
                                   href="{{route('admin.asset.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.asset_list')}}">
                                    <span class="circle"></span>{{__('menu_management.menu_name.asset_list')}}
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
                <li class="parent-menu @if(!checkPermission('admin.asset.allocated-revoke.index') && !checkPermission('admin.asset.transfer.index') && !checkPermission('admin.asset.lost-cancel-liquidation.index')) d-none  @endif">
                    <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.asset.allocated-revoke.index','admin.asset.transfer.index','admin.asset.lost-cancel-liquidation.index'])) active @endif"
                       href="#" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.asset_volatility')}}">
                        <span class="circle"></span> {{__('menu_management.menu_name.asset_volatility')}}
                    </a>
                    <ul id="drop_menu"
                        class="dropdown-menu-left  @if(in_array(Route::currentRouteName(), ['admin.asset.allocated-revoke.index','admin.asset.transfer.index','admin.asset.lost-cancel-liquidation.index'])) active @endif">
                        @if(checkPermission('admin.asset.allocated-revoke.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(Route::currentRouteName() == 'admin.asset.allocated-revoke.index') active @endif"
                                   href="{{route('admin.asset.allocated-revoke.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.asset-allocated-revoke')}}">
                                    <span
                                        class="circle"></span> {{__('menu_management.menu_name.asset-allocated-revoke')}}
                                </a>
                            </li>
                        @endif
                        @if(checkPermission('admin.asset.transfer.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(Route::currentRouteName() == 'admin.asset.transfer.index') active @endif"
                                   href="{{route('admin.asset.transfer.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.transfer')}}">
                                    <span class="circle"></span> {{__('menu_management.menu_name.transfer')}}
                                </a>
                            </li>
                        @endif
                        @if(checkPermission('admin.asset.lost-cancel-liquidation.index'))
                            <li class="child-menu">
                                <a class="nav-link @if(Route::currentRouteName() == 'admin.asset.lost-cancel-liquidation.index') active @endif"
                                   href="{{route('admin.asset.lost-cancel-liquidation.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.lost-cancel-liquidation')}}">
                                    <span class="circle"></span>{{__('menu_management.menu_name.lost-cancel-liquidation')}}
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>

                @if(checkPermission('admin.asset.maintenance.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.asset.maintenance.index') active @endif"
                           href="{{route('admin.asset.maintenance.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.maintenance')}}">
                            <span class="circle"></span>{{__('menu_management.menu_name.maintenance')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.asset.inventory_period.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.asset.inventory_period.index') active @endif"
                           href="{{route('admin.asset.inventory_period.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.inventory_period')}}">
                            <span class="circle"></span>
                            {{__('menu_management.menu_name.inventory_period')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.asset.inventory.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.asset.inventory.index') active @endif"
                           href="{{route('admin.asset.inventory.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.inventory')}}">
                            <span class="circle"></span>{{__('menu_management.menu_name.inventory')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.asset.report.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(Route::currentRouteName() == 'admin.asset.report.index') active @endif" href="{{route('admin.asset.report.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.asset_report')}}">
                            <span class="circle"></span>{{__('menu_management.menu_name.asset_report')}}
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.asset.provider.index'))
                    <li class="child-menu">
                        <a class="nav-link @if(in_array(Route::currentRouteName(), ['admin.asset.provider.index'])) active @endif" href="{{route('admin.asset.provider.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.asset-provider')}}">
                            <span class="circle"></span> {{__('menu_management.menu_name.asset-provider')}}
                        </a>
                    </li>
                @endif
            </ul>
        </li>
        <!-- Quản lý mua sắm -->
        @if(checkPermission('admin.executive.shopping.index'))
            <li class="nav-item dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.executive.shopping.index'])) active @endif" style="border:0;display:block;padding:0">
                <a class="nav-link @if(in_array(Route::currentRouteName(), [ 'admin.executive.shopping.index'])) active @endif"
                   href="{{route('admin.executive.shopping.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.manage-shopping')}}">
                    <span><img src="/images/clipboard-list.svg" alt="clipboard-list"
                               height="24"></span> {{__('menu_management.menu_name.manage-shopping')}}
                </a>
            </li>
        @endif

        @if(checkPermission('admin.finance.index'))
            <li class="nav-item dropdown-menu-left @if(in_array(Route::currentRouteName(), ['admin.finance.index'])) active @endif" style="border:0;display:block;padding:0">
                <a class="nav-link @if(in_array(Route::currentRouteName(), [ 'admin.finance.index'])) active @endif" href="{{route('admin.finance.index')}}" data-toggle="tooltip" data-placement="right" title="{{__('menu_management.menu_name.finance_management')}}">
                    <span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span> {{__('menu_management.menu_name.finance_management')}}
                </a>
            </li>
        @endif
    </ul>
</div>
