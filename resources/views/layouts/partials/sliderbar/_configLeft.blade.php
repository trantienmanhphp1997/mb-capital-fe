<!--start sidebar-left-->
<div class="col-md-2 col-xl-3 sidebar-left">
    <ul class="nav flex-column memu-main-left">
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span> Cấu hình hệ thống
            </a>
            <span class="angle-right">
                <img src="/images/angle-right.svg" alt="angle-right">
            </span>
            <ul class="dropdown-menu-left">
                <li>
                    <a class="nav-link active" href="{{ route('admin.config.notification.public') }}">
                        <span class="circle"></span> Cấu hình thông báo
                    </a>
                </li>
                <li>
                    <a class="nav-link active" href="{{ route('admin.config.notification.private') }}">
                        <span class="circle"></span> Cấu hình riêng
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
<!--end sidebar-left-->
