<div class="col-md-2 col-xl-3 sidebar-left">
              <div class="title-menu">
                  <livewire:common.modal.modal-config-menu />
              </div>
              <ul class="nav flex-column">
                  @foreach($menu as $page)
                      @if(checkPermission($page->menu->alias))
                          <li class="nav-item">
                              <a class="nav-link" href="{{$page->menu ? route($page->menu->alias) : '#'}}" data-toggle="tooltip" data-placement="right"
                                title="@if($page->menu && isset(\App\Enums\EMenu::listNameMenu()[$page->menu->alias]))
                                {{ \App\Enums\EMenu::listNameMenu()[$page->menu->alias]}}
                                  @else
                                      chưa xác định
                                  @endif">
                                  <span><img src="/images/Clipboard.svg" alt="clipboard" height="24"></span>
                                  @if($page->menu && isset(\App\Enums\EMenu::listNameMenu()[$page->menu->alias]))
                                      {{ \App\Enums\EMenu::listNameMenu()[$page->menu->alias]}}
                                  @else
                                      chưa xác định
                                  @endif
                              </a>
                          </li>
                      @endif
                  @endforeach
                {{--<li class="nav-item">
                    <a class="nav-link" href="#">
                      <span><img src="/images/Shield-protected.svg" alt="shield-protected" height="24"></span> Số lương
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">
                      <span ><img src="/images/Clipboard.svg" alt="clipboard" height="24"></span> Thống kê đi làm
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">
                      <span><img src="/images/Chart-pie.svg" alt="chart-pie" height="24"></span> Quản lý công tác phí
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">
                      <span><img src="/images/Chart-bar.svg" alt="chart-bar" height="24"></span> Quản lý ngân sách
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">
                      <span><img src="/images/Door-open.svg" alt="door-open" height="24"></span> Đăng ký thông hành
                    </a>
                  </li>--}}
              </ul>
        </div>
