<!--start sidebar-left-->
<div class="col-md-2 col-xl-3 sidebar-left">
    <ul class="nav flex-column memu-main-left">
    
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span> Danh sách kế hoạch công tác
            </a>
            <span class="angle-right">
                        <img src="/images/angle-right.svg" alt="angle-right">
                    </span>
            <ul class="dropdown-menu-left">
                <li>
                    <a class="nav-link active" href="#">
                        <span class="circle"></span> Quản lý thông tin dự án
                    </a>
                    <ul class="dropdown-menu-left">
                        <li>
                            <a class="nav-link active" href="#">
                                <span class="circle2"></span> Quản lý thông tin dự án
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="#">
                                <span class="circle2"></span> Quản lý công việc dự án
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="nav-link" href="#">
                        <span class="circle"></span> Quản lý công việc dự án
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="#">
                        <span class="circle"></span> Quản lý chi phí dự án
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="#">
                        <span class="circle"></span> Hồ sơ cá nhân
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span><img src="/images/Flag.svg" alt="flag" height="24"></span> Quản lý dự án nghiên cứu
            </a>
            <span class="angle-right">
                        <img src="/images/angle-right.svg" alt="angle-right">
                    </span>
            <ul class="dropdown-menu-left">
                <li>
                    <a class="nav-link active" href="#">
                        <span class="circle"></span> Quản lý thông tin dự án
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="#">
                        <span class="circle"></span> Quản lý công việc dự án
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="#">
                        <span class="circle"></span> Quản lý chi phí dự án
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="#">
                        <span class="circle"></span> Hồ sơ cá nhân
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span><img src="/images/Calculator.svg" alt="calculator" height="24"></span> Quản lý chi phí dự án
            </a>
            <span class="angle-right">
                        <img src="/images/angle-right.svg" alt="angle-right">
                    </span>
            <ul class="dropdown-menu-left">
                <li>
                    <a class="nav-link active" href="#">
                        <span class="circle"></span> Quản lý thông tin dự án
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="#">
                        <span class="circle"></span> Quản lý công việc dự án
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="#">
                        <span class="circle"></span> Quản lý chi phí dự án
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="#">
                        <span class="circle"></span> Hồ sơ cá nhân
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span><img src="/images/Box.svg" alt="box" height="24"></span> Quản lý thành quả nghiên cứu
            </a>
            <span class="angle-right">
                        <img src="/images/angle-right.svg" alt="angle-right">
                    </span>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span><img src="/images/File-done.svg" alt="file-done" height="24"></span> Quản lý báo cáo
            </a>
            <span class="angle-right">
                        <img src="/images/angle-right.svg" alt="angle-right">
                    </span>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span><img src="/images/calculator1.svg" alt="calculator" height="24"></span> Quản lý công tác phí
            </a>
            <span class="angle-right">
                        <img src="/images/angle-right.svg" alt="angle-right">
                    </span>
            <ul class="dropdown-menu-left">
            @if(checkPermission('admin.general.businessfee.index'))
                <li>
                    <a class="nav-link active" href="{{route('admin.general.businessfee.index')}}">
                        <span class="circle"></span> Danh sách công tác phí
                    </a>
                </li>
            @endif
               
            </ul>
        </li>
    </ul>
</div>
<!--end sidebar-left-->
