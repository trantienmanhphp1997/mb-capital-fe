<div class="col-md-2 col-xl-3 sidebar-left">
  <ul class="nav flex-column memu-main-left">
      <li class="nav-item">
          <a class="nav-link" href="#">
              Quản lý nhân sự
          </a>

      </li>
        @if(checkPermission('admin.user.index'))
            <li class="nav-item">
              <a class="nav-link" href="{{route('admin.user.index')}}">
                <span><img src="/images/clipboard-list.svg" alt="clipboard-list" height="24"></span> Danh sách nhân sự
              </a>
            </li>
        @endif
        @if(checkPermission('admin.department.index'))
            <li class="nav-item">
              <a class="nav-link" href="{{route('admin.department.index')}}">
                <span ><img src="/images/Flag.svg" alt="flag" height="24"></span> Danh sách phòng ban
              </a>
            </li>
        @endif
  </ul>
</div>
