  <!-- ======= Top Bar ======= -->
  <section id="topbar">
    <div class="container d-flex justify-content-center justify-content-md-between h-100">
      <div class="contact-info d-flex align-items-center">
      </div>
      <div class="social-links d-none d-md-flex align-items-center">
        <div class="mr-3">
          <a href="{{route('page.search.index')}}"><img src="{{asset('assets/icon/search-op1.svg')}}" alt=""></a>
        </div>
        <div class="mr-3">
          <a href="{{route('page.mb.about')}}">{{__('common.topbar.about_MB')}}</a>
        </div>
        <div style="border: 1px solid #E5E5E5; height: 15px;"></div>
        <div class="mr-3 ml-3">
          <a href="{{route('page.news.index')}}">{{__('common.topbar.news')}}</a>
        </div>
        <div style="border: 1px solid #E5E5E5; height: 15px;"></div>
        <div class="mr-4 ml-3">
          <a href="{{ route('page.contact.index') }}">{{__('common.topbar.contact')}}</a>
        </div>
        <div class="transt-EN ml-3">

              @if (\Illuminate\Support\Facades\App::isLocale('en'))
                <a class="translate-btn" href="{{ route('lang', ['lang' => 'vi']) }}">
                <img src="{{ asset('assets/icon/VN.png') }}" alt="">
                </a>
              @else

                <a class="translate-btn" href="{{ route('lang', ['lang' => 'en']) }}">
                  <img style="width: 28px;height: 20px" src="{{ asset('assets/icon/EN.png') }}" alt="">
                </a>
              @endif

        </div>
        {{-- <div class="transt-EN">
          <a  class="translate-btn" href="{{ route('lang', ['lang' => 'en']) }}" ><img src="{{ asset('assets/icon/au.png') }}" alt=""></a>
        </div> --}}
        {{-- <div style="border: 1px solid #E5E5E5; height: 15px;"></div>
         --}}
      </div>
    </div>
  </section>
