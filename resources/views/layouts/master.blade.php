<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<x-head/>
@yield('css')
    <link rel="stylesheet" href="{{ asset('assets/css/datepicker.css') }}">
<body>
@include('layouts.partials._topbar')
@include('layouts.partials._header')

  <!-- ======= Hero Section ======= -->
  <!-- <section id="hero" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <h1>Welcome to <span>BizLand</span></h1>
      <h2>We are team of talented designers making websites with Bootstrap</h2>
      <div class="d-flex">
        <a href="#about" class="btn-get-started scrollto">Get Started</a>
        <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i
            class="bi bi-play-circle"></i><span>Watch Video</span></a>
      </div>
    </div>
  </section> -->
        @yield('content')

  <!-- ======= Footer ======= -->

<!--   <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
      class="bi bi-arrow-up-short"></i></a> -->
@include('layouts.partials._footer')

<x-toast/>
@include('layouts.partials._script')
@yield('js')
@livewireStyles
@livewireScripts

</body>

</html>
