<?php

return [
    'survey_start'=>'Bảng câu hỏi đánh giá tính phù hợp mà Quý khách sắp điền vào cho phép MB Capital hiêu khả năng và sự
                    sẵn sàng chấp nhận rủi ro của Quý khách, qua đó đề xuất những chiến lược đầu tư phù hợp nhất',
    'survey_start_btn'=>'Bắt đầu khảo sát',
    'survey_back'=>'Quay lại',
    'survey_next'=>'Tiếp theo',
    'survey_result'=>'Xem kết quả',
    'survey_reset'=>'Làm lại khảo sát',
    'message'=>'Vui lòng hoàn thành tất cả câu hỏi',
    'title' => 'Công cụ',
    'survey' => 'Khảo sát',
    'product_consulting' => 'Tư vấn sản phẩm',
    'calculation_tool' => 'Công cụ tính',


    'calculator' => [
        'determine_value_invest' => 'Xác định giá trị khoản đầu tư',
        'investment_amount' => 'Số tiền mua CCQ',
        'holding_period' => 'Thời gian nắm giữ',
        'average_return' => 'Lãi suất bình quân',
        'ending_nav' => 'Bạn sẽ nhận được',    
        'calculate' => 'XEM KẾT QUẢ',
        'estimated_nav' => 'Số tiền dự kiến nhận được',
        'profit' => 'Tiền lãi',

        'estimated_return' => 'Ước tính lợi nhuận từ các hoạt động đầu tư định kỳ (SIP)',
        'if_you_invest' => 'Nếu bạn đầu tư',
        'monthly_with_return' => 'hàng tháng với lợi nhuận kỳ vọng',
        'in' => 'trong vòng',
        'you_will_receive' => 'Bạn sẽ nhận được',
        'total_invest' => 'Tổng vốn đầu tư',
        'expected_return' => 'Lợi nhuận kỳ vọng',
        'total_profit' => 'Tổng lợi nhuận',
        'total_nav' => 'Số tiền nhận được',
        'result_for_reference' => 'Kết quả chỉ mang tính chất tham khảo, không hàm ý lợi nhuận của quỹ trong tương lai',

        'determine_amount_to_invest' => 'Xác định số tiền cần đầu tư định kỳ để đạt được các mục tiêu tài chính',
        'amount_need' => 'Số tiền bạn cần đầu tư hàng tháng',
        'proposed_investment_plan' => 'Phương án đầu tư đề xuất',
        'number_of_years' => 'Số năm đầu tư',
        'financial_goals' => 'Mục tiêu tài chính',
    ],
    'result_survey'=>'Kết quả khảo sát',
    'rate_level_risk'=>'Mức độ rủi ro',
    'asset_categoties'=>'Các loại tài sản',
    'answered'=>'Đã trả lời',
    'chart_asset'=>'Biểu đồ các loại tài sản',
    'trust' => 'Ủy thác đầu tư'
];

