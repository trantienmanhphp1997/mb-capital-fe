<?php 

return [
    'working_environment' => 'Môi trường làm việc',
    'see_more' => 'Xem thêm',
    'recruitment_details' => 'Chi tiết thông tin tuyển dụng',
    'title' => 'Tiêu đề',
    'content' => 'Nội dung',
    'back' => 'Quay lại',
    'social_responsibility'=>'Trách nhiệm xã hội',
    'recruitment'=>'Tuyển dụng',
    'downloadTemp' => 'Download CV template',
    'home_page' => 'Trang chủ',
    'recruitment_info' => 'THÔNG TIN TUYỂN DỤNG',
    'recruitment_date' => 'Ngày đăng tuyển',
    'rank' => 'Cấp bậc',
    'career' => 'Ngành nghề',
    'skills' => 'Kỹ năng',
    'language_of_profile_presentation' => 'Ngôn ngữ trình bày hồ sơ'
];