<?php

return [
    'invest_through' => 'Đầu tư thông qua:',
    'video_introduce_guide' => 'VIDEO GIỚI THIỆU QUỸ VÀ HƯỚNG DẪN GIAO DỊCH',
    'periodic_investment' => 'Đầu tư định kỳ',
    'calculation_tool' => [
        'title' => 'Công cụ tính',
        'determine_the_target' => 'Xác định mục tiêu',
        'if_you_invest' => 'Nếu bạn đầu tư',
        'monthly_with_expected_profit' => 'hàng tháng với lợi nhuận kỳ vọng',
        'with_in' => 'trong vòng',
        'year' => 'năm',
        'you_will_get' => 'Bạn sẽ nhận được',
        'see_the_results' => 'XEM KẾT QUẢ',
        'total_investment' => 'Tổng vốn đầu tư',
        'expected_profit' => 'Lợi nhuận kỳ vọng',
        'total_profit' => 'Tổng lợi nhuận',
        'amount_received' => 'Số tiền nhận được',
        'the_results_are_for_reference_only_and_do_not_imply_future_profit_of_the_fund' => 'Kết quả chỉ mang tính chất tham khảo, không hàm ý lợi nhuận của quỹ trong tương lai..',
        'amount_to_invest_periodically_according_to_purpose' => 'Số tiền cần đầu tư định kỳ theo mục đích',
        'number_of_years_to_invest_in_sip' => 'Số năm dự định đầu tư SIP',
        'your_investment_goals' => 'Mục tiêu đầu tư của bạn',
        'amount_you_need' => 'Số tiền bạn cần',
        'amount_you_need_to_invest_monthly' => 'Số tiền bạn cần đầu tư hàng tháng',
        'type'=>'Loại hình',
        'investment_property'=>'Tài sản đầu tư',
        'safety_level'=>'Mức độ an toàn',
        'fund_size'=>'Quy mô quỹ(Tại 31/10/2021)',
        'investment_objective'=>'Mục tiêu đầu tư',
        'bilion' => 'tỷ',
        'car' => 'Xe ô tô',
        'house' => 'Nhà',
        'study_abroad' => 'Cho con đi du học',
        'retire' => 'Nghỉ hưu',
    ],
    'nav_growth' => 'Tăng trưởng NAV',
];
