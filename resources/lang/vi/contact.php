<?php 
return [
    'contact' => 'Liên hệ',
    'contact_information' => [
        'title' => 'Thông tin liên hệ',
        'company_name' => 'Công ty cổ phần quản lý quỹ đầu tư MB',
        'head_office' => 'Trụ sở chính',
        'address' => 'Tầng 12, tòa nhà số 21 Cát Linh, Phường Cát Linh, Quận Đống Đa, Hà Nội.'
    ]

];