<?php

return [
    'general_information' => 'Thông tin chung',
    'performance_results' => 'Kết quả hoạt động',
    'information_disclosure' => 'Công bố thông tin',
    'have_no_data' => 'Chưa có số liệu',
    'investment_fund_management' => 'Quản lý quỹ đầu tư',
    'fund_nav_chart' => [
        'fund_charter' => 'Điều lệ quỹ',
        'prospectus' => 'Bản cáo bạch',
        'fund_info' => 'Thông tin quỹ',
    ],
    'line_chart_nav' => [
        'nav_growth' => 'Tăng trưởng NAV',
        'month' => ' tháng',
        'months' => ' tháng',
        'year' => ' năm',
        'all' => 'Tất cả',
        'from' => 'Từ',
        'to' => 'đến',
        'search' => 'Tìm kiếm',
        'growth' => 'tăng',
        'reduce' => 'giảm',
        'stable' => 'giữ nguyên',
    ],
    'document_report_file' => [
        'select_document' => 'Chọn tài liệu',
    ],
    'download' => 'Tải xuống',
    'back' => 'Quay lại',
    'view_more' => 'Xem thêm',
    'maybe_you_are_interested' => [
        'title' => 'Có thể bạn quan tâm',
        'investment_trust' => 'ỦY THÁC ĐẦU TƯ',
        'content' => 'Gửi trọn niềm tin, tài chính thịnh vượng với danh mục đầu tư chuyên biệt theo từng khách hàng',

    ],
    'profit_investment' => [
        'name' => 'NAV/đvq (VNĐ) :name',
        'one_month' => '1 tháng',
        'three_month' => '3 tháng',
        'start_year' => 'Kể từ đầu năm',
        'twelve_month' => '12 tháng',
        'found' => 'Kể từ khi thành lập :date',
    ],
    'vnd' => 'VNĐ',
];
