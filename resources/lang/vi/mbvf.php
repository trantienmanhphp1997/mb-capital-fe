<?php

return [
    'expected_profit_upto' => 'Lợi nhuận kỳ vọng lên đến ',
    'year' => 'năm',
    'invest_through' => 'Đầu tư thông qua:',
    'video_introduce_guide' => 'VIDEO GIỚI THIỆU QUỸ VÀ HƯỚNG DẪN GIAO DỊCH',

    'net_access_value_report' => 'Báo cáo NAV',
    'activity_report' => 'Báo cáo hoạt động',
    'publish_other_news' => 'Công bố thông tin',

    'why_should_you_invest_in_mbvf' => 'Tại sao nên đầu tư vào MBVF?',

    'calculation_tool' => [
        'title' => 'Công cụ tính',
        'amount_to_buy_cccq' => 'Số tiền mua CCQ',
        'holding_time' => 'Thời gian nắm giữ',
        'average_interest_rate' => 'Lãi suất bình quân',
        'you_will_receive' => 'Bạn sẽ nhận được',
        'see_the_results' => 'XEM KẾT QUẢ',
        'amount_expected_to_receive' => 'Số tiền dự kiến nhận được',
        'taxs_and_fees_are_not_included_when_selling_fund_certificate' => 'Chưa bao gồm thuế, phí khi NĐT bán CCQ',
        'interest' => 'Tiền lãi',
    ],
    'investment_reports'=>'Tài liệu quỹ',
    'general_introduction_of_the_fund' => 'Giới thiệu chung về Quỹ',
    'fees_charged_on_transaction_value' => 'Các loại phí tính trên giá trị giao dịch',
    'board_of_representatives' => 'Ban đại diện Quỹ',
    
    'trading_instructions_via_the_app' => 'Hướng dẫn giao dịch App MBBank',
    'trading_instructions_via_the_website' => 'Hướng dẫn giao dịch Website MBCapital Online',

    'industry_allocation' => 'Phân bổ ngành',
    'data_updated_on' => 'Dữ liệu cập nhật tại ngày',
    
    'mr' => 'Ông ',
    'mrs' => 'Bà ',
    'unit' => 'Đơn vị',
];