<?php

return [
    'investor_relations' => 'Quan hệ nhà đầu tư',
    'net_access_value_report' => 'Báo cáo NAV',
    'activity_report' => 'Báo cáo hoạt động',
    'publish_other_news' => 'Công bố thông tin',
    'select' => 'CHỌN',
    'search' => 'Gõ từ khóa tìm kiếm...',
];