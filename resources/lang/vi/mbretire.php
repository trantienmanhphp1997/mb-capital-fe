<?php

return [
    'AK_fund' => 'Quỹ An Khang',
    'TV_fund' => 'Quỹ Thịnh Vượng',
    'net_access_value_report' => 'Báo cáo NAV',
    'activity_report' => 'Báo cáo hoạt động',
    'publish_other_news' => 'Công bố thông tin',
    'increase_prosperity_give_peace_of_mind' => 'Tăng thịnh vượng, trao an tâm',
    'calculation_tool' => [
        'title' => 'Công cụ tính',
        'unit' => 'Đơn vị tính',
        'current_age' => 'Tuổi hiện tại',
        'age' => 'tuổi',
        'total_monthly_payment' => 'Tổng mức Doanh nghiệp và Người lao động đóng hàng tháng',
        'retired_age' => 'Tuổi nghỉ hưu',
        'number_of_years_to_receive_payment_upon_retirement' => 'Số năm nhận chi trả khi về hưu',
        'year' => 'năm',
        'reset' => 'Điền lại',
        'see_the_results' => 'Xem kết quả',
        'amount_received_monthly_in' => 'Số tiền nhận hàng tháng trong',
        'or' => 'hoặc',
        'amount_received_once' => 'Số tiền nhận một lần',
        'based_on_expected_long_term_profit' => 'Dựa trên lợi nhuận dài hạn kì vọng',
        'validate_current_age' => 'Nhập số tuổi hiện tại từ 20-61',
        'validate_retirement_age' => 'Nhập số tuổi nghỉ hưu từ 50-65',
        'validate_year_receive' => 'Nhập số năm nhận chi trả từ 10-30',
    ],
];
