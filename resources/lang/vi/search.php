<?php

return [
    'search' => 'Tìm kiếm',
    'option_search' => 'Lựa chọn tìm kiếm',
    'search_placeholder' => 'Gõ từ khóa tìm kiếm......',
    'select_fund' => 'Chọn sản phẩm',
];
