<?php

return [
    'instruct' => 'Hướng dẫn',
    'mbbond_fund' => 'Quỹ MBBOND',
    'mbvf_fund' => 'Quỹ MBVF',
    'choose' => 'CHỌN',
    'instructions_via_app' => 'Hướng dẫn qua APP MB Bank',
    'instructions_via_web' => 'Hướng dẫn qua Website',
    'document' => 'Tài liệu',
    'file' => 'Sổ tay nhà đầu tư',
    'video_tutorial' => 'Video hướng dẫn',
    'implementation_process' => 'Quy trình thực hiện',
];
