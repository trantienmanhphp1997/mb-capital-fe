<?php 
return [
    'news' => 'Tin tức',
    'hot_news' => [
        'title' => 'Tin nổi bật',
        'see_more' => 'Xem thêm',
        'no_data' => 'Không có dữ liệu'
    ],
    'related_news' => 'Tin liên quan',
    'home_page' => 'Trang chủ',
    'author' => 'Tác giả',
];