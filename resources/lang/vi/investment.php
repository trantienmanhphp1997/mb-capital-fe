<?php

return [
    'investment_knowledge' => 'Kiến thức đầu tư',
    'mb_capital_private'=>'ỦY THÁC ĐẦU TƯ KHÁCH HÀNG CÁ NHÂN',
    'investment_trust'=>'ỦY THÁC ĐẦU TƯ',
    'content_investment_trust'=>'Gửi trọn niềm tin, tài chính thịnh vượng với danh mục đầu tư chuyên biệt theo từng khách hàng',
    'content_investment_trust_2' => 'Dịch vụ quản lý tài sản chuyên nghiệp dành cho tổ chức của bạn',
    'new_investor' => [
        'title' => 'Nhà đầu tư mới',
        'determine_risk_appetite' => 'Xác định khẩu vị rủi ro',
    ],
    'professional_investor' => [
        'title' => 'Nhà đầu tư chuyên nghiệp',
        'product_portfolio' => 'Danh mục sản phẩm',
        'open_fund' => 'Quỹ mở',
        'retirement_programme' => 'Chương trình Hưu trí',
        'investment_funds' => 'Quỹ đầu tư','investment_trust_content_institutional_customer' => 'Dịch vụ quản lý tài sản chuyên nghiệp dành cho tổ chức của bạn',
    ],
    'learn_more' => 'Tìm hiểu thêm về danh mục sản phẩm phù hợp với bạn',
    'instruction' => 'Giới thiệu quỹ và hướng dẫn giao dịch',
];
