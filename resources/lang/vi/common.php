<?php

return [
    'topbar'=>[
        'about_MB'=>'Về MBCapital',
        'news'=>'Tin tức',
        'contact'=>'Liên hệ'
    ],
    'header'=>[
        'manager_fund'=>'Quản lý quỹ',
        'investment_trust'=>'Ủy thác đầu tư',
        'investor_relations'=>'Quan hệ nhà đầu tư',
        'customer_support'=>'Hỗ trợ khách hàng',
        'individual_customer'=>'Khách hàng Cá nhân',
        'institutional_customer'=>'Khách hàng Tổ chức',
        'investment_knowledge'=>'Kiến thức đầu tư',
        'investment_guide'=>'Hướng dẫn đầu tư',
        'login'=>'Đăng nhập',
        'open_account'=>'Mở tài khoản',
        'support_tool' => 'Công cụ hỗ trợ',
    ],
    'footer'=>[
        'introduction'=>'Giới thiệu',
        'hr_team'=>'Đội ngũ nhân sự',
        'investor_relation'=>'Quan hệ nhà đầu tư',
        'environments_work'=>'Môi trường làm việc',
        'tool_supports'=>'Công cụ hỗ trợ',
        'alignment_of_companies'=>'Liên kết MB GROUP',

    ]
];
