<?php 

return [ 
    'investment_trusts_individual_customers' => 'Ủy thác đầu tư Khách hàng cá nhân',
    'investment_trust_institutional_customers' => 'Ủy thác đầu tư Khách hàng tổ chức',
    'contact_consultant' => 'Liên hệ tư vấn',
    'MBPrivate_link' => 'Liên kết MBPrivate',
    'participation_process' => [
        'title' =>  'Quy trình tham gia',
        'identify_customer_needs' => 'Xác định nhu cầu khách hàng',
        'develop_an_investment_strategy' => 'Xây dựng chiến lược đầu tư',
        'implementing_investment_strategies' => 'Triển khai chiến lược đầu tư',
        'track_and_manage_categories' => 'Theo dõi, quản lý danh mục',
        'periodic_reports' => 'Báo cáo định kỳ'
    ],
    'reference_portfolio_structure' => [
        'title' => 'Cơ cấu danh mục tham khảo',
        'level_of_risk-taking' => 'Mức độ chấp nhận rủi ro',
        'with_expected_returns' => 'Với lợi nhuận kỳ vọng'
    ]
];