<?php

return [
    'investment_needs_title' => 'Bạn đang có nhu cầu đầu tư?',
    'investment_options_title' => 'Lựa chọn đầu tư?',
    'investment_advice_title' => 'Bạn cần tư vấn đầu tư ngay?',
    'investment_step' => 'Các bước đầu tư',
    'investment_advice_subtitle' => 'MB Capital cung cấp các sản phẩm tài chính ưu việt đáp ứng nhu cầu của khách hàng',
    'advise' => [
        'name' => 'Họ và tên',
        'email_or_phone' => 'Email hoặc SĐT',
        'content' => 'Nội dung cần tư vấn',
        'send_request' => 'Gửi yêu cầu',
    ],
    'list_fund'=>[
        'investment_trust'=>'Ủy thác đầu tư',
        'investment_trust_content'=>'Gửi trọn niềm tin, tài chính thịnh vượng với danh mục đầu tư chuyên biệt theo từng khách hàng',
    ],
    'more'=>'Xem thêm'
];
