<?php

return [
    'topbar'=>[
        'about_MB'=>'About MB Capital',
        'news'=>'News',
        'contact'=>'Contact'
    ],
    'header'=>[
        'manager_fund'=>'Investment Funds',
        'investment_trust'=>'Portfolio Management',
        'investor_relations'=>'Investor Relations',
        'customer_support'=>'Investor Support',
            'individual_customer'=>'Individual Investor',
            'institutional_customer'=>'Institutional Investor',
        'investment_knowledge'=>'Investment Knowledge',
        'investment_guide'=>'Investment Guideline',
        'login'=>'Sign in',
        'open_account'=>'Sign up',
        'support_tool' => 'Investment Tools',
    ],
    'footer'=>[
        'introduction'=>'Introduction',
        'hr_team'=>'Our people',
        'investor_relation'=>'Investor Relations',
        'environments_work'=>'Working Environment',
        'tool_supports'=>'Investment Tools ',
        'alignment_of_companies'=>'MB Group',

    ]
];
