<?php 
return [
    'news' => 'News',
    'hot_news' => [
        'title' => 'Highlight',
        'see_more' => 'See more',
        'no_data' => 'No data'
    ],
    'related_news' => 'Others',
    'home_page' => 'Home page',
    'author' => 'Author',
];
