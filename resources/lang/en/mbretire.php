<?php

return [
    'AK_fund' => 'An Khang Fund',
    'TV_fund' => 'Thinh Vuong Fund',
    'net_access_value_report' => 'NAV report',
    'activity_report' => 'Performance report',
    'publish_other_news' => 'Information disclosure',
    'increase_prosperity_give_peace_of_mind' => 'Carry prosperity and peace',
    'calculation_tool' => [
        'title' => 'Calculation tool',
        'unit' => 'Unit',
        'current_age' => 'Current age',
        'age' => 'years old',
        'total_monthly_payment' => 'Total monthly contribution by Company and Employee',
        'retired_age' => 'Retirement age',
        'number_of_years_to_receive_payment_upon_retirement' => 'Number of years to receive benefits upon retirement',
        'year' => 'year',
        'reset' => 'Reset',
        'see_the_results' => 'See the results',
        'amount_received_monthly_in' => 'Monthly payment received in',
        'or' => 'or',
        'amount_received_once' => 'One-time payment received at  retirement',
        'based_on_expected_long_term_profit' => 'Based on expected long-term return',
        'validate_current_age' => 'Enter current age from 20 to 61',
        'validate_retirement_age' => 'Enter retirement age from 50 to 65',
        'validate_year_receive' => 'Enter the number of years to receive payment from 10 to 30',
    ],
];
