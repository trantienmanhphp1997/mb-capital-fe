<?php

return [
    'investment_knowledge' => 'Investment knowledge',
    'mb_capital_private'=> 'MB Capital Private',
    'investment_trust'=>'Investment Trust',
    'content_investment_trust'=>'Sending full confidence, financial prosperity with a specialized investment portfolio for each customer',
    'content_investment_trust_2' => 'Professional asset management services for your organization',
    'new_investor' => [
        'title' => 'First-time investor',
        'determine_risk_appetite' => 'Determine risk appetite',
    ],
    'professional_investor' => [
        'title' => 'Investing professionals',
        'product_portfolio' => 'Product Portfolio',
        'open_fund' => 'Open-ended fund',
        'retirement_programme' => 'Pension Program',
        'investment_funds' => 'Investment funds',
    ],
    'learn_more' => 'Learn more about the product category that\'s right for you',
    'instruction' => 'Fund introduction and trading instructions',
];
