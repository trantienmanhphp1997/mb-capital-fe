<?php

return [
    'search' => 'Search',
    'option_search' => 'Option search',
    'search_placeholder' => 'Type search keyword......',
    'select_fund' => 'Select fund',
];
