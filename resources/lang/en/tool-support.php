<?php

return [
    'survey_start'=>'The suitability assessment questionnaire that you are about to fill out allows MB Capital to understand your ability and willingness to accept risk, thereby recommending the most suitable investment strategies.',
    'survey_start_btn'=>'Start',
    'survey_back'=>'Back',
    'survey_next'=>'Next',
    'survey_result'=>'Result',
    'survey_reset'=>'Redo',
    'message'=>'Please complete all questions',

    'title' => 'Tool',
    'survey' => 'Survey',
    'product_consulting' => 'Product consulting',
    'calculation_tool' => 'Tools and Calculations',

    'calculator' => [
        'determine_value_invest' => 'NAV Calculation',
        'investment_amount' => 'Beginning investment amount',
        'holding_period' => 'Holding period',
        'average_return' => 'Average return',
        'ending_nav' => 'Ending NAV',    
        'calculate' => 'CALCULATE',
        'estimated_nav' => 'Estimated NAV',
        'profit' => 'Profit',

        'estimated_return' => 'Estimate profit from systematic investment program (SIP)',
        'if_you_invest' => 'If you invest',
        'monthly_with_return' => 'monthly with an average return of',
        'in' => 'in',
        'you_will_receive' => 'You will receive',
        'total_invest' => 'Total invested capital',
        'expected_return' => 'Average return',
        'total_profit' => 'Total profit',
        'total_nav' => 'Total NAV',
        'result_for_reference' => 'Results are for reference only, not implying future profit of the Fund',

        'determine_amount_to_invest' => 'Monthly investment amount calculation',
        'amount_need' => 'You need to invest monthly',
        'proposed_investment_plan' => 'Recommended asset allocation',
        'number_of_years' => 'Investment horizon',
        'financial_goals' => 'Financial Goal',
    ],
    'result_survey'=>'Survey results',
    'rate_level_risk'=>'Risk appetite',
    'asset_categoties'=>'Asset allocation recommendation',
    'answered'=>'Answered',
    'chart_asset'=>'Chart of asset allocation recommendation',
    'trust' => 'Investment trust'
];
