<?php

return [
    'instruct' => 'Investment guideline',
    'mbbond_fund' => 'MBBOND',
    'mbvf_fund' => 'MBVF',
    'choose' => "SELECT",
    'instructions_via_app' => 'MBBANK APP Instruction',
    'instructions_via_web' => 'MBCapital Online Instruction',
    'document' => 'Document',
    'file' => 'Investment Guideline',
    'video_tutorial' => 'Video tutorial',
    'implementation_process' => 'Implementation process',
];
