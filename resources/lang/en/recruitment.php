<?php 

return [
    'working_environment' => 'Working Environment',
    'see_more' => 'See more',
    'recruitment_details' => 'Recruitment details',
    'title' => 'Title',
    'content' => 'Content',
    'back' => 'Back',
    'social_responsibility'=>'Social responsibility',
    'recruitment'=>'Recruitment',
    'downloadTemp' => 'Download CV template',
    'home_page' => 'Home',
    'recruitment_info' => 'Recruitment Information',
    'recruitment_date' => 'Recruitment date',
    'rank' => 'Rank',
    'career' => 'Career',
    'skills' => 'Skills',
    'language_of_profile_presentation' => 'Language of profile presentation'
];