<?php 
return [
    'contact' => 'Contact',
    'contact_information' => [
        'title' => 'Contact information',
        'company_name' => 'MB Capital Management Joint Stock Company',
        'head_office' => 'Head office',
        'address' => '12th floor, Building No. 21 Cat Linh, Cat Linh Ward, Dong Da District, Hanoi.'
    ]

];
