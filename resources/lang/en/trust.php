<?php 

return [ 
    'investment_trusts_individual_customers' => 'Portfolios management for individual investors',
    'investment_trust_institutional_customers' => 'Portfolios management for institutional investors',
    'contact_consultant' => 'Contact MB Capital',
    'MBPrivate_link' => 'MB Private Link',
    'participation_process' => [
        'title' =>  'Participation process',
        'identify_customer_needs' => 'Identify customer needs',
        'develop_an_investment_strategy' => 'Develop an investment strategy',
        'implementing_investment_strategies' => 'Implementing investment strategies',
        'track_and_manage_categories' => 'Track and manage categories',
        'periodic_reports' => 'Periodic reports'
    ],
    'reference_portfolio_structure' => [
        'title' => 'Reference portfolio structure',
        'level_of_risk-taking' => 'Level of risk-taking',
        'with_expected_returns' => 'With expected returns'
    ]   
];