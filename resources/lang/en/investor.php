<?php

return [
    'investor_relations' => 'Investor Relations',
    'net_access_value_report' => 'NAV reports',
    'activity_report' => 'Performance reports',
    'publish_other_news' => 'Information disclosure',
    'select' => 'SELECT',
    'search' => 'Type search keywords...'
];