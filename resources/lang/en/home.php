<?php

return [
    'investment_needs_title' => 'Do you have investment needs?',
    'investment_options_title' => 'Investment Solutions',
    'investment_advice_title' => 'Do you need investment advice right away?',
    'investment_step' => 'Investment Steps',
    'investment_advice_subtitle' => 'MB Capital provides superior financial products to meet the needs of investors',
    'advise' => [
        'name' => 'Name',
        'email_or_phone' => 'Email or phone',
        'content' => 'Content to advise',
        'send_request' => 'Send request',
    ],
    'list_fund'=>[
        'investment_trust'=>'Portfolio Management',
        'investment_trust_content'=>'Send your complete trust, financial prosperity with the specialize investment portfolio for each customer',
    ],
    'more'=>'View more',
    'advise-request'=>'',
    'content_advice'=>'Content need advice'
];
