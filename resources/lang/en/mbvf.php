<?php

return [
    'expected_profit_upto' => 'Expected return upto',
    'year' => 'year',
    'invest_through' => 'Invest via:',
    'video_introduce_guide' => 'FUND INTRODUCTION AND TRADING INSTRUCTIONS VIDEOS',

    'net_access_value_report' => 'NAV report',
    'activity_report' => 'Performance report',
    'publish_other_news' => 'Information disclosure',

    'why_should_you_invest_in_mbvf' => 'Why should you invest in MBVF?',

    'calculation_tool' => [
        'title' => 'Calculation tool',
        'amount_to_buy_cccq' => 'Amount to buy fund certificate',
        'holding_time' => 'Holding time',
        'you_will_receive' => 'You will receive',
        'average_interest_rate' => 'Average interest rate',
        'see_the_results' => 'SEE THE RESULTS',
        'amount_expected_to_receive' => 'Amount expected to receive',
        'taxs_and_fees_are_not_included_when_selling_fund_certificate' => 'Taxes and fees are not included when selling fund certificate',
        'interest' => 'Interest',
    ],
    'investment_reports'=>'Fund Documents',

    'general_introduction_of_the_fund' => 'Key information',
    'fees_charged_on_transaction_value' => 'Fees charged on transaction value',
    'board_of_representatives' => 'Board of representatives',
    
    'trading_instructions_via_the_app' => 'Trading instructions via App MBBank',
    'trading_instructions_via_the_website' => 'Trading instructions via Website MBCapital Online',

    'industry_allocation' => 'Asset allocation',
    'data_updated_on' => 'Data updated on',
    
    'mr' => 'Mr.',
    'mrs' => 'Mrs.',
    'unit' => 'Unit',
];
