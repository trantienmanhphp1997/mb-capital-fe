<?php

return [
    'general_information' => 'General information',
    'performance_results' => 'Performance',
    'information_disclosure' => 'Information disclosure',
    'have_no_data' => 'Have no data.',
    'investment_fund_management' => 'Investment fund management',
    'fund_nav_chart' => [
        'fund_charter' => 'Fund charter',
        'prospectus' => 'Prospectus',
        'fund_info' => 'Fact sheet',
    ],
    'line_chart_nav' => [
        'nav_growth' => 'NAV growth',
        'month' => 'm',
        'months' => 'm',
        'year' => 'y',
        'all' => 'All',
        'from' => 'From',
        'to' => 'to',
        'search' => 'Search',
        'growth' => 'growth',
        'reduce' => 'reduce',
        'stable' => 'stable',
    ],
    'document_report_file' => [
        'select_document' => 'Select document',
    ],
    'download' => 'Download',
    'back' => 'Back',
    'view_more' => 'View more',
    'maybe_you_are_interested' => [
        'title' => 'Maybe you are interested',
        'investment_trust' => 'Portfolio Management',
        'content' => 'Sending full confidence, financial prosperity with a specialized investment portfolio for each customer',

    ],
    'profit_investment' => [
        'name' => 'NAV/fund unit (VND) :name',
        'one_month' => '1m',
        'three_month' => '3m',
        'start_year' => 'YTD',
        'twelve_month' => '12m',
        'found' => 'Since Inception :date',
    ],
    'vnd' => 'VND',
];
